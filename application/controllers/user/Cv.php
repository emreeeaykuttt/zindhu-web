<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cv extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('UserID')) {
			redirect(base_url('zindhu-hr/kullanici/giris')); exit();
		} else {
			if ($this->session->userdata('UserAdminConfirm') == 'pending' && $this->session->userdata('UserStatus') == 1) {
				redirect(base_url('zindhu-hr/kullanici/onay-bekliyor')); exit();
			} elseif ($this->session->userdata('UserAdminConfirm') == 'rejected' && $this->session->userdata('UserStatus') == 1) {
				redirect(base_url('zindhu-hr/kullanici/reddedildi')); exit();
			}
		}
	}

	public function index()
	{	
		if ($this->session->userdata('UserStatus') == 1) {
			redirect(base_url('zindhu-hr/kullanici/dashboard')); 
			exit();
		} else {
			$this->load->view('frontend/user/cv_view');
		}
	}

	public function user()
	{
	    $this->load->model('user/user_model', 'user');

	    $user_id = $this->session->userdata('UserID');

	    $data['user'] = $this->user->getByID($user_id);

	    if (empty($data)) 
	    {
	        $data = '';
	    }

	    echo json_encode($data);
	}

	public function cv_status($status = '')
	{
		$this->load->model('user/user_model', 'user');

		$user_id = $this->session->userdata('UserID');

		if ($status == 1) {
			$this->session->set_userdata('UserCvStatus', 1);
			$this->user->update(array('id' => $user_id), array('cv_status' => 1));
			$message = 'Profil açık hale getirilmiştir.';
			$cv_status = true;
		} else {
			$this->session->set_userdata('UserCvStatus', 0);
			$this->user->update(array('id' => $user_id), array('cv_status' => 0));
			$message = 'Profil kapalı hale getirilmiştir.';
			$cv_status = false;
		}

		echo json_encode(array('status' => true, 'message' => $message, 'cv_status' => $cv_status));
	}

	public function working_status_edit()
	{
	    $this->load->model('user/user_model', 'user');

	    $user_id = $this->session->userdata('UserID');

	    $data = $this->user->getByID($user_id);

	    echo json_encode($data);
	}

	public function working_status_save()
	{
		$this->load->model('user/user_model', 'user');

		$post = $this->input->post(null,true);
		$user_id = $this->session->userdata('UserID');

		$this->working_status_validate();

		$data = array(
			'company_id' => $post['working_status_company_id'],
			'company_text' => $post['working_status_company_id'] == 0 ? $post['working_status_company_text'] : NULL,
			'is_working' => $post['working_status_is_working'] == 1 ? 1 : 0,
		);

		if ($post['working_status_is_working'] == 0) {
			$data['company_id'] = 0;
			$data['company_text'] = NULL;
		} 
		else if ($post['working_status_company_id'] != 0)
		{
			// $this->load->model('company/hires_model', 'company_hires');

			// $is_save = FALSE;
			// $company_hires = $this->company_hires->getByCompanyIDAndUserID($post['working_status_company_id'], $user_id);
			// if ($company_hires) {
			// 	if ($company_hires->enddate) {
			// 		$is_save = TRUE;
			// 	}
			// } else {
			// 	$is_save = TRUE;
			// }

			// if ($is_save) {
			// 	$this->company_hires->update(
			// 		array('company_id' => $company_id, 'user_id' => $user_id, 'enddate' => NULL, 'continues' => 1), 
			// 		array('continues' => 0, 'enddate' => date('Y-m-d H:i:s', NOW_DATE_TIME), 'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME))
			// 	);

			//  	$this->company_hires->save(
			// 		array(
			// 			'company_id' => $post['working_status_company_id'],
			// 			'user_id' => $user_id,
			// 			'user_confirm' => 1,
			// 			'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
			// 		)
			// 	);
			// } 
		}
		
		$this->user->update(array('id' => $user_id), $data);

		echo json_encode(array('status' => TRUE));
	}

	private function working_status_validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if ($this->input->post('working_status_is_working') == '1') 
		{
			if ($this->input->post('working_status_company_id') == '')
			{
				$data['inputerror'][] = 'working_status_company_id';
				$data['error_string'][] = 'Stüdyo seçiniz';
				$data['status'] = FALSE;
			} 
			elseif ($this->input->post('working_status_company_id') == '0') 
			{
				if ($this->input->post('working_status_company_text') == '') 
				{
					$data['inputerror'][] = 'working_status_company_text';
					$data['error_string'][] = 'Stüdyo Adı alanı boş bırakılamaz';
					$data['status'] = FALSE;
				}
			}
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function view_personal_information_all()
	{
	    $this->load->model('cv/personal_information_model', 'personal_information');
	    $this->load->model('user/user_model', 'user');
	    $this->load->model('company/company_model', 'company');

	    $user_id = $this->session->userdata('UserID');

	    $data['personal_information'] = $this->personal_information->getAllByUserID($user_id);
	    $data['user'] = $this->user->getByID($user_id);
	    $data['company_names'] = '';
	    if (!empty($data['user']->company_ids)) 
	    {
	    	$companies = $this->company->getAllByIDs(json_decode($data['user']->company_ids), true);
	    	$company_names = '';
	    	$company_count = count($companies) - 1;
	    	foreach ($companies as $key => $value) {
	    		if ($key == $company_count) {
	    			$company_names .= $value['company_name'];
	    		} else {
	    			$company_names .= $value['company_name'] . ' - ';
	    		}
	    	}
	    	$data['company_names'] = $company_names;
	    }

	    if (empty($data)) 
	    {
	        $data = '';
	    }

	    echo json_encode($data);
	}

	public function personal_information_edit($id)
	{
	    $this->load->model('cv/personal_information_model', 'personal_information');
	    $this->load->model('user/user_model', 'user');

	    $user_id = $this->session->userdata('UserID');

	    $data = $this->personal_information->getByID($id);
	    $data->user = $this->user->getByID($user_id);

	    echo json_encode($data);
	}

	public function personal_information_save()
	{
	    $this->load->model('cv/personal_information_model', 'personal_information');
	    $this->load->model('user/user_model', 'user');

	    $post = $this->input->post(null,true);
	    $user_id = $this->session->userdata('UserID');

	    $this->personal_information_validate();

	    $data = array(
	        'phone' => $post['personal_information_phone'],
	        'position_id' => $post['personal_information_position_id'],
	        'city_id' => $post['personal_information_city_id'],
	        'city_text' => $post['personal_information_city_text'],
	        'city_json' => json_encode($post['personal_information_city_json']),
	        'is_remote' => $post['personal_information_is_remote'],
	        'user_id' => $user_id,
	        'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
	    );

	    if (empty($post['personal_information_id']) || $post['personal_information_id'] == 0) {
	        if ($this->personal_information->personalControl($user_id)) {
	            $data['inputerror'][] = 'personal_information_city_id';
	            $data['error_string'][] = 'Kişisel Bilgiler zaten ekli!';
	            $data['status'] = FALSE;
	            echo json_encode($data);
	            exit();
	        }
	        $id = $this->personal_information->save($data);
	    } else {

	    	if (!empty($post['user_firstname']) && !empty($post['user_lastname']) && !empty($post['user_email'])) {
	    		$this->user_validate();

			    $data_user = array(
			    	'firstname' => $post['user_firstname'],
			    	'lastname' => $post['user_lastname'],
			    	'email' => $post['user_email']
			    );
			    $this->user->update(array('id' => $user_id), $data_user);
	    	}

	        $id = 0;
	        $this->personal_information->update(array('id' => $post['personal_information_id']), $data);
	    }

	    echo json_encode(array('status' => TRUE, 'id' => $id));
	}

	public function personal_information_delete($id)
	{
	    $this->load->model('cv/personal_information_model', 'personal_information');

	    $data = $this->personal_information->getByID($id);

	    $this->personal_information->deleteByID($id);

	    echo json_encode(array("status" => TRUE));
	}

	private function user_validate()
	{
	    $data = array();
	    $data['error_string'] = array();
	    $data['inputerror'] = array();
	    $data['status'] = TRUE;

	    if($this->input->post('user_email') == '')
		{
			$data['inputerror'][] = 'user_email';
			$data['error_string'][] = 'Email alanı boş bırakılamaz';
			$data['status'] = FALSE;
		}
		elseif (!filter_var($this->input->post('user_email'), FILTER_VALIDATE_EMAIL)) 
		{
			$data['inputerror'][] = 'user_email';
			$data['error_string'][] = 'Girdiğiniz email adresi geçerli değildir';
			$data['status'] = FALSE;
		}
		elseif ($this->user->emailControl($this->input->post('user_email'), $this->session->userdata('UserID'))) 
		{
			$data['inputerror'][] = 'user_email';
			$data['error_string'][] = 'Girdiğiniz email adresi kullanılmaktadır';
			$data['status'] = FALSE;
		}

	    if($this->input->post('user_firstname') == '')
		{
			$data['inputerror'][] = 'user_firstname';
			$data['error_string'][] = 'Ad alanı boş bırakılamaz';
			$data['status'] = FALSE;
		}

		if($this->input->post('user_lastname') == '')
		{
			$data['inputerror'][] = 'user_lastname';
			$data['error_string'][] = 'Soyad alanı boş bırakılamaz';
			$data['status'] = FALSE;
		}

	    if($data['status'] === FALSE)
	    {
	        echo json_encode($data);
	        exit();
	    }
	}

	private function personal_information_validate()
	{
	    $data = array();
	    $data['error_string'] = array();
	    $data['inputerror'] = array();
	    $data['status'] = TRUE;

	    if(empty($this->input->post('personal_information_phone')))
	    {
	        $data['inputerror'][] = 'personal_information_phone';
	        $data['error_string'][] = 'Telefon alanı boş bırakılamaz';
	        $data['status'] = FALSE;
	    }
	    elseif (!is_numeric($this->input->post('personal_information_phone'))) 
	    {
	        $data['inputerror'][] = 'personal_information_phone';
	        $data['error_string'][] = 'Telefon alanına sadece rakam girilebilir';
	        $data['status'] = FALSE;
	    }
	    elseif(strlen($this->input->post('personal_information_phone')) != 10)
	    {
	        $data['inputerror'][] = 'personal_information_phone';
	        $data['error_string'][] = 'Telefon alanı 10 karakterden oluşmaktadır';
	        $data['status'] = FALSE;
	    }

	    if($this->input->post('personal_information_position_id') == '0' || empty($this->input->post('personal_information_position_id')))
	    {
	        $data['inputerror'][] = 'personal_information_position_id';
	        $data['error_string'][] = 'Pozisyon seçiniz';
	        $data['status'] = FALSE;
	    }

	    if($this->input->post('personal_information_city_id') == '0' || empty($this->input->post('personal_information_city_id')))
	    {
	        $data['inputerror'][] = 'personal_information_city_id';
	        $data['error_string'][] = 'Yaşadığın Şehiri seçiniz';
	        $data['status'] = FALSE;
	    }
	    elseif ($this->input->post('personal_information_city_id') == '341') 
	    {
	    	if(empty($this->input->post('personal_information_city_text')))
	    	{
	    		$data['inputerror'][] = 'personal_information_city_text';
		        $data['error_string'][] = 'Yaşadığın Ülke-Şehiri yazınız';
		        $data['status'] = FALSE;
	    	}
	    }

	    if($this->input->post('personal_information_city_json') == '0' || empty($this->input->post('personal_information_city_json')))
	    {
	        $data['inputerror'][] = 'personal_information_city_json';
	        $data['error_string'][] = 'Çalışmak İstediğiniz Şehirleri seçiniz';
	        $data['status'] = FALSE;
	    }

	    if($data['status'] === FALSE)
	    {
	        echo json_encode($data);
	        exit();
	    }
	}

	public function view_educational_background_all()
	{
		$this->load->model('cv/educational_background_model', 'educational_background');

		$user_id = $this->session->userdata('UserID');

		$data = $this->educational_background->getAllByUserID($user_id);

		if (empty($data)) 
		{
			$data = '';
		}

		echo json_encode($data);
	}

	public function educational_background_edit($id)
	{
		$this->load->model('cv/educational_background_model', 'educational_background');

		$data = $this->educational_background->getByID($id);

		echo json_encode($data);
	}

	public function educational_background_save()
	{
		$this->load->model('cv/educational_background_model', 'educational_background');

		$post = $this->input->post(null,true);

		$this->educational_background_validate();

		$data = array(
			'education_level' => $post['education_level'],
			'university_id' => $post['education_university_id'],
			'university_text' => $post['education_university_text'],
			'university_section' => $post['education_university_section'],
			'start_year' => $post['education_start_year'],
			'end_year' => $post['education_continues'] == 0 ? $post['education_end_year'] : 0,
			'continues' => $post['education_continues'],
			'user_id' => $this->session->userdata('UserID')
		);
		
		if (empty($post['education_id']) || $post['education_id'] == 0) {
			$data['created_at'] = date('Y-m-d H:i:s', NOW_DATE_TIME);
			$id = $this->educational_background->save($data);
		} else {
			$data['updated_at'] = date('Y-m-d H:i:s', NOW_DATE_TIME);
			$id = 0;
			$this->educational_background->update(array('id' => $post['education_id']), $data);
		}

		echo json_encode(array('status' => TRUE, 'id' => $id));
	}

	public function educational_background_delete($id)
	{
		$this->load->model('cv/educational_background_model', 'educational_background');

		$data = $this->educational_background->getByID($id);

		$this->educational_background->deleteByID($id);

		echo json_encode(array("status" => TRUE));
	}

	private function educational_background_validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('education_level') == '0' || empty($this->input->post('education_level')))
		{
			$data['inputerror'][] = 'education_level';
			$data['error_string'][] = 'Eğitim Seviyesi seçiniz';
			$data['status'] = FALSE;
		}

		if($this->input->post('education_university_id') == '')
		{
			$data['inputerror'][] = 'education_university_id';
			$data['error_string'][] = 'Üniversite seçiniz';
			$data['status'] = FALSE;
		} elseif ($this->input->post('education_university_id') == 0) {
			if(empty($this->input->post('education_university_text'))) {
				$data['inputerror'][] = 'education_university_text';
				$data['error_string'][] = 'Üniversite seçiniz';
				$data['status'] = FALSE;
			}
		}

		if(empty($this->input->post('education_university_section')))
		{
			$data['inputerror'][] = 'education_university_section';
			$data['error_string'][] = 'Bölüm alanı boş bırakılamaz';
			$data['status'] = FALSE;
		}

		if ($this->input->post('education_start_year') == '0' || empty($this->input->post('education_start_year'))) {
			$data['inputerror'][] = 'education_start_year';
			$data['error_string'][] = 'Başlangıç Yılı seçiniz';
			$data['status'] = FALSE;
		}

		if ($this->input->post('education_end_year') == '0' || empty($this->input->post('education_end_year'))) {
			if ($this->input->post('education_continues') != '1') {
				$data['inputerror'][] = 'education_end_year';
				$data['error_string'][] = 'Bitiş Yılı seçiniz';
				$data['status'] = FALSE;
			}
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function view_work_experience_all()
	{
		$this->load->model('cv/work_experience_model', 'work_experience');

		$user_id = $this->session->userdata('UserID');

		$data = $this->work_experience->getAllByUserID($user_id);

		if (empty($data)) 
		{
			$data = '';
		}

		echo json_encode($data);
	}

	public function work_experience_edit($id)
	{
		$this->load->model('cv/work_experience_model', 'work_experience');

		$data = $this->work_experience->getByID($id);

		echo json_encode($data);
	}

	public function work_experience_save()
	{
		$this->load->model('cv/work_experience_model', 'work_experience');

		$post = $this->input->post(null,true);
		$user_id = $this->session->userdata('UserID');

		$this->work_experience_validate();

		$data = array(
			'company_name' => $post['work_company_name'],
			'company_sector' => $post['work_company_sector'],
			'company_position' => $post['work_company_position'],
			'start_month' => $post['work_start_month'],
			'start_year' => $post['work_start_year'],
			'end_month' => $post['work_working'] == 0 ? $post['work_end_month'] : 0,
			'end_year' => $post['work_working'] == 0 ? $post['work_end_year'] : 0,
			'working' => $post['work_working'],
			'detail' => $post['work_detail'],
			'user_id' => $user_id,
			'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if ($post['work_working'] == 1 && $post['work_status'] == 1) {
			$this->load->model('user/user_model', 'user');

			$user_info = $this->user->getByID($user_id);

			if (empty($user_info->company_ids)) {
				$work_status = array(
					'company_text' => $post['work_company_name'],
					'is_working' => 1,
				);

				$this->user->update(array('id' => $user_id), $work_status);
			}
		}
		
		if (empty($post['work_id']) || $post['work_id'] == 0) {
			$id = $this->work_experience->save($data);
		} else {
			$id = 0;
			$this->work_experience->update(array('id' => $post['work_id']), $data);
		}

		echo json_encode(array('status' => TRUE, 'id' => $id));
	}

	public function work_experience_delete($id)
	{
		$this->load->model('cv/work_experience_model', 'work_experience');

		$data = $this->work_experience->getByID($id);

		$this->work_experience->deleteByID($id);

		echo json_encode(array("status" => TRUE));
	}

	private function work_experience_validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if(empty($this->input->post('work_company_name')))
		{
			$data['inputerror'][] = 'work_company_name';
			$data['error_string'][] = 'Firma Adı alanı boş bırakılamaz';
			$data['status'] = FALSE;
		}

		if(empty($this->input->post('work_company_sector')))
		{
			$data['inputerror'][] = 'work_company_sector';
			$data['error_string'][] = 'Firma Sektorü alanı boş bırakılamaz';
			$data['status'] = FALSE;
		} 
		elseif (!preg_match('/^[a-zA-ZğıüşöçİĞÜŞÖÇ![:space:]]+$/', $this->input->post('work_company_sector'))) 
		{
			$data['inputerror'][] = 'work_company_sector';
			$data['error_string'][] = 'Firma Sektorü alanına sayı girilmez';
			$data['status'] = FALSE;
		}

		if(empty($this->input->post('work_company_position')))
		{
			$data['inputerror'][] = 'work_company_position';
			$data['error_string'][] = 'Firmadaki Pozisyon alanı boş bırakılamaz';
			$data['status'] = FALSE;
		}
		// elseif (!preg_match('/^[a-zA-ZğıüşöçİĞÜŞÖÇ![:space:]]+$/', $this->input->post('work_company_position'))) 
		// {
		// 	$data['inputerror'][] = 'work_company_position';
		// 	$data['error_string'][] = 'Firmadaki Pozisyon alanına sayı girilmez';
		// 	$data['status'] = FALSE;
		// }

		if ($this->input->post('work_start_month') == '0' || empty($this->input->post('work_start_month'))) {
			$data['inputerror'][] = 'work_start_month';
			$data['error_string'][] = 'Başlangıç Ayı seçiniz';
			$data['status'] = FALSE;
		}

		if ($this->input->post('work_start_year') == '0' || empty($this->input->post('work_start_year'))) {
			$data['inputerror'][] = 'work_start_year';
			$data['error_string'][] = 'Başlangıç Yılı seçiniz';
			$data['status'] = FALSE;
		} else {
			if (intval($this->input->post('work_start_year')) > 0 && intval($this->input->post('work_end_year')) > 0) {
				if (intval($this->input->post('work_start_year')) > intval($this->input->post('work_end_year'))) {
					$data['inputerror'][] = 'work_start_year';
					$data['error_string'][] = 'Başlangıç yılı bitiş yılından büyük olamaz';
					$data['status'] = FALSE;
				}
			}
		}

		if ($this->input->post('work_end_year') == '0' || empty($this->input->post('work_end_year'))) {
			if ($this->input->post('work_working') != '1') {
				$data['inputerror'][] = 'work_end_year';
				$data['error_string'][] = 'Bitiş Yılı seçiniz';
				$data['status'] = FALSE;
			}
		}

		if ($this->input->post('work_end_month') == '0' || empty($this->input->post('work_end_month'))) {
			if ($this->input->post('work_working') != '1') {
				$data['inputerror'][] = 'work_end_month';
				$data['error_string'][] = 'Bitiş Ayı seçiniz';
				$data['status'] = FALSE;
			}
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function view_used_program_all()
	{
		$this->load->model('cv/used_program_model', 'used_program');

		$user_id = $this->session->userdata('UserID');

		$data = $this->used_program->getAllByUserID($user_id);

		if (empty($data)) 
		{
			$data = '';
		}

		echo json_encode($data);
	}

	public function used_program_edit($id)
	{
		$this->load->model('cv/used_program_model', 'used_program');

		$data = $this->used_program->getByID($id);

		echo json_encode($data);
	}

	public function used_program_save()
	{
		$this->load->model('cv/used_program_model', 'used_program');

		$post = $this->input->post(null,true);
		$user_id = $this->session->userdata('UserID');

		$this->used_program_validate();

		$data = array(
			'program_id' => $post['used_program_id'],
			'duration' => $post['used_program_duration'],
			'user_id' => $user_id,
			'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);
		
		if (empty($post['program_id']) || $post['program_id'] == 0) {
			if ($this->used_program->programControl($post['used_program_id'], $user_id)) {
				$data['inputerror'][] = 'used_program_id';
				$data['error_string'][] = 'Bu program zaten ekli!';
				$data['status'] = FALSE;
				echo json_encode($data);
				exit();
			}
			$id = $this->used_program->save($data);
		} else {
			$id = 0;
			$this->used_program->update(array('id' => $post['program_id']), $data);
		}

		echo json_encode(array('status' => TRUE, 'id' => $id));
	}

	public function used_program_delete($id)
	{
		$this->load->model('cv/used_program_model', 'used_program');

		$data = $this->used_program->getByID($id);

		$this->used_program->deleteByID($id);

		echo json_encode(array("status" => TRUE));
	}

	private function used_program_validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('used_program_id') == '0' || empty($this->input->post('used_program_id')))
		{
			$data['inputerror'][] = 'used_program_id';
			$data['error_string'][] = 'Program seçiniz';
			$data['status'] = FALSE;
		}

		if(empty($this->input->post('used_program_duration')))
		{
			$data['inputerror'][] = 'used_program_duration';
			$data['error_string'][] = 'Kullanma Süresi alanı boş bırakılamaz';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function view_game_choice_all()
	{
		$this->load->model('cv/game_choice_model', 'game_choice');

		$user_id = $this->session->userdata('UserID');

		$data = $this->game_choice->getAllByUserID($user_id);

		if (empty($data)) 
		{
			$data = '';
		}

		echo json_encode($data);
	}

	public function game_choice_edit($id)
	{
		$this->load->model('cv/game_choice_model', 'game_choice');

		$data = $this->game_choice->getByID($id);

		echo json_encode($data);
	}

	public function game_choice_save()
	{
		$this->load->model('cv/game_choice_model', 'game_choice');

		$post = $this->input->post(null,true);
		$user_id = $this->session->userdata('UserID');

		$this->game_choice_validate();

		$data = array(
			'future_json' => json_encode($post['game_future_json']),
			'past_json' => json_encode($post['game_past_json']),
			'user_id' => $user_id,
			'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);
		
		if (empty($post['game_id']) || $post['game_id'] == 0) {
			if ($this->game_choice->gameControl($user_id)) {
				$data['inputerror'][] = 'game_future_json';
				$data['error_string'][] = 'Üretimine İlgi Duyduğun Türler zaten ekli!';
				$data['status'] = FALSE;
				echo json_encode($data);
				exit();
			}
			$id = $this->game_choice->save($data);
		} else {
			$id = 0;
			$this->game_choice->update(array('id' => $post['game_id']), $data);
		}

		echo json_encode(array('status' => TRUE, 'id' => $id));
	}

	public function game_choice_delete($id)
	{
		$this->load->model('cv/game_choice_model', 'game_choice');

		$data = $this->game_choice->getByID($id);

		$this->game_choice->deleteByID($id);

		echo json_encode(array("status" => TRUE));
	}

	private function game_choice_validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('game_future_json') == '0' || empty($this->input->post('game_future_json')))
		{
			$data['inputerror'][] = 'game_future_json';
			$data['error_string'][] = 'Gelecekte üretiminde bulunmak istediğiniz türleri seçiniz';
			$data['status'] = FALSE;
		}

		if($this->input->post('game_past_json') == '0' || empty($this->input->post('game_past_json')))
		{
			$data['inputerror'][] = 'game_past_json';
			$data['error_string'][] = 'Geçmişte üretiminde bulunduğunuz türleri seçiniz';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function view_portfolio_all()
	{
		$this->load->model('cv/portfolio_model', 'portfolio');

		$user_id = $this->session->userdata('UserID');

		$data = $this->portfolio->getAllByUserID($user_id);

		if (empty($data)) 
		{
			$data = '';
		}

		echo json_encode($data);
	}

	public function portfolio_edit($id)
	{
		$this->load->model('cv/portfolio_model', 'portfolio');

		$data = $this->portfolio->getByID($id);

		echo json_encode($data);
	}

	public function portfolio_save()
	{
		$this->load->model('cv/portfolio_model', 'portfolio');
		$this->load->library('file_upload');

		$post = $this->input->post(null,true);
		$user_id = $this->session->userdata('UserID');
		
		$this->portfolio_validate();

		$data = array(
			'user_id' => $user_id,
			'link' => $post['portfolio_link'],
			'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if ($this->portfolio->portfolioControl($user_id) && (empty($post['portfolio_id']) || $post['portfolio_id'] == 0)) {
			$data['inputerror'][] = 'portfolio_name';
			$data['error_string'][] = 'Portfolyo zaten ekli!';
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}

		$file_name = 'portfolio_name';
		if(!empty($_FILES[$file_name]['name'])) {
			if (!empty($post['portfolio_name_text'])) {
				if(file_exists(UPLOAD_FOLDER.'portfolios/'.$post['portfolio_name_text']) && $post['portfolio_name_text'])
					unlink(UPLOAD_FOLDER.'portfolios/'.$post['portfolio_name_text']);
			}

			$upload = $this->file_upload->do_upload($file_name, '*', 10000, './upload/portfolios/');
			$data['name'] = $upload;
		} else {
			$data['name'] = '';
		}
		
		if (empty($post['portfolio_id']) || $post['portfolio_id'] == 0) {
			$id = $this->portfolio->save($data);
		} else {
			$id = 0;
			$this->portfolio->update(array('id' => $post['portfolio_id']), $data);
		}

		echo json_encode(array('status' => TRUE, 'id' => $id));
	}

	public function portfolio_delete($id)
	{
		$this->load->model('cv/portfolio_model', 'portfolio');

		$portfolio = $this->portfolio->getByID($id);

		if(file_exists(UPLOAD_FOLDER.'portfolios/'.$portfolio->name) && $portfolio->name)
			unlink(UPLOAD_FOLDER.'portfolios/'.$portfolio->name);

		$this->portfolio->deleteByID($id);

		echo json_encode(array("status" => TRUE));
	}

	private function portfolio_validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if(empty($_FILES['portfolio_name']['name']) && empty($this->input->post('portfolio_link')))
		{
			$data['inputerror'][] = 'portfolio_link';
			$data['error_string'][] = 'Portfolyo ekleyebilmek için Dosya veya Link eklemelisiniz';
			$data['status'] = FALSE;
		}

		if (!empty($this->input->post('portfolio_link'))) 
		{
			$regex = "((https?|ftp)\:\/\/)?"; // SCHEME 
		    $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass 
		    $regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP 
		    $regex .= "(\:[0-9]{2,5})?"; // Port 
		    $regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path 
		    $regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query 
		    $regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor 
		    
			if (!preg_match("/^$regex$/i", $this->input->post('portfolio_link'))) { 
				$data['inputerror'][] = 'portfolio_link';
				$data['error_string'][] = 'Portfolyo Linki geçerli değildir.';
				$data['status'] = FALSE;
			}
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function confirm_save()
	{
		$this->load->model('user/user_model', 'user');
		$this->load->model('cv/personal_information_model', 'personal_information');
		$this->load->model('cv/educational_background_model', 'educational_background');
		$this->load->model('cv/work_experience_model', 'work_experience');
		$this->load->model('cv/used_program_model', 'used_program');
		$this->load->model('cv/game_choice_model', 'game_choice');
		$this->load->model('cv/portfolio_model', 'portfolio');

		$post = $this->input->post(null,true);
		$user_id = $this->session->userdata('UserID');

		$this->confirm_validate();

		$data = array(
			'user_privacy' => $post['user_privacy'],
			'user_kvkk' => $post['user_kvkk'],
			'user_confirm' => $post['user_confirm'],
			'cv_created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if (!$this->personal_information->personalControl($user_id)) {
			$data['inputerror'][] = 'user_control';
			$data['error_string'][] = 'Kişisel Bilgiler alanı boş kalmıştır. Lütfen geri dönüp formu doldurunuz.';
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		else if (!$this->educational_background->educationControl($user_id)) {
			$data['inputerror'][] = 'user_control';
			$data['error_string'][] = 'Eğitim Geçmişi alanı boş kalmıştır. Lütfen geri dönüp formu doldurunuz.';
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		} else if (!$this->game_choice->gameControl($user_id)) {
			$data['inputerror'][] = 'user_control';
			$data['error_string'][] = 'Üretimine İlgi Duyduğun Türler alanı boş kalmıştır. Lütfen geri dönüp formu doldurunuz.';
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		} else if (!$this->portfolio->portfolioControl($user_id)) {
			$data['inputerror'][] = 'user_control';
			$data['error_string'][] = 'Portfolyo alanı boş kalmıştır. Lütfen geri dönüp formu doldurunuz.';
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}

		// else if (!$this->work_experience->workControl($user_id)) {
		// 	$data['inputerror'][] = 'user_control';
		// 	$data['error_string'][] = 'Geçmiş Deneyimler alanı boş kalmıştır. Lütfen geri dönüp formu doldurunuz.';
		// 	$data['status'] = FALSE;
		// 	echo json_encode($data);
		// 	exit();
		// } else if (!$this->used_program->usedProgramControl($user_id)) {
		// 	$data['inputerror'][] = 'user_control';
		// 	$data['error_string'][] = 'Kullanılan Programlar alanı boş kalmıştır. Lütfen geri dönüp formu doldurunuz.';
		// 	$data['status'] = FALSE;
		// 	echo json_encode($data);
		// 	exit();
		// }
		
		if (!empty($user_id)) {
			$this->user->update(array('id' => $user_id), $data);
			$this->session->set_userdata('UserStatus', 1);

			echo json_encode(array('status' => TRUE));
		}
	}

	private function confirm_validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('user_privacy') == 0)
		{
			$data['inputerror'][] = 'user_privacy';
			$data['error_string'][] = 'Onayla, Gizlilik Politikaları alanı işaretleyiniz';
			$data['status'] = FALSE;
		}

		if($this->input->post('user_kvkk') == 0)
		{
			$data['inputerror'][] = 'user_kvkk';
			$data['error_string'][] = 'KVKK Bilgilerini okudum ve onayladım alanı işaretleyiniz';
			$data['status'] = FALSE;
		}

		if($this->input->post('user_confirm') == 0)
		{
			$data['inputerror'][] = 'user_confirm';
			$data['error_string'][] = 'Verdiğim bilgilerin doğru olduğunu onaylıyorumalanı işaretleyiniz';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}

?>