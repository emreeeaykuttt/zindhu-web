<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if (!$this->session->userdata('UserID')) {
			redirect(base_url('zindhu-hr/kullanici/giris')); exit();
		} else {
			if ($this->session->userdata('UserStatus') == 0) {
				redirect(base_url('zindhu-hr/kullanici/cv')); exit();
			} elseif ($this->session->userdata('UserAdminConfirm') == 'pending') {
				redirect(base_url('zindhu-hr/kullanici/onay-bekliyor')); exit();
			} elseif ($this->session->userdata('UserAdminConfirm') == 'rejected') {
				redirect(base_url('zindhu-hr/kullanici/reddedildi')); exit();
			}

			$this->load->model('contact/contact_model','contact');
		}
	}

	public function index()
	{
		$this->load->view('frontend/user/contact_view');
	}

	public function start_contact()
	{
		$user_id = $this->session->userdata('UserID');

		$data['messages'] = $this->contact->getAllByUserID($user_id);
		$this->contact->update(array('user_id' => $user_id, 'sender_type' => 'admin'), array('is_read' => 1));

		echo json_encode($data);
	}

	public function send_message()
	{
		$post = $this->input->post(null,true);
		$user_id = $this->session->userdata('UserID');
		$content = $post['message'];

		$data = array(
			'sender_type' => 'user',
			'user_id' => $user_id,
			'content' => $content,
			'ip_address' => $this->input->ip_address(),
			'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if (!empty($content) && strlen(trim($content)) > 3) {
			if (!empty($user_id)) {
				$this->contact->save($data);
				echo json_encode(array('status' => TRUE, 'message' => 'Mesaj gönderildi.', 'record' => $data)); exit();
			} else {
				echo json_encode(array('status' => FALSE, 'message' => 'Mesaj gönderilmedi.')); exit();
			}
		} else {
			echo json_encode(array('status' => FALSE, 'message' => 'Mesaj içeriği girmeden mesaj gönderemezsiniz.')); exit();
		}
	}

	public function unread_contact()
	{
		$user_id = $this->session->userdata('UserID');
	
		$data = $this->contact->unreadCountByUserID($user_id, 'admin');

		echo json_encode($data);
	}

	public function control_conversation()
	{
		$user_id = $this->session->userdata('UserID');
		
		$data['messages'] = $this->contact->getUnreadMessagesByUserID($user_id, 'admin');
		$this->contact->update(array('user_id' => $user_id, 'sender_type' => 'admin'), array('is_read' => 1));

		echo json_encode($data);
	}
}

?>