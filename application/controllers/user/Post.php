<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('UserID')) {
			redirect(base_url('zindhu-hr/kullanici/giris')); exit();
		} else {
			if ($this->session->userdata('UserStatus') == 0) {
				redirect(base_url('zindhu-hr/kullanici/cv')); exit();
			} elseif ($this->session->userdata('UserAdminConfirm') == 'pending') {
				redirect(base_url('zindhu-hr/kullanici/onay-bekliyor')); exit();
			} elseif ($this->session->userdata('UserAdminConfirm') == 'rejected') {
				redirect(base_url('zindhu-hr/kullanici/reddedildi')); exit();
			}
		}
	}

	public function index()
	{
		redirect(base_url('zindhu-hr/kullanici/dashboard'));
	}

	public function detail($title_and_id = '')
	{
		$this->load->model('post/post_model','post');

		$get_parameter = explode('-', $title_and_id);
		$post_id = end($get_parameter);

		$post_detail = $this->post->getByID($post_id);

		if (!empty($post_detail)) {
			$data['post_detail'] = $post_detail;
			$this->load->view('frontend/user/post_detail_view', $data);
		} else {
			redirect(base_url('zindhu-hr/kullanici/dashboard'));
		}
	}
}

?>