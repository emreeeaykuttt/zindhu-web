<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conversations extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if (!$this->session->userdata('UserID')) {
			redirect(base_url('zindhu-hr/kullanici/giris')); exit();
		} else {
			if ($this->session->userdata('UserStatus') == 0) {
				redirect(base_url('zindhu-hr/kullanici/cv')); exit();
			} elseif ($this->session->userdata('UserAdminConfirm') == 'pending') {
				redirect(base_url('zindhu-hr/kullanici/onay-bekliyor')); exit();
			} elseif ($this->session->userdata('UserAdminConfirm') == 'rejected') {
				redirect(base_url('zindhu-hr/kullanici/reddedildi')); exit();
			}

			$this->load->model('company/company_model','company');
		}
	}

	public function index()
	{
		$this->load->view('frontend/user/conversations_view');
	}

	public function ajax_list()
	{
		$this->load->model('conversation/conversation_model','conversation');
		$this->load->model('company/hires_model', 'company_hires');

		$get_data = array();
		$user_id = $this->session->userdata('UserID');
		$get_data['conversation_user'] = 1;
		$get_data['user_id'] = $user_id;
		
		if ($this->input->get('unread') == 1) {
			$get_data['unread'] = $this->input->get('unread');
		}

		$list = $this->company->getDatatables($get_data);
		$data = array();

		foreach ($list as $company) 
		{
			$row = array();

			$last_message = $this->conversation->getLastMessage($company->id, $user_id, 'company');

			$unread_count = $this->conversation->unreadCountByCompanyIDAndUserID($company->id, $user_id, 'company');

			if ($unread_count > 0) {
				$conversation_icon = '<div class="unread"></div>';
			} else {
				$conversation_icon = '';
			}

			// $hires = $this->company_hires->getByCompanyIDAndUserID($company->id, $user_id);
			// $work_status_color = '';
			// $work_status_text = 'Herhangi bir işlem yapılmamıştır.';
			// if ($hires) {
			// 	if ($hires->company_confirm == 1 && $hires->user_confirm == 0 && $hires->continues == 0 && !$hires->enddate) {
   //              	$work_status_color = 'blue';
			// 		$work_status_text = 'Stüdyodan size istek gönderildi.';
   //              } else if ($hires->company_confirm == 0 && $hires->user_confirm == 1 && $hires->continues == 0 && !$hires->enddate) {
   //              	$work_status_color = 'orange';
			// 		$work_status_text = 'Stüdyodan dönüş bekleniyor.';
   //              } else if ($hires->company_confirm == 1 && $hires->continues == 1 && !$hires->enddate) {
   //              	$work_status_color = 'green';
			// 		$work_status_text = 'Stüdyo ile çalışmaya başladım.';
   //              } else if ($hires->company_confirm == 1 && $hires->continues == 0 && $hires->enddate) {
   //              	$work_status_color = 'red';
			// 		$work_status_text = $hires->enddate . ' tarihinde işten çıkışım yapıldı.';
   //              }
			// }

			$row['transactions'] = 
				'<div class="conversation-item" onclick="start_conversation('.$company->id.')">' .
					'<div class="first-letter">'. substr($company->company_name, 0, 1) .'</div>' .
					'<div class="info">' .
						'<div class="from">'. $company->company_name .'</div>' .
						'<div class="subject">'. text_limitation($last_message->content, 50) .'</div>' .
					'</div>' .
					'<div class="status">' .
						'<div class="time">'. time_render($last_message->created_at) .'</div>' .
						$conversation_icon .
					'</div>' .
				'</div>';

		
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->company->getCountAll($get_data),
			"recordsFiltered" => $this->company->getCountFiltered($get_data),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
}

?>