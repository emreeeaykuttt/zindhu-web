<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('UserID')) {
			redirect(base_url('zindhu-hr/kullanici/giris')); exit();
		} else {
			if ($this->session->userdata('UserStatus') == 0) {
				redirect(base_url('zindhu-hr/kullanici/cv')); exit();
			} elseif ($this->session->userdata('UserAdminConfirm') == 'pending') {
				redirect(base_url('zindhu-hr/kullanici/onay-bekliyor')); exit();
			} elseif ($this->session->userdata('UserAdminConfirm') == 'rejected') {
				redirect(base_url('zindhu-hr/kullanici/reddedildi')); exit();
			}
		}
	}

	public function index()
	{
		redirect(base_url());
	}

	public function all_statistics()
	{
		$this->load->model('company/watch_model', 'watch');
		$this->load->model('company/views_model', 'company_views');
		$this->load->model('conversation/conversation_model','conversation');

		$user_id = $this->session->userdata('UserID');

		$data['watch_count'] = $this->watch->getCountByUserID($user_id);
		$data['conversation_count'] = $this->conversation->getCountByUserID($user_id);
		$data['views_count'] = $this->company_views->getCountByUserID($user_id);

		echo json_encode($data);
	}

	public function unread_conversations()
	{
		$this->load->model('conversation/conversation_model','conversation');

		$user_id = $this->session->userdata('UserID');
	
		$data = $this->conversation->unreadCountByUserID($user_id);

		echo json_encode($data);
	}

	public function company_view($company_id)
	{
		$this->load->model('company/company_model','company');
		$this->load->model('company/hires_model', 'company_hires');

		$user_id = $this->session->userdata('UserID');

		$data['company'] = $this->company->getByIDAndJoinUserID($company_id, $user_id);
		$data['company_hires'] = $this->company_hires->getByCompanyIDAndUserID($company_id, $user_id);

		echo json_encode($data);
	}

	public function start_conversation($company_id)
	{
		$this->load->model('company/company_model','company');
		$this->load->model('conversation/conversation_model','conversation');
		$this->load->model('company/hires_model', 'company_hires');

		$user_id = $this->session->userdata('UserID');

		$data['company'] = $this->company->getByIDAndJoinUserID($company_id, $user_id);
		$data['conversations'] = $this->conversation->getAllByCompanyIDAndUserID($company_id, $user_id);
		$data['company_hires'] = $this->company_hires->getByCompanyIDAndUserID($company_id, $user_id);
		$this->conversation->update(array('company_id' => $company_id, 'user_id' => $user_id, 'sender_type' => 'company'), array('is_read' => 1));

		echo json_encode($data);
	}

	public function send_message()
	{
		$this->load->model('conversation/conversation_model','conversation');

		$post = $this->input->post(null,true);
		$user_id = $this->session->userdata('UserID');
		$company_id = $post['company_id'];
		$content = $post['message'];

		$data = array(
			'sender_type' => 'user',
			'is_read' => 0,
			'company_id' => $company_id,
			'user_id' => $user_id,
			'content' => $content,
			'ip_address' => $this->input->ip_address(),
			'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if (!empty($content)) {
			if (!empty($company_id) && !empty($user_id)) {
				
				$is_send_mail = false;
				$row = $this->conversation->getLastMessage($company_id, $user_id, 'user');
				$this->conversation->save($data);

				if (!empty($row)) {
					$time_start = strtotime($row->created_at);
					$time_end = strtotime(date('Y-m-d H:i:s', NOW_DATE_TIME));
					$interval = $time_end - $time_start;
					$hour = floor($interval / (60 * 60));

					if ($hour >= 1) {
						$is_send_mail = true;
					}
				} else {
					$is_send_mail = true;
				}

				if ($is_send_mail) {
					$this->load->model('company/company_model', 'company');
					$this->load->model('user/user_model', 'user');

					$company_row = $this->company->getByID($company_id);
					$user_row = $this->user->getByID($user_id);

					$tos = array($company_row->email);
		            $ccs = array();
		            $bccs = array();
		            $link = base_url() . 'zindhu-hr/studyo/konusmalar?user=' . $user_id;
		            $link_text = 'Mesajı Görüntüle';

		            $mail_content = 
		                'Merhaba <b>'. $company_row->firstname . ' ' . $company_row->lastname .',</b><br><br>
		                1 yeni mesajınız var.<br><br>
		                Mesajı Gönderen Kişi: ' . $user_row->firstname;

		            $html_message = $this->mail_template($mail_content, $link, $link_text);

		            $status = $this->send_email($tos, $ccs, $bccs, 'zindhu.com', $user_row->firstname  .' sana yeni bir mesaj gönderdi', $html_message);
				}

				echo json_encode(array('status' => TRUE, 'message' => 'Mesaj gönderildi.', 'record' => $data)); exit();
			} else {
				echo json_encode(array('status' => FALSE, 'message' => 'Mesaj gönderilmedi.')); exit();
			}
		} else {
			echo json_encode(array('status' => FALSE, 'message' => 'Mesaj içeriği girmeden mesaj gönderemezsiniz.')); exit();
		}
	}

	public function company_invite()
	{
		$this->load->model('company/hires_model', 'company_hires');
		$this->load->model('conversation/conversation_model','conversation');

		$post = $this->input->post(null,true);
		$user_id = $this->session->userdata('UserID');
		$company_id = $post['company_id'];

		$data = array(
			'company_id' => $company_id,
			'user_id' => $user_id,
			'user_confirm' => 1,
			'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if (!empty($company_id) && !empty($user_id)) {
			$is_save = FALSE;
			$company_hires = $this->company_hires->getByCompanyIDAndUserID($company_id, $user_id);
			if ($company_hires) {
				if ($company_hires->enddate) {
					$is_save = TRUE;
				}
			} else {
				$is_save = TRUE;
			}

			if ($is_save) {
				$this->company_hires->save($data);

				$message = array(
					'sender_type' => 'user',
					'is_read' => 0,
					'company_id' => $company_id,
					'user_id' => $user_id,
					'is_system_message' => 1,
					'content' => 'Sizinle çalışmak istiyorum. Eğer kabul ediyorsanız yukarıdaki "İŞE AL" butonuna tıklar mısınız.',
					'ip_address' => $this->input->ip_address(),
					'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
				);

				$this->conversation->save($message);

				echo json_encode(array('status' => TRUE, 'message' => 'İstek gönderildi.', 'record' => $data));
			} else {
				echo json_encode(array('status' => FALSE, 'message' => 'İstek zaten gönderilmiş.'));
			}
		} else {
			echo json_encode(array('status' => FALSE, 'message' => 'İstek gönderilmedi.'));
		}
	}

	public function company_accept_invite()
	{
		$this->load->model('company/hires_model', 'company_hires');
		$this->load->model('conversation/conversation_model','conversation');
		$this->load->model('user/user_model', 'user');

		$post = $this->input->post(null,true);
		$user_id = $this->session->userdata('UserID');
		$company_id = $post['company_id'];

		$data = array(
			'user_confirm' => 1,
			'continues' => 1,
			'startdate' => date('Y-m-d H:i:s', NOW_DATE_TIME),
			'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if (!empty($company_id) && !empty($user_id)) {
			$is_update = FALSE;
			$company_hires = $this->company_hires->getByCompanyIDAndUserID($company_id, $user_id);
			if ($company_hires) {
				if ($company_hires->user_confirm == 0) {
					$is_update = TRUE;
				}
			}

			if ($is_update) {
				$this->company_hires->update(
					array('company_id' => $company_id, 'user_id' => $user_id, 'user_confirm' => 0), 
					$data
				);

				$message = array(
					'sender_type' => 'user',
					'is_read' => 0,
					'company_id' => $company_id,
					'user_id' => $user_id,
					'is_system_message' => 1,
					'content' => 'İşe alım isteğinizi kabul ettim.',
					'ip_address' => $this->input->ip_address(),
					'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
				);

				$this->conversation->save($message);

				$user_info = $this->user->getByID($user_id);

				if (!empty($user_info->company_ids)) {
					$company_arr = json_decode($user_info->company_ids, true);
					$company_arr[] = $company_id;
					$company_ids = json_encode($company_arr);
				} else {
					$company_ids = json_encode(array($company_id));
				}
				
				$this->user->update(array('id' => $user_id), array('company_ids' => $company_ids, 'company_text' => NULL, 'is_working' => 1));

				echo json_encode(array('status' => TRUE, 'message' => 'İşe alındınız.', 'record' => $data));
			} else {
				echo json_encode(array('status' => FALSE, 'message' => 'Böyle bir kayıt yoktur.'));
			}
		} else {
			echo json_encode(array('status' => FALSE, 'message' => 'Böyle bir kayıt yoktur.'));
		}
	}

	public function company_exit()
	{
		$this->load->model('company/hires_model', 'company_hires');
		$this->load->model('conversation/conversation_model','conversation');
		$this->load->model('user/user_model', 'user');

		$post = $this->input->post(null,true);
		$user_id = $this->session->userdata('UserID');
		$company_id = $post['company_id'];

		$data = array(
			'continues' => 0,
			'enddate' => date('Y-m-d H:i:s', NOW_DATE_TIME),
			'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if (!empty($company_id) && !empty($user_id)) {
			$is_update = FALSE;
			$company_hires = $this->company_hires->getByCompanyIDAndUserID($company_id, $user_id);
			if ($company_hires) {
				if (!$company_hires->enddate && $company_hires->continues == 1) {
					$is_update = TRUE;
				}
			}

			if ($is_update) {
				$this->company_hires->update(
					array('company_id' => $company_id, 'user_id' => $user_id, 'enddate' => NULL, 'continues' => 1), 
					$data
				);

				$message = array(
					'sender_type' => 'user',
					'is_read' => 0,
					'company_id' => $company_id,
					'user_id' => $user_id,
					'is_system_message' => 1,
					'content' => 'İşten ayrıldım.',
					'ip_address' => $this->input->ip_address(),
					'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
				);

				$this->conversation->save($message);

				$user_info = $this->user->getByID($user_id);

				if (!empty($user_info->company_ids)) {
					$company_arr = json_decode($user_info->company_ids, true);
					if (($key = array_search($company_id, $company_arr)) !== false) {
					    unset($company_arr[$key]);
					}
					if (empty($company_arr)) {
						$company_ids = NULL;
						$is_working = 0;
					} else {
						$company_ids = json_encode($company_arr);
						$is_working = 1;
					}
				} else {
					$company_ids = NULL;
					$is_working = 0;
				}

				$this->user->update(array('id' => $user_id), array('company_ids' => $company_ids, 'company_text' => NULL, 'is_working' => $is_working));

				echo json_encode(array('status' => TRUE, 'message' => 'İşten ayrıldınız.', 'record' => $data));
			} else {
				echo json_encode(array('status' => FALSE, 'message' => 'Böyle bir kayıt yoktur.'));
			}
		} else {
			echo json_encode(array('status' => FALSE, 'message' => 'Böyle bir kayıt yoktur.'));
		}
	}

	public function watch_companies()
	{
		$this->load->model('company/watch_model', 'watch');
		$user_id = $this->session->userdata('UserID');

		$data['companies'] = $this->watch->getAllByUserID($user_id);

		echo json_encode($data);
	}

	public function views_companies()
	{
		$this->load->model('company/views_model', 'company_views');
		$user_id = $this->session->userdata('UserID');

		$now_date = date('Y-m-d H:i:s', NOW_DATE_TIME);
		$last30days_date = date('Y-m-d H:i:s', strtotime($now_date . "-30 days"));
		$last7days_date = date('Y-m-d H:i:s', strtotime($now_date . "-7 days"));
		
		$data['companies_all'] = $this->company_views->getAllByUserID($user_id);
		$this->company_views->startdate = $last30days_date;
		$this->company_views->enddate = $now_date;
		$data['companies_last30days'] = $this->company_views->getAllByUserID($user_id);
		$this->company_views->startdate = $last7days_date;
		$data['companies_last7days'] = $this->company_views->getAllByUserID($user_id);

		echo json_encode($data);
	}

	public function control_conversation($company_id)
	{
		$this->load->model('conversation/conversation_model','conversation');

		$user_id = $this->session->userdata('UserID');
		
		$data['messages'] = $this->conversation->getUnreadMessages($company_id, $user_id, 'company');
		$this->conversation->update(array('company_id' => $company_id, 'user_id' => $user_id, 'sender_type' => 'company'), array('is_read' => 1));

		echo json_encode($data);
	}
}

?>