<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ad extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('ad/ad_model','ad');
		$this->load->model('ad/ad_user_model','ad_user');
	}

	public function index()
	{
		redirect(base_url());
	}

	public function listing($page_id = '')
	{
		$this->load->library('pagination');
		$this->load->driver('cache', array('adapter' => 'file', 'backup' => 'file'));
		$this->load->model('db/position_model', 'position');
		$this->load->model('db/city_model', 'city');

		$data['positions'] = $this->position->getAll();
		$data['cities'] = $this->city->getAll();
		$data['study_types'] = array('Full-Time','Part-Time','Freelance');
		$user_id = $this->session->userdata('UserID');
		$get = $this->input->get(null, true);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 1;
		$per_page = 12;

		if ($this->input->get('clear') == 1) {
			redirect(base_url('zindhu-hr/kullanici/is-ilanlari'));
			exit;
		}
		
		$search = array(
			'keywords' => !empty($get['keywords']) ? $get['keywords'] : NULL,
			'position' => !empty($get['position']) ? $get['position'] : NULL,
			'city' => !empty($get['city']) ? $get['city'] : NULL,
			'study_type' => !empty($get['study_type']) ? $get['study_type'] : NULL,
			'transaction' => !empty($get['transaction']) ? $get['transaction'] : NULL,
			'user_id' => !empty($user_id) ? $user_id : NULL,
		);

		$this->ad->status = 1;
		$this->ad->admin_confirm = 'approved';
		$this->ad->search = $search;
		$this->ad->page = $page;
		$this->ad->per_page = $per_page;
		$this->ad->total_rows = TRUE;
		$total_rows = $this->ad->getAll();
		$this->ad->total_rows = FALSE;
		$data['ads'] = $this->ad->getAll();
		$data['total_page'] = ceil($total_rows / $per_page);
		$data['total_rows'] = $total_rows ? $total_rows : 0;
		$data['search'] = $search;

		// pagination config (start)
		$config = array();
		$config['base_url'] = base_url('zindhu-hr/kullanici/is-ilanlari');
		$config['per_page'] = $per_page;
		$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = '<ul class="pagination justify-content-center">';
		$config['full_tag_close'] = '</ul>';
		$config['attributes'] = ['class' => 'page-link'];
		$config['first_link'] = '1';
		$config['last_link'] = $data['total_page'];
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link bg-black">';
		$config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		$config['total_rows'] = $total_rows;
		$this->pagination->initialize($config);
		$str_links = $this->pagination->create_links();
		$data['links'] = explode('&nbsp;', $str_links);
		// pagination config (end)

		$this->load->view('frontend/user/ad/listing_view', $data);
	}

	public function detail($title_and_id = '')
	{
		$get_parameter = explode('-', $title_and_id);
		$id = end($get_parameter);
		$user_id = $this->session->userdata('UserID');

		$this->ad->status = 1;
		$this->ad->admin_confirm = 'approved';
		$ad = $this->ad->getByID($id);

		$this->ad->per_page = 3;
		$this->ad->page = 1;
		$this->ad->search = array('position' => json_decode($ad->position_json));
		$this->ad->excluding_id = $id;
		$data['related_records'] = $this->ad->getAll();
		$data['ad'] = $ad;

		if (!empty($ad)) {

			$ad_user = $this->ad_user->getByAdIDAndUserID($id, $user_id);
			$data['ad_user'] = $ad_user;
			if (empty($ad_user) && isset($user_id)) {
				$this->ad_user->save(
					array('ad_id' => $ad->id, 'user_id' => $user_id, 'views_date' => date('Y-m-d H:i:s', NOW_DATE_TIME))
				);
			}

			array_pop($get_parameter);
			if ($ad->slug != implode('-', $get_parameter)) {
				redirect(base_url('zindhu-hr/kullanici/is-ilani/' . $ad->slug . '-' . $ad->id)); 
				exit;
			}

			$this->load->view('frontend/user/ad/detail_view', $data);

		} else {
			redirect(base_url('zindhu-hr/kullanici/is-ilanlari'));		
		}
	}

	public function ajax_apply()
	{
		$post = $this->input->post(null,true);
		$user_id = $this->session->userdata('UserID');

		$data = array(
			'apply_date' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		$ad = $this->ad->getByID($post['ad_id']);

		if (!empty($ad)) {

			if (!$user_id && $this->session->userdata('UserAdminConfirm') != 'approved') {
				echo json_encode(
					array(
						'status' => false, 
						'message' => 'İlana başvurmak için önce giriş yapınız',
						'referral_link' => 'zindhu-hr/kullanici/giris?referral-link=zindhu-hr/is-ilani/' . $ad->slug . '-' . $ad->id
					)
				);
				die;
			}

			$updated = $this->ad_user->update(array('ad_id' => $post['ad_id'], 'user_id' => $user_id), $data);

			if ($updated) {
				echo json_encode(array('status' => true, 'message' => 'İlana başvuruldu'));
			} else {
				echo json_encode(array('status' => false, 'message' => 'İlanı başvururken bir sorun oluştu'));
			}
			
		} else {
			echo json_encode(array('status' => false, 'message' => 'İlan Bulunamadı'));
		}
	}

	public function ajax_follow()
	{
		$post = $this->input->post(null,true);
		$user_id = $this->session->userdata('UserID');

		$data = array(
			'follow_date' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		$ad = $this->ad->getByID($post['ad_id']);

		if (!empty($ad)) {

			if (!$user_id && $this->session->userdata('UserAdminConfirm') != 'approved') {
				echo json_encode(
					array(
						'status' => false, 
						'message' => 'İlanı takip etmek için önce giriş yapınız',
						'referral_link' => 'zindhu-hr/kullanici/giris?referral-link=zindhu-hr/is-ilani/' . $ad->slug . '-' . $ad->id
					)
				);
				die;
			}

			$updated = $this->ad_user->update(array('ad_id' => $post['ad_id'], 'user_id' => $user_id), $data);

			if ($updated) {
				echo json_encode(array('status' => true, 'message' => 'İlan takip edildi'));
			} else {
				echo json_encode(array('status' => false, 'message' => 'İlanı takip ederken bir sorun oluştu'));
			}
			
		} else {
			echo json_encode(array('status' => false, 'message' => 'İlan Bulunamadı'));
		}
	}

	public function ajax_unfollow()
	{
		$post = $this->input->post(null,true);
		$user_id = $this->session->userdata('UserID');

		$data = array(
			'follow_date' => NULL
		);

		$ad = $this->ad->getByID($post['ad_id']);

		if (!empty($ad) && $user_id && $this->session->userdata('UserAdminConfirm') == 'approved') {

			$updated = $this->ad_user->update(array('ad_id' => $post['ad_id'], 'user_id' => $user_id), $data);

			if ($updated) {
				echo json_encode(array('status' => true, 'message' => 'İlan takipten çıkarıldı'));
			} else {
				echo json_encode(array('status' => false, 'message' => 'İlanı takipten çıkarırken bir sorun oluştu'));
			}
			
		} else {
			echo json_encode(array('status' => false, 'message' => 'İlan Bulunamadı'));
		}
	}
}

?>