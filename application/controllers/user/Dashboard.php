<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if (!$this->session->userdata('UserID')) {
			redirect(base_url('zindhu-hr/kullanici/giris')); exit();
		} else {
			if ($this->session->userdata('UserStatus') == 0) {
				redirect(base_url('zindhu-hr/kullanici/cv')); exit();
			} elseif ($this->session->userdata('UserAdminConfirm') == 'pending') {
				redirect(base_url('zindhu-hr/kullanici/onay-bekliyor')); exit();
			} elseif ($this->session->userdata('UserAdminConfirm') == 'rejected') {
				redirect(base_url('zindhu-hr/kullanici/reddedildi')); exit();
			}
		}
	}

	public function index()
	{
		$this->load->model('post/post_model','post');
		$this->load->model('ad/ad_user_model','ad_user');

		$user_id = $this->session->userdata('UserID');
		$this->post->limit = 5;

		$data['announcements'] = $this->post->getByType('duyuru');
		$data['educations'] = $this->post->getByType('egitim');
		$data['blogs'] = $this->post->getByType('blog');
		$this->ad_user->user_id = $user_id;
		$this->ad_user->follow_date = TRUE;
		$data['followed_ads'] = $this->ad_user->getAll();

		$this->load->view('frontend/user/dashboard_view', $data);
	}
}

?>