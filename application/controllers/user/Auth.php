<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user/user_model', 'user');
	}

	public function index()
	{	
		redirect(base_url('zindhu-hr/kullanici/giris'));
	}

	public function login()
	{
		$post = $this->input->post(null, true);
		$this->session->set_flashdata('account_enabled', NULL);
		$this->session->set_flashdata('login_status', NULL);

		if ($post) 
		{
			$this->load->library('form_validation');

			$this->form_validation->set_rules('email', 'E-Posta', 'trim|required|valid_email');
			$this->form_validation->set_rules('password', 'Şifre', 'trim|required|min_length[6]');

			$this->form_validation->set_message('min_length', '{field} alanı en az {param} karakterli olmalıdır.');
			$this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');
			$this->form_validation->set_message('valid_email', '{field} alanı geçerli bir e-posta adresi içermelidir.');
			
			if ($this->form_validation->run() == FALSE) 
			{
				$this->load->view('frontend/user/auth/login_view');
			}
			else
			{
				$result = $this->user->validate($post['email'], $post['password']);
				if($result)
				{	
					$this->load->model('cv/personal_information_model', 'personal_information');
					$personal_information = $this->personal_information->getAllByUserID($result->id);

					$this->session->unset_userdata(array('UserID', 'UserEmail', 'UserFullname', 'UserFirstname', 'UserPosition', 'UserCvStatus', 'UserStatus', 'UserAdminConfirm', 'UserFirstLogin'));
					$this->session->set_userdata('UserID', $result->id);
					$this->session->set_userdata('UserEmail', $result->email);
					$this->session->set_userdata('UserFullname', $result->firstname . ' ' . $result->lastname);
					$this->session->set_userdata('UserFirstname', $result->firstname);
					$this->session->set_userdata('UserPosition', position_render($personal_information[0]['position_id']));
					$this->session->set_userdata('UserCvStatus', $result->cv_status);
					$this->session->set_userdata('UserAdminConfirm', $result->admin_confirm);
					$this->session->set_userdata('UserFirstLogin', 0);

					$remember = empty($post['remember']) ? 0 : 1;
					if($remember == 1) {
						$this->session->set_tempdata('RememberUserEmail', $post['email'], 604800);
						$this->session->set_tempdata('RememberUserPassword', $post['password'], 604800);
					} else {
						$this->session->unset_tempdata(array('RememberUserEmail', 'RememberUserPassword'));
					}

					if ($result->admin_confirm == 'approved') {
						if ($result->first_login == 0) {
							$this->user->update(array('id' => $result->id), array('first_login' => 1));
							$this->session->set_userdata('UserFirstLogin', 1);
						}
					}

					if ($result->user_confirm == 1) {

						$this->session->set_userdata('UserStatus', 1);
						
						if ($this->input->get('referral-link')) {
							redirect(base_url($this->input->get('referral-link')));
						} else {
							redirect(base_url('zindhu-hr/kullanici/dashboard'));
						}

					} else {
						$this->session->set_userdata('UserStatus', 0);
						redirect(base_url('zindhu-hr/kullanici/cv'));
					}
				}
				else
				{
					$this->session->set_flashdata('login_status', array(
						'status' => false, 'message' => 'Email veya şifreyi yanlış girmiş olabilirsiniz.')
					);
					$this->load->view('frontend/user/auth/login_view');
				}
			}
		}
		else
		{
			if ($this->session->userdata('UserID'))
			{
				redirect(base_url('zindhu-hr/kullanici/dashboard'));
			}
			else
			{
				$get = $this->input->get(null, true);
				$token = isset($get['token']) ? $get['token'] : NULL;
				$user_id = isset($get['key']) ? $get['key'] : NULL;

				if (isset($token) && isset($user_id)) 
				{
					$result = $this->user->verification($token, $user_id);
					if ($result > 0)
					{
						$this->session->set_flashdata('account_enabled', array(
							'status' => true, 'message' => 'Hesabınız başarılı bir şekilde etkinleştirilmiştir.')
						);
					}
				}

				$this->load->view('frontend/user/auth/login_view');
			}
		}
	}

	public function alpha_turkish($val){
	    if (! preg_match('/^[a-zA-ZğıüşöçİĞÜŞÖÇ![:space:]]+$/', $val)) {
	        $this->form_validation->set_message('alpha_turkish', '{field} alanına boş bırakılamaz ve harf dışında birşey girilemez.');
	        return FALSE;
	    } else {
	        return TRUE;
	    }
	}

	public function is_accept() {
	    if (isset($_POST['is_accept'])) return true;
	    $this->form_validation->set_message('is_accept', 'Aydınlatma Metnini onaylamadan devam edemezsiniz');
	    return false;
	}

	public function register()
	{	
		if (!$this->session->userdata('UserID'))
		{
			$post = $this->input->post(null, true);

			if ($post)
			{
				$this->load->library('form_validation');

				$this->form_validation->set_rules('firstname', 'İsim', 'trim|required|min_length[2]|callback_alpha_turkish');
		        $this->form_validation->set_rules('lastname', 'Soyisim', 'trim|required|min_length[2]|callback_alpha_turkish');
		        $this->form_validation->set_rules('password', 'Şifre', 'trim|required|min_length[6]');
		        $this->form_validation->set_rules('password_again', 'Şifre Tekrar', 'trim|matches[password]');
		        $this->form_validation->set_rules('email', 'E-Posta', 'trim|required|valid_email|is_unique[users.email]');
		        $this->form_validation->set_rules('is_accept', '', 'callback_is_accept');

		        $this->form_validation->set_message('min_length', '{field} alanı en az {param} karakterli olmalıdır.');
		        $this->form_validation->set_message('max_length', '{field} alanı en fazla {param} karakterli olmalıdır.');
		        $this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');
		        $this->form_validation->set_message('matches', '{field} alanı {param} alanı ile eşleşmiyor.');
		        $this->form_validation->set_message('valid_email', '{field} alanı geçerli bir e-posta adresi içermelidir.');
		        $this->form_validation->set_message('is_unique', '{field} başka bir kullanıcı tarafından kullanılıyor.');

				if ($this->form_validation->run() == FALSE) 
				{
					$this->load->view('frontend/user/auth/register_view');
				}
				else
				{
					$data = array(
		                'firstname' => $post['firstname'],
		                'lastname' => $post['lastname'],
		                'password' => password_hash($post['password'], PASSWORD_DEFAULT, ['cost' => 12]),
		                'email' => $post['email'],
		                'verified' => 0,
		                'verification_token' => md5(md5(uniqid(mt_rand(), true)) . $post['email'] . date("mdY_His")),
		                'remember_token' => md5(md5(uniqid(mt_rand(), true)) . $post['email'] . date("mdY_His")),
		                'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
		                'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
		            );
		            
		            $user_id = $this->user->save($data);

			        if ($user_id)
			        {
			        	$tos = array($data['email']);
			            $ccs = array();
			            $bccs = array();
			            $link = base_url() . 'zindhu-hr/kullanici/giris?token='.$data['verification_token'].'&key='.$user_id;
			            $link_text = 'Doğrula';

			            $mail_content = 
			                'Merhaba <b>'. $data['firstname'] . ' ' . $data['lastname'] .' ,</b><br>
			                Kaydınız oluşturulmuştur.<br />
			                Aktifleştirmek için onayınız gerekmektedir.<br />
			                Lütfen aşağıdaki butona tıklayarak kaydınızı onaylayınız.';

			            $html_message = $this->mail_template($mail_content, $link, $link_text);

			            $status = $this->send_email($tos, $ccs, $bccs, 'zindhu.com', $data['firstname'] . ' ' . $data['lastname'] .' kayıt için son bir işlem kaldı', $html_message);

			        	$this->session->set_flashdata('create_status', array(
							'status' => true, 'message' => 'Tebrikler, üyeliğiniz başarıyla alındı. Hesabınızı doğrulamak için kayıtlı e-posta adresinize üyelik onayı gönderildi. Spam klasörünüzü kontrol etmeyi unutmayın!')
						);
			        } 
			        else 
			        {
			        	$this->session->set_flashdata('create_status', array(
							'status' => false, 'message' => 'Kayıt işleminiz başarısız oldu. Lütfen tekrar deneyiniz.')
						);
			        }

			        redirect(base_url('zindhu-hr/kullanici/giris'));
				}
			}
			else
			{
				$this->load->view('frontend/user/auth/register_view');
			}
		}
		else
		{
			redirect(base_url('zindhu-hr/kullanici/dashboard'));
		}
	}

	public function logout()
	{
		$this->session->unset_userdata(array('UserID', 'UserEmail', 'UserFullname', 'UserFirstname', 'UserPosition', 'UserCvStatus', 'UserStatus', 'UserAdminConfirm', 'UserFirstLogin'));

		redirect(base_url('zindhu-hr/kullanici/giris'));
	}

	public function forgot()
	{
		$this->session->set_flashdata('forgot_status', NULL);

		if (!$this->session->userdata('UserID'))
		{
			$post = $this->input->post(null, true);

			if ($post)
			{
				$this->load->library('form_validation');
				$this->form_validation->set_rules('email', 'E-Posta', 'trim|required|valid_email');
				$this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');
				$this->form_validation->set_message('valid_email', '{field} alanı geçerli bir e-posta adresi içermelidir.');

				if ($this->form_validation->run() == FALSE) 
				{
					$this->load->view('frontend/user/auth/forgot_view');
				}
				else
				{
					$user = $this->user->getByEmail($post['email']);

					if ($user)
					{   
						$tos = array($user->email);
		                $ccs = array();
		                $bccs = array();
		                $link = base_url() . 'zindhu-hr/kullanici/sifre-yenile?token='.$user->remember_token.'&key='.$user->id;
		                $link_text = 'Şifreyi Değiştir';

		                $mail_content = 
		                    'Merhaba <b>'. $user->firstname . ' ' . $user->lastname .' ,</b><br />
		                    Şifrenizi değiştirmek için doğrulanma yapılmıştır.<br />
		                    Aşağıdaki butona tıklayıp şifreyi değiştirebilirsiniz.';

		                $html_message = $this->mail_template($mail_content, $link, $link_text);
		                
		                $status = $this->send_email($tos, $ccs, $bccs, 'zindhu.com', 'Kullanıcı Şifre Değiştirme', $html_message);

						$this->session->unset_tempdata(array('RememberUserEmail', 'RememberUserPassword'));
						$this->session->set_flashdata('forgot_status', array(
							'status' => true, 'message' => 'E-Posta adresinize şifrenizi sıfırlamak için bağlantı gönderdik. Maile girip şifrenizi sıfırlayabilirsiniz.')
						);

				        redirect(base_url('zindhu-hr/kullanici/giris'));
					}
					else
					{
						$this->session->set_flashdata('forgot_status', array(
							'status' => false, 'message' => 'Bu e-postaya kayıtlı kimse yok.')
						);

						$this->load->view('frontend/user/auth/forgot_view');
					}
				}
			}
			else
			{	
				$this->load->view('frontend/user/auth/forgot_view');
			}
		}
		else
		{
			redirect(base_url('zindhu-hr/kullanici/dashboard'));
		}
	}

	public function reset()
	{
		$get = $this->input->get(null, true);
		$remember_token = isset($get['token']) ? $get['token'] : NULL;
		$user_id = isset($get['key']) ? $get['key'] : NULL;

		if (isset($remember_token) && isset($user_id)) 
		{
			$post = $this->input->post(null, true);

			if ($post)
			{
				$this->load->library('form_validation');

				$this->form_validation->set_rules('password', 'Şifre', 'trim|required|min_length[6]');
				$this->form_validation->set_rules('password_again', 'Şifre Tekrar', 'trim|matches[password]');

				$this->form_validation->set_message('min_length', '{field} alanı en az {param} karakterli olmalıdır.');
				$this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');
				$this->form_validation->set_message('matches', '{field} alanı {param} alanı ile eşleşmiyor.');

				if ($this->form_validation->run() == FALSE) 
				{	
					$this->load->view('frontend/user/auth/reset_view');
				}
				else
				{
					$data = array(
		                'password' => password_hash($post['password'], PASSWORD_DEFAULT, ['cost' => 12]),
		                'remember_date' => date('Y-m-d H:i:s', NOW_DATE_TIME),
		                'remember_token' => md5(md5(uniqid(mt_rand(), true)) . $post['password'] . date("mdY_His")),
		            );

					$result = $this->user->passwordUpdate($data, $remember_token, $user_id);
					if ($result > 0)
					{
						$this->session->unset_tempdata(array('RememberUserEmail', 'RememberUserPassword'));
						$this->session->set_flashdata('reset_status', array(
							'status' => true, 'message' => 'Şifreniz başarılı bir şekilde değiştirilmiştir. Giriş yapabilirsiniz.')
						);
					}
					else
					{
						$this->session->set_flashdata('reset_status', array(
							'status' => false, 'message' => 'Şifre değiştirme işleminde sorun oldu. Lütfen tekrar istek gönderiniz.')
						);
					}

					redirect(base_url('zindhu-hr/kullanici/giris'));
				}
			}
			else
			{
				$this->load->view('frontend/user/auth/reset_view');
			}
		}
		else
		{
			redirect(base_url('zindhu-hr/kullanici/giris'));
		}
	}

	public function pending()
	{
		$user_id = $this->session->userdata('UserID');
		$user = $this->user->getByID($user_id);
		$this->session->unset_userdata(array('UserAdminConfirm'));
		$this->session->set_userdata('UserAdminConfirm', $user->admin_confirm);

		if ($this->session->userdata('UserAdminConfirm') == 'pending') {
			$this->load->view('frontend/user/auth/pending_view');
		} else {
			redirect(base_url('zindhu-hr/kullanici/giris'));
		}
	}

	public function rejected()
	{
		$user_id = $this->session->userdata('UserID');
		$user = $this->user->getByID($user_id);
		$this->session->unset_userdata(array('UserAdminConfirm'));
		$this->session->set_userdata('UserAdminConfirm', $user->admin_confirm);
		
		if ($this->session->userdata('UserAdminConfirm') == 'rejected') {
			$this->load->view('frontend/user/auth/rejected_view');
		} else {
			redirect(base_url('zindhu-hr/kullanici/giris'));
		}
	}

}



?>