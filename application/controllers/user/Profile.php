<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('UserID')) {
			redirect(base_url('zindhu-hr/kullanici/giris')); exit();
		} else {
			if ($this->session->userdata('UserStatus') == 0) {
				redirect(base_url('zindhu-hr/kullanici/cv')); exit();
			} elseif ($this->session->userdata('UserAdminConfirm') == 'pending') {
				redirect(base_url('zindhu-hr/kullanici/onay-bekliyor')); exit();
			} elseif ($this->session->userdata('UserAdminConfirm') == 'rejected') {
				redirect(base_url('zindhu-hr/kullanici/reddedildi')); exit();
			}
		}
	}

	public function index()
	{
		$this->load->view('frontend/user/profile_view');
	}
}

?>