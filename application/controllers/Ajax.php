<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		redirect(base_url());
	}

	public function company()
	{
		if (!$this->session->userdata('UserID')) {
			echo json_encode(array('status' => FALSE)); exit();
		}

		$this->load->model('company/company_model', 'company');
		$data = $this->company->getAll();

		echo json_encode($data);
	}

	public function city()
	{
		$this->load->model('db/city_model', 'city');
		$data = $this->city->getAll();

		echo json_encode($data);
	}

	public function city_by_id($id)
	{
		$this->load->model('db/city_model', 'city');
		$data = $this->city->getByID($id);

		echo json_encode($data);
	}

	public function city_by_ids()
	{
		$this->load->model('db/city_model', 'city');
		$data = $this->city->getAllByIDs($this->input->get('json'));

		echo json_encode($data);
	}

	public function district($city_id)
	{
		$this->load->model('db/district_model', 'district');
		$data = $this->district->getAllByCityID($city_id);

		echo json_encode($data);
	}

	public function district_by_id($id)
	{
		$this->load->model('db/district_model', 'district');
		$data = $this->district->getByID($id);

		echo json_encode($data);
	}

	public function position()
	{
		$this->load->model('db/position_model', 'position');
		$data = $this->position->getAll();

		echo json_encode($data);
	}

	public function position_by_id($id)
	{
		$this->load->model('db/position_model', 'position');
		$data = $this->position->getByID($id);

		echo json_encode($data);
	}

	public function game()
	{
		$this->load->model('db/game_model', 'game');
		$data = $this->game->getAll();

		echo json_encode($data);
	}

	public function game_by_id($id)
	{
		$this->load->model('db/game_model', 'game');
		$data = $this->game->getByID($id);

		echo json_encode($data);
	}

	public function game_by_ids()
	{
		$this->load->model('db/game_model', 'game');
		$data = $this->game->getAllByIDs($this->input->get('json'));

		echo json_encode($data);
	}

	public function program()
	{
		$this->load->model('db/program_model', 'program');
		$data = $this->program->getAll();

		echo json_encode($data);
	}

	public function program_by_id($id)
	{
		$this->load->model('db/program_model', 'program');
		$data = $this->program->getByID($id);

		echo json_encode($data);
	}

	public function university()
	{
		$this->load->model('db/university_model', 'university');
		$data = $this->university->getAll();

		echo json_encode($data);
	}

	public function university_by_id($id)
	{
		$this->load->model('db/university_model', 'university');
		$data = $this->university->getByID($id);

		echo json_encode($data);
	}

	public function education_level($val)
	{
		$data = education_level_render($val);

		echo $data;
	}

	public function contact_save()
	{
		// $this->load->model('contact/contact_model', 'contact');

		$this->_contact_validate();
		
		$post = $this->input->post(null,true);
		// $data = array(
		// 		'fullname' => $this->input->post('fullname'),
		// 		'company_name' => $this->input->post('company_name'),
		// 		'email' => $this->input->post('email'),
		// 		'subject' => $this->input->post('subject'),
		// 		'message' => $this->input->post('message'),
		// 		'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
		// 	);

		// $insert_id = $this->contact->save($data);

		// if ($insert_id) 
		// {
			// $data = $this->contact->get_by_id($insert_id);
			$tos = array('info@zindhugames.com');
			$ccs = array();
			$bccs = array();

			$html_message = '<p><b>Adı Soyadı</b>: ' . $post['fullname'] . '</p>';
			$html_message .= '<p><b>E-posta</b>: ' . $post['email'] . '</p>';
			$html_message .= '<p><b>Konu</b>: ' . $post['subject'] . '</p>';
			$html_message .= '<p><b>Mesaj</b>: ' . $post['message'] . '</p>';

			$status = $this->send_email($tos, $ccs, $bccs, 'Zindhu HR Web Site', 'Bize Ulaşın', $html_message);
		// }

		echo json_encode(array("status" => TRUE));
	}

	private function _contact_validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('fullname') == '')
		{
			$data['inputerror'][] = 'fullname';
			$data['error_string'][] = l('z_ad_soyad') . ' ' . l('field_cannot_be_left_blank');
			$data['status'] = FALSE;
		}

		if($this->input->post('email') == '')
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = l('z_e_posta') . ' ' . l('field_cannot_be_left_blank');
			$data['status'] = FALSE;
		}
		elseif (!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)) 
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = l('girdiginiz_e_posta_gecerli_degildir');
			$data['status'] = FALSE;
		}

		if($this->input->post('subject') == '')
		{
			$data['inputerror'][] = 'subject';
			$data['error_string'][] = l('konu') . ' ' . l('field_cannot_be_left_blank');
			$data['status'] = FALSE;
		}

		if($this->input->post('message') == '')
		{
			$data['inputerror'][] = 'message';
			$data['error_string'][] = l('mesaj') . ' ' . l('field_cannot_be_left_blank');
			$data['status'] = FALSE;
		}

		if(empty($this->input->post('is_accept')))
		{
			$data['inputerror'][] = 'is_accept';
			$data['error_string'][] = l('aydinlatma_metnini_kabul_etmeden_devam_edemezsiniz');
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function home_contact_save()
	{
		$this->_home_contact_validate();
		
		$post = $this->input->post(null,true);

		$tos = array('info@zindhugames.com');
		$ccs = array();
		$bccs = array();

		$html_message = '<p><b>Adı Soyadı</b>: ' . $post['fullname'] . '</p>';
		$html_message .= '<p><b>E-posta</b>: ' . $post['email'] . '</p>';
		$html_message .= '<p><b>Mesaj</b>: ' . $post['message'] . '</p>';

		$status = $this->send_email($tos, $ccs, $bccs, 'Zindhu Web Site', 'Bize Ulaşın', $html_message);

		echo json_encode(array("status" => TRUE));
	}

	private function _home_contact_validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('fullname') == '')
		{
			$data['inputerror'][] = 'fullname';
			$data['error_string'][] = l('z_ad_soyad') . ' ' . l('field_cannot_be_left_blank');
			$data['status'] = FALSE;
		}

		if($this->input->post('email') == '')
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = l('z_e_posta') . ' ' . l('field_cannot_be_left_blank');
			$data['status'] = FALSE;
		}
		elseif (!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)) 
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = l('girdiginiz_e_posta_gecerli_degildir');
			$data['status'] = FALSE;
		}

		if($this->input->post('message') == '')
		{
			$data['inputerror'][] = 'message';
			$data['error_string'][] = l('mesaj') . ' ' . l('field_cannot_be_left_blank');
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function sg_contact_save()
	{
		$this->_sg_contact_validate();
		
		$post = $this->input->post(null,true);

		$tos = array('info@zindhugames.com');
		$ccs = array();
		$bccs = array();

		$html_message = '<p><b>Adı Soyadı</b>: ' . $post['sg_fullname'] . '</p>';
		$html_message .= '<p><b>E-posta</b>: ' . $post['sg_email'] . '</p>';
		$html_message .= '<p><b>Oyun linkiniz</b>: ' . $post['sg_game_link'] . '</p>';
		$html_message .= '<p><b>Oyununuzu anlatın</b>: ' . $post['sg_message'] . '</p>';

		$status = $this->send_email($tos, $ccs, $bccs, 'Zindhu Web Site', 'Zindhu Games - Yayıncı arıyorum formu', $html_message);

		echo json_encode(array("status" => TRUE));
	}

	private function _sg_contact_validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('sg_fullname') == '')
		{
			$data['inputerror'][] = 'sg_fullname';
			$data['error_string'][] = l('z_ad_soyad') . ' ' . l('field_cannot_be_left_blank');
			$data['status'] = FALSE;
		}

		if($this->input->post('sg_email') == '')
		{
			$data['inputerror'][] = 'sg_email';
			$data['error_string'][] = l('z_e_posta') . ' ' . l('field_cannot_be_left_blank');
			$data['status'] = FALSE;
		}
		elseif (!filter_var($this->input->post('sg_email'), FILTER_VALIDATE_EMAIL)) 
		{
			$data['inputerror'][] = 'sg_email';
			$data['error_string'][] = l('girdiginiz_e_posta_gecerli_degildir');
			$data['status'] = FALSE;
		}

		if($this->input->post('sg_game_link') == '')
		{
			$data['inputerror'][] = 'sg_game_link';
			$data['error_string'][] = l('oyun_linkiniz') . ' ' . l('field_cannot_be_left_blank');
			$data['status'] = FALSE;
		}

		if($this->input->post('sg_message') == '')
		{
			$data['inputerror'][] = 'sg_message';
			$data['error_string'][] = l('oyununuzu_anlatin') . ' ' . l('field_cannot_be_left_blank');
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}

?>