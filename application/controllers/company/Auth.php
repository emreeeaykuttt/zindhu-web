<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('company/company_model', 'company');
	}

	public function index()
	{	
		redirect(base_url('zindhu-hr/studyo/giris'));
	}

	public function login()
	{
		$post = $this->input->post(null, true);
		$this->session->set_flashdata('account_enabled', NULL);
		$this->session->set_flashdata('login_status', NULL);

		if ($post) 
		{
			$this->load->library('form_validation');

			$this->form_validation->set_rules('email', 'E-Posta', 'trim|required|valid_email');
			$this->form_validation->set_rules('password', 'Şifre', 'trim|required|min_length[6]');

			$this->form_validation->set_message('min_length', '{field} alanı en az {param} karakterli olmalıdır.');
			$this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');
			$this->form_validation->set_message('valid_email', '{field} alanı geçerli bir e-posta adresi içermelidir.');
			
			if ($this->form_validation->run() == FALSE) 
			{
				$this->load->view('frontend/company/auth/login_view');
			}
			else
			{
				$result = $this->company->validate($post['email'], $post['password']);
				if($result)
				{	
					$this->session->unset_userdata(array('CompanyID', 'CompanyEmail', 'CompanyFullname'));
					$this->session->set_userdata('CompanyID', $result->id);
					$this->session->set_userdata('CompanyEmail', $result->email);
					$this->session->set_userdata('CompanyFullname', $result->firstname . ' ' . $result->lastname);
					$this->session->set_userdata('CompanyName', $result->company_name);


					$remember = empty($post['remember']) ? 0 : 1;
					if($remember == 1)
					{
						$this->session->set_tempdata('RememberCompanyEmail', $post['email'], 604800);
						$this->session->set_tempdata('RememberCompanyPassword', $post['password'], 604800);
					}
					else
					{
						$this->session->unset_tempdata(array('RememberCompanyEmail', 'RememberCompanyPassword'));
					}

					if ($this->session->userdata('which_page'))
					{
						redirect($this->session->userdata('which_page'));
					}
					else
					{
						redirect(base_url('zindhu-hr/studyo/dashboard'));
					}
				}
				else
				{
					$this->session->set_flashdata('login_status', array(
						'status' => false, 'message' => 'Email veya şifreyi yanlış girmiş olabilirsiniz.')
					);
					$this->load->view('frontend/company/auth/login_view');
				}
			}
		}
		else
		{
			if ($this->session->userdata('CompanyID'))
			{
				redirect(base_url('zindhu-hr/studyo/dashboard'));
			}
			else
			{
				$get = $this->input->get(null, true);
				$token = isset($get['token']) ? $get['token'] : NULL;
				$company_id = isset($get['key']) ? $get['key'] : NULL;

				if (isset($token) && isset($company_id)) 
				{
					$result = $this->company->verification($token, $company_id);
					if ($result > 0)
					{
						$this->session->set_flashdata('account_enabled', array(
							'status' => true, 'message' => 'Hesabınız başarılı bir şekilde etkinleştirilmiştir.')
						);
					}
				}

				$this->load->view('frontend/company/auth/login_view');
			}
		}
	}

	public function alpha_turkish($val){
	    if (! preg_match('/^[a-zA-ZğıüşöçİĞÜŞÖÇ![:space:]]+$/', $val)) {
	        $this->form_validation->set_message('alpha_turkish', '{field} alanına boş bırakılamaz ve harf dışında birşey girilemez.');
	        return FALSE;
	    } else {
	        return TRUE;
	    }
	}
	
	public function is_accept() {
	    if (isset($_POST['is_accept'])) return true;
	    $this->form_validation->set_message('is_accept', 'Aydınlatma Metnini onaylamadan devam edemezsiniz');
	    return false;
	}

	public function register()
	{	
		$post = $this->input->post(null, true);

		if (!$this->session->userdata('AccessID')) 
		{
			if ($post)
			{
				$username = $post['username'];
				$password = $post['password'];

				if ($username == 'zindhuco' && $password == 'zindhu*2020*co') {
					$this->session->set_userdata('AccessID', 'zindhuco-access-id');

					redirect(base_url('zindhu-hr/studyo/kayit-ol'));
				} else {
					$this->session->set_flashdata('access_status', array(
						'status' => false, 'message' => 'Bilgiler yanlış. Tekrar deneyiniz.')
					);

					$this->load->view('frontend/company/auth/access_view');
				}
			} 
			else 
			{
				$this->load->view('frontend/company/auth/access_view');
			}
		} 
		else 
		{
			if (!$this->session->userdata('CompanyID'))
			{
				if ($post)
				{
					$this->load->library('form_validation');

					$this->form_validation->set_rules('firstname', 'İsim', 'trim|required|min_length[2]|callback_alpha_turkish');
			        $this->form_validation->set_rules('lastname', 'Soyisim', 'trim|required|min_length[2]|callback_alpha_turkish');
			        $this->form_validation->set_rules('password', 'Şifre', 'trim|required|min_length[6]');
			        $this->form_validation->set_rules('password_again', 'Şifre Tekrar', 'trim|matches[password]');
			        $this->form_validation->set_rules('email', 'E-Posta', 'trim|required|valid_email|is_unique[companies.email]');
			        $this->form_validation->set_rules('phone', 'Cep Telefonu', 'trim|required|regex_match[/^[0-9]{10}$/]');
			        $this->form_validation->set_rules('company_name', 'Stüdyo İsmi', 'trim|required|min_length[2]');
			        $this->form_validation->set_rules('city_id', 'Şehir', 'trim|required');
			        $this->form_validation->set_rules('district_id', 'İlçe', 'trim|required');
			        $this->form_validation->set_rules('is_accept', '', 'callback_is_accept');

			        $this->form_validation->set_message('min_length', '{field} alanı en az {param} karakterli olmalıdır.');
			        $this->form_validation->set_message('max_length', '{field} alanı en fazla {param} karakterli olmalıdır.');
			        $this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');
			        $this->form_validation->set_message('matches', '{field} alanı {param} alanı ile eşleşmiyor.');
			        $this->form_validation->set_message('valid_email', '{field} alanı geçerli bir e-posta adresi içermelidir.');
			        $this->form_validation->set_message('is_unique', '{field} başka bir kullanıcı tarafından kullanılıyor.');
			        $this->form_validation->set_message('regex_match', '{field} alanı 10 rakamdan oluşmaktadır. Örn: 5300000000');

					if ($this->form_validation->run() == FALSE) 
					{
						$this->load->view('frontend/company/auth/register_view');
					}
					else
					{
						$data = array(
			                'firstname' => $post['firstname'],
			                'lastname' => $post['lastname'],
			                'password' => password_hash($post['password'], PASSWORD_DEFAULT, ['cost' => 12]),
			                'email' => $post['email'],
			                'phone' => $post['phone'],
			                'company_name' => $post['company_name'],
			                'partners' => $post['partners'],
			                'city_id' => $post['city_id'],
			                'district_id' => $post['district_id'],
			                'number_employees' => $post['number_employees'],
			                'game_type' => $post['game_type'],
			                'company_linkedin' => $post['company_linkedin'],
			                'founder_linkedin' => $post['founder_linkedin'],
			                'verified' => 0,
			                'verification_token' => md5(md5(uniqid(mt_rand(), true)) . $post['email'] . date("mdY_His")),
			                'remember_token' => md5(md5(uniqid(mt_rand(), true)) . $post['email'] . date("mdY_His")),
			                'communication_permit' => 1,
			                'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
			                'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
			            );
			            
			            $company_id = $this->company->save($data);

				        if ($company_id)
				        {
				        	$tos = array($data['email']);
				            $ccs = array();
				            $bccs = array();
				            $link = base_url() . 'company/auth/login?token='.$data['verification_token'].'&key='.$company_id;
				            $link_text = 'Doğrula';

				            $mail_content = 
				                'Merhaba <b>'. $data['firstname'] . ' ' . $data['lastname'] .' ,</b><br>
				                Kaydınız oluşturulmuştur.<br />
				                Aktifleştirmek için onayınız gerekmektedir.<br />
				                Lütfen aşağıdaki butona tıklayarak kaydınızı onaylayınız.';

				            $html_message = $this->mail_template($mail_content, $link, $link_text);

				            $status = $this->send_email($tos, $ccs, $bccs, 'zindhu.com', 'Stüdyo Hesap Doğrulaması', $html_message);

				        	$this->session->set_flashdata('create_status', array(
								'status' => true, 'message' => 'Kayıt işleminiz onaylanması için email adresinize gelen maildeki linke tıklayınız.')
							);

							$this->session->unset_userdata(array('AccessID'));
				        } 
				        else 
				        {
				        	$this->session->set_flashdata('create_status', array(
								'status' => false, 'message' => 'Kayıt işleminiz başarısız oldu. Lütfen tekrar deneyiniz.')
							);
				        }

				        redirect(base_url('zindhu-hr/studyo/giris'));
					}
				}
				else
				{
					$this->load->view('frontend/company/auth/register_view');
				}
			}
			else
			{
				redirect(base_url('zindhu-hr/studyo/dashboard'));
			}
		}
	}

	public function logout()
	{
		$this->session->unset_userdata(array('CompanyID', 'CompanyEmail', 'CompanyFullname', 'CompanyName'));

		redirect(base_url('zindhu-hr/studyo/giris'));
	}

	public function forgot()
	{
		$this->session->set_flashdata('forgot_status', NULL);

		if (!$this->session->userdata('CompanyID'))
		{
			$post = $this->input->post(null, true);

			if ($post)
			{
				$this->load->library('form_validation');
				$this->form_validation->set_rules('email', 'E-Posta', 'trim|required|valid_email');
				$this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');
				$this->form_validation->set_message('valid_email', '{field} alanı geçerli bir e-posta adresi içermelidir.');

				if ($this->form_validation->run() == FALSE) 
				{
					$this->load->view('frontend/company/auth/forgot_view');
				}
				else
				{
					$company = $this->company->getByEmail($post['email']);

					if ($company)
					{   
						$tos = array($company->email);
		                $ccs = array();
		                $bccs = array();
		                $link = base_url() . 'zindhu-hr/studyo/sifre-yenile?token='.$company->remember_token.'&key='.$company->id;
		                $link_text = 'Şifreyi Değiştir';

		                $mail_content = 
		                    'Merhaba <b>'. $company->firstname . ' ' . $company->lastname .' ,</b><br />
		                    Şifrenizi değiştirmek için doğrulanma yapılmıştır.<br />
		                    Aşağıdaki butona tıklayıp şifreyi değiştirebilirsiniz.';

		                $html_message = $this->mail_template($mail_content, $link, $link_text);
		                
		                $status = $this->send_email($tos, $ccs, $bccs, 'zindhu.com', 'Stüdyo Şifre Değiştirme', $html_message);

						$this->session->unset_tempdata(array('RememberCompanyEmail', 'RememberCompanyPassword'));
						$this->session->set_flashdata('forgot_status', array(
							'status' => true, 'message' => 'E-Posta adresinize şifrenizi sıfırlamak için bağlantı gönderdik. Maile girip şifrenizi sıfırlayabilirsiniz.')
						);

				        redirect(base_url('zindhu-hr/studyo/giris'));
					}
					else
					{
						$this->session->set_flashdata('forgot_status', array(
							'status' => false, 'message' => 'Bu e-postaya kayıtlı kimse yok.')
						);

						$this->load->view('frontend/company/auth/forgot_view');
					}
				}
			}
			else
			{	
				$this->load->view('frontend/company/auth/forgot_view');
			}
		}
		else
		{
			redirect(base_url('zindhu-hr/studyo/dashboard'));
		}
	}

	public function reset()
	{
		$get = $this->input->get(null, true);
		$remember_token = isset($get['token']) ? $get['token'] : NULL;
		$company_id = isset($get['key']) ? $get['key'] : NULL;

		if (isset($remember_token) && isset($company_id)) 
		{
			$post = $this->input->post(null, true);

			if ($post)
			{
				$this->load->library('form_validation');

				$this->form_validation->set_rules('password', 'Şifre', 'trim|required|min_length[6]');
				$this->form_validation->set_rules('password_again', 'Şifre Tekrar', 'trim|matches[password]');

				$this->form_validation->set_message('min_length', '{field} alanı en az {param} karakterli olmalıdır.');
				$this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');
				$this->form_validation->set_message('matches', '{field} alanı {param} alanı ile eşleşmiyor.');

				if ($this->form_validation->run() == FALSE) 
				{	
					$this->load->view('frontend/company/auth/reset_view');
				}
				else
				{
					$data = array(
		                'password' => password_hash($post['password'], PASSWORD_DEFAULT, ['cost' => 12]),
		                'remember_date' => date('Y-m-d H:i:s', NOW_DATE_TIME),
		                'remember_token' => md5(md5(uniqid(mt_rand(), true)) . $post['password'] . date("mdY_His")),
		            );

					$result = $this->company->passwordUpdate($data, $remember_token, $company_id);
					if ($result > 0)
					{
						$this->session->unset_tempdata(array('RememberCompanyEmail', 'RememberCompanyPassword'));
						$this->session->set_flashdata('reset_status', array(
							'status' => true, 'message' => 'Şifreniz başarılı bir şekilde değiştirilmiştir. Giriş yapabilirsiniz.')
						);
					}
					else
					{
						$this->session->set_flashdata('reset_status', array(
							'status' => false, 'message' => 'Şifre değiştirme işleminde sorun oldu. Lütfen tekrar istek gönderiniz.')
						);
					}

					redirect(base_url('zindhu-hr/studyo/giris'));
				}
			}
			else
			{
				$this->load->view('frontend/company/auth/reset_view');
			}
		}
		else
		{
			redirect(base_url('zindhu-hr/studyo/giris'));
		}
	}

}



?>