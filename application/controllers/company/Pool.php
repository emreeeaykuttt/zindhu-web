<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pool extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('CompanyID')) {
			redirect(base_url('zindhu-hr/studyo/giris')); exit();
		}

		$this->load->model('user/user_model','user');
	}

	public function index()
	{
		$this->load->view('frontend/company/pool_view');
	}

	public function ajax_list()
	{
		$this->load->model('company/watch_model', 'watch');
		$this->load->model('conversation/conversation_model','conversation');
		$this->load->model('company/hires_model', 'company_hires');

		$get_data = array();
		$company_id = $this->session->userdata('CompanyID');
		$get_data['company_id'] = $company_id;

		if ($this->input->get('watch') == 1) {
			$get_data['watch'] = $this->input->get('watch');
		} elseif ($this->input->get('conversation') == 1) {
			$get_data['conversation_company'] = $this->input->get('conversation');
		} elseif ($this->input->get('ad')) {
			$get_data['ad'] = $this->input->get('ad');
		}

		$get_data['position_id'] = $this->input->get('position_id');
		$get_data['city_id'] = $this->input->get('city_id');
		$get_data['city_json'] = $this->input->get('city_json');
		$get_data['education_level'] = $this->input->get('education_level');
		$get_data['university_id'] = $this->input->get('university_id');
		$get_data['program_id'] = $this->input->get('program_id');
		$get_data['program_duration'] = $this->input->get('program_duration');
		$get_data['game_future'] = $this->input->get('game_future');
		$get_data['game_past'] = $this->input->get('game_past');
		$get_data['is_remote'] = $this->input->get('is_remote');

		$our_employees = $this->company_hires->getAllByCompanyIDAndContinues($company_id);
		$this->user->cv_status = 1;
		$this->user->is_working_system = 1;
		$this->user->our_employees = $our_employees;

		$list = $this->user->getDatatables($get_data);
		$data = array();
		
		foreach ($list as $user) 
		{
			$row = array();

			// $unread_count = $this->conversation->unreadCountByCompanyIDAndUserID($company_id, $user->id, 'user');

			// if ($unread_count === FALSE) {
			// 	$conversation_icon = '';
			// 	$conversation_tooltip = '';
			// } elseif ($unread_count > 0) {
			// 	$conversation_icon = '<i class="fas fa-envelope"></i>';
			// 	$conversation_tooltip = 'data-toggle="tooltip" title="Okunmamış Mesaj Var"';
			// } else {
			// 	$conversation_icon = '<i class="fas fa-envelope-open"></i>';
			// 	$conversation_tooltip = 'data-toggle="tooltip" title="Mesaj Okunmuş"';
			// }

			// $hires = $this->company_hires->getByCompanyIDAndUserID($company_id, $user->id);
			// $work_status_color = '';
			// $work_status_text = 'Herhangi bir işlem yapılmamıştır.';
			// if ($hires) {
			// 	if ($hires->user_confirm == 1 && $hires->company_confirm == 0 && $hires->continues == 0 && !$hires->enddate) {
   //              	$work_status_color = 'blue';
			// 		$work_status_text = 'Kullanıcıdan size istek gönderildi.';
   //              } else if ($hires->user_confirm == 0 && $hires->company_confirm == 1 && $hires->continues == 0 && !$hires->enddate) {
   //              	$work_status_color = 'orange';
			// 		$work_status_text = 'Kullanıcıdan dönüş bekleniyor.';
   //              } else if ($hires->user_confirm == 1 && $hires->continues == 1 && !$hires->enddate) {
   //              	$work_status_color = 'green';
			// 		$work_status_text = 'Kullanıcı işe alındı.';
   //              } else if ($hires->user_confirm == 1 && $hires->continues == 0 && $hires->enddate) {
   //              	$work_status_color = 'red';
			// 		$work_status_text = 'Kullanıcı ' . $hires->enddate . ' tarihinde işten çıktı.';
   //              }
			// }

			// $row['firstname'] = $user->firstname;
			// $row['position'] = $user->position;
			// $row['city'] = $user->city;
			// $row['transactions'] = 
			// 	'<a href="javascript:void(0)" onclick="view('.$user->id.')" class="btn btn-success btn-sm db-btn">GÖRÜNTÜLE</a>' .
			// 	$watch_btn .
			// 	'<a href="javascript:void(0)" onclick="start_conversation('.$user->id.')" '. $conversation_tooltip .' class="btn btn-amber btn-sm db-btn">MESAJ AT '. $conversation_icon .'</a>' .
			// 	'<a href="javascript:void(0)" data-toggle="tooltip" title="'.$work_status_text.'" class="work-info-btn" style="color: '.$work_status_color.'"><i class="fas fa-info-circle fa-2x"></i>'
			// 	;

			if ($this->watch->userControl($company_id, $user->id)) {
				$watch_btn = '<a href="javascript:void(0)" onclick="remove_watch('.$user->id.')" id="watch'.$user->id.'" class="btn btn-gray following"><i class="las la-times mr-1"></i> Takibi bırak</a>';
			} else {
				$watch_btn = '<a href="javascript:void(0)" onclick="add_watch('.$user->id.')" id="watch'.$user->id.'" class="btn btn-gray"><i class="icon-telescope-icon mr-1"></i> Takip et</a>';
			}

			$is_message = $this->conversation->getLastMessage($company_id, $user->id, 'company');

			$row['transactions'] = 
				'<div class="conversation-item">' .
					'<div class="first-letter align-self-start">'. mb_substr($user->firstname, 0, 1) .'</div>' .
					'<div class="info w-100">' .
						'<div class="from">'. 
							$user->firstname . ' ' . mb_substr($user->lastname, 0, 1) . '.' .
							'<span class="position">'. $user->position .'</span>' .
						'</div>' .
						'<div class="location"><i class="la la-map-marker"></i>'. $user->city .'</div>' .
						'<div class="d-lg-flex w-100 justify-content-between align-items-center my-1">' .
							'<div><a href="javascript:void(0)" onclick="view('.$user->id.')" class="blue-text"><u>Profili görüntüle</u></a></div>' .
							'<div>' .
								$watch_btn .
								'<a href="/zindhu-hr/studyo/konusmalar?user='.$user->id.'" target="_blank" class="btn btn-dark ml-2 '. ($is_message ? 'active' : '') .'">'.
									'<i class="icon-conversations-icon mr-1"></i> Mesaj gönder'.
								'</a>' .
							'</div>' .
						'</div>' .
					'</div>' .
				'</div>';
		
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->user->getCountAll($get_data),
			"recordsFiltered" => $this->user->getCountFiltered($get_data),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
}

?>