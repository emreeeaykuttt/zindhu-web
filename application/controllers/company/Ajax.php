<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('CompanyID') && !$this->session->userdata('AdminID')) {
			redirect(base_url('zindhu-hr/studyo/giris')); exit();
		}
	}

	public function index()
	{
		redirect(base_url());
	}

	public function all_statistics()
	{
		$this->load->model('company/watch_model', 'watch');
		$this->load->model('conversation/conversation_model','conversation');
		$this->load->model('user/user_model', 'user');
		$this->load->model('company/hires_model', 'company_hires');

		$company_id = $this->session->userdata('CompanyID');
		$our_employees = $this->company_hires->getAllByCompanyIDAndContinues($company_id);
		$this->user->cv_status = 1;
		$this->user->is_working_system = 1;
		$this->user->our_employees = $our_employees;
		$this->watch->cv_status = 1;
		$this->watch->is_working_system = 1;
		$this->watch->our_employees = $our_employees;
		$this->conversation->cv_status = 1;
		$this->conversation->is_working_system = 1;
		$this->conversation->our_employees = $our_employees;

		$data['watch_count'] = $this->watch->getCountByCompanyID($company_id);
		$data['conversation_count'] = $this->conversation->getCountByCompanyID($company_id);
		$data['user_count'] = $this->user->getCount();

		echo json_encode($data);
	}

	public function unread_conversations()
	{
		$this->load->model('conversation/conversation_model','conversation');

		$company_id = $this->session->userdata('CompanyID');
	
		$data = $this->conversation->unreadCountByCompanyID($company_id);

		echo json_encode($data);
	}

	public function start_conversation($user_id)
	{
		$this->load->model('user/user_model','user');
		$this->load->model('cv/personal_information_model', 'personal_information');
		$this->load->model('conversation/conversation_model','conversation');
		$this->load->model('company/hires_model', 'company_hires');

		$company_id = $this->session->userdata('CompanyID');

		$data['user'] = $this->user->getByID($user_id);
		$data['personal_information'] = $this->personal_information->getAllByUserID($user_id);
		$data['conversations'] = $this->conversation->getAllByCompanyIDAndUserID($company_id, $user_id);
		$data['company_hires'] = $this->company_hires->getByCompanyIDAndUserID($company_id, $user_id);
		$this->conversation->update(array('company_id' => $company_id, 'user_id' => $user_id, 'sender_type' => 'user'), array('is_read' => 1));

		echo json_encode($data);
	}

	public function send_message()
	{
		$this->load->model('conversation/conversation_model','conversation');

		$post = $this->input->post(null,true);
		$company_id = $this->session->userdata('CompanyID');
		$user_id = $post['user_id'];
		$content = $post['message'];

		$data = array(
			'sender_type' => 'company',
			'is_read' => 0,
			'company_id' => $company_id,
			'user_id' => $user_id,
			'content' => $content,
			'ip_address' => $this->input->ip_address(),
			'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if (!empty($content)) {
			if (!empty($company_id) && !empty($user_id)) {
				
				$is_send_mail = false;
				$row = $this->conversation->getLastMessage($company_id, $user_id, 'company');
				$this->conversation->save($data);

				if (!empty($row)) {
					$time_start = strtotime($row->created_at);
					$time_end = strtotime(date('Y-m-d H:i:s', NOW_DATE_TIME));
					$interval = $time_end - $time_start;
					$hour = floor($interval / (60 * 60));

					if ($hour >= 1) {
						$is_send_mail = true;
					}
				} else {
					$is_send_mail = true;
				}
				
				if ($is_send_mail) {
					$this->load->model('company/company_model', 'company');
					$this->load->model('user/user_model', 'user');

					$company_row = $this->company->getByID($company_id);
					$user_row = $this->user->getByID($user_id);

					$tos = array($user_row->email);
		            $ccs = array();
		            $bccs = array();
		            $link = base_url() . 'zindhu-hr/kullanici/konusmalar?company=' . $company_id;
		            $link_text = 'Mesajı Görüntüle';

		            $mail_content = 
		                'Merhaba <b>'. $user_row->firstname . ' ' . $user_row->lastname .',</b><br><br>
		                1 yeni mesajınız var.<br><br>
		                Mesajı Gönderen Stüdyo: ' . $company_row->company_name;

		            $html_message = $this->mail_template($mail_content, $link, $link_text);

		            $status = $this->send_email($tos, $ccs, $bccs, 'zindhu.com', $company_row->company_name .' sana yeni bir mesaj gönderdi', $html_message);
				}

				echo json_encode(array('status' => TRUE, 'message' => 'Mesaj gönderildi.', 'record' => $data)); exit();
			} else {
				echo json_encode(array('status' => FALSE, 'message' => 'Mesaj gönderilmedi.')); exit();
			}
		} else {
			echo json_encode(array('status' => FALSE, 'message' => 'Mesaj içeriği girmeden mesaj gönderemezsiniz.')); exit();
		}
	}

	public function user_invite()
	{
		$this->load->model('company/hires_model', 'company_hires');
		$this->load->model('conversation/conversation_model','conversation');

		$post = $this->input->post(null,true);
		$company_id = $this->session->userdata('CompanyID');
		$user_id = $post['user_id'];

		$data = array(
			'company_id' => $company_id,
			'user_id' => $user_id,
			'company_confirm' => 1,
			'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if (!empty($company_id) && !empty($user_id)) {
			$is_save = FALSE;
			$company_hires = $this->company_hires->getByCompanyIDAndUserID($company_id, $user_id);
			if ($company_hires) {
				if ($company_hires->enddate) {
					$is_save = TRUE;
				}
			} else {
				$is_save = TRUE;
			}

			if ($is_save) {
				$this->company_hires->save($data);

				$message = array(
					'sender_type' => 'company',
					'is_read' => 0,
					'company_id' => $company_id,
					'user_id' => $user_id,
					'is_system_message' => 1,
					'content' => 'Bizimle çalışır mısın? Eğer kabul ediyorsanız yukarıdaki "EVET, KABUL EDİYORUM" butonuna tıklar mısınız.',
					'ip_address' => $this->input->ip_address(),
					'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
				);

				$this->conversation->save($message);

				echo json_encode(array('status' => TRUE, 'message' => 'İstek gönderildi.', 'record' => $data));
			} else {
				echo json_encode(array('status' => FALSE, 'message' => 'İstek zaten gönderilmiş.'));
			}
		} else {
			echo json_encode(array('status' => FALSE, 'message' => 'İstek gönderilmedi.'));
		}
	}

	public function user_accept_invite()
	{
		$this->load->model('company/hires_model', 'company_hires');
		$this->load->model('conversation/conversation_model','conversation');
		$this->load->model('user/user_model', 'user');

		$post = $this->input->post(null,true);
		$company_id = $this->session->userdata('CompanyID');
		$user_id = $post['user_id'];

		$data = array(
			'company_confirm' => 1,
			'continues' => 1,
			'startdate' => date('Y-m-d H:i:s', NOW_DATE_TIME),
			'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if (!empty($company_id) && !empty($user_id)) {
			$is_update = FALSE;
			$company_hires = $this->company_hires->getByCompanyIDAndUserID($company_id, $user_id);
			if ($company_hires) {
				if ($company_hires->company_confirm == 0) {
					$is_update = TRUE;
				}
			}

			if ($is_update) {
				$this->company_hires->update(
					array('company_id' => $company_id, 'user_id' => $user_id, 'company_confirm' => 0), 
					$data
				);

				$message = array(
					'sender_type' => 'company',
					'is_read' => 0,
					'company_id' => $company_id,
					'user_id' => $user_id,
					'is_system_message' => 1,
					'content' => 'İşe girme isteğinizi kabul ettik.',
					'ip_address' => $this->input->ip_address(),
					'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
				);

				$this->conversation->save($message);

				$user_info = $this->user->getByID($user_id);

				if (!empty($user_info->company_ids)) {
					$company_arr = json_decode($user_info->company_ids, true);
					$company_arr[] = $company_id;
					$company_ids = json_encode($company_arr);
				} else {
					$company_ids = json_encode(array($company_id));
				}

				$this->user->update(array('id' => $user_id), array('company_ids' => $company_ids, 'company_text' => NULL, 'is_working' => 1));

				echo json_encode(array('status' => TRUE, 'message' => 'İşe alındı.', 'record' => $data));
			} else {
				echo json_encode(array('status' => FALSE, 'message' => 'Böyle bir kayıt yoktur.'));
			}
		} else {
			echo json_encode(array('status' => FALSE, 'message' => 'Böyle bir kayıt yoktur.'));
		}
	}

	public function user_exit()
	{
		$this->load->model('company/hires_model', 'company_hires');
		$this->load->model('conversation/conversation_model','conversation');
		$this->load->model('user/user_model', 'user');

		$post = $this->input->post(null,true);
		$company_id = $this->session->userdata('CompanyID');
		$user_id = $post['user_id'];

		$data = array(
			'continues' => 0,
			'enddate' => date('Y-m-d H:i:s', NOW_DATE_TIME),
			'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if (!empty($company_id) && !empty($user_id)) {
			$is_update = FALSE;
			$company_hires = $this->company_hires->getByCompanyIDAndUserID($company_id, $user_id);
			if ($company_hires) {
				if (!$company_hires->enddate && $company_hires->continues == 1) {
					$is_update = TRUE;
				}
			}

			if ($is_update) {
				$this->company_hires->update(
					array('company_id' => $company_id, 'user_id' => $user_id, 'enddate' => NULL, 'continues' => 1), 
					$data
				);

				$message = array(
					'sender_type' => 'company',
					'is_read' => 0,
					'company_id' => $company_id,
					'user_id' => $user_id,
					'is_system_message' => 1,
					'content' => 'İşten çıkarıldınız.',
					'ip_address' => $this->input->ip_address(),
					'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
				);

				$this->conversation->save($message);

				$user_info = $this->user->getByID($user_id);

				if (!empty($user_info->company_ids)) {
					$company_arr = json_decode($user_info->company_ids, true);
					if (($key = array_search($company_id, $company_arr)) !== false) {
					    unset($company_arr[$key]);
					}
					if (empty($company_arr)) {
						$company_ids = NULL;
						$is_working = 0;
					} else {
						$company_ids = json_encode($company_arr);
						$is_working = 1;
					}
				} else {
					$company_ids = NULL;
					$is_working = 0;
				}

				$this->user->update(array('id' => $user_id), array('company_ids' => $company_ids, 'company_text' => NULL, 'is_working' => $is_working));

				echo json_encode(array('status' => TRUE, 'message' => 'İşten çıkardınız.', 'record' => $data));
			} else {
				echo json_encode(array('status' => FALSE, 'message' => 'Böyle bir kayıt yoktur.'));
			}
		} else {
			echo json_encode(array('status' => FALSE, 'message' => 'Böyle bir kayıt yoktur.'));
		}
	}

	public function cv_view($user_id)
	{
		$this->load->model('user/user_model','user');
		$this->load->model('cv/personal_information_model', 'personal_information');
		$this->load->model('cv/educational_background_model', 'educational_background');
		$this->load->model('cv/work_experience_model', 'work_experience');
		$this->load->model('cv/used_program_model', 'used_program');
		$this->load->model('cv/game_choice_model', 'game_choice');
		$this->load->model('cv/portfolio_model', 'portfolio');
		$this->load->model('company/watch_model', 'watch');
		$this->load->model('company/views_model', 'company_views');
		$this->load->model('company/hires_model', 'company_hires');

		$company_id = $this->session->userdata('CompanyID');

		$is_record = $this->company_views->userControl($company_id, $user_id);
		if (!$is_record) {
		 	$this->company_views->save(
				array('company_id' => $company_id, 'user_id' => $user_id, 'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME))
			);
		} 

		$data['user'] = $this->user->getByID($user_id);
		$data['personal_information'] = $this->personal_information->getAllByUserID($user_id);
		$data['educational_background'] = $this->educational_background->getAllByUserID($user_id);
		$data['work_experience'] = $this->work_experience->getAllByUserID($user_id);
		$data['used_program'] = $this->used_program->getAllByUserID($user_id);
		$data['game_choice'] = $this->game_choice->getAllByUserID($user_id);
		$data['portfolio'] = $this->portfolio->getAllByUserID($user_id);
		$data['watch'] = $this->watch->userControl($company_id, $user_id);
		$data['company_hires'] = $this->company_hires->getByCompanyIDAndUserID($company_id, $user_id);

		echo json_encode($data);
	}

	public function add_watch()
	{
		$this->load->model('company/watch_model', 'watch');

		$post = $this->input->post(null,true);
		$company_id = $this->session->userdata('CompanyID');
		$user_id = $post['user_id'];

		$data = array(
			'company_id' => $company_id,
			'user_id' => $user_id,
			'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		$user_control = $this->watch->userControl($company_id, $user_id);

		if ($user_control) {
			echo json_encode(array('status' => FALSE, 'message' => 'Kişi zaten takip ediliyor.'));
		} else {
			$this->watch->save($data);
			echo json_encode(array('status' => TRUE, 'message' => 'Kişi takip edildi.'));
		}
	}

	public function remove_watch()
	{
		$this->load->model('company/watch_model', 'watch');

		$post = $this->input->post(null,true);
		$company_id = $this->session->userdata('CompanyID');
		$user_id = $post['user_id'];

		$data = array(
			'company_id' => $company_id,
			'user_id' => $user_id,
			'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		$user_control = $this->watch->userControl($company_id, $user_id);

		if (!$user_control) {
			echo json_encode(array('status' => FALSE, 'message' => 'Kişi zaten takip edilmiyor.'));
		} else {
			$this->watch->deleteByCompanyIDAndUserID($company_id, $user_id);
			echo json_encode(array('status' => TRUE, 'message' => 'Kişi takipten çıkarıldı.'));
		}
	}

	public function control_conversation($user_id)
	{
		$this->load->model('conversation/conversation_model','conversation');

		$company_id = $this->session->userdata('CompanyID');
		
		$data['messages'] = $this->conversation->getUnreadMessages($company_id, $user_id, 'user');
		$this->conversation->update(array('user_id' => $user_id, 'company_id' => $company_id, 'sender_type' => 'user'), array('is_read' => 1));

		echo json_encode($data);
	}
}

?>