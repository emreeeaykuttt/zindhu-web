<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conversations extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('CompanyID')) {
			redirect(base_url('zindhu-hr/studyo/giris')); exit();
		}

		$this->load->model('user/user_model','user');
	}

	public function index()
	{
		$this->load->view('frontend/company/conversations_view');
	}

	public function ajax_list()
	{
		$this->load->model('company/watch_model', 'watch');
		$this->load->model('conversation/conversation_model','conversation');
		$this->load->model('company/hires_model', 'company_hires');

		$get_data = array();
		$company_id = $this->session->userdata('CompanyID');
		$get_data['company_id'] = $company_id;

		$get_data['conversation_company'] = 1;
		if ($this->input->get('unread') == 1) {
			$get_data['unread'] = $this->input->get('unread');
		}

		$our_employees = $this->company_hires->getAllByCompanyIDAndContinues($company_id);
		$this->user->cv_status = 1;
		$this->user->is_working_system = 1;
		$this->user->our_employees = $our_employees;

		$list = $this->user->getDatatables($get_data);
		$data = array();
		
		foreach ($list as $user) 
		{
			$row = array();

			$last_message = $this->conversation->getLastMessage($company_id, $user->id, 'user');

			$unread_count = $this->conversation->unreadCountByCompanyIDAndUserID($company_id, $user->id, 'user');

			if ($unread_count > 0) {
				$conversation_icon = '<div class="unread"></div>';
			} else {
				$conversation_icon = '';
			}

			$row['transactions'] = 
				'<div class="conversation-item" onclick="start_conversation('.$user->id.')">' .
					'<div class="first-letter">'. substr($user->firstname, 0, 1) .'</div>' .
					'<div class="info">' .
						'<div class="from">'. $user->firstname . ' ' . mb_substr($user->lastname, 0, 1) .'.</div>' .
						'<div class="subject">'. ($last_message ? text_limitation($last_message->content, 50) : '') .'</div>' .
					'</div>' .
					'<div class="status">' .
						'<div class="time">'. ($last_message ? time_render($last_message->created_at) : '') .'</div>' .
						$conversation_icon .
					'</div>' .
				'</div>';
		
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->user->getCountAll($get_data),
			"recordsFiltered" => $this->user->getCountFiltered($get_data),
			"data" => $data,
		);
		// ignore 
		echo json_encode($output, JSON_INVALID_UTF8_IGNORE);
	}
}

?>