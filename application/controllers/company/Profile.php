<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('CompanyID')) {
			redirect(base_url('zindhu-hr/studyo/giris')); exit();
		}
	}

	public function index()
	{
		$this->load->model('company/company_model', 'company');

		$company_id = $this->session->userdata('CompanyID');
		$data['profile'] = $this->company->getByID($company_id);
		$post = $this->input->post(null, true);

		if ($post)
		{
			$this->load->library('form_validation');

			if($post['email'] != $data['profile']->email) {
			   $is_unique = '|is_unique[companies.email]';
			} else {
			   $is_unique = '';
			}

			$this->form_validation->set_rules('firstname', 'İsim', 'trim|required|min_length[2]|callback_alpha_turkish');
	        $this->form_validation->set_rules('lastname', 'Soyisim', 'trim|required|min_length[2]|callback_alpha_turkish');
	        $this->form_validation->set_rules('email', 'E-Posta', 'trim|required|valid_email' . $is_unique);
	        $this->form_validation->set_rules('phone', 'Cep Telefonu', 'trim|required|regex_match[/^[0-9]{10}$/]');
	        $this->form_validation->set_rules('company_name', 'Stüdyo İsmi', 'trim|required|min_length[2]');
	        $this->form_validation->set_rules('city_id', 'Şehir', 'trim|required');
	        $this->form_validation->set_rules('district_id', 'İlçe', 'trim|required');

	        $this->form_validation->set_message('min_length', '{field} alanı en az {param} karakterli olmalıdır.');
	        $this->form_validation->set_message('max_length', '{field} alanı en fazla {param} karakterli olmalıdır.');
	        $this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');
	        $this->form_validation->set_message('valid_email', '{field} alanı geçerli bir e-posta adresi içermelidir.');
	        $this->form_validation->set_message('is_unique', '{field} başka bir kullanıcı tarafından kullanılıyor.');
	        $this->form_validation->set_message('regex_match', '{field} alanı 10 rakamdan oluşmaktadır. Örn: 5300000000');

			if ($this->form_validation->run() == FALSE) 
			{
				$data['profile_status']['key'] = FALSE;
	            $data['profile_status']['message'] = 'Profil güncellenirken bir sorun oluştu. Formu kontrol ediniz.';
			} 
			else 
			{
				$profile_data = array(
	                'firstname' => $post['firstname'],
	                'lastname' => $post['lastname'],
	                'email' => $post['email'],
	                'phone' => $post['phone'],
	                'company_name' => $post['company_name'],
	                'partners' => $post['partners'],
	                'city_id' => $post['city_id'],
	                'district_id' => $post['district_id'],
	                'number_employees' => $post['number_employees'],
	                'game_type' => $post['game_type'],
	                'company_linkedin' => $post['company_linkedin'],
	                'founder_linkedin' => $post['founder_linkedin'],
	                'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
	            );
	            
	            $this->company->update(array('id' => $company_id), $profile_data);

	            $data['profile_status']['key'] = TRUE;
	            $data['profile_status']['message'] = 'Profil güncellendi.';
			}
		}

		$this->load->view('frontend/company/profile_view', $data);
	}

	public function alpha_turkish($val){
	    if (! preg_match('/^[a-zA-ZğıüşöçİĞÜŞÖÇ![:space:]]+$/', $val)) {
	        $this->form_validation->set_message('alpha_turkish', '{field} alanına boş bırakılamaz ve harf dışında birşey girilemez.');
	        return FALSE;
	    } else {
	        return TRUE;
	    }
	}
}

?>