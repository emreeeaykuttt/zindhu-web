<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('CompanyID')) {
			redirect(base_url('zindhu-hr/studyo/giris')); exit();
		}
	}

	public function index()
	{
		$this->load->view('frontend/company/dashboard_view');
	}
}

?>