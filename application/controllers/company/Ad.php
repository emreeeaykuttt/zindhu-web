<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ad extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('CompanyID')) {
			redirect(base_url('zindhu-hr/studyo/giris')); exit();
		}

		$this->load->model('ad/ad_model','ad');
	}

	public function index()
	{
		$this->load->view('frontend/company/ad_view');
	}

	public function ajax_list()
	{
		$company_id = $this->session->userdata('CompanyID');
		$this->ad->company_id = $company_id;

		$list = $this->ad->getDatatables();
		$data = array();

		foreach ($list as $ad) 
		{
			$row = array();

			$row['title'] = '<div class="position"> <div class="icon"> <i class="icon-zindhu-icon"></i></div> <div class="title">'.$ad->title.'<br><span class="date">'.$ad->created_date.'</span></div> </div>';
			$row['status'] = $ad->status;
			$row['admin_confirm'] = $ad->admin_confirm;
			$row['transactions'] = 
				'<div class="actions">'.
				'<a href="cv-havuzu?ad='.$ad->id.'" class="btn btn-applied">Başvuranlar</a>'.
				'<a href="javascript:void(0)" onclick="edit('.$ad->id.')" class="btn btn-edit"><i class="la la-edit"></i></a>'.
				'<a href="javascript:void(0)" onclick="remove('.$ad->id.')" class="btn btn-remove"><i class="la la-remove"></i></a>'. 
				'</div>'

			;
		
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->ad->getCountAll(),
			"recordsFiltered" => $this->ad->getCountFiltered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}

	public function ajax_view($id)
	{
		$data = $this->ad->getByID($id);

		echo json_encode($data);
	}

	public function ajax_save()
	{
		$this->load->library('file_upload');

		$post = $this->input->post(null,true);
		$company_id = $this->session->userdata('CompanyID');
		
		$this->_validate();

		$data = array(
			'company_id' => $company_id,
	        'title' => $post['title'],
	        'slug' => seo_url($post['title']),
	        'study_type' => $post['study_type'],
	        'short_description' => $this->input->post('short_description'),
	        'job_description' => $this->input->post('job_description'),
	        'status' => isset($post['status']) ? 1 : 0,
	        'position_json' => json_encode($post['position_json']),
	        'city_text' => $post['city_text'],
	        'district_id' => isset($post['district_id']) ? $post['district_id'] : NULL,
	        'city_id' => $post['city_id'],
	        'is_remote' => isset($post['is_remote']) ? 1 : 0,
	    );

	    if (empty($post['ad_id']) || $post['ad_id'] == 0) 
	    {
	    	$data['created_at'] = date('Y-m-d H:i:s', NOW_DATE_TIME);
			$id = $this->ad->save($data);

			echo json_encode(array('status' => TRUE, 'message' => 'İlan Oluşturuldu', 'id' => $id));
		} 
		else 
		{
			$data['updated_at'] = date('Y-m-d H:i:s', NOW_DATE_TIME);
			$this->ad->update(array('id' => $post['ad_id']), $data);

			echo json_encode(array('status' => TRUE, 'message' => 'İlan Güncellendi', 'id' => 0));
		}
	}

	public function ajax_remove($id)
	{
		$this->ad->deleteByID($id);

		echo json_encode(array('status' => TRUE, 'message' => 'İlan Silindi'));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('title') == '')
		{
			$data['inputerror'][] = 'title';
			$data['error_string'][] = 'Başlık alanı boş bırakılmaz';
			$data['status'] = FALSE;
		}

		if($this->input->post('position_json') == '0' || empty($this->input->post('position_json')))
	    {
	        $data['inputerror'][] = 'position_json';
	        $data['error_string'][] = 'Pozisyon seçiniz';
	        $data['status'] = FALSE;
	    }

	    if($this->input->post('study_type') == '0' || empty($this->input->post('study_type')))
	    {
	        $data['inputerror'][] = 'study_type';
	        $data['error_string'][] = 'Çalışma Şekli seçiniz';
	        $data['status'] = FALSE;
	    }

	    if($this->input->post('short_description') == '')
		{
			$data['inputerror'][] = 'short_description';
			$data['error_string'][] = 'Kısa Açıklama alanı boş bırakılmaz';
			$data['status'] = FALSE;
		}

		if($this->input->post('city_id') == '0' || empty($this->input->post('city_id')))
	    {
	        $data['inputerror'][] = 'city_id';
	        $data['error_string'][] = 'Şehir seçiniz';
	        $data['status'] = FALSE;
	    }
	    elseif ($this->input->post('city_id') == '341') 
	    {
	    	if(empty($this->input->post('city_text')))
	    	{
	    		$data['inputerror'][] = 'city_text';
		        $data['error_string'][] = 'Ülke-Şehiri yazınız';
		        $data['status'] = FALSE;
	    	}
	    }
	    else if($this->input->post('city_id') > 0)
	    {
	    	if($this->input->post('district_id') == '0' || empty($this->input->post('district_id')))
		    {
		        $data['inputerror'][] = 'district_id';
		        $data['error_string'][] = 'İlçe seçiniz';
		        $data['status'] = FALSE;
		    }
	    }

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}

?>