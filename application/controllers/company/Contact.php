<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('CompanyID')) {
			redirect(base_url('zindhu-hr/studyo/giris')); exit();
		}

		$this->load->model('contact/contact_model','contact');
	}

	public function index()
	{
		$this->load->view('frontend/company/contact_view');
	}

	public function start_contact()
	{
		$company_id = $this->session->userdata('CompanyID');

		$data['messages'] = $this->contact->getAllByCompanyID($company_id);
		$this->contact->update(array('company_id' => $company_id, 'sender_type' => 'admin'), array('is_read' => 1));

		echo json_encode($data);
	}

	public function send_message()
	{
		$post = $this->input->post(null,true);
		$company_id = $this->session->userdata('CompanyID');
		$content = $post['message'];

		$data = array(
			'sender_type' => 'company',
			'company_id' => $company_id,
			'content' => $content,
			'ip_address' => $this->input->ip_address(),
			'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if (!empty($content) && strlen(trim($content)) > 3) {
			if (!empty($company_id)) {
				$this->contact->save($data);
				echo json_encode(array('status' => TRUE, 'message' => 'Mesaj gönderildi.', 'record' => $data)); exit();
			} else {
				echo json_encode(array('status' => FALSE, 'message' => 'Mesaj gönderilmedi.')); exit();
			}
		} else {
			echo json_encode(array('status' => FALSE, 'message' => 'Mesaj içeriği girmeden mesaj gönderemezsiniz.')); exit();
		}
	}

	public function unread_contact()
	{
		$company_id = $this->session->userdata('CompanyID');
	
		$data = $this->contact->unreadCountByCompanyID($company_id, 'admin');

		echo json_encode($data);
	}

	public function control_conversation()
	{
		$company_id = $this->session->userdata('CompanyID');
		
		$data['messages'] = $this->contact->getUnreadMessagesByCompanyID($company_id, 'admin');
		$this->contact->update(array('company_id' => $company_id, 'sender_type' => 'admin'), array('is_read' => 1));

		echo json_encode($data);
	}
}

?>