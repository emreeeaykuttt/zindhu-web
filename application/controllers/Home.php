<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->autorun();
	}

	public function index()
	{
		$this->load->view('frontend/home_view');
	}

	public function page()
	{
		$this->load->view('frontend/home_view');
	}

	public function sitemap()
	{
		header('Content-Type: text/xml');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		echo "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">";
		echo "<url><loc>" . base_url() . "</loc><changefreq>daily</changefreq><priority>1.0</priority></url>";
		echo "<url><loc>" . base_url() . "zindhu-hr</loc><changefreq>daily</changefreq><priority>1.0</priority></url>";
		echo "<url><loc>" . base_url() . "kullanici/giris</loc><changefreq>daily</changefreq><priority>0.8</priority></url>";
		echo "<url><loc>" . base_url() . "kullanici/kayit-ol</loc><changefreq>daily</changefreq><priority>0.8</priority></url>";
		echo "<url><loc>" . base_url() . "studyo/giris</loc><changefreq>daily</changefreq><priority>0.8</priority></url>";

		echo "</urlset>";
	}

	public function test()
	{
		// $tos = array('emreaykut@strajedi.com.tr');
		$tos = array('emreeeaykutt@gmail.com');
        $ccs = array();
        $bccs = array();
        $link = '';
        $link_text = '';

        $mail_content = 'test';

        $html_message = $this->mail_template($mail_content, $link, $link_text);

        $status = $this->send_email($tos, $ccs, $bccs, 'zindhu.com', 'Test maill', $html_message);

        print_r($status);
	}

	public function repeatmail($limit = 10, $offset = 0)
	{
		$users = $this->db->where('verified', 0)->limit($limit, $offset)->get('users')->result_array();

		foreach ($users as $key => $val) 
		{
			$tos = array($val['email']);
            $ccs = array();
            $bccs = array();
            $link = base_url() . 'zindhu-hr/kullanici/giris?token='.$val['verification_token'].'&key='.$val['id'];
            $link_text = 'Doğrula';

            $mail_content = 
                'Merhaba <b>'. $val['firstname'] . ' ' . $val['lastname'] .' ,</b><br>
               	Hesabınız halen aktive edilmemiş gözüküyor, dilerseniz aşağıdaki butona tıklayarak hesabınızı aktive edebilirsiniz.';

            $html_message = $this->mail_template($mail_content, $link, $link_text);

            $status = $this->send_email($tos, $ccs, $bccs, 'zindhu.com', 'Zinhu HR Hesap Doğrulaması', $html_message);
            print_r($status);
            echo "<br>";
            echo $val['email'];
            echo "<hr>";
		}

		echo "success";
	}
}


?>
