<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Zindhu extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->autorun();
	}

	public function index()
	{
		$this->load->model('ad/ad_model','ad');

		$this->ad->status = 1;
		$this->ad->admin_confirm = 'approved';
		$this->ad->per_page = 4;
		$this->ad->page = 1;

		$this->ad->search = array('position' => array('22','23','24'));
		$data['unity_records'] = $this->ad->getAll();

		$this->ad->search = array('position' => array('20'));
		$data['artist3d_records'] = $this->ad->getAll();

		$this->ad->search = array('position' => array('15'));
		$data['game_records'] = $this->ad->getAll();
		

		$data['unity_count'] = $this->ad->getCountByPositionID(array('22','23','24'));
		$data['artist3d_count'] = $this->ad->getCountByPositionID(array('20'));
		$data['game_count'] = $this->ad->getCountByPositionID(array('15'));

		$this->load->view('frontend/zindhu_view', $data);
	}
}


?>