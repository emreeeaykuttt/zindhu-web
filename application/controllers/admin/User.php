<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('AdminID')) {
			redirect(base_url('admin/auth/login')); exit();
		}

		$this->load->model('user/user_model','user');
	}

	public function index()
	{
		$this->load->view('frontend/admin/user_view');
	}

	public function ajax_list()
	{
		$this->load->model('company/company_model','company');

		$get_data = array();

		$cv_get = $this->input->get('cv');
		$contact_get = $this->input->get('contact');
		$hires_get = $this->input->get('hires');
		$ad_get = $this->input->get('ad');

		if ($cv_get == 'new') {
			$get_data['cv'] = $cv_get;
		} elseif ($cv_get == 'confirm' && $this->session->userdata('AdminRole') == 'superadmin') {
			$get_data['cv'] = $cv_get;
		} elseif ($contact_get == '1' && $this->session->userdata('AdminRole') == 'superadmin') {
			$this->load->model('contact/contact_model','contact');
			$get_data['contact_user'] = $contact_get;
		} elseif ($hires_get == '1') {
			$get_data['hires'] = $hires_get;
		} elseif ($ad_get) {
			$get_data['ad_user'] = $ad_get;
		}

		$get_data['position_id'] = $this->input->get('position_id');
		$get_data['city_id'] = $this->input->get('city_id');
		$get_data['city_json'] = $this->input->get('city_json');
		$get_data['education_level'] = $this->input->get('education_level');
		$get_data['university_id'] = $this->input->get('university_id');
		$get_data['program_id'] = $this->input->get('program_id');
		$get_data['program_duration'] = $this->input->get('program_duration');
		$get_data['game_future'] = $this->input->get('game_future');
		$get_data['game_past'] = $this->input->get('game_past');

		$list = $this->user->getDatatables($get_data);
		$data = array();
		
		foreach ($list as $user) 
		{
			$confirm_btn = '';
			$contact_btn = '';
			$note_btn = '';

			if ($cv_get == 'confirm' && $this->session->userdata('AdminRole') == 'superadmin') {
				$confirm_btn = '
				<a href="javascript:void(0)" onclick="approve('.$user->id.')" class="btn btn-deep-purple btn-sm admin-btn"><i class="fas fa-check-circle"></i> ONAYLA</a>' .
				'<a href="javascript:void(0)" onclick="rejected('.$user->id.')" class="btn btn-pink btn-sm admin-btn"><i class="fas fa-times-circle"></i> REDDET</a>';
			} else if ($contact_get == '1' && $this->session->userdata('AdminRole') == 'superadmin') {
				$unread_count = $this->contact->unreadCountByUserID($user->id, 'user');

				if ($unread_count > 0) {
					$conversation_icon = '<i class="fas fa-envelope"></i>';
					$conversation_tooltip = 'data-toggle="tooltip" title="Okunmamış Mesaj Var"';
				} else {
					$conversation_icon = '<i class="fas fa-envelope-open"></i>';
					$conversation_tooltip = 'data-toggle="tooltip" title="Mesaj Okunmuş"';
				}

				$contact_btn = '<a href="javascript:void(0)" onclick="start_contact('.$user->id.')" '. $conversation_tooltip .' class="btn btn-amber btn-sm admin-btn">MESAJ AT '. $conversation_icon .'</a>';
			}

			if (empty($user->note)) {
				$note_icon = '<i class="fas fa-plus"></i>';
			} else {
				$note_icon = '<i class="fas fa-pen"></i>';
			}

			if ($this->session->userdata('AdminRole') == 'superadmin') {
				$note_btn = '<a href="javascript:void(0)" onclick="view_note('.$user->id.')" class="btn btn-info btn-sm admin-btn">NOT'.$note_icon.'</a>';
			}

			if ($user->company_ids) {
				$companies = $this->company->getAllByIDs(json_decode($user->company_ids), true);
				$company_names = '';
		    	$company_count = count($companies) - 1;
		    	foreach ($companies as $key => $value) {
		    		if ($key == $company_count) {
		    			$company_names .= $value['company_name'];
		    		} else {
		    			$company_names .= $value['company_name'] . ' - ';
		    		}
		    	}
			}

			$work_status_color = '';
			$work_status_text = 'Kullanıcı çalışmıyor';
			if ($user->is_working == 1) {
				if ($user->company_ids) {
					$work_status_color = 'green';
					$work_status_text = 'Kullanıcı ' . $company_names . ' çalışıyor';
				} else {
					$work_status_color = 'orange';
					$work_status_text = 'Kullanıcı çalışıyor fakat sisteme ilk girdiği zaman çalışıyor şeklinde girdi ve sistemden herhangi bir stüdyo ile anlaşmamış';
				}
			}

			$hires_btn = '';
			if ($hires_get == '1') {
				$hires_btn = '<a href="javascript:void(0)" onclick="hires('.$user->id.')" class="btn btn-warning btn-sm admin-btn">ÇALIŞTIĞI STUDYOLAR</a>';
			}

			$row = array();
			$row['firstname'] = $user->firstname . ' ' . $user->lastname;
			$row['position'] = $user->position;
			$row['city'] = $user->city;
			$row['transactions'] = 
				$hires_btn .
				'<a href="javascript:void(0)" onclick="view('.$user->id.')" class="btn btn-success btn-sm admin-btn">GÖRÜNTÜLE</a>' .
				'<a href="javascript:void(0)" onclick="remove('.$user->id.')" class="btn btn-danger btn-sm admin-btn">SİL</a>' .
				$note_btn .
				$confirm_btn . 
				$contact_btn .
				'<a href="javascript:void(0)" data-toggle="tooltip" title="'.$work_status_text.'" class="work-info-btn" style="color: '.$work_status_color.'"><i class="fas fa-info-circle fa-2x"></i>'
				;
		
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->user->getCountAll($get_data),
			"recordsFiltered" => $this->user->getCountFiltered($get_data),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
}

?>