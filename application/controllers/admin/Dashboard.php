<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('AdminID')) {
			redirect(base_url('admin/auth/login')); exit();
		}
	}

	public function index()
	{
		$this->load->view('frontend/admin/dashboard_view');
	}
}

?>