<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('AdminID')) {
			redirect(base_url('admin/auth/login')); exit();
		} 
		
		$this->load->model('company/company_model','company');
	}

	public function index()
	{
		$this->load->view('frontend/admin/company_view');
	}

	public function ajax_list()
	{
		$this->load->model('contact/contact_model','contact');

		$get_data = array();
		$hires_get = $this->input->get('hires');
		
		if ($this->input->get('conversations') == 1) {
			$get_data['conversation_company'] = $this->input->get('conversations');
		} else if ($this->input->get('contact') == 1 && $this->session->userdata('AdminRole') == 'superadmin') {
			$get_data['contact_company'] = $this->input->get('contact');
		} elseif ($hires_get == '1') {
			$get_data['hires'] = $hires_get;
		}

		$list = $this->company->getDatatables($get_data);
		$data = array();

		foreach ($list as $company) 
		{
			$row = array();

			if ($this->input->get('conversations') == 1) {
				$transactions = '<a href="javascript:void(0)" onclick="conversations('.$company->id.')" class="btn btn-warning btn-sm db-btn">GÖRÜŞMELER</a>';
			} elseif ($this->input->get('contact') == 1 && $this->session->userdata('AdminRole') == 'superadmin') {
				$unread_count = $this->contact->unreadCountByCompanyID($company->id, 'company');

				if ($unread_count > 0) {
					$conversation_icon = '<i class="fas fa-envelope"></i>';
					$conversation_tooltip = 'data-toggle="tooltip" title="Okunmamış Mesaj Var"';
				} else {
					$conversation_icon = '<i class="fas fa-envelope-open"></i>';
					$conversation_tooltip = 'data-toggle="tooltip" title="Mesaj Okunmuş"';
				}

				$transactions = '<a href="javascript:void(0)" onclick="start_contact('.$company->id.')" '. $conversation_tooltip .' class="btn btn-amber btn-sm db-btn">MESAJ AT '. $conversation_icon .'</a>';
			} else {

				$transactions = '';
				if ($hires_get == '1') {
					$transactions .= '<a href="javascript:void(0)" onclick="hires('.$company->id.')" class="btn btn-warning btn-sm db-btn">ÇALIŞTIĞI KİŞİLER</a>';
				}
				$transactions .= '<a href="javascript:void(0)" onclick="view('.$company->id.')" class="btn btn-success btn-sm db-btn">GÖRÜNTÜLE</a>';
			}

			$row['company_name'] = $company->company_name;
			$row['firstname'] = $company->firstname . ' ' . $company->lastname;
			$row['email'] = $company->email;
			$row['phone'] = $company->phone;
			$row['transactions'] = $transactions;
		
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->company->getCountAll($get_data),
			"recordsFiltered" => $this->company->getCountFiltered($get_data),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
}

?>