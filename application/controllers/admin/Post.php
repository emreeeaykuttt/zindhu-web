<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('AdminID')) {
			redirect(base_url('admin/auth/login')); exit();
		} elseif ($this->session->userdata('AdminRole') != 'superadmin') {
			redirect(base_url('admin/dashboard')); exit();
		}

		$this->load->model('post/post_model','post');
	}

	public function index()
	{
		$this->load->view('frontend/admin/post_view');
	}

	public function ajax_list()
	{
		$get_data = array();

		$list = $this->post->getDatatables($get_data);
		$data = array();

		foreach ($list as $post) 
		{
			$row = array();

			if ($post->post_type == 'duyuru') {
				$post_type = 'Duyuru';
			} elseif ($post->post_type == 'egitim') {
				$post_type = 'Eğitim';
			} elseif ($post->post_type == 'blog') {
				$post_type = 'Blog';
			} else {
				$post_type = '';
			}

			$row['post_type'] = $post_type;
			$row['title'] = $post->title;
			$row['transactions'] = 
				'<a href="javascript:void(0)" onclick="edit('.$post->id.')" class="btn btn-warning btn-sm db-btn">DÜZENLE</a>'.
				'<a href="javascript:void(0)" onclick="remove('.$post->id.')" class="btn btn-danger btn-sm db-btn">SİL</a>'
				;
		
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->post->getCountAll($get_data),
			"recordsFiltered" => $this->post->getCountFiltered($get_data),
			"data" => $data,
		);
		
		echo json_encode($output);
	}

	public function ajax_view($id)
	{
		$data = $this->post->getByID($id);
		$data->detail = str_replace('<br />', '', $data->detail);

		echo json_encode($data);
	}

	public function ajax_save()
	{
		$this->load->library('file_upload');

		$post = $this->input->post(null,true);
		
		$this->_validate();

		$data = array(
	        'post_type' => $post['post_type'],
	        'title' => $post['post_title'],
	        'detail' => nl2br($post['post_detail'])
	    );

	    if($this->input->post('remove_photo'))
		{
			$remove_photo = $this->input->post('remove_photo');
			if(file_exists(UPLOAD_FOLDER . $remove_photo) && $remove_photo)
				unlink(UPLOAD_FOLDER . $remove_photo);
			$data['photo'] = '';
		}

		if(!empty($_FILES['post_photo']['name']))
		{
			$upload = $this->file_upload->do_upload('post_photo', 'gif|jpg|jpeg|png|JPG|PNG|svg', 30000, './upload/posts/');
			
			if ($post['post_id'] > 0) {
				$post_data = $this->post->getByID($post['post_id']);
				if(file_exists(UPLOAD_FOLDER . $post_data->photo) && $post_data->photo)
					unlink(UPLOAD_FOLDER . $post_data->photo);
			}

			$data['photo'] = 'posts/' . $upload;
		}

	    if (empty($post['post_id']) || $post['post_id'] == 0) 
	    {
	    	$data['created_at'] = date('Y-m-d H:i:s', NOW_DATE_TIME);
			$id = $this->post->save($data);

			echo json_encode(array('status' => TRUE, 'message' => 'Kayıt Oluşturuldu', 'id' => $id));
		} 
		else 
		{
			$data['updated_at'] = date('Y-m-d H:i:s', NOW_DATE_TIME);
			$this->post->update(array('id' => $post['post_id']), $data);

			echo json_encode(array('status' => TRUE, 'message' => 'Kayıt Güncellendi', 'id' => 0));
		}
	}

	public function ajax_remove($id)
	{
		$post = $this->post->getByID($id);
		if(file_exists(UPLOAD_FOLDER . $post->photo) && $post->photo)
				unlink(UPLOAD_FOLDER . $post->photo);

		$this->post->deleteByID($id);

		echo json_encode(array('status' => TRUE, 'message' => 'Kayıt Silindi'));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if(empty($this->input->post('post_type')))
		{
			$data['inputerror'][] = 'post_type';
			$data['error_string'][] = 'İçerik Tipi seçiniz';
			$data['status'] = FALSE;
		}

		if($this->input->post('post_title') == '')
		{
			$data['inputerror'][] = 'post_title';
			$data['error_string'][] = 'Başlık alanı boş bırakılmaz';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}

?>