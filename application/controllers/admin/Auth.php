<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/admin_model', 'admin');
	}

	public function index()
	{	
		redirect(base_url('admin/auth/login'));
	}

	public function login()
	{
		$post = $this->input->post(null, true);
		$this->session->set_flashdata('login_status', NULL);

		if ($post) 
		{
			$this->load->library('form_validation');

			$this->form_validation->set_rules('email', 'E-Posta', 'trim|required|valid_email');
			$this->form_validation->set_rules('password', 'Şifre', 'trim|required|min_length[3]');

			$this->form_validation->set_message('min_length', '{field} alanı en az {param} karakterli olmalıdır.');
			$this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');
			$this->form_validation->set_message('valid_email', '{field} alanı geçerli bir e-posta adresi içermelidir.');
			
			if ($this->form_validation->run() == FALSE) 
			{
				$this->load->view('frontend/admin/auth/login_view');
			}
			else
			{
				$result = $this->admin->validate($post['email'], $post['password']);
				if($result)
				{	
					$this->session->unset_userdata(array('AdminID', 'AdminEmail', 'AdminFullname'));
					$this->session->set_userdata('AdminID', $result->id);
					$this->session->set_userdata('AdminEmail', $result->email);
					$this->session->set_userdata('AdminFullname', $result->firstname . ' ' . $result->lastname);
					$this->session->set_userdata('AdminRole', $result->role);


					$remember = empty($post['remember']) ? 0 : 1;
					if($remember == 1)
					{
						$this->session->set_tempdata('RememberAdminEmail', $post['email'], 604800);
						$this->session->set_tempdata('RememberAdminPassword', $post['password'], 604800);
					}
					else
					{
						$this->session->unset_tempdata(array('RememberAdminEmail', 'RememberAdminPassword'));
					}

					if ($this->session->userdata('which_page'))
					{
						redirect($this->session->userdata('which_page'));
					}
					else
					{
						redirect(base_url('admin/dashboard'));
					}
				}
				else
				{
					$this->session->set_flashdata('login_status', array(
						'status' => false, 'message' => 'Email veya şifreyi yanlış girmiş olabilirsiniz.')
					);
					$this->load->view('frontend/admin/auth/login_view');
				}
			}
		}
		else
		{
			if ($this->session->userdata('AdminID'))
			{
				redirect(base_url('admin/dashboard'));
			}
			else
			{
				$this->load->view('frontend/admin/auth/login_view');
			}
		}
	}

	public function logout()
	{
		$this->session->unset_userdata(array('AdminID', 'AdminEmail', 'AdminFullname', 'AdminRole'));

		redirect(base_url('admin/auth/login'));
	}
}



?>