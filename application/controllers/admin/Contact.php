<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('AdminID')) {
			redirect(base_url('admin/auth/login')); exit();
		} else if ($this->session->userdata('AdminRole') != 'superadmin') {
			redirect(base_url('admin/dashboard'));
		}

		$this->load->model('contact/contact_model','contact');
	}

	public function index()
	{
		redirect(base_url('admin/dashboard'));
	}

	public function unread_contact()
	{
		$data['company'] = $this->contact->unreadCountBySenderType('company');
		$data['user'] = $this->contact->unreadCountBySenderType('user');

		echo json_encode($data);
	}

	public function start_contact_company($company_id)
	{
		$this->load->model('company/company_model','company');

		$data['company'] = $this->company->getByID($company_id);
		$data['messages'] = $this->contact->getAllByCompanyID($company_id);
		$this->contact->update(array('company_id' => $company_id, 'sender_type' => 'company'), array('is_read' => 1));

		echo json_encode($data);
	}

	public function send_message_company()
	{
		$post = $this->input->post(null,true);
		$user_id = $this->session->userdata('UserID');
		$company_id = $post['company_id'];
		$content = $post['message'];

		$data = array(
			'sender_type' => 'admin',
			'company_id' => $company_id,
			'content' => $content,
			'ip_address' => $this->input->ip_address(),
			'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if (!empty($content) && strlen(trim($content)) > 3) {
			if (!empty($company_id)) {
				$this->contact->save($data);
				echo json_encode(array('status' => TRUE, 'message' => 'Mesaj gönderildi.', 'record' => $data)); exit();
			} else {
				echo json_encode(array('status' => FALSE, 'message' => 'Mesaj gönderilmedi.')); exit();
			}
		} else {
			echo json_encode(array('status' => FALSE, 'message' => 'Mesaj içeriği girmeden mesaj gönderemezsiniz.')); exit();
		}
	}

	public function start_contact_user($user_id)
	{
		$this->load->model('user/user_model','user');
		$this->load->model('cv/personal_information_model', 'personal_information');

		$data['user'] = $this->user->getByID($user_id);
		$data['personal_information'] = $this->personal_information->getAllByUserID($user_id);
		$data['messages'] = $this->contact->getAllByUserID($user_id);
		$this->contact->update(array('user_id' => $user_id, 'sender_type' => 'user'), array('is_read' => 1));

		echo json_encode($data);
	}

	public function send_message_user()
	{
		$post = $this->input->post(null,true);
		$user_id = $this->session->userdata('UserID');
		$user_id = $post['user_id'];
		$content = $post['message'];

		$data = array(
			'sender_type' => 'admin',
			'user_id' => $user_id,
			'content' => $content,
			'ip_address' => $this->input->ip_address(),
			'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if (!empty($content) && strlen(trim($content)) > 3) {
			if (!empty($user_id)) {
				$this->contact->save($data);
				echo json_encode(array('status' => TRUE, 'message' => 'Mesaj gönderildi.', 'record' => $data)); exit();
			} else {
				echo json_encode(array('status' => FALSE, 'message' => 'Mesaj gönderilmedi.')); exit();
			}
		} else {
			echo json_encode(array('status' => FALSE, 'message' => 'Mesaj içeriği girmeden mesaj gönderemezsiniz.')); exit();
		}
	}
}

?>