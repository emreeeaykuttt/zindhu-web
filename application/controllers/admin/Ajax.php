<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('AdminID')) {
			redirect(base_url('admin/auth/login')); exit();
		}
	}

	public function index()
	{
		redirect(base_url());
	}

	public function all_statistics()
	{
		$this->load->model('company/company_model','company');
		$this->load->model('user/user_model','user');
		$this->load->model('conversation/conversation_model','conversation');
		$this->load->model('company/hires_model', 'company_hires');
		$this->load->model('ad/ad_model','ad');

		$data['company_count'] = $this->company->getCount();
		$data['user_count'] = $this->user->getCount();
		$data['new_user_count'] = $this->user->getCountNew();
		$data['conversations_count'] = $this->conversation->getCount();
		$data['hires_count'] = $this->company_hires->getCountByContinues();
		$data['layoffs_count'] = $this->company_hires->getCountByNotContinues();
		$data['ad_count'] = $this->ad->getCount();
		$data['ad_pending_count'] = $this->ad->getCountByPending();

		echo json_encode($data);
	}
	
	public function company_view($company_id)
	{
		$this->load->model('company/company_model','company');

		$data['company'] = $this->company->getByID($company_id);

		echo json_encode($data);
	}

	public function cv_view($user_id)
	{
		$this->load->model('user/user_model','user');
		$this->load->model('cv/personal_information_model', 'personal_information');
		$this->load->model('cv/educational_background_model', 'educational_background');
		$this->load->model('cv/work_experience_model', 'work_experience');
		$this->load->model('cv/used_program_model', 'used_program');
		$this->load->model('cv/game_choice_model', 'game_choice');
		$this->load->model('cv/portfolio_model', 'portfolio');

		$data['user'] = $this->user->getByID($user_id);
		$data['personal_information'] = $this->personal_information->getAllByUserID($user_id);
		$data['educational_background'] = $this->educational_background->getAllByUserID($user_id);
		$data['work_experience'] = $this->work_experience->getAllByUserID($user_id);
		$data['used_program'] = $this->used_program->getAllByUserID($user_id);
		$data['game_choice'] = $this->game_choice->getAllByUserID($user_id);
		$data['portfolio'] = $this->portfolio->getAllByUserID($user_id);

		echo json_encode($data);
	}

	public function conversations($company_id)
	{
		$this->load->model('conversation/conversation_model','conversation');

		$data['users_by_conversation'] = $this->conversation->getAllByCompanyIDJoinUser($company_id);

		echo json_encode($data);
	}

	public function conversation_by_user_id($company_id, $user_id)
	{
		$this->load->model('conversation/conversation_model','conversation');

		$data['conversations'] = $this->conversation->getAllByCompanyIDAndUserID($company_id, $user_id);

		echo json_encode($data);
	}

	public function user_approve()
	{
		if ($this->session->userdata('AdminRole') != 'superadmin') {
			echo json_encode(array('status' => false, 'message' => 'Yetkiniz Yok')); exit();
		}

		$this->load->model('user/user_model','user');

		$post = $this->input->post(null,true);

		$data = array(
			'admin_confirm' => 'approved',
			'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if (!empty($post['user_id'])) {
			$updated = $this->user->update(array('id' => $post['user_id']), $data);

			if ($updated) {
				$user = $this->user->getByID($post['user_id']);
				$tos = array($user->email);
	            $ccs = array();
	            $bccs = array();
	            $link = base_url() . 'user/auth/logout';
	            $link_text = 'Zindhu Panel';

	            $mail_content = 
	                'Merhaba <b>'. $user->firstname . ' ' . $user->lastname .' ,</b><br>
	                CV\'niz onaylanmıştır.<br />
	                Aşağıdaki butona tıklayarak sisteme giriş yapabilirsiniz.';

	            $html_message = $this->mail_template($mail_content, $link, $link_text);

	            $status = $this->send_email($tos, $ccs, $bccs, 'zindhu.com', 'Hesabınız Onaylandı', $html_message);

				echo json_encode(array('status' => true, 'message' => 'Kullanıcı Onaylandı'));
			} else {
				echo json_encode(array('status' => false, 'message' => 'Değişiklik Yapılamadı'));
			}
			
		} else {
			echo json_encode(array('status' => false, 'message' => 'Kullanıcı Bulunamadı'));
		}
	}

	public function user_rejected()
	{
		if ($this->session->userdata('AdminRole') != 'superadmin') {
			echo json_encode(array('status' => false, 'message' => 'Yetkiniz Yok')); exit();
		}

		$this->load->model('user/user_model','user');

		$post = $this->input->post(null,true);

		$data = array(
			'admin_confirm' => 'rejected',
			'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if (!empty($post['user_id'])) {
			$updated = $this->user->update(array('id' => $post['user_id']), $data);

			if ($updated) {
				$user = $this->user->getByID($post['user_id']);
				$tos = array($user->email);
	            $ccs = array();
	            $bccs = array();
	            $link = base_url() . 'user/auth/logout';
	            $link_text = 'Zindhu Panel';

	            $mail_content = 
	                'Merhaba <b>'. $user->firstname . ' ' . $user->lastname .' ,</b><br>
	                Hesabınız reddedildi.<br />
	                Bilgileriniz eksik veya bizim sistemimize uygun olmadığından dolayı hesabınız onaylanmamıştır.';

	            $html_message = $this->mail_template($mail_content, $link, $link_text);

	            $status = $this->send_email($tos, $ccs, $bccs, 'zindhu.com', 'Hesabınız Reddedildi', $html_message);

				echo json_encode(array('status' => true, 'message' => 'Kullanıcı Reddedildi'));
			} else {
				echo json_encode(array('status' => false, 'message' => 'Değişiklik Yapılamadı'));
			}
			
		} else {
			echo json_encode(array('status' => false, 'message' => 'Kullanıcı Bulunamadı'));
		}
	}

	public function user_remove()
	{
		if ($this->session->userdata('AdminRole') != 'superadmin' && $this->session->userdata('AdminRole') != 'admin') {
			echo json_encode(array('status' => false, 'message' => 'Yetkiniz Yok')); exit();
		}

		$this->load->model('user/user_model','user');
		$this->load->model('contact/contact_model','contact');
		$this->load->model('cv/portfolio_model', 'portfolio');

		$post = $this->input->post(null,true);
		$user_id = $post['user_id'];

		if (!empty($user_id)) {
			$portfolio = $this->portfolio->getByUserID($user_id);

			if(file_exists(UPLOAD_FOLDER.'portfolios/'.$portfolio->name) && $portfolio->name)
				unlink(UPLOAD_FOLDER.'portfolios/'.$portfolio->name);

			$this->user->deleteByID($user_id);
			$this->contact->deleteByUserID($user_id);

			echo json_encode(array('status' => true, 'message' => 'Kullanıcı Silindi'));
		} else {
			echo json_encode(array('status' => true, 'message' => 'Kullanıcı Bulunamadı'));
		}
	}

	public function view_note($user_id)
	{
		$this->load->model('user/user_model','user');
		$this->load->model('cv/personal_information_model', 'personal_information');

		$data['user'] = $this->user->getByID($user_id);
		$data['personal_information'] = $this->personal_information->getAllByUserID($user_id);

		echo json_encode($data);
	}

	public function add_note()
	{
		// if ($this->session->userdata('AdminRole') != 'superadmin') {
		// 	echo json_encode(array('status' => false, 'message' => 'Yetkiniz Yok')); exit();
		// }

		$this->load->model('user/user_model','user');

		$post = $this->input->post(null,true);

		$data = array(
			'note' => $post['note'],
			'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if (!empty($post['user_id'])) {
			$updated = $this->user->update(array('id' => $post['user_id']), $data);

			if ($updated) {
				echo json_encode(array('status' => true, 'message' => 'Not Eklendi'));
			} else {
				echo json_encode(array('status' => false, 'message' => 'Değişiklik Yapılamadı'));
			}
			
		} else {
			echo json_encode(array('status' => false, 'message' => 'Kullanıcı Bulunamadı'));
		}
	}

	public function hires_user($user_id)
	{
		$this->load->model('user/user_model','user');
		$this->load->model('cv/personal_information_model', 'personal_information');
		$this->load->model('company/hires_model', 'company_hires');

		$data['user'] = $this->user->getByID($user_id);
		$data['personal_information'] = $this->personal_information->getAllByUserID($user_id);
		$data['hires'] = $this->company_hires->getAllByUserIDAndStartdate($user_id);

		echo json_encode($data);
	}

	public function hires_company($company_id)
	{
		$this->load->model('company/company_model','company');
		$this->load->model('company/hires_model', 'company_hires');

		$data['company'] = $this->company->getByID($company_id);
		$data['hires'] = $this->company_hires->getAllByCompanyIDAndStartdate($company_id);

		echo json_encode($data);
	}
}

?>