<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ad extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('AdminID')) {
			redirect(base_url('admin/auth/login')); exit();
		}

		$this->load->model('ad/ad_model','ad');
	}

	public function index()
	{
		$this->load->view('frontend/admin/ad_view');
	}

	public function ajax_list()
	{
		$confirm = $this->input->get('confirm');
		$this->ad->admin_confirm = $confirm;

		$list = $this->ad->getDatatables();
		$data = array();

		foreach ($list as $ad) 
		{
			$confirm_btn = '';
			$ad_btn = '';

			if ($confirm == 'pending') {
				$confirm_btn = '
					<a href="javascript:void(0)" onclick="approve('.$ad->id.')" class="btn btn-deep-purple btn-sm admin-btn"><i class="fas fa-check-circle"></i> ONAYLA</a>' .
					'<a href="javascript:void(0)" onclick="rejected('.$ad->id.')" class="btn btn-pink btn-sm admin-btn"><i class="fas fa-times-circle"></i> REDDET</a>';
			} else {
				$ad_btn = '<a href="/admin/user?ad='.$ad->id.'" class="btn btn-primary btn-sm admin-btn">BAŞVURANLAR</a>';
			}

			$row = array();
			$row['title'] = $ad->title;
			$row['company_name'] = $ad->company_name;
			$row['status'] = $ad->status;
			$row['admin_confirm'] = $ad->admin_confirm;
			$row['transactions'] = 
				$confirm_btn .
				$ad_btn .
				'<a href="javascript:void(0)" onclick="edit('.$ad->id.')" class="btn btn-warning btn-sm admin-btn">DÜZENLE</a>' .
				'<a href="javascript:void(0)" onclick="remove('.$ad->id.')" class="btn btn-danger btn-sm admin-btn">SİL</a>'
			;
		
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->ad->getCountAll(),
			"recordsFiltered" => $this->ad->getCountFiltered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}

	public function ajax_view($id)
	{
		$data = $this->ad->getByID($id);

		echo json_encode($data);
	}

	public function ajax_save()
	{
		$this->load->library('file_upload');

		$post = $this->input->post(null,true);
		
		$this->_validate();

	    $data = array(
	        'title' => $post['title'],
	        'slug' => seo_url($post['title']),
	        'study_type' => $post['study_type'],
	        'short_description' => $this->input->post('short_description'),
	        'job_description' => $this->input->post('job_description'),
	        'status' => isset($post['status']) ? 1 : 0,
	        'position_json' => json_encode($post['position_json']),
	        'city_text' => $post['city_text'],
	        'district_id' => isset($post['district_id']) ? $post['district_id'] : NULL,
	        'city_id' => $post['city_id'],
	        'is_remote' => isset($post['is_remote']) ? 1 : 0,
	    );

	    if (empty($post['ad_id']) || $post['ad_id'] == 0) 
	    {
	    	$data['created_at'] = date('Y-m-d H:i:s', NOW_DATE_TIME);
			$id = $this->ad->save($data);

			echo json_encode(array('status' => TRUE, 'message' => 'İlan Oluşturuldu', 'id' => $id));
		} 
		else 
		{
			$data['updated_at'] = date('Y-m-d H:i:s', NOW_DATE_TIME);
			$this->ad->update(array('id' => $post['ad_id']), $data);

			echo json_encode(array('status' => TRUE, 'message' => 'İlan Güncellendi', 'id' => 0));
		}
	}

	public function ajax_remove($id)
	{
		$this->ad->deleteByID($id);

		echo json_encode(array('status' => TRUE, 'message' => 'İlan Silindi'));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('title') == '')
		{
			$data['inputerror'][] = 'title';
			$data['error_string'][] = 'Başlık alanı boş bırakılmaz';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function ajax_approve()
	{
		$this->load->model('company/company_model','company');

		$post = $this->input->post(null,true);

		$data = array(
			'admin_confirm' => 'approved',
			'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if (!empty($post['ad_id'])) {
			$updated = $this->ad->update(array('id' => $post['ad_id']), $data);
			$ad_row = $this->ad->getByID($post['ad_id']);

			if ($updated) {
				$company = $this->company->getByID($ad_row->company_id);
				$tos = array($company->email);
	            $ccs = array();
	            $bccs = array();
	            $link = base_url() . 'zindhu-hr/studyo/is-ilanlari';
	            $link_text = 'İlanlarınız';

	            $mail_content = 
	                'Merhaba <b>'. $company->firstname . ' ' . $company->lastname .' ,</b><br>
	                İlanınız onaylanmıştır.<br />
	                Aşağıdaki butona tıklayarak sisteme giriş yapabilirsiniz.';

	            $html_message = $this->mail_template($mail_content, $link, $link_text);

	            $status = $this->send_email($tos, $ccs, $bccs, 'zindhu.com', 'İlanınız Onaylandı', $html_message);

				echo json_encode(array('status' => true, 'message' => 'İlan Onaylandı'));
			} else {
				echo json_encode(array('status' => false, 'message' => 'Değişiklik Yapılamadı'));
			}
			
		} else {
			echo json_encode(array('status' => false, 'message' => 'İlan Bulunamadı'));
		}
	}

	public function ajax_rejected()
	{
		$this->load->model('company/company_model','company');

		$post = $this->input->post(null,true);

		$data = array(
			'admin_confirm' => 'rejected',
			'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
		);

		if (!empty($post['ad_id'])) {
			$updated = $this->ad->update(array('id' => $post['ad_id']), $data);
			$ad_row = $this->ad->getByID($post['ad_id']);

			if ($updated) {
				$company = $this->company->getByID($ad_row->company_id);
				$tos = array($company->email);
	            $ccs = array();
	            $bccs = array();
	            $link = base_url() . 'zindhu-hr/studyo/is-ilanlari';
	            $link_text = 'İlanlarınız';

	            $mail_content = 
	                'Merhaba <b>'. $company->firstname . ' ' . $company->lastname .' ,</b><br>
	                İlanınız reddedildi.<br />
	                Bilgileriniz eksik veya bizim sistemimize uygun olmadığından dolayı ilanınız yayınlanmamıştır.';

	            $html_message = $this->mail_template($mail_content, $link, $link_text);

	            $status = $this->send_email($tos, $ccs, $bccs, 'zindhu.com', 'İlanınız Reddedildi', $html_message);

				echo json_encode(array('status' => true, 'message' => 'İlan Reddedildi'));
			} else {
				echo json_encode(array('status' => false, 'message' => 'Değişiklik Yapılamadı'));
			}
			
		} else {
			echo json_encode(array('status' => false, 'message' => 'İlan Bulunamadı'));
		}
	}

}

?>