<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language extends MY_Controller {

	public function change($lang = '')
	{
		if ($lang == 'en') {
			$lang_code = 'en';
		} else {
			$lang_code = 'tr';
		}

		$this->session->set_userdata('lang_code', $lang_code);
			
		if (!empty($this->input->get('page'))) {
			header('location: ' . base_url($this->input->get('page')));
		} else {
			header('location: ' . base_url());
		}
	}

}