<?php 

function seo_url($urlx)
{
    $url = trim($urlx);
    $url = strtolower($url);
    $find = array('<b>', '</b>');
    $url = str_replace ($find, '', $url);
    $url = preg_replace('/<(\/{0,1})img(.*?)(\/{0,1})\>/', 'image', $url);

    $find = array(' ', '&quot;', '&amp;', '&', '\r\n', '\n', '/', '\\', '+', '<', '>');
    $url = str_replace ($find, '-', $url);

    $find = array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ë', 'Ê');
    $url = str_replace ($find, 'e', $url);

    $find = array('í', 'ı', 'ì', 'î', 'ï', 'I', 'İ', 'Í', 'Ì', 'Î', 'Ï');
    $url = str_replace ($find, 'i', $url);

    $find = array('ó', 'ö', 'Ö', 'ò', 'ô', 'Ó', 'Ò', 'Ô');
    $url = str_replace ($find, 'o', $url);

    $find = array('á', 'ä', 'â', 'à', 'â', 'Ä', 'Â', 'Á', 'À', 'Â');
    $url = str_replace ($find, 'a', $url);

    $find = array('ú', 'ü', 'Ü', 'ù', 'û', 'Ú', 'Ù', 'Û');
    $url = str_replace ($find, 'u', $url);

    $find = array('ç', 'Ç');
    $url = str_replace ($find, 'c', $url);
    
    $find = array('ş', 'Ş');
    $url = str_replace ($find, 's', $url);

    $find = array('ğ', 'Ğ');
    $url = str_replace ($find, 'g', $url);

    $find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
    $repl = array('', '-', '');
    
    $url = preg_replace ($find, $repl, $url);
    $url = str_replace ('--', '-', $url);
    
    return $url;
}

function image_url($urlx)
{
    $url = trim($urlx);
    $url = strtolower($url);

    $find = array(' ', '&quot;', '&amp;', '&', '\r\n', '\n', '/', '\\', '+', '<', '>');
    $url = str_replace ($find, '-', $url);

    $find = array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ë', 'Ê');
    $url = str_replace ($find, 'e', $url);

    $find = array('í', 'ı', 'ì', 'î', 'ï', 'I', 'İ', 'Í', 'Ì', 'Î', 'Ï');
    $url = str_replace ($find, 'i', $url);

    $find = array('ó', 'ö', 'Ö', 'ò', 'ô', 'Ó', 'Ò', 'Ô');
    $url = str_replace ($find, 'o', $url);

    $find = array('á', 'ä', 'â', 'à', 'â', 'Ä', 'Â', 'Á', 'À', 'Â');
    $url = str_replace ($find, 'a', $url);

    $find = array('ú', 'ü', 'Ü', 'ù', 'û', 'Ú', 'Ù', 'Û');
    $url = str_replace ($find, 'u', $url);

    $find = array('ç', 'Ç');
    $url = str_replace ($find, 'c', $url);
    
    $find = array('ş', 'Ş');
    $url = str_replace ($find, 's', $url);

    $find = array('ğ', 'Ğ');
    $url = str_replace ($find, 'g', $url);

    $find = array('(', ')');
    $url = str_replace ($find, '', $url);
    
    return $url;
}

function text_limitation($val, $limit = 10)
{
    if (strlen($val) > $limit)
    {
        if (function_exists("mb_substr")) $val = mb_substr($val, 0, $limit, "UTF-8").'...';
        else $val = substr($val, 0, $limit).'...';
    }
    return $val;
}

function number_limitation($val, $limit = 10)
{
    if (strlen($val) > $limit)
    {
        if (function_exists("mb_substr")) $val = mb_substr($val, 0, $limit, "UTF-8");
        else $val = substr($val, 0, $limit);
    }
    return $val;
}

function general_date_format($val)
{
    return date('d-m-Y', strtotime($val));
}

function general_datetime_format($val)
{
    return date('d.m.Y H:i', strtotime($val));
}

function turkish_date_format($format, $datetime = 'now')
{
    $z = date("$format", strtotime($datetime));
    $day_arr = array(
        'Monday'    => 'Pazartesi',
        'Tuesday'   => 'Salı',
        'Wednesday' => 'Çarşamba',
        'Thursday'  => 'Perşembe',
        'Friday'    => 'Cuma',
        'Saturday'  => 'Cumartesi',
        'Sunday'    => 'Pazar',
        'January'   => 'Ocak',
        'February'  => 'Şubat',
        'March'     => 'Mart',
        'April'     => 'Nisan',
        'May'       => 'Mayıs',
        'June'      => 'Haziran',
        'July'      => 'Temmuz',
        'August'    => 'Ağustos',
        'September' => 'Eylül',
        'October'   => 'Ekim',
        'November'  => 'Kasım',
        'December'  => 'Aralık',
        'Mon'       => 'Pts',
        'Tue'       => 'Sal',
        'Wed'       => 'Çar',
        'Thu'       => 'Per',
        'Fri'       => 'Cum',
        'Sat'       => 'Cts',
        'Sun'       => 'Paz',
        'Jan'       => 'Oca',
        'Feb'       => 'Şub',
        'Mar'       => 'Mar',
        'Apr'       => 'Nis',
        'Jun'       => 'Haz',
        'Jul'       => 'Tem',
        'Aug'       => 'Ağu',
        'Sep'       => 'Eyl',
        'Oct'       => 'Eki',
        'Nov'       => 'Kas',
        'Dec'       => 'Ara',
    );
    foreach($day_arr as $en => $tr){
        $z = str_replace($en, $tr, $z);
    }
    if(strpos($z, 'Mayıs') !== false && strpos($format, 'F') === false) $z = str_replace('Mayıs', 'May', $z);
    return $z;
}

function education_level_render($val)
{
    $data = '';
    if ($val == 'on-lisans') {
        $data = 'Ön Lisans';
    } elseif ($val == 'lisans') {
        $data = 'Lisans';
    } elseif ($val == 'yuksek-lisans') {
        $data = 'Yüksek Lisans';
    } elseif ($val == 'doktora') {
        $data = 'Doktora';
    }

    return $data;
}

function city_render($val)
{   
    $ci =& get_instance();

    $query = $ci->db->where('id', $val)->get('cities')->row();
    $data = $query->city;

    return $data; 
}

function positions_render($json)
{
    $ci =& get_instance();

    $ci->db->group_start();
    foreach ($json as $key => $value) {
        if ($key == 0) {
            $ci->db->where('id', $value);
        } else {
            $ci->db->or_where('id', $value);
        }
    }
    $ci->db->group_end(); 

    $ci->db->order_by('title', 'asc');
    $query = $ci->db->get('positions');
    
    if ($query->num_rows() > 0) 
    {
        $data = $query->result_array();
        $positions = '';

        foreach ($data as $key => $value) {
            $positions .=  '<div class="tag">' . $value['title'] . '</div>';
        }

        return $positions;
    }
    else
    {
        return false;
    }
}

function position_render($val)
{   
    $ci =& get_instance();

    $query = $ci->db->where('id', $val)->get('positions')->row();
    $data = $query->title;

    return $data; 
}

function position_count_by_ad($val, $search = '')
{
    $ci =& get_instance();

    // if (!empty($search)) {
    //     if (!empty($search['keywords'])) {
    //         $ci->db->group_start();
    //         $ci->db->like('title', $search['keywords']);
    //         $ci->db->or_like('job_description', $search['keywords']);
    //         $ci->db->group_end();
    //     }
    // }

    $ci->db->where('status', 1);
    $ci->db->where('admin_confirm', 'approved');

    $query = $ci->db->like('position_json', '"'. $val .'"')->get('ads');

    $data = $query->num_rows();

    return $data;
}

function study_type_count_by_ad($val, $search = '')
{
    $ci =& get_instance();

    $ci->db->where('status', 1);
    $ci->db->where('admin_confirm', 'approved');

    $query = $ci->db->where('study_type', $val)->get('ads');

    $data = $query->num_rows();

    return $data;
}

function select_find($val, $id)
{
    if (!empty($val) && is_array($val))
    {
        foreach ($val as $key => $value) 
        {
            if ($id == $value)
            {
                return 'selected';
            }
        }   
    }
}

function checked_find($val, $id)
{
    if (!empty($val) && is_array($val))
    {
        foreach ($val as $key => $value) 
        {
            if ($id == $value)
            {
                return 'checked';
            }
        }   
    }
}

function radio_checked_find($val, $id)
{
    if (!empty($val))
    {
        if ($id == $val)
        {
            return 'checked';
        }  
    }
}

function ad_user_control($ad_id, $user_id = '')
{
    $ci =& get_instance();

    $ci->db->where('ad_id', $ad_id);
    $ci->db->where('user_id', $user_id);
    
    $query = $ci->db->get('ad_users');

    if($query->num_rows() > 0)
    {
        return $query->row();
    }
    else
    {
        return FALSE;
    }
}

function transaction_render($val)
{   
    $data = '';

    if ($val == 'views') {
        $data = 'İncelediklerim';
    } elseif ($val == 'follow') {
        $data = 'Takip Ettiklerim';
    } elseif ($val == 'apply') {
        $data = 'Başvurduklarım';
    }

    return $data;
}

function transaction_count_by_ad($val)
{
    return 0;
}

function time_render($val = '')
{
    $date_difference = date_diff(date_create(date('Y-m-d H:i:s', NOW_DATE_TIME)), date_create($val));
    $diff_time = '';
    
    if($date_difference->y >= 1) {
        $diff_time = $date_difference->y . ' yıl önce';
    }
    else if($date_difference->m >= 1) {
        $diff_time = $date_difference->m . ' ay önce';
    }
    else if($date_difference->d >= 1) {
        $diff_time = $date_difference->d . ' gün önce';
    }
    else if($date_difference->h >= 1) {
        $diff_time = $date_difference->h . ' saat önce';
    }
    else if($date_difference->i >= 1) {
        $diff_time = $date_difference->i . ' dakika önce';
    }
    else if($date_difference->s >= 1) {
        $diff_time = $date_difference->s . ' saniye önce';
    }

    return $diff_time;
}

function city_district_render($city = '', $district = '', $city_text = '')
{
    if ($city && $district) {
        return $city . ' - ' . $district;
    } elseif ($city_text) {
        return $city_text;
    }
}

function is_remote_render($val = '')
{
    if ($val == 1) {
        return '(Remote)';
    }
}

?>