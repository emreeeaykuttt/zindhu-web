<?php 

function l($key)
{
	$ci =& get_instance();
	$dil = $ci->session->userdata("lang_code");


	if(!$dil)
	{
		$dil = "tr";
	}

	if($dil == "tr")
	{
		$lang['iletisim'] = 'İletişim';
		$lang['bize_ulasin'] = 'Bize Ulaşın';
		$lang['adres'] = 'Adres';
		$lang['hosgeldiniz'] = 'HOŞGELDİNİZ';
		$lang['oyun_sektorun_kalbi'] = ' Oyun Sektörünün Kalbi';
		$lang['oyun_sektorune_fayda_text'] = 'Oyun sektörüne fayda sağlamaya ve gelişimine odaklı ilk ve tek bütünleşik servis yapısı olan Zindhu 2018 yılında İstanbul\'da kuruldu.';
		$lang['zindhu_co_catisi_altinda_text'] = 'Zindhu Co, çatısı altında Zindhu HR, Zindhu Games ve Zindhu Ventures olacak şekilde özelleştirilmiş servisler sunan, kaliteli ve sürekliliğe odaklanmış hizmet anlayışıyla danışmanlık veren bir oyun danışmanlık şirketidir.';
		$lang['zindhu_hr_ulkemizde_oyun_text'] = 'Zindhu HR, ülkemizdeki oyun sektörü odaklı ilk ve tek insan kaynakları platformudur. Zindhu HR portalı süre gelen işe alım imkanı sunan diğer insan kaynakları platformlarına kıyasla Türkiye\'deki oyun sektörünü tek bir çatı altında toplamayı hedefleyen, işe alımın ötesinde, eğitim ve iletişim imkanlarının ücretsiz bulunabildiği yegane platformdur.';
		$lang['hemen_basvur'] = 'HEMEN BAŞVUR';
		$lang['hedefimiz'] = 'HEDEFİMİZ';
		$lang['oyun_sektorunu_degistirmek'] = 'Oyun Sektörünü Değiştirmek';
		$lang['mobil_oyunlarin_gelisimini_text'] = 'Mobil oyunların gelişimini ilk günden beri takip ederek sektöre yön vermek amacıyla kurulan Zindhu Games, oyun stüdyoları, geliştiriciler, indie oluşumlar ve sektörün uzmanlarını bir araya getirmekte, yayıncılarla yapımcıların aynı anlayışı benimseyerek birlikte çalışabilmelerine olanak sağlayan danışmanlık şirketidir.';
		$lang['oyununuzu_paylasin'] = 'Oyununuzu Paylaşın';
		$lang['zindhu_ventures_her_turden_text'] = 'Zindhu Ventures, her türden oyun şirketi, oyun stüdyoları, girişimciler için yatırım programları yöneten, ayrıca yatırımcılar için yatırımlarını doğru kanalize edebilmek ve yönetebilmek için danışmanlık veren bir Zindhu servisidir.';
		$lang['yatirim_almak'] = 'Yatırım Almak';
		$lang['yatirimci_olmak'] = 'Yatırımcı Olmak';
		$lang['hakkimizda'] = 'Hakkımızda';
		$lang['giris_yap'] = 'Giriş Yap';
		$lang['bireysel_giris'] = 'Bireysel Giriş';
		$lang['kurumsal_giris'] = 'Kurumsal Giriş';
		$lang['bireysel'] = 'Bireysel';
		$lang['kurumsal'] = 'Kurumsal';
		$lang['giris'] = 'Giriş';
		$lang['oyun_sektorunde_hayalindeki_isi_bul'] = 'Oyun Sektöründe Hayalindeki İşi Bul!';
		$lang['turkiyenin_ilk_ve_tek_text'] = 'Türkiye\'nin ilk ve tek oyun sektörü odaklı insan kaynakları platformu Zindhu HR\'da 100+ stüdyo ve ilanları seni bekliyor.';
		$lang['zindhu_hr_100e_yakin_studyo_ve_is_ilani_seni_bekliyor'] = 'Zindhu HR\'da 100+ stüdyo ve ilanları seni bekliyor.';
		$lang['kaydol'] = 'Kayıt Ol';
		$lang['oyun_sektorunde_hayalindeki_isi_nasil_bulabilirsin'] = 'Oyun Sektöründe Hayalindeki Işi Nasıl Bulabilirsin?';
		$lang['hemen_profilini_olustur'] = 'Hemen profilini oluştur';
		$lang['ozgecmisini_eksiksiz_doldur'] = 'Özgeçmişini eksiksiz doldur';
		$lang['ornek_islerini_icinde_bulundugum_projeleri_ekle'] = 'Örnek işlerini, içinde bulunduğun projeleri ekle';
		$lang['prestijli_oyun_studyolarindan_is_teklifini_al'] = 'Prestijli Oyun Stüdyolarından iş teklifini al!';
		$lang['ad_soyad'] = 'Ad Soyad';
		$lang['e_posta'] = 'E-Posta';
		$lang['konu'] = 'Konu';
		$lang['mesajiniz'] = 'Mesajınız';
		$lang['aydinlatma_metnini_okudum_ve_onayladim'] = 'Aydınlatma Metnini okudum ve onayladım.';
		$lang['formu_gonder'] = 'FORMU GÖNDER';
		$lang['gonder'] = 'GÖNDER';
		$lang['kisisel_veri_ve_gizlilik_politikasi'] = 'Kişisel Veri ve Gizlilik Politikası';
		$lang['acik_pozisyonlar'] = 'Açık Pozisyonlar';
		$lang['kategori_secerek_ilanlara_goz_atin'] = 'Kategori seçerek ilanlara göz atın.';
		$lang['hayalini_oyuna_donusturenler_anlatiyor'] = 'Hayalini oyuna <br>dönüştürenler <br>anlatıyor.';
		$lang['hesabiniz_yok_mu'] = 'Hesabınız yok mu?';
		$lang['kayit_olun'] = 'Kayıt olun';
		$lang['field_cannot_be_left_blank'] = 'alanı boş bırakılamaz';
		$lang['girdiginiz_e_posta_gecerli_degildir'] = 'Girdiğiniz E-posta geçerli değildir';
		$lang['aydinlatma_metnini_kabul_etmeden_devam_edemezsiniz'] = 'Aydınlatma Metnini kabul etmeden devam edemezsiniz';

		// anasayfa
		$lang['zindhu_oyun_sektorunun_kalbi'] = '<b>Zindhu:</b> Oyun Sektörünün Kalbi';
		$lang['oyun_sektorunu_olusturan_text'] = 'Oyun sektörünü oluşturan oyun geliştiricilerden,  yayıncılara, oyun monetizasyonu  yöntemlerinden insan kaynaklarına kadar oyun sektöründe 360 derece çözümler sunan tüm hizmetler Zindhu çatısı altında!';
		$lang['z_hakkimizda'] = 'Hakkımızda';
		$lang['istanbul_merkezli_kurulan_text'] = 'İstanbul merkezli kurulan Zindhu, çatısı altında insan kaynakları, oyun stüdyoları danışmanlığı, yayıncı ve yatırımcı ilişkileri olacak şekilde farklı hizmet alanlarında servis sağlayan markalarıyla, başta Türkiye oyun pazarı olmak üzere global oyun sektörüne hizmet sağlamaktadır.';
		$lang['sektorel_vizyon_hedeflerimiz'] = '<span>Sektörel</span><br /> Vizyon <span>& Hedeflerimiz</span>';
		$lang['hedefi_turkiye_oyun_text'] = 'Türkiye oyun sektöründe mevcut <b>süreçleri geliştirmek</b>, sektöre <b>yeni projeksiyonlar</b> çizmek ve Türkiye’deki oyun firmalarının global ölçekte <b>büyümelerine</b> destek olmak için';
		$lang['insan_kaynaklari'] = 'İnsan kaynakları danışmanlığı<br />';
		$lang['yatirim_surecleri_ve_kullanici_edinimi'] = 'Yatırım süreçleri ve kullanıcı edinimi<br />';
		$lang['is_plani_olusturma_ve_olceklendirme'] = 'İş planı oluşturma ve ölçeklendirme<br />';
		$lang['alanlarinda_hizmet_veren_text'] = 'alanlarında, özelleştirilmiş hizmetler vermekteyiz. Hazır ve paket servisler/çözümler sunmak yerine, oyun sektöründe yer alan tüm şirketlerin ve ekosistemlerin oluşum süreçlerini inceleyerek, ihtiyaçlarına göre farklı iş modelleri geliştiren Zindhu, Türkiye’de yeni değerler yaratmayı hedefleyen projeler geliştirmeye ve yayınlamaya devam etmektedir. </p><p>Bizler Zindhu ailesi olarak, stüdyoların hareket kabiliyetini arttıracak yeni takım yapılarını oluşturmak için insan kaynaklarını beslemek, ürünlerin geliştirilmesi/pazarlanması konusunda yayıncı ilişkileri kurmak ve kapital oluşturmak için doğru yatırımcı iletişiminde çözümler üretiyoruz.';
		$lang['turkiyennin_ilk_ve_tek_text'] = 'Türkiye’nin ilk ve tek oyun sektörü odaklı insan kaynakları platformu.';
		$lang['basvuru_yap'] = '<span>Başvuru Yap</span>';
		$lang['zindhu_hrin_tem_hedeleri'] = 'Zindhu HR’ın temel hedefleri';
		$lang['oyun_sektorunde_calisan_kisilere_text'] = '* Oyun sektöründe çalışan kişilere farklı kariyer opsiyonları sunmak,';
		$lang['oyun_sektorunde_kariyer_text'] = '* Oyun sektöründe kariyer hedefi olan öğrenci veya yeni mezun kişilere bir sonraki adımlarında, kendilerine en uygun takımı bulmaları için aracılık etmek,';
		$lang['potansiyelini_oyun_sektorunde_text'] = '* Potansiyelini oyun sektöründe kullanmak isteyen deneyimli yazılımcı, tasarımcı ve benzeri pozisyonlardaki kişilere hızlı adaptasyon ve geçiş süreçlerinde destek olmak,';
		$lang['oyun_studyolarina_buyume_text'] = '* Oyun stüdyolarına, büyüme hedefleri doğrultusunda gerekli iş gücü ve insan kaynağını sürekli olacak şekilde sağlayabilmek,';
		$lang['yurt_disinda_kariyerine_text'] = '* Yurt dışında kariyerine devam etmekte olan kaliteli insan gücünü, Türk oyun pazarında değerlendirmek ve uluslararası işbirlikleri tetiklemek,';
		$lang['dogrultusunda_platforma_uye_text'] = 'doğrultusunda platforma üye tüm bireysel ve kurumsal kullanıcılara hizmet vermekteyiz.';
		$lang['oyun_sektorunde_dogru_text'] = 'Oyun sektöründe doğru yetenek yerleşiminin büyümenin temel faktörü olduğuna inanıyor, sektördeki tüm oyuncular için yaratıcı çözümler üretmeye devam ediyoruz.';
		$lang['human_resources'] = '<span>Human Resources</span>';
		$lang['studyolarin_is_modellerinin_gelistirilmesi_text'] = 'Stüdyoların iş modellerinin geliştirilmesi, ürünlerinin yatırım öncesi ve sonrası süreçlere hazırlanması ve doğru yatırım ortaklığının kurulmasında, taraflara kişiselleştirilmiş planlar hazırlıyoruz.';
		$lang['yatirim_al'] = '<span>Yatırım al</span>';
		$lang['yatirimci_ol'] = '<span>Yatırımcı ol</span>';
		$lang['studyo_tarafinda'] = 'Stüdyo Tarafında';
		$lang['oyun_studyosunun_gelisimini_text'] = 'Oyun Stüdyosunun gelişimini desteklemek için ihtiyaçların belirlenmesi,';
		$lang['yatirimci_sunumu_ve_finansal_text'] = 'Yatırımcı sunumu ve finansal projeksiyonlarının hazırlanması,';
		$lang['sirket_ve_takim_imajlarini_text'] = 'Şirket ve takım imajlarının vizyon ve misyon çerçevelerinde şekillendirilmesi,';
		$lang['oyun_sektorunde_denenmis_text'] = 'Oyun sektöründe denenmiş fikirlerin olasılıklar için değerlendirilmesi ve iş modeli seçeneklerinden janra özelinde seçim yapılması,';
		$lang['oyunun_ve_oyun_studyosunun_text'] = 'Oyunun ve oyun stüdyosunun pazar hedeflerinin ölçeklendirilmesi ve sürdürülebilir gelir modellerinin oturtulması,';
		$lang['ozelinde_studyo_ile_text'] = 'özelinde stüdyo ile birlikte çalışıyor, hedeflere ulaşmak için en iyi planı uyarlıyoruz.';
		$lang['yatirimci_tarafinda'] = 'Yatırımcı Tarafında';
		$lang['buyume_potansiyeli_yüksek_text'] = 'Büyüme potansiyeli yüksek ve hit oyun üretebilecek yetenekte stüdyoların keşfedilmesi,';
		$lang['yatirim_surecleri_boyunca_text'] = 'Yatırım süreçleri boyunca stüdyoya insan kaynakları, pazarlama ve oyunlar özelinde stratejik danışmanlık desteği verilmesi,';
		$lang['oyun_studyosunun_geliştigi_text'] = 'Oyun stüdyosunun geliştiği türde, anlamlı büyümeyi tetikleyecek yayıncı ilişkilerinin kurulması,';
		$lang['sureclerinde_yaitrim_planinda_text'] = 'süreçlerinde yatırım planında yer alıyor, sürdürülebilir iş planlarını stüdyo ve yatırımcı ile birlikte hazırlıyoruz.';
		$lang['oyun_studyosunun_degerlendirilmesi_text'] = 'Oyun stüdyosunun değerlendirmesi / ihtiyaçların belirlenmesi;';
		$lang['oyun_studyosuna_uygun_bir_finansman_modelinin_bulunmasi'] = 'Oyun stüdyosuna uygun finansman modelinin bulunması;';
		$lang['bulunan_model_dahilinde_oyun_studyosu_ile_calismalarin_baslamasi'] = 'Bulunan model dahilinde oyun stüdyosu ile çalışmaların başlaması';
		$lang['is_modelinin_ve_finansal_projeksiyonlarin_optimizasyonu'] = 'İş modelinin ve finansal projeksiyonların optimizasyonu';
		$lang['oyun_studyosu_ve_yatirimci_arasindaki_text'] = 'Oyun stüdyosu ve yatırımcı arasındaki süreçleri sürdürülebilir bir modele taşımak';
		$lang['oyun_sektorunun_gelisiminin_text'] = 'Oyun sektörünün gelişiminin oyun stüdyoları ve doğru insan kaynakları çözümlerinden başladığına inanarak yola çıkan Zindhu Games, sektördeki türünden bağımsız tüm oyun stüdyoları ile;';
		$lang['yayinci_ariyorum'] = '<span>Yayıncı arıyorum</span>';
		$lang['sorularim_var'] = '<span>Sorularım var</span>';
		$lang['sektoru_ve_studyolarin_mevcut_asamalarini_degerlendirip'] = 'Sektörü ve stüdyoların mevcut iş modellerini ve ürün özelliklerini, kazanç modellerini analiz etmek,';
		$lang['studyolarin_varsa_sorun_text'] = 'Oyun Stüdyolarının gelişimi yolundaki engelleri tespit edip, akıllı iş modellerinin uygulayarak ortadan kaldırmak,';
		$lang['kisisellestirilmis_cozumlerle_zindhu_text'] = 'Kişiselleştirilmiş çözümlerle Zindhu altyapısından insan kaynakları ve risk sermayesi alanlarında destek vermek,';
		$lang['oyun_studyosunun_gelistirdiği_text'] = 'Oyun Stüdyosunun geliştirdiği ürün özelinde en doğru yayıncı iletişimini seçmek, ';
		$lang['icin_ozellestirilmis_hizmetler_text'] = 'için, özelleştirilmiş hizmetler sağlamakta ve sektördeki ağın faydalarını kullanmak ve geliştirmektedir.';
		$lang['turkiye_oyun_pazarında_text'] = '<b>Türkiye oyun pazarında büyümekte olan Zindhu, global oyun pazarını geliştirmek için anlamlı ve büyüme odaklı çözümler üretmeye devam etmektedir.</b>';
		$lang['aklinizda_sorularmi_var'] = ' Aklınızda sorular mı var?';
		$lang['bir_gorusme_ayarlayalim'] = ' Bir görüşme ayarlayalım.';
		$lang['bizi_takip_edin'] = ' BİZİ TAKİP EDİN <br><br>';
		$lang['z_iletisim'] = 'İletişim';
		$lang['z_bize_ulasin'] = 'BİZE ULAŞIN';
		$lang['z_ad_soyad'] = 'İsim Soyisim';
		$lang['z_e_posta'] = 'E-posta';
		$lang['mesaj'] = 'Mesaj';
		$lang['kisisel_veri_ve_gizlilik_politikasi'] = 'Kişisel Veri ve Gizlilik Politikası';
		$lang['oyununuzu_paylasin'] = 'Oyununuzu paylaşın';
		$lang['oyun_linkiniz'] = 'Oyun linkiniz';
		$lang['oyununuzu_anlatin'] = 'Oyununuzu anlatın';
		$lang['anladim'] = '<span>Anladım</span>';
		$lang['mesajiniz_basariyla_gonderildi'] = 'Mesajınız başarıyla gönderildi';
		$lang['gonderiliyor'] = 'Gönderiliyor...';
		$lang['oyununuzu_bize_basariyla_gonderdiniz'] = 'Oyununuzu bize başarıyla gönderdiniz.';
		$lang['yatirim_ve_is_modelleri_text'] = 'Yatırım ve İş Modeli Sürecinde Neler Yapıyoruz?';

	}
	elseif($dil == "en")
	{
		$lang['iletisim'] = 'Contact';
		$lang['bize_ulasin'] = 'Contact Us';
		$lang['adres'] = 'Address';
		$lang['hosgeldiniz'] = 'Welcome to';
		$lang['oyun_sektorun_kalbi'] = ', Where the heart of gaming beats at';
		$lang['oyun_sektorune_fayda_text'] = 'Zindhu, the first and only integrated service structure focused on benefiting and developing the game industry, was founded in 2018 in Istanbul.';
		$lang['zindhu_co_catisi_altinda_text'] = 'Zindhu is a game consultancy company that offers customized services aiming to innovate the gaming industry such as: Zindhu HR, Zindhu Games and Zindhu Ventures, providing consultancy with a quality and continuity-focused service approach';
		$lang['zindhu_hr_ulkemizde_oyun_text'] = 'Zindhu HR is the first and only human resources platform focused on the gaming industry in Turkey and recognised by the Turkish Employment Agency. It offers visibility for employees to be seen by over 100 prestigious gaming companies and interact with them. Aside from recruitment, it enables developers, artists designers to enhance their soft-skills as well as their understanding of the industry.';
		$lang['hemen_basvur'] = 'APPLY NOW';
		$lang['hedefimiz'] = 'We are here to';
		$lang['oyun_sektorunu_degistirmek'] = 'Change the gaming industry';
		$lang['mobil_oyunlarin_gelisimini_text'] = 'Zindhu Games, which was established to lead the industry by keeping a close eye on following the development and progression of mobile games from the very first day, brings together game studios, developers, indies and industry experts, enables publishers and producers to work together and have a meaningful collaboration';
		$lang['oyununuzu_paylasin'] = 'Share Your Game';
		$lang['zindhu_ventures_her_turden_text'] = 'Zindhu Ventures is a Zindhu service that manages investment programs for all kinds of gaming companies and studios. It also provides consultancy for investors to manage their investments and channel them into profitable opportunities.';
		$lang['yatirim_almak'] = 'How to receive an investment';
		$lang['yatirimci_olmak'] = 'Want to be an investor?';
		$lang['hakkimizda'] = 'About Us';
		$lang['giris_yap'] = 'Login';
		$lang['bireysel_giris'] = 'Personel Login';
		$lang['kurumsal_giris'] = 'Corporate Login';
		$lang['bireysel'] = 'Personel';
		$lang['kurumsal'] = 'Corporate';
		$lang['giris'] = 'Login';
		$lang['oyun_sektorunde_hayalindeki_isi_bul'] = 'Find Your Dream Job in the Mobile Gaming Industry';
		$lang['turkiyenin_ilk_ve_tek_text'] = 'Discover the first and only human resources portal where you can access over 100 gaming studios, apply for jobs and enhance your skills.';
		$lang['zindhu_hr_100e_yakin_studyo_ve_is_ilani_seni_bekliyor'] = 'Zindhu HR\'da 100\'e yakın stüdyo ve iş ilanı seni bekliyor.';
		$lang['kaydol'] = 'Register';
		$lang['oyun_sektorunde_hayalindeki_isi_nasil_bulabilirsin'] = 'How can you find your dream job?';
		$lang['hemen_profilini_olustur'] = 'Register to the portal';
		$lang['ozgecmisini_eksiksiz_doldur'] = 'Fill out your CV in full';
		$lang['ornek_islerini_icinde_bulundugum_projeleri_ekle'] = 'Add your previous experience. Make sure to add some details';
		$lang['prestijli_oyun_studyolarindan_is_teklifini_al'] = 'Apply for positions or wait for offers. Do not forget, you are always visible to over 100 prestigious gaming studios!';
		$lang['ad_soyad'] = 'Name Surname';
		$lang['e_posta'] = 'E-mail';
		$lang['konu'] = 'Subject';
		$lang['mesajiniz'] = 'Your Message';
		$lang['aydinlatma_metnini_okudum_ve_onayladim'] = ' I have read and accepted the clarification text';
		$lang['formu_gonder'] = 'SEND THE FORM';
		$lang['gonder'] = 'SEND';
		$lang['kisisel_veri_ve_gizlilik_politikasi'] = 'Personal Data and Privacy Policy';
		$lang['acik_pozisyonlar'] = 'Açık Pozisyonlar';
		$lang['kategori_secerek_ilanlara_goz_atin'] = 'Kategori seçerek ilanlara göz atın.';
		$lang['hayalini_oyuna_donusturenler_anlatiyor'] = 'Hayalini oyuna <br>dönüştürenler <br>anlatıyor.';
		$lang['hesabiniz_yok_mu'] = 'Don\'t have an account?';
		$lang['kayit_olun'] = 'Sign up';
		$lang['field_cannot_be_left_blank'] = 'field cannot be left blank';
		$lang['girdiginiz_e_posta_gecerli_degildir'] = 'The email you entered is not valid';
		$lang['aydinlatma_metnini_kabul_etmeden_devam_edemezsiniz'] = 'You cannot continue without accepting the Clarification Text';

		// homepage
		$lang['zindhu_oyun_sektorunun_kalbi'] = '<b>Zindhu:</b> The Heart of Gaming Industry';
		$lang['oyun_sektorunu_olusturan_text'] = 'With its vast developer, studio, and publisher network, Zindhu creates a 360 degrees business view in-game monetization, human resources and business development!';
		$lang['z_hakkimizda'] = 'About Zindhu';
		$lang['istanbul_merkezli_kurulan_text'] = 'Founded in İstanbul, with an aim to serve both in the global and Turkish market, Zindhu created a smart business model in the gaming industry by establishing its sub-brands focusing on various aspects.';
		$lang['sektorel_vizyon_hedeflerimiz'] = '<span>What Are</span><br /> <span>We Here For?</span>';
		$lang['hedefi_turkiye_oyun_text'] = 'With a vision of improving the existing business models, Zindhu aims to become a pioneer in the generation process of new and smart business solutions for gaming industry players in the global market. With its three major sub-brands Zindhu offers multi-perspective problem-solving approaches;';
		$lang['insan_kaynaklari'] = 'Human resources solutions<br />';
		$lang['yatirim_surecleri_ve_kullanici_edinimi'] = 'Growth and testing solutions<br />';
		$lang['is_plani_olusturma_ve_olceklendirme'] = 'Venture capital solutions<br />';
		$lang['alanlarinda_hizmet_veren_text'] = 'In Zindhu, we believe that each company, studio, and ecosystem in the gaming industry has a unique story that needs exclusive strategies to perform better in a fast-growing global market. We keep on working hard to create more value in the gaming industry.';
		$lang['turkiyennin_ilk_ve_tek_text'] = 'Turkey’s first and only gaming industry-focused human resources platform, Zindhu HR aims to create value on all players in the industry by:';
		$lang['basvuru_yap'] = '<span>Click to Explore</span>';
		$lang['zindhu_hrin_tem_hedeleri'] = 'Zindhu HR aims to create value on all players in the industry by:';
		$lang['oyun_sektorunde_calisan_kisilere_text'] = '* Lowering the rotation rate among the employers in the market,';
		$lang['oyun_sektorunde_kariyer_text'] = '* Creating gateways for students and fresh graduates to improve their career development,';
		$lang['potansiyelini_oyun_sektorunde_text'] = '* Applying exclusive consultancy services for junior to senior developers, artists, and designers while they seek the next opportunity in their career,';
		$lang['oyun_studyolarina_buyume_text'] = '* Collaborating with all players in the gaming industry to create a meaningful and sustainable human resources business plan.';
		$lang['yurt_disinda_kariyerine_text'] = '* Among all values, Zindhu HR eliminates the barriers between different markets by generating an international network of talents, studios, and publishers to become the key accelerator in human resource development.';
		$lang['dogrultusunda_platforma_uye_text'] = '';
		$lang['oyun_sektorunde_dogru_text'] = '';
		$lang['human_resources'] = '<span>Human Resources</span>';
		$lang['studyolarin_is_modellerinin_gelistirilmesi_text'] = 'Zindhu Ventures plays a key role in improving the investment processes between studios and venture capitals by clarifying the needs and wants of the players.';
		$lang['yatirim_al'] = '<span>Grow with Investment</span>';
		$lang['yatirimci_ol'] = '<span>Invest in Growth</span>';
		$lang['studyo_tarafinda'] = 'For Studios';
		$lang['oyun_studyosunun_gelisimini_text'] = 'Understanding the “must” concepts of the studio’s growth plan.';
		$lang['yatirimci_sunumu_ve_finansal_text'] = 'Optimizing monetization strategies by applying new business models.';
		$lang['sirket_ve_takim_imajlarini_text'] = 'Creating and improving the corporate image of the business and the team for pitches.';
		$lang['oyun_sektorunde_denenmis_text'] = 'Offering business and technical libraries about the gaming industry.';
		$lang['oyunun_ve_oyun_studyosunun_text'] = 'Competitor and market capitalization analysis.';
		$lang['ozelinde_studyo_ile_text'] = '';
		$lang['yatirimci_tarafinda'] = 'For Investors';
		$lang['buyume_potansiyeli_yüksek_text'] = 'Scouting target studios that carry the potential of becoming a hit.';
		$lang['yatirim_surecleri_boyunca_text'] = 'During the investment period, supporting the studio with exclusive business development incentives in human resources, marketing, and user acquisition.';
		$lang['oyun_studyosunun_geliştigi_text'] = 'Partnering with publishers that leads the trend in its genre, expanding the business plan through various markets.';
		$lang['sureclerinde_yaitrim_planinda_text'] = '';
		$lang['oyun_studyosunun_degerlendirilmesi_text'] = 'Business and technical evaluation of the studio / determining initial needs';
		$lang['oyun_studyosuna_uygun_bir_finansman_modelinin_bulunmasi'] = 'Creating customer exclusive financial and monetization models';
		$lang['bulunan_model_dahilinde_oyun_studyosu_ile_calismalarin_baslamasi'] = 'Initialization of the growth plans together with testing, deviating, and optimizing processes to utilize best KPIs.';
		$lang['is_modelinin_ve_finansal_projeksiyonlarin_optimizasyonu'] = 'Network development with multiple partners to ensure continuous improvement in the long run.';
		$lang['oyun_studyosu_ve_yatirimci_arasindaki_text'] = 'Ensuring sustainable and profitable growth in the created ecosystem and expanding knowledge in multiple genres and categories.';
		$lang['oyun_sektorunun_gelisiminin_text'] = 'We believe that the gaming industry takes its power from creative and result-oriented studios and coherency in the human resources allocations. With Zindhu Games, the problem of taking “the product” to the next level is resolved by;';
		$lang['yayinci_ariyorum'] = '<span>Looking for a publisher</span>';
		$lang['sorularim_var'] = '<span>Let’s set a meeting</span>';
		$lang['sektoru_ve_studyolarin_mevcut_asamalarini_degerlendirip'] = 'Business and technical evaluation of the studio,';
		$lang['studyolarin_varsa_sorun_text'] = 'Understanding the key drawbacks and creating execution plans in order to eliminate them,';
		$lang['uretilen_cozumlere_bagli_text'] = 'Managing the triggered improvement with its multidisciplinary sub-brands to ensure meaningful growth,';
		$lang['kisisellestirilmis_cozumlerle_zindhu_text'] = 'Managing the triggered improvement with its multidisciplinary sub-brands to ensure meaningful growth.';
		$lang['oyun_studyosunun_gelistirdiği_text'] = 'With past experiences and proven models in the gaming industry, arranging profitable solutions for publisher relations and monitoring the generated business model.';
		$lang['icin_ozellestirilmis_hizmetler_text'] = '';
		$lang['turkiye_oyun_pazarında_text'] = '<b>Growing in the Turkish gaming industry, Zindhu aims to generate meaningful and growth-oriented solutions to scale up the global market.</b>';
		$lang['aklinizda_sorularmi_var'] = 'Do you have questions in mind?';
		$lang['bir_gorusme_ayarlayalim'] = 'Let’s set a meeting.';
		$lang['bizi_takip_edin'] = '';
		$lang['z_iletisim'] = 'Contact';
		$lang['z_bize_ulasin'] = '';
		$lang['z_ad_soyad'] = 'Name Surname';
		$lang['z_e_posta'] = 'E-mail';
		$lang['mesaj'] = 'Message';
		$lang['kisisel_veri_ve_gizlilik_politikasi'] = 'Personal Data and Privacy Policy';
		$lang['oyununuzu_paylasin'] = 'Share your game';
		$lang['oyun_linkiniz'] = 'Your game link';
		$lang['oyununuzu_anlatin'] = 'Please write about your game';
		$lang['anladim'] = '<span>Got it</span>';
		$lang['mesajiniz_basariyla_gonderildi'] = 'Your message has been successfully sent.';
		$lang['gonderiliyor'] = 'Sending ...';
		$lang['oyununuzu_bize_basariyla_gonderdiniz'] = 'Your game has been successfully sent.';
		$lang['yatirim_ve_is_modelleri_text'] = 'The Process';

	}

	return $lang[$key];
}

?>