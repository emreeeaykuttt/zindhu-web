<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conversation_model extends CI_Model {

	var $table = 'conversations';
	var $users_table = 'users';

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

	public function getAllByCompanyIDAndUserID($company_id, $user_id)
	{
		$this->db->where('company_id', $company_id);
		$this->db->where('user_id', $user_id);

		$query = $this->db->order_by('id', 'asc')->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function unreadCountByCompanyIDAndUserID($company_id, $user_id, $sender_type = '')
	{
		$this->db->where('company_id', $company_id);
		$this->db->where('user_id', $user_id);
		
		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0) {
			$last_query = $this->db->where('is_read', 0)->where('sender_type', $sender_type)->where('company_id', $company_id)->where('user_id', $user_id)->get($this->table);

			return $last_query->num_rows();	
		} else {
			return FALSE;
		}
	}

	public function unreadCountByCompanyID($company_id)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->where('company_id', $company_id);
		$this->db->where('sender_type', 'user');
		$this->db->where('is_read', 0);
		$this->db->group_by('user_id');
		
		$query = $this->db->get($this->table);

		return $query->num_rows();
	}

	public function unreadCountByUserID($user_id)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->where('user_id', $user_id);
		$this->db->where('sender_type', 'company');
		$this->db->where('is_read', 0);
		$this->db->group_by('company_id');
		
		$query = $this->db->get($this->table);

		return $query->num_rows();
	}

	public function getCountByUserID($user_id)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->where('user_id', $user_id);
		$this->db->where('sender_type', 'user');
		$this->db->group_by('company_id');
		
		$query = $this->db->get($this->table);

		return $query->num_rows();
	}

	public function getCountByCompanyID($company_id)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->where($this->table .'.company_id', $company_id);
		$this->db->where($this->table .'.sender_type', 'company');

		if (!empty($this->cv_status)) {
            $this->db->where('cv_status', $this->cv_status);
        }

        if (!empty($this->is_working_system)) {
            $this->db->group_start();
            $this->db->where('company_ids', null);

            if (!empty($this->our_employees)) {
                foreach ($this->our_employees as $key => $value) {
                    $this->db->or_where($this->users_table .'.id', $value['user_id']);
                }
            }
            $this->db->group_end();
        }

        $this->db->join($this->users_table, $this->users_table .'.id = '. $this->table .'.user_id', 'left');

		$this->db->group_by($this->table .'.user_id');
		
		$query = $this->db->get($this->table);

		return $query->num_rows();
	}

	public function getAllByCompanyIDJoinUser($company_id)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->select($this->users_table . '.*, ' . $this->table . '.company_id');

		$this->db->join($this->users_table, $this->users_table . '.id = '. $this->table .'.user_id', 'left');
		$this->db->where($this->table .'.company_id', $company_id);
		$this->db->group_by($this->table .'.user_id');
		
		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return FALSE;
		}
	}

	public function getCount()
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->group_by('user_id');
		$this->db->group_by('company_id');
		
		$query = $this->db->get($this->table);

		return $query->num_rows();
	}

	public function getLastMessage($company_id, $user_id, $sender_type = '')
	{
		$this->db->where('company_id', $company_id);
		$this->db->where('user_id', $user_id);
		$this->db->where('sender_type', $sender_type);
		$this->db->order_by('id', 'desc');
		
		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0) {
			return $query->row();	
		} else {
			return FALSE;
		}
	}

	public function getUnreadMessages($company_id, $user_id, $sender_type = '')
	{
		$this->db->where('company_id', $company_id);
		$this->db->where('user_id', $user_id);
		$this->db->where('sender_type', $sender_type);
		$this->db->where('is_read', 0);
		$this->db->order_by('id', 'desc');
		
		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0) {

			return $query->result_array();	
		} else {
			return FALSE;
		}
	}
}