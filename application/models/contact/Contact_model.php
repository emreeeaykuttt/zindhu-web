<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_model extends CI_Model {

	var $table = 'contacts';
	var $users_table = 'users';
	var $companies_table = 'companies';

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function deleteByUserID($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->delete($this->table);
    }

    public function getAllByUserID($user_id)
	{
		$this->db->where('user_id', $user_id);

		$query = $this->db->order_by('id', 'asc')->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getAllByCompanyID($company_id)
	{
		$this->db->where('company_id', $company_id);

		$query = $this->db->order_by('id', 'asc')->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function unreadCountBySenderType($sender_type)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->where('is_read', 0);
		$this->db->where('sender_type', $sender_type);

		if ($sender_type == 'company') {
			$this->db->group_by('company_id');
		} elseif ($sender_type == 'user') {
			$this->db->group_by('user_id');
		}
		
		$query = $this->db->get($this->table);

		return $query->num_rows();
	}

	public function unreadCountByCompanyID($company_id, $sender_type = '')
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->where('company_id', $company_id);
		$this->db->where('sender_type', $sender_type);
		$this->db->where('is_read', 0);
		$this->db->group_by('company_id');
		
		$query = $this->db->get($this->table);

		return $query->num_rows();
	}

	public function unreadCountByUserID($user_id, $sender_type = '')
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->where('user_id', $user_id);
		$this->db->where('sender_type', $sender_type);
		$this->db->where('is_read', 0);
		$this->db->group_by('user_id');
		
		$query = $this->db->get($this->table);

		return $query->num_rows();
	}

	public function getCountByUserID($user_id)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->where('user_id', $user_id);
		$this->db->where('sender_type', 'user');
		$this->db->group_by('company_id');
		
		$query = $this->db->get($this->table);

		return $query->num_rows();
	}

	public function getCountByCompanyID($company_id)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->where('company_id', $company_id);
		$this->db->where('sender_type', 'company');
		$this->db->group_by('user_id');
		
		$query = $this->db->get($this->table);

		return $query->num_rows();
	}

	public function getAllByCompanyIDJoinUser($company_id)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->select($this->users_table . '.*, ' . $this->table . '.company_id');

		$this->db->join($this->users_table, $this->users_table . '.id = '. $this->table .'.user_id', 'left');
		$this->db->where($this->table .'.company_id', $company_id);
		$this->db->group_by($this->table .'.user_id');
		
		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return FALSE;
		}
	}

	public function getCount()
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->group_by('user_id');
		$this->db->group_by('company_id');
		
		$query = $this->db->get($this->table);

		return $query->num_rows();
	}

	public function getUnreadMessagesByUserID($user_id, $sender_type = '')
	{
		$this->db->where('user_id', $user_id);
		$this->db->where('sender_type', $sender_type);
		$this->db->where('is_read', 0);
		$this->db->order_by('id', 'desc');
		
		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0) {

			return $query->result_array();	
		} else {
			return FALSE;
		}
	}

	public function getUnreadMessagesByCompanyID($company_id, $sender_type = '')
	{
		$this->db->where('company_id', $company_id);
		$this->db->where('sender_type', $sender_type);
		$this->db->where('is_read', 0);
		$this->db->order_by('id', 'desc');
		
		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0) {

			return $query->result_array();	
		} else {
			return FALSE;
		}
	}
}