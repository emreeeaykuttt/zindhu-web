<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	var $table = 'users';
    var $ads_table = 'ads';
    var $ad_users_table = 'ad_users';
    var $watch_users_table = 'watch_users';
    var $conversations_table = 'conversations';
    var $contacts_table = 'contacts';
    var $company_hires_table = 'company_hires';
    var $personal_informations_table = 'personal_informations';
    var $educational_background_table = 'educational_background';
    var $used_programs_table = 'used_programs';
    var $game_choices_table = 'game_choices';
    var $cities_table = 'cities';
    var $positions_table = 'positions';
    var $column_order = array('firstname', 'positions.title', 'cities.city', null);
    var $column_search = array('firstname', 'positions.title', 'cities.city');
    var $order = array('users.id' => 'desc');

    private function _search($data)
    {
        $this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
        
        $this->db->select($this->table . '.*, ' . $this->cities_table . '.city as city, ' . $this->positions_table . '.title as position');

        $this->db->from($this->table);

        $this->db->where($this->table . '.user_confirm', 1);

        if (!empty($this->cv_status)) {
            $this->db->where('cv_status', $this->cv_status);
        }

        if (!empty($this->is_working_system)) {
            $this->db->group_start();
            $this->db->where('company_ids', null);

            if (!empty($this->our_employees)) {
                foreach ($this->our_employees as $key => $value) {
                    $this->db->or_where($this->table . '.id', $value['user_id']);
                }
            }
            $this->db->group_end();
        }

        if (!empty($data['cv'])) {
            if ($data['cv'] == 'new') {
                $this->db->where($this->table . '.cv_created_at >=', date('Y-m-d H:i:s', strtotime('-24 hours')));   
                $this->db->where($this->table . '.admin_confirm', 'approved');
            } else if ($data['cv'] == 'confirm') {
                $this->db->where($this->table . '.admin_confirm', 'pending');
            } else {
                $this->db->where($this->table . '.admin_confirm', 'approved');
            }
        } else {
            $this->db->where($this->table . '.admin_confirm', 'approved');
        }

        if (!empty($data['watch'])) {
            $this->db->join($this->watch_users_table, $this->watch_users_table . '.user_id = '. $this->table .'.id', 'left');
            $this->db->where($this->watch_users_table . '.company_id', $data['company_id']);
        }

        if (!empty($data['ad'])) {
            $this->db->join($this->ad_users_table, $this->ad_users_table . '.user_id = '. $this->table .'.id', 'left');
            $this->db->join($this->ads_table, $this->ads_table . '.id = '. $this->ad_users_table .'.ad_id', 'left');
            $this->db->where($this->ad_users_table . '.ad_id', $data['ad']);
            $this->db->where($this->ad_users_table . '.apply_date !=', NULL);
            $this->db->where($this->ads_table . '.company_id', $data['company_id']);
        }

        if (!empty($data['ad_user'])) {
            $this->db->join($this->ad_users_table, $this->ad_users_table . '.user_id = '. $this->table .'.id', 'left');
            $this->db->join($this->ads_table, $this->ads_table . '.id = '. $this->ad_users_table .'.ad_id', 'left');
            $this->db->where($this->ad_users_table . '.ad_id', $data['ad_user']);
            $this->db->where($this->ad_users_table . '.apply_date !=', NULL);
        }

        if (!empty($data['conversation_company'])) {
            if (!empty($data['unread'])) {
                $this->db->where('is_read', 0);
                $this->db->where('sender_type', 'user');
            }
            
            $this->db->join($this->conversations_table, $this->conversations_table . '.user_id = '. $this->table .'.id', 'left');
            $this->db->where($this->conversations_table . '.company_id', $data['company_id']);
            $this->db->group_by($this->conversations_table . '.user_id');
        }

        if (!empty($data['contact_user'])) {
            $this->db->join($this->contacts_table, $this->contacts_table . '.user_id = '. $this->table .'.id', 'left');
            $this->db->where($this->contacts_table . '.user_id !=', 0);
            $this->db->group_by($this->contacts_table . '.user_id');
        }

        if (!empty($data['hires'])) {
            $this->db->join($this->company_hires_table, $this->company_hires_table . '.user_id = ' . $this->table . '.id', 'right');
            $this->db->where($this->company_hires_table . '.startdate !=', null);
            $this->db->group_by($this->company_hires_table . '.user_id');
        }

        $this->db->join($this->personal_informations_table, $this->personal_informations_table . '.user_id = '. $this->table .'.id', 'left');
        $this->db->join($this->cities_table, $this->cities_table . '.id = '. $this->personal_informations_table .'.city_id', 'left');
        $this->db->join($this->positions_table, $this->positions_table . '.id = '. $this->personal_informations_table .'.position_id', 'left');

        if (!empty($data['position_id'])) {
            $this->db->where($this->personal_informations_table . '.position_id', $data['position_id']);
        }

        if (!empty($data['city_id'])) {
            $this->db->where($this->personal_informations_table . '.city_id', $data['city_id']);
        }

        if (!empty($data['city_json'])) {
            $this->db->like($this->personal_informations_table . '.city_json', '"' . $data['city_json'] . '"');
        }

        if (!empty($data['education_level'])) {
            $this->db->join($this->educational_background_table, $this->educational_background_table . '.user_id = '. $this->table .'.id', 'left');
            $this->db->where($this->educational_background_table . '.education_level', $data['education_level']);

            if (!empty($data['university_id'])) {
                $this->db->where($this->educational_background_table . '.university_id', $data['university_id']);
            }

            $this->db->group_by($this->educational_background_table . '.user_id');
        } else {
            if (!empty($data['university_id'])) {
                $this->db->join($this->educational_background_table, $this->educational_background_table . '.user_id = '. $this->table .'.id', 'left');
                $this->db->where($this->educational_background_table . '.university_id', $data['university_id']);
                $this->db->group_by($this->educational_background_table . '.user_id');
            }
        }

        if (!empty($data['program_id'])) {
            $this->db->join($this->used_programs_table, $this->used_programs_table . '.user_id = '. $this->table .'.id', 'left');
            $this->db->where($this->used_programs_table . '.program_id', $data['program_id']);

            if (!empty($data['program_duration'])) {
                $this->db->where($this->used_programs_table . '.duration >=', $data['program_duration']);
            }
        } else {
            if (!empty($data['program_duration'])) {
                $this->db->join($this->used_programs_table, $this->used_programs_table . '.user_id = '. $this->table .'.id', 'left');
                $this->db->where($this->used_programs_table . '.duration >=', $data['program_duration']);
            }
        }

        if (!empty($data['game_future'])) {
            $this->db->join($this->game_choices_table, $this->game_choices_table . '.user_id = '. $this->table .'.id', 'left');
            $this->db->like($this->game_choices_table . '.future_json', '"' . $data['game_future'] . '"');

            if (!empty($data['game_past'])) {
                $this->db->like($this->game_choices_table . '.past_json', '"' . $data['game_past'] . '"');
            }
        } else {
            if (!empty($data['game_past'])) {
                $this->db->join($this->game_choices_table, $this->game_choices_table . '.user_id = '. $this->table .'.id', 'left');
                $this->db->like($this->game_choices_table . '.past_json', '"' . $data['game_past'] . '"');
            }
        }

        if (!empty($data['is_remote'])) {
            $this->db->where($this->personal_informations_table . '.is_remote', 1);
        }
    }

    private function _getDatatablesQuery($data)
    {
        $this->_search($data);

        $i = 0;

        foreach ($this->column_search as $item)
        {
            if($_POST['search']['value'])
            {
                
                if($i===0)
                {
                    $this->db->group_start();

                    if ($item == 'firstname') 
                    {
                        $this->db->like('CONCAT(firstname, " ", lastname)', $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->like($item, $_POST['search']['value']);
                    }
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
        
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function getDatatables($data)
    {
        $this->_getDatatablesQuery($data);

        if($_POST['length'] != -1)
        {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function getCountFiltered($data)
    {
        $this->_getDatatablesQuery($data);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getCountAll($data)
    {
        $this->_search($data);

        return $this->db->count_all_results();
    }

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function deleteByID($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function getByID($id)
    {
        $this->db->where('id', $id);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return FALSE;
        }
    }

    public function emailControl($email, $user_id)
    {
        $this->db->where('email', $email);
        $this->db->where('id !=', $user_id);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	public function getByEmail($email)
	{
		$this->db->where('email', $email);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}

    public function getAllByNameAndSurname($name_and_surname)
    {
        $this->db->like('CONCAT(firstname, " ", lastname)', $name_and_surname);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    public function passwordValidate($email, $password)
    {
        $query = $this->db->select('*')->from($this->table)->where('email', $email)->get()->row();

        if ($query != "")
        {
            $hashed_password = $query->password;

            if (password_verify($password, $hashed_password) && $query->verified == 1)
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }

	public function validate($email, $password)
    {
        $query = $this->db->select('*')->from($this->table)->where('email', $email)->get()->row();

        if ($query != "")
        {
            $hashed_password = $query->password;
            $id = $query->id;

            if (password_verify($password, $hashed_password) && $query->verified == 1)
            {
                $last_login = date('Y-m-d H:i:s', NOW_DATE_TIME);
                $this->db->trans_start();
                $this->db->trans_strict(FALSE);
                $this->db->update($this->table, array('last_login' => $last_login, 'last_login_ip_address' => $this->input->ip_address()), array('id' => $id));
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                    return FALSE;
                }
                else
                {
                    $this->db->trans_commit();
                    return $query;
                }
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }

    public function verification($verification_token, $id)
    {
        $this->db->update($this->table, 
        	array('verified' => 1, 'verification_date' => date('Y-m-d H:i:s', NOW_DATE_TIME)), 
        	array('id' => $id, 'verification_token' => $verification_token)
        );

        return $this->db->affected_rows();
    }

    public function passwordUpdate($data, $remember_token, $id)
    {
    	$this->db->update($this->table, $data, array('id' => $id, 'remember_token' => $remember_token, 'verified' => 1));

    	return $this->db->affected_rows();
    }

    public function getCount()
    {
        $this->db->where('verified', 1);
        $this->db->where('user_confirm', 1);
        $this->db->where('admin_confirm', 'approved');

        if (!empty($this->cv_status)) {
            $this->db->where('cv_status', $this->cv_status);
        }

        if (!empty($this->is_working_system)) {
            $this->db->group_start();
            $this->db->where('company_ids', null);

            if (!empty($this->our_employees)) {
                foreach ($this->our_employees as $key => $value) {
                    $this->db->or_where('id', $value['user_id']);
                }
            }
            $this->db->group_end();
        }

        $query = $this->db->get($this->table);

        return $query->num_rows();
    }

    public function getCountNew()
    {
        $this->db->where('verified', 1);
        $this->db->where('user_confirm', 1);
        $this->db->where('admin_confirm', 'approved');
        $this->db->where('cv_created_at >=', date('Y-m-d H:i:s', strtotime('-24 hours')));

        $query = $this->db->get($this->table);

        return $query->num_rows();
    }

}