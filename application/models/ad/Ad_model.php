<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ad_model extends CI_Model {

	var $table = 'ads';
    var $companies_table = 'companies';
    var $ad_users = 'ad_users';
    var $cities_table = 'cities';
    var $districts_table = 'districts';
    var $column_order = array('title', 'company_name', 'status', 'admin_confirm', null);
    var $column_search = array('title', 'company_name', 'status', 'admin_confirm');
    var $order = array('ads.id' => 'desc');

    private function _search()
    {
        $this->db->query("SET lc_time_names='tr_TR'");
        
        $this->db->select(
            $this->table . '.*, ' . 
            $this->cities_table . '.city as city, ' . 
            $this->districts_table . '.district as district, ' . 
            $this->companies_table . '.company_name as company_name,' .
            'DATE_FORMAT('. $this->table . '.created_at, "%d %M %Y") as created_date'
        );

        $this->db->from($this->table);

        $this->db->join($this->companies_table, $this->companies_table . '.id = '. $this->table .'.company_id', 'left');
        $this->db->join($this->cities_table, $this->cities_table . '.id = '. $this->table .'.city_id', 'left');
        $this->db->join($this->districts_table, $this->districts_table . '.id = '. $this->table .'.district_id', 'left');

        if (!empty($this->company_id)) {
            $this->db->where('company_id', $this->company_id);
        }

        if (!empty($this->status)) {
            $this->db->where('status', $this->status);
        }

        if (!empty($this->admin_confirm)) {
            $this->db->where('admin_confirm', $this->admin_confirm);
        }
    }

    private function _getDatatablesQuery()
    {
        $this->_search();

        $i = 0;

        foreach ($this->column_search as $item)
        {
            if($_POST['search']['value'])
            {
                
                if($i===0)
                {
                    $this->db->group_start();

                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
        
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function getDatatables()
    {
        $this->_getDatatablesQuery();

        if($_POST['length'] != -1)
        {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function getCountFiltered()
    {
        $this->_getDatatablesQuery();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getCountAll()
    {
        $this->_search();
        return $this->db->count_all_results();
    }

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function deleteByID($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function getByID($id)
    {
        $this->db->query("SET lc_time_names='tr_TR'");

        $this->db->select(
            $this->table . '.*, ' . 
            $this->cities_table . '.city, ' . 
            $this->districts_table . '.district, ' . 
            $this->companies_table . '.company_name,' .
            $this->companies_table . '.number_employees,' .
            'DATE_FORMAT('. $this->table . '.created_at, "%d %M %Y") as created_date'
         );

        $this->db->join($this->companies_table, $this->companies_table . '.id = '. $this->table .'.company_id', 'left');
        $this->db->join($this->cities_table, $this->cities_table . '.id = '. $this->table .'.city_id', 'left');
        $this->db->join($this->districts_table, $this->districts_table . '.id = '. $this->table .'.district_id', 'left');

        $this->db->where($this->table . '.id', $id);

        if (!empty($this->status)) {
            $this->db->where('status', $this->status);
        }

        if (!empty($this->admin_confirm)) {
            $this->db->where('admin_confirm', $this->admin_confirm);
        }

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    public function getAll()
    {
        $this->db->query("SET lc_time_names='tr_TR'");

        $this->db->select(
            $this->table . '.*, ' .
            $this->cities_table . '.city, ' . 
            $this->districts_table . '.district, ' . 
            $this->companies_table . '.company_name,' .
            $this->companies_table . '.number_employees,' .
            'DATE_FORMAT('. $this->table . '.created_at, "%d %M %Y") as created_date'
         );

        $this->db->join($this->companies_table, $this->companies_table . '.id = '. $this->table .'.company_id', 'left');
        $this->db->join($this->cities_table, $this->cities_table . '.id = '. $this->table .'.city_id', 'left');
        $this->db->join($this->districts_table, $this->districts_table . '.id = '. $this->table .'.district_id', 'left');

        if (!empty($this->status)) {
            $this->db->where('status', $this->status);
        }

        if (!empty($this->admin_confirm)) {
            $this->db->where('admin_confirm', $this->admin_confirm);
        }

        if (!empty($this->excluding_id)) {
            $this->db->where($this->table . '.id !=', $this->excluding_id);
        }

        if (!empty($this->search)) {
            $search = $this->search;

            if (!empty($search['keywords'])) {
                $this->db->group_start();
                $this->db->like('title', $search['keywords']);
                $this->db->or_like('short_description', $search['keywords']);
                $this->db->or_like('job_description', $search['keywords']);
                $this->db->group_end();
            }
            
            if (!empty($search['position']) && is_array($search['position'])) {
                $this->db->group_start();
                foreach ($search['position'] as $key => $position) {
                    if ($key == 0) {
                        $this->db->like('position_json', '"'.$position.'"');
                    } else {
                        $this->db->or_like('position_json', '"'.$position.'"');
                    }
                }
                $this->db->group_end();
            }

            if (!empty($search['city']) && is_array($search['city'])) {
                $this->db->group_start();
                foreach ($search['city'] as $key => $city) {
                    if ($key == 0) {
                        $this->db->where($this->companies_table . '.city_id', $city);
                    } else {
                        $this->db->or_where($this->companies_table . '.city_id', $city);
                    }
                }
                $this->db->group_end();
            }

            if (!empty($search['study_type']) && is_array($search['study_type'])) {
                $this->db->group_start();
                foreach ($search['study_type'] as $key => $study_type) {
                    if ($key == 0) {
                        $this->db->where('study_type', $study_type);
                    } else {
                        $this->db->or_where('study_type', $study_type);
                    }
                }
                $this->db->group_end();
            }

            if (!empty($search['transaction'])) {
                $this->db->join($this->ad_users, $this->ad_users . '.ad_id = '. $this->table .'.id', 'left');
                $this->db->where($this->ad_users . '.user_id', $search['user_id']);
                
                $this->db->group_start();
                foreach ($search['transaction'] as $key => $transaction) {

                    if ($key == 0) {
                        if ($transaction == 'views') {
                            $this->db->where('views_date !=', NULL);
                        } elseif ($transaction == 'follow') {
                            $this->db->where('follow_date !=', NULL);
                        } elseif ($transaction == 'apply') {
                            $this->db->where('apply_date !=', NULL);
                        }
                    } else {
                        if ($transaction == 'views') {
                            $this->db->or_where('views_date !=', NULL);
                        } elseif ($transaction == 'follow') {
                            $this->db->or_where('follow_date !=', NULL);
                        } elseif ($transaction == 'apply') {
                            $this->db->or_where('apply_date !=', NULL);
                        }
                    }

                }
                $this->db->group_end();
            }
        }

        if (empty($this->total_rows)) {
            $limit_start = $this->per_page * ($this->page - 1);
            $this->db->limit($this->per_page, $limit_start);
        }

        $this->db->order_by('created_at', 'desc');

        $query = $this->db->get($this->table);

        $num_rows = $query->num_rows();

        if ($num_rows > 0) {

            if (!empty($this->total_rows)) {
                return $num_rows;
            } else {
                return $query->result_array();
            }

        } else {
            return FALSE;
        }
    }

    public function getCount()
    {
        $query = $this->db->get($this->table);

        return $query->num_rows();
    }

    public function getCountByPending()
    {
        $this->db->where('admin_confirm', 'pending');

        $query = $this->db->get($this->table);

        return $query->num_rows();
    }

    public function getCountByPositionID($position_arr = array(''))
    {
        $this->db->where('status', 1);
        $this->db->where('admin_confirm', 'approved');

        $this->db->group_start();
        foreach ($position_arr as $key => $position) {
            if ($key == 0) {
                $this->db->like('position_json', '"'.$position.'"');
            } else {
                $this->db->or_like('position_json', '"'.$position.'"');
            }
        }
        $this->db->group_end(); 

        $query = $this->db->get($this->table);

        return $query->num_rows();
    }

}