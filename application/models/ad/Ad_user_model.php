<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ad_user_model extends CI_Model {

	var $table = 'ad_users';
    var $ads_table = 'ads';

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function deleteByID($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function getByID($id)
    {
        $query = $this->db->where('id', $id)->get($this->table);

        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return FALSE;
        }
    }

    public function getAll()
    {
        $this->db->select(
            $this->table . '.*, ' . 
            $this->ads_table . '.title,' .
            $this->ads_table . '.is_remote,' .
            $this->ads_table . '.slug'
        );

        if (!empty($this->user_id)) {
            $this->db->where('user_id', $this->user_id);
        }

        if (!empty($this->follow_date)) {
            $this->db->where('follow_date !=', NULL);
        }

        $this->db->where($this->ads_table . '.status', 1);
        $this->db->where($this->ads_table . '.admin_confirm', 'approved');

        $this->db->join($this->ads_table, $this->ads_table . '.id = '. $this->table .'.ad_id', 'left');

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    public function getByAdIDAndUserID($ad_id, $user_id)
    {
        $this->db->where('ad_id', $ad_id);
        $this->db->where('user_id', $user_id);
        
        $query = $this->db->get($this->table);

        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return FALSE;
        }
    }

}