<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_model extends CI_Model {

	var $table = 'posts';
    var $column_order = array('post_type', 'title', null);
    var $column_search = array('post_type', 'title');
    var $order = array('id' => 'desc');

    private function _getDatatablesQuery($data)
    {
        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column_search as $item)
        {
            if($_POST['search']['value'])
            {
                
                if($i===0)
                {
                    $this->db->group_start();

                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
        
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function getDatatables($data)
    {
        $this->_getDatatablesQuery($data);

        if($_POST['length'] != -1)
        {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function getCountFiltered($data)
    {
        $this->_getDatatablesQuery($data);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getCountAll($data)
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function deleteByID($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function getByID($id)
    {
        $this->db->where('id', $id);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    public function getByType($post_type = '')
    {
        if (!empty($post_type)) {
            $this->db->where('post_type', $post_type);
        }

        if (!empty($this->limit)) {
            $this->db->limit($this->limit);
        }

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }

    public function getCount($post_type = '')
    {
        if (!empty($post_type)) {
            $this->db->where('post_type', $post_type);
        }

        $query = $this->db->get($this->table);

        return $query->num_rows();
    }

}