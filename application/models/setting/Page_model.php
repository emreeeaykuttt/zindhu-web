<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_model extends CI_Model {

	var $table = 'pages';
	var $column_order = array('title', 'link', 'sort', 'status', 'default_page', null);
	var $column_search = array('title', 'link', 'sort', 'status', 'default_page', ); 
	var $order = array('id' => 'desc'); 

	public function __construct()
	{
		parent::__construct();
	}

	private function _get_datatables_query()
	{
		
		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value']) 
			{
				
				if($i===0) 
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) 
					$this->db->group_end(); 
			}
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function get_datatables($lang_id)
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->where('lang_id', $lang_id);
		$this->db->where('parent_id !=', 0);
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function count_filtered($lang_id)
	{
		$this->_get_datatables_query();
		$this->db->where('lang_id', $lang_id);
		$this->db->where('parent_id !=', 0);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($lang_id)
	{
		$this->db->from($this->table);
		$this->db->where('lang_id', $lang_id);
		$this->db->where('parent_id !=', 0);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($content_id)
	{
		$this->db->where('content_id', $content_id);
		$this->db->where('default_page !=', 1);
		$this->db->delete($this->table);
	}

	public function get_by_content_id($content_id)
	{	
		$this->db->select('id');
		$this->db->from($this->table);
		$this->db->where('content_id',$content_id);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_all($lang_id)
	{
		$query = $this->db->where('lang_id', $lang_id)->get('pages');

		return $query->result_array();
	}

	public function page_detail($page_id)
	{
		$this->db->where('lang_id', $this->lang_id);
		$this->db->where('content_id', $page_id);
		$query = $this->db->get($this->table);
		
		if ($query->num_rows() > 0) 
		{

			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}

}
