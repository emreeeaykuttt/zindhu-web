<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_content_relation_model extends CI_Model {

	var $table = 'page_content_relations';

	public $id;
	public $modul;

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}


}