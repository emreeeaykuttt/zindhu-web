<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Used_program_model extends CI_Model {

	var $table = 'used_programs';

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function deleteByID($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	public function getAll()
	{
		$query = $this->db->get($this->table)->result_array();
		return $query;
	}

	public function getByID($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function getAllByUserID($user_id)
	{
		$this->db->select('*');
		$this->db->order_by('id', 'desc');
		$this->db->where('user_id', $user_id);
		$query = $this->db->get($this->table);

		return $query->result_array();
	}

	public function programControl($program_id, $user_id)
	{
		$this->db->where('program_id', $program_id);
		$this->db->where('user_id', $user_id);
		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function usedProgramControl($user_id)
	{
		$this->db->where('user_id', $user_id);
		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}