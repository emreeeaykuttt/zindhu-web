<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_model extends CI_Model {

	var $table = 'companies';
    var $conversations_table = 'conversations';
    var $contacts_table = 'contacts';
    var $company_hires_table = 'company_hires';
    var $column_order = array('company_name', 'firstname', 'email', 'phone', null);
    var $column_search = array('company_name', 'firstname', 'email', 'phone');
    var $order = array('companies.id' => 'desc');

    private function _getDatatablesQuery($data)
    {
        $this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

        $this->db->select($this->table . '.*');

        $this->db->from($this->table);

        $this->db->where('verified', 1);

        if (!empty($data['conversation_user'])) {
            if (!empty($data['unread'])) {
                $this->db->where('is_read', 0);
                $this->db->where('sender_type', 'company');
            }

            $this->db->join($this->conversations_table, $this->conversations_table . '.company_id = '. $this->table .'.id', 'left');
            $this->db->where('user_id', $data['user_id']);
            $this->db->group_by('company_id');
        }

        if (!empty($data['conversation_company'])) {
            $this->db->join($this->conversations_table, $this->conversations_table . '.company_id = '. $this->table .'.id', 'right');
            $this->db->group_by('company_id');
        }

        if (!empty($data['contact_company'])) {
            $this->db->join($this->contacts_table, $this->contacts_table . '.company_id = '. $this->table .'.id', 'right');
            $this->db->where($this->contacts_table . '.company_id !=', 0);
            $this->db->group_by('company_id');
        }

        if (!empty($data['hires'])) {
            $this->db->join($this->company_hires_table, $this->company_hires_table . '.company_id = ' . $this->table . '.id', 'right');
            $this->db->where($this->company_hires_table . '.startdate !=', null);
            $this->db->group_by($this->company_hires_table . '.company_id');
        }

        $i = 0;

        foreach ($this->column_search as $item)
        {
            if($_POST['search']['value'])
            {
                
                if($i===0)
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    if ($item == 'firstname') 
                    {
                        $this->db->or_like('CONCAT(firstname, " ", lastname)', $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }
                }

                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
        
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function getDatatables($data)
    {
        $this->_getDatatablesQuery($data);

        if($_POST['length'] != -1)
        {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function getCountFiltered($data)
    {
        $this->_getDatatablesQuery($data);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getCountAll($data)
    {
        $this->db->select($this->table . '.*');

        $this->db->where('verified', 1);

        if (!empty($data['conversation_user'])) {
            if (!empty($data['unread'])) {
                $this->db->where('is_read', 0);
                $this->db->where('sender_type', 'company');
            }

            $this->db->join($this->conversations_table, $this->conversations_table . '.company_id = '. $this->table .'.id', 'left');
            $this->db->where('user_id', $data['user_id']);
            $this->db->group_by('company_id');
        }

        if (!empty($data['conversation_company'])) {
            $this->db->join($this->conversations_table, $this->conversations_table . '.company_id = '. $this->table .'.id', 'right');
            $this->db->group_by('company_id');
        }

        if (!empty($data['contact_company'])) {
            $this->db->join($this->contacts_table, $this->contacts_table . '.company_id = '. $this->table .'.id', 'right');
            $this->db->where($this->contacts_table . '.company_id !=', 0);
            $this->db->group_by('company_id');
        }

        if (!empty($data['hires'])) {
            $this->db->join($this->company_hires_table, $this->company_hires_table . '.company_id = ' . $this->table . '.id', 'right');
            $this->db->where($this->company_hires_table . '.startdate !=', null);
            $this->db->group_by($this->company_hires_table . '.company_id');
        }

        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function getByID($id)
    {
        $this->db->where($this->table . '.id', $id);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return FALSE;
        }
    }

    public function getAllByIDs($id_arr)
    {
        $this->db->group_start();
        foreach ($id_arr as $key => $id) {
            if ($key == 0) {
                $this->db->where($this->table . '.id', $id);
            } else {
                $this->db->or_where($this->table . '.id', $id);
            }
        }
        $this->db->group_end(); 

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    public function getByIDAndJoinUserID($id, $user_id)
    {
        $this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
        $this->db->select($this->table . '.*, ' . $this->conversations_table . '.is_read as is_read');
        $this->db->where($this->table.'.id', $id);
        $this->db->where($this->conversations_table.'.user_id', $user_id);
        $this->db->join($this->conversations_table, $this->conversations_table . '.company_id = '. $this->table .'.id', 'left');
        $this->db->group_by('company_id');

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return FALSE;
        }
    }

	public function getByEmail($email)
	{
		$this->db->where('email', $email);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}

    public function getAll()
    {
        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    public function getAllByNameAndSurname($name_and_surname)
    {
        $this->db->like('CONCAT(firstname, " ", lastname)', $name_and_surname);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    public function passwordValidate($email, $password)
    {
        $query = $this->db->select('*')->from($this->table)->where('email', $email)->get()->row();

        if ($query != "")
        {
            $hashed_password = $query->password;

            if (password_verify($password, $hashed_password) && $query->verified == 1)
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }

	public function validate($email, $password)
    {
        $query = $this->db->select('*')->from($this->table)->where('email', $email)->get()->row();

        if ($query != "")
        {
            $hashed_password = $query->password;
            $id = $query->id;

            if (password_verify($password, $hashed_password) && $query->verified == 1)
            {
                $last_login = date('Y-m-d H:i:s', NOW_DATE_TIME);
                $this->db->trans_start();
                $this->db->trans_strict(FALSE);
                $this->db->update($this->table, array('last_login' => $last_login, 'last_login_ip_address' => $this->input->ip_address()), array('id' => $id));
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                    return FALSE;
                }
                else
                {
                    $this->db->trans_commit();
                    return $query;
                }
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }

    public function verification($verification_token, $id)
    {
        $this->db->update($this->table, 
        	array('verified' => 1, 'verification_date' => date('Y-m-d H:i:s', NOW_DATE_TIME)), 
        	array('id' => $id, 'verification_token' => $verification_token)
        );

        return $this->db->affected_rows();
    }

    public function passwordUpdate($data, $remember_token, $id)
    {
    	$this->db->update($this->table, $data, array('id' => $id, 'remember_token' => $remember_token, 'verified' => 1));

    	return $this->db->affected_rows();
    }

    public function getCount()
    {
        $this->db->where('verified', 1);

        $query = $this->db->get($this->table);

        return $query->num_rows();
    }

}