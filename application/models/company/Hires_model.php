<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hires_model extends CI_Model {

	var $table = 'company_hires';
    var $users_table = 'users';
    var $companies_table = 'companies';

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function getByID($id)
    {
        $this->db->where('id', $id);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return FALSE;
        }
    }

    public function deleteByCompanyIDAndUserID($company_id, $user_id)
    {
        $this->db->where('company_id', $company_id);
        $this->db->where('user_id', $user_id);
        $this->db->delete($this->table);
    }

    public function getByCompanyIDAndUserID($company_id, $user_id)
    {
        $this->db->where('company_id', $company_id);
        $this->db->where('user_id', $user_id);
        $this->db->order_by('id', 'desc');

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    public function getCountByUserID($user_id)
    {
        $this->db->where('user_id', $user_id);

        $query = $this->db->get($this->table);

        return $query->num_rows();
    }

    public function getCountByCompanyID($company_id)
    {
        $this->db->where('company_id', $company_id);

        $query = $this->db->get($this->table);

        return $query->num_rows();
    }

    public function getAllByCompanyIDAndContinues($company_id)
    {
        $this->db->where('company_id', $company_id);
        $this->db->where('continues', 1);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }

    public function getCountByContinues()
    {
        $this->db->where('continues', 1);

        $query = $this->db->get($this->table);

        return $query->num_rows();
    }

    public function getCountByNotContinues()
    {
        $this->db->where('continues', 0);
        $this->db->where('company_confirm', 1);
        $this->db->where('user_confirm', 1);

        $query = $this->db->get($this->table);

        return $query->num_rows();
    }

    public function getAllByUserIDAndStartdate($user_id)
    {
        $this->db->select($this->table . '.*, ' . $this->companies_table . '.company_name');

        $this->db->where($this->table .'.user_id', $user_id);
        $this->db->where($this->table .'.startdate !=', null);
        $this->db->order_by($this->table .'.id', 'asc');

        $this->db->join($this->users_table, $this->users_table . '.id = '. $this->table .'.user_id', 'left');
        $this->db->join($this->companies_table, $this->companies_table . '.id = '. $this->table .'.company_id', 'left');

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }

    public function getAllByCompanyIDAndStartdate($company_id)
    {
        $this->db->select($this->table . '.*, ' . $this->users_table . '.firstname, ' . $this->users_table . '.lastname,' . $this->users_table . '.email as user_email');

        $this->db->where($this->table .'.company_id', $company_id);
        $this->db->where($this->table .'.startdate !=', null);
        $this->db->order_by($this->table .'.id', 'asc');

        $this->db->join($this->users_table, $this->users_table . '.id = '. $this->table .'.user_id', 'left');

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }
}