<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Watch_model extends CI_Model {

	var $table = 'watch_users';
    var $users_table = 'users';
    var $companies_table = 'companies';

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function getByID($id)
    {
        $this->db->where('id', $id);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return FALSE;
        }
    }

    public function deleteByCompanyIDAndUserID($company_id, $user_id)
    {
        $this->db->where('company_id', $company_id);
        $this->db->where('user_id', $user_id);
        $this->db->delete($this->table);
    }

    public function userControl($company_id, $user_id)
    {
        $this->db->where('company_id', $company_id);
        $this->db->where('user_id', $user_id);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getAllByUserID($user_id)
    {
        $this->db->select(
            $this->table . '.*, ' . 
            $this->companies_table . '.company_name'
        );

        $this->db->join($this->companies_table, $this->companies_table . '.id = '. $this->table .'.company_id', 'left');

        $this->db->where($this->table .'.user_id', $user_id);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    public function getCountByUserID($user_id)
    {
        $this->db->where('user_id', $user_id);

        $query = $this->db->get($this->table);

        return $query->num_rows();
    }

    public function getCountByCompanyID($company_id)
    {
        $this->db->where($this->table . '.company_id', $company_id);

        if (!empty($this->cv_status)) {
            $this->db->where('cv_status', $this->cv_status);
        }

        if (!empty($this->is_working_system)) {
            $this->db->group_start();
            $this->db->where('company_ids', null);

            if (!empty($this->our_employees)) {
                foreach ($this->our_employees as $key => $value) {
                    $this->db->or_where($this->users_table .'.id', $value['user_id']);
                }
            }
            $this->db->group_end();
        }

        $this->db->join($this->users_table, $this->users_table .'.id = '. $this->table .'.user_id', 'left');

        $query = $this->db->get($this->table);

        return $query->num_rows();
    }
}