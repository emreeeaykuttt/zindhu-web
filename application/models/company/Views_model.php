<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Views_model extends CI_Model {

	var $table = 'company_views';
    var $companies_table = 'companies';

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function getByID($id)
    {
        $this->db->where('id', $id);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return FALSE;
        }
    }

    public function deleteByCompanyIDAndUserID($company_id, $user_id)
    {
        $this->db->where('company_id', $company_id);
        $this->db->where('user_id', $user_id);
        $this->db->delete($this->table);
    }

    public function userControl($company_id, $user_id)
    {
        $this->db->where('company_id', $company_id);
        $this->db->where('user_id', $user_id);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getAllByUserID($user_id)
    {
        $this->db->select(
            $this->table . '.*, ' . 
            $this->companies_table . '.company_name'
        );

        $this->db->join($this->companies_table, $this->companies_table . '.id = '. $this->table .'.company_id', 'left');

        $this->db->where($this->table .'.user_id', $user_id);

        if (!empty($this->startdate) && !empty($this->enddate)) {
            $this->db->where($this->table .'.created_at >=', $this->startdate);
            $this->db->where($this->table .'.created_at <=', $this->enddate);
        }

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    public function getCountByUserID($user_id)
    {
        $this->db->where('user_id', $user_id);

        $query = $this->db->get($this->table);

        return $query->num_rows();
    }

    public function getCountByCompanyID($company_id)
    {
        $this->db->where('company_id', $company_id);

        $query = $this->db->get($this->table);

        return $query->num_rows();
    }
}