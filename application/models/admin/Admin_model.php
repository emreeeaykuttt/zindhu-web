<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

	var $table = 'admins';
    var $column_order = array('firstname', 'email', null);
    var $column_search = array('firstname', 'email');
    var $order = array('id' => 'desc');

    private function _getDatatablesQuery($data)
    {
        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column_search as $item)
        {
            if($_POST['search']['value'])
            {
                
                if($i===0)
                {
                    $this->db->group_start();

                    if ($item == 'firstname') 
                    {
                        $this->db->like('CONCAT(firstname, " ", lastname)', $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->like($item, $_POST['search']['value']);
                    }
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
        
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function getDatatables($data)
    {
        $this->_getDatatablesQuery($data);

        if($_POST['length'] != -1)
        {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function getCountFiltered($data)
    {
        $this->_getDatatablesQuery($data);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getCountAll($data)
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function getByID($id)
    {
        $this->db->where('id', $id);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return FALSE;
        }
    }

    public function validate($email, $password)
    {
        $query = $this->db->select('*')->from($this->table)->where('email', $email)->get()->row();

        if ($query != "")
        {
            $hashed_password = $query->password;
            $id = $query->id;

            if (password_verify($password, $hashed_password))
            {
                $last_login = date('Y-m-d H:i:s', NOW_DATE_TIME);
                $this->db->trans_start();
                $this->db->trans_strict(FALSE);
                $this->db->update($this->table, array('last_login' => $last_login, 'last_login_ip_address' => $this->input->ip_address()), array('id' => $id));
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                    return FALSE;
                }
                else
                {
                    $this->db->trans_commit();
                    return $query;
                }
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }

    public function emailControl($email, $user_id)
    {
        $this->db->where('email', $email);
        $this->db->where('id !=', $user_id);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	public function getByEmail($email)
	{
		$this->db->where('email', $email);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}

    public function getAllByNameAndSurname($name_and_surname)
    {
        $this->db->like('CONCAT(firstname, " ", lastname)', $name_and_surname);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    public function getCount()
    {
        $query = $this->db->get($this->table);

        return $query->num_rows();
    }

}