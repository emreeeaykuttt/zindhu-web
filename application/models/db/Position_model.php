<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Position_model extends CI_Model {

	var $table = 'positions';

	public function getAll()
	{
		$query = $this->db->order_by('title', 'asc')->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getByID($id)
	{
		$query = $this->db->where('id', $id)->get($this->table);	

		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}

	public function getAllByIDs($json)
	{
		foreach (json_decode($json) as $key => $id) {
			if ($key == 0) {
				$this->db->where('id', $id);
			} else {
				$this->db->or_where('id', $id);
			}
		}

		$query = $this->db->order_by('title', 'asc')->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}
}