<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class University_model extends CI_Model {

	var $table = 'universities';

	public function getAll()
	{
		$query = $this->db->order_by('university', 'asc')->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getByID($id)
	{
		$query = $this->db->where('id', $id)->get($this->table);	

		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}
}