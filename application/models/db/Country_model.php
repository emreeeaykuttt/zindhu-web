<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country_model extends CI_Model {

	var $table = 'countries';

	public function getList()
	{
		$query = $this->db->where('country_id !=',0)->where('lang_id', get_lang())->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}
}