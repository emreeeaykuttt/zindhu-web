<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City_model extends CI_Model {

	var $table = 'cities';

	public function getAllByCountryID($id)
	{
		$query = $this->db->where('country_id', $id)->where('id!=',0)->order_by('sort', 'asc')->get($this->table);	

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getSearchList($text)
	{
		$query = $this->db->select('id, city as text')->where('country_id', 65)->where('id!=',0)->like('city', $text)->order_by('sort', 'asc')->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return FALSE;
		}
	}

	public function getByID($id)
    {
        $query = $this->db->where('id', $id)->get($this->table);

        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return FALSE;
        }
    }

    public function getAll()
	{
		$query = $this->db->select('city, id')->where('country_id', 65)->where('id !=', 0)->order_by('sort', 'asc')->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getAllByIDs($json)
	{
		foreach (json_decode($json) as $key => $id) {
			if ($key == 0) {
				$this->db->where('id', $id);
			} else {
				$this->db->or_where('id', $id);
			}
		}

		$query = $this->db->order_by('sort', 'asc')->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

}