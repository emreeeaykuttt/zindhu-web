<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class District_model extends CI_Model {

	var $table = 'districts';

	public function getAllByCityID($id)
	{
		$query = $this->db->where('city_id', $id)->where('id!=', 0)->order_by('district', 'asc')->get($this->table);	

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getByID($id)
    {
        $query = $this->db->where('id', $id)->get($this->table);

        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return FALSE;
        }
    }
}