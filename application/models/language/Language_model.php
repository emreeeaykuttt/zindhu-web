<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language_model extends CI_Model {

	var $table = 'languages';
	var $column_order = array('name','code','status','default_lang',null);
	var $column_search = array('name','code'); 
	var $order = array('id' => 'desc');

	public $id;
	public $name;
	public $code;
	public $photo;
	public $status;
	public $default_lang;

	private function _get_datatables_query()
	{
		
		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	public function get_list()
	{
		$query = $this->db->get($this->table)->result_array();
		return $query;
	}

	public function get_list_active()
	{
		$query = $this->db->where('status', 1)->order_by('id','asc')->get($this->table)->result_array();
		return $query;
	}

	public function get_lang_default()
	{
		$query = $this->db->where('default_lang', 1)->where('status', 1)->get($this->table)->row();
		return $query;
	}

	public function save_default_column_zero_all_language()
	{
		$this->db->update($this->table, array('default_lang' => 0));
	}

	public function default_lang_find($id)
	{
		$result_data = $this->db->where('id', $id)->get($this->table)->row_array();
		return $result_data;
	}

	public function default_lang_change_first()
	{
		$random_data = $this->db->limit(1)->order_by('id', 'asc')->get($this->table)->row_array();	
		$this->db->update($this->table, array('default_lang' => 1), array('id' => $random_data['id']));
	}

	public function get_count()
	{
		$this->db->where('status', 1);
		$query = $this->db->get($this->table);
		return $query->num_rows();
	}

	public function get_item()
	{
		$this->db->where('code', $this->code);
		$query = $this->db->get($this->table)->row();
		return $query;
	}

	public function get_all()
	{
		$query = $this->db->get($this->table);

		if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
	}

	public function get_lang_id($code)
	{
		$this->db->select('id');
		$this->db->where('code', $code);
		$this->db->where('status', 1);
		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0) 
		{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}

}