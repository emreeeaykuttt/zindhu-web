<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
	}

	public function get_lang_id()
	{
		$lang = $this->session->userdata("lang");

		if(!$lang)
		{
			$this->load->model('language/language_model', 'language');
			$default_lang = $this->language->get_lang_default();
			return $default_lang->id;
		}
		else
		{
			return $this->session->userdata("lang");
		}
	}


	public function autorun()
	{	
        if (!$this->session->userdata('lang_code')) 
        {
            $this->session->set_userdata('lang_code', 'tr');
        } 
        elseif ($this->input->get('l') == 'en')
        {
            $this->session->set_userdata('lang_code', 'en');
        }
	}

	public function send_email($tos=array(), $ccs=array(), $bccs=array(), $sender_name='', $subject='', $message='', $attachments = array())
    {
        require_once APPPATH . 'third_party/phpmailer/PHPMailerAutoload.php';

        $mail = new PHPMailer;

        // $mail->IsSMTP();
        // $mail->Host = 'smtp.mandrillapp.com';
        // $mail->Port = 587;
        // $mail->CharSet = 'UTF-8';
        // $mail->SMTPAuth = true;
        // $mail->Username = 'Zindhu';
        // $mail->Password = '0a84SBaKJO5ZFGfVrFfBDg';
        // $mail->SMTPSecure = 'tls'; 

        $mail->isSMTP();
        $mail->SMTPAuth = true;
        // $mail->Host = 'localhost'; 
        $mail->Host = '172.18.48.10';
        $mail->Port = 587;
        $mail->CharSet = 'UTF-8';
        $mail->Username = 'info@zindhu.com';
        $mail->Password = 'TgTHd5Yd!xt';

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $mail->setFrom('info@zindhu.com', $sender_name);

        foreach ($tos as $to){
            if ($to){
                $mail->addAddress($to);
            }
        }

        $mail->addReplyTo('info@zindhu.com', $sender_name);

        foreach ($ccs as $cc){
            if ($cc){
                $mail->addCC($cc);
            }
        }

        foreach ($bccs as $bcc){
            if ($bcc){
                $mail->addBCC($bcc);
            }

        }

        foreach ($attachments as $attachment){
            $mail->addAttachment($attachment['file_with_path']);
        }


        $mail->isHTML(true);

        $mail->Subject = $subject;
        $mail->Body    = $message;
        $mail->AltBody = strip_tags($message);


        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
            return false;
        } else {
            return true;
        }

    }

    public function mail_template($data, $link = '', $link_text = '', $img = '1.png')
    {
        $link_area = '';

        if (!empty($link)) 
        {
            $link_area = 
            '<tr>
                <td style="padding: 10px 0 30px 0;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="35%"></td>
                            <td align="center" style="padding: 15px 0 15px 0; border: 2px solid #000;">
                                <a href="'. $link .'" target="_blank" style="color: #000000; font-family: Helvetica, Arial, sans-serif; font-size: 18px; text-decoration: none;">
                                    <b>'. $link_text .'</b>
                                </a>
                            </td>
                            <td width="35%"></td>
                        </tr>
                    </table>
                </td>
            </tr>'; 
        }

        $template_html = 
        '<!DOCTYPE html>
        <html>

        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Mailing</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        </head>

        <body style="margin: 0; padding: 0;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
                <tr>
                    <td style="padding: 10px 0 30px 0;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #000; border-collapse: collapse;">
                            <tr>
                                <td align="center" bgcolor="#000000" style="padding: 30px 0 30px 0;">
                                    <img src="'.base_url().'assets/admin/img/zindhu-hr-logo.png" style="display: block;" width="200" />
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffffff" style="padding: 30px 0 10px 0;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="center" style="padding: 10px 30px 30px 30px; color: #000000; font-family: Helvetica, Arial, sans-serif; font-size: 16px; line-height: 22px;">
                                                '. $data .'
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            '. $link_area .'
                            <tr>
                                <td bgcolor="#000000" align="center" style="padding: 20px 0px 20px 0px;">
                                    <a href="https://zindhu.com" target="_blank" style="color: #ffffff; font-family: Helvetica, Arial, sans-serif; font-size: 14px; text-decoration: none;">www.zindhu.com</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </body>
        </html>';

        return $template_html;
    }

}





?>