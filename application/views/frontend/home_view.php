<!DOCTYPE html>
<html lang="tr">
<head>
    <title>Zindhu: Oyun sektörünün kalbi</title>
    <meta charset="utf-8" />
    <meta name="description" content="Oyun sektörünü oluşturan oyun geliştiricilerden,  yayıncılara, oyun monetizasyonu  yöntemlerinden insan kaynaklarına kadar oyun sektöründe 360 çözümler sunan tüm hizmetler Zindhu çatısı altında!" />
    <meta property="og:title" content="Zindhu: Oyun sektörünün kalbi" />
    <meta property="og:description" content="Oyun sektörünü oluşturan oyun geliştiricilerden,  yayıncılara, oyun monetizasyonu  yöntemlerinden insan kaynaklarına kadar oyun sektöründe 360 çözümler sunan tüm hizmetler Zindhu çatısı altında!" />
    <meta property="og:image" content="<?=base_url()?>assets/home/og.jpg">
    <meta property="og:url" content="<?=base_url()?>">
    <meta property="og:type" content="website">
    <meta name="twitter:card" content="summary" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="theme-color" content="#3d00bf" />

    <link rel="icon" href="<?=base_url()?>assets/home/favicon.ico" />
    <link rel="icon" href="<?=base_url()?>assets/home/favicon.svg" type="image/svg+xml" />
    <link rel="apple-touch-icon" href="<?=base_url()?>assets/home/apple-touch-icon.png" />
    
    <link rel="stylesheet" href="<?=base_url()?>assets/home/styles/main-45902f7356.css" />

    <style type="text/css">
        .invalid {
            color: #f859a0;
            position: absolute;
            font-size: 0.8rem;
            right: 0;
        }
        .contact-form-area .invalid {
            color: #fff;
        }
        .share-game-area.overlay .box .content {
            width: 37.5vw;
        }
        #kvk-area.overlay .box p {
            margin-bottom: 15px;
            font: 1.25vw/1.45 'Visuelt Pro',sans-serif;
        }
        ul.circle-list  {
            margin-bottom:10px
        }
        ul.circle-list li {
            list-style: disc;
            margin-left: 15px;
        }
        @media screen and (max-width: 480px) {
            .share-game-area.overlay .box .content{
                width: 100%;
            }
        }
    </style>
      <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-175472421-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-175472421-1');
    </script>
</head>
<body>

<header>
    <button type="button" id="home-btn">
        <img src="<?=base_url()?>assets/home/images/zindhu-logo-img.svg" alt="Zindhu" />
        <span>
            <img src="<?=base_url()?>assets/home/images/zindhu-logo-txt.svg" alt="Zindhu" />
        </span>
    </button>
    <div class="fl">
        <button type="button" data-scroll="detail-hr">Zindhu HR</button>
        <button type="button" data-scroll="detail-games">Zindhu Games</button>
        <button type="button" data-scroll="detail-venture">Zindhu Ventures</button>
        <button type="button" data-scroll="contact"><?=l('z_iletisim')?></button>
    </div>
    <div class="dd-btn lang">
        <button>
            <span>
            <?php if ($this->session->userdata('lang_code') == 'tr') { ?>
                Türkçe
            <?php } else { ?>
                English
            <?php } ?>
            </span>
        </button>
        <div>
            <a href="<?= base_url('language/change/tr') ?>">Türkçe</a>
            <a href="<?= base_url('language/change/en') ?>">English</a>
        </div>
    </div>
    <button type="button" id="menu-btn">
        <img src="<?=base_url()?>assets/home/images/menu.svg" alt="" />
        <img src="<?=base_url()?>assets/home/images/menu-close.svg" alt="" />
    </button>
</header>


<div id="menu" class="fm"></div>


<section id="home" class="oh">

    <div id="home-bg">
        <div id="saturn">
            <img src="<?=base_url()?>assets/home/images/saturn.svg" alt="" />
        </div>

        <div id="small-planet">
            <img src="<?=base_url()?>assets/home/images/planet-1.svg" alt="" />
        </div>

        <div id="planet-man">
            <img src="<?=base_url()?>assets/home/images/planet-man.svg" alt="" />
        </div>
    </div>

    <div class="txt">
        <h1><?=l('zindhu_oyun_sektorunun_kalbi')?></h1>
        <p><?=l('oyun_sektorunu_olusturan_text')?></p>

    </div>

    <div id="scroll-down">
        <img src="<?=base_url()?>assets/home/images/scroll-text-tr.svg" alt="" />
        <img src="<?=base_url()?>assets/home/images/scroll-arrow.svg" alt="" />
    </div>

    <script>
        var wrappedTitle = "";
        document.querySelector("h1").innerHTML.split(" ").forEach(function (part, index) {
            wrappedTitle += "<span><span style='transition-delay: " + (index * 0.07).toFixed(2) + "s;'>" + part + "</span></span>";
        });
        document.querySelector("h1").innerHTML = wrappedTitle;
        document.querySelector("h1").classList.add("wrapped");
    </script>
</section>


<section id="about" class="oh">
    <div class="txt">
        <h2><?=l('z_hakkimizda')?></h2>
        <p><?=l('istanbul_merkezli_kurulan_text')?></p>
        <div class="fl">
            <div>
                <img src="<?=base_url()?>assets/home/images/games.png" alt="Games" />
            </div>
            <div>
                <img src="<?=base_url()?>assets/home/images/hr.png" alt="HR" />
            </div>
            <div>
                <img src="<?=base_url()?>assets/home/images/ventures.png" alt="Ventures" />
            </div>
        </div>
    </div>

    <div id="about-anim">
        <div>
            <img src="<?=base_url()?>assets/home/images/statue.svg" alt="" />
            <div id="planets-1" class="pl">
                <svg width="761" height="281" viewBox="0 0 761 281" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path id="path-1" d="M759.636 265.327C758.261 269.501 754.361 272.823 748.002 275.263C741.651 277.701 732.935 279.223 722.099 279.851C700.431 281.108 670.411 278.788 634.17 273.209C561.697 262.053 464.444 237.879 359.593 203.328C254.742 168.777 162.169 130.399 97.2613 96.285C64.8037 79.2262 39.285 63.2448 22.6095 49.3531C14.2697 42.4055 8.16599 36.0003 4.50818 30.2644C0.846167 24.5219 -0.314976 19.532 1.06051 15.3578C2.436 11.1836 6.33616 7.86153 12.6947 5.42101C19.0459 2.98331 27.7617 1.46153 38.598 0.832955C60.2654 -0.423896 90.2858 1.89611 126.526 7.47503C198.999 18.6316 296.253 42.8057 401.104 77.3566C505.954 111.907 598.527 150.286 663.435 184.399C695.893 201.458 721.412 217.44 738.087 231.331C746.427 238.279 752.531 244.684 756.189 250.42C759.851 256.163 761.012 261.152 759.636 265.327Z" />
                </svg>
                <div id="ap-1">
                    <img src="<?=base_url()?>assets/home/images/planet-1.svg" alt="" />
                </div>
                <div id="ap-2">
                    <img src="<?=base_url()?>assets/home/images/planet-2.svg" alt="" />
                </div>
            </div>
            <div id="planets-2" class="pl">
                <svg width="793" height="208" viewBox="0 0 793 208" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path id="path-2" d="M792.155 47.236C792.988 53.035 791.118 59.1287 786.701 65.4425C782.281 71.7591 775.335 78.2621 766.088 84.8503C747.597 98.0258 720 111.479 685.328 124.434C615.994 150.341 518.474 174.219 409.205 189.919C299.936 205.619 199.638 210.163 125.814 204.826C88.8983 202.156 58.6299 197.018 37.1769 189.582C26.4496 185.863 17.9529 181.579 11.9339 176.762C5.91753 171.948 2.40735 166.627 1.57414 160.828C0.740933 155.029 2.61108 148.935 7.02852 142.622C11.4479 136.305 18.3947 129.802 27.641 123.214C46.1326 110.038 73.7297 96.5854 108.401 83.6304C177.735 57.7234 275.256 33.8451 384.524 18.1452C493.793 2.44531 594.091 -2.09934 667.915 3.23848C704.831 5.90769 735.099 11.0463 756.552 18.4826C767.28 22.2009 775.776 26.4849 781.795 31.3017C787.812 36.1164 791.322 41.437 792.155 47.236Z" />
                </svg>
                <div id="ap-3"></div>
                <div id="ap-4"></div>
            </div>
        </div>

    </div>
</section>


<section id="vision" data-anim-width="768">

    <div class="fl">
        <h2><?=l('sektorel_vizyon_hedeflerimiz')?></h2>
        <div id="hand">
            <img src="<?=base_url()?>assets/home/images/hand-hole.svg" alt="" />
            <img src="<?=base_url()?>assets/home/images/hand.svg" alt="" />
            <img src="<?=base_url()?>assets/home/images/hand-mask.svg" alt="" />
        </div>
    </div>

    <div class="txt">
        <p><?=l('hedefi_turkiye_oyun_text')?></p>
        <ol>
            <li>
                <div>
                    <?=l('insan_kaynaklari')?>  
                    <img src="<?=base_url()?>assets/home/images/hr-p.png" alt="HR" />
                </div>
            </li>
            <li>
                <div>
                    <?=l('yatirim_surecleri_ve_kullanici_edinimi')?>
                    <img src="<?=base_url()?>assets/home/images/games-p.png" alt="Games" />
                </div>
            </li>
            <li>
                <div>
                    <?=l('is_plani_olusturma_ve_olceklendirme')?>
                    <img src="<?=base_url()?>assets/home/images/ventures-p.png" alt="Ventures" />
                </div>
            </li>
        </ol>
        <p><?=l('alanlarinda_hizmet_veren_text')?></p>
    </div>
</section>


<section id="detail">

    <div id="detail-hr">
        <div class="txt">
            <h2>Zindhu <b>HR</b></h2>
            <p><?=l('turkiyennin_ilk_ve_tek_text')?></p>
            <div class="btns">
                <a href="<?=base_url('zindhu-hr')?>" target="_blank" class="rbtn"><?=l('basvuru_yap')?></a>
            </div>
            <h3><?=l('zindhu_hrin_tem_hedeleri')?></h3>
            <ul>
                
                    <li><?=l('oyun_sektorunde_calisan_kisilere_text')?></li>
                
                    <li><?=l('oyun_sektorunde_kariyer_text')?></li>
                
                    <li><?=l('potansiyelini_oyun_sektorunde_text')?></li>
                
                    <li><?=l('oyun_studyolarina_buyume_text')?></li>
                
                    <li><?=l('yurt_disinda_kariyerine_text')?></li>
                
                    <li><?=l('dogrultusunda_platforma_uye_text')?></li>

                    <li><?=l('oyun_sektorunde_dogru_text')?></li>
                
            </ul>
        </div>
        <div class="sticky">
            <div class="marq vertical">
                <p>
                    <span>Human Resources</span>
                </p>
            </div>
            <div class="img">
                <img src="<?=base_url()?>assets/home/images/detail-hr-img.svg" alt="" />
            </div>
        </div>
    </div>

    <div id="detail-venture">
        <div class="txt">
            <h2>Zindhu <b>Ventures</b></h2>
            <p><?=l('studyolarin_is_modellerinin_gelistirilmesi_text')?></p>
            <div class="btns">
                <button class="rbtn" data-scroll="contact"><?=l('yatirim_al')?></button>
                <button class="rbtn rev" data-scroll="contact"><?=l('yatirimci_ol')?></button>
            </div>
            
                <h3><?=l('studyo_tarafinda')?></h3>
                <ul class="circle-list">
                    
                        <li><?=l('oyun_studyosunun_gelisimini_text')?></li>
                    
                        <li><?=l('yatirimci_sunumu_ve_finansal_text')?></li>
                    
                        <li><?=l('sirket_ve_takim_imajlarini_text')?></li>
                    
                        <li><?=l('oyun_sektorunde_denenmis_text')?></li>
                    
                        <li><?=l('oyunun_ve_oyun_studyosunun_text')?></li>

                        <li><?=l('ozelinde_studyo_ile_text')?></li>
                    
                </ul>
            
                <h3><?=l('yatirimci_tarafinda')?></h3>
                <ul class="circle-list">
                    
                        <li><?=l('buyume_potansiyeli_yüksek_text')?></li>
                    
                        <li><?=l('yatirim_surecleri_boyunca_text')?></li>
                    
                        <li><?=l('oyun_studyosunun_geliştigi_text')?></li>

                        <li><?=l('sureclerinde_yaitrim_planinda_text')?></li>
                    
                </ul>
            
                <h3><?=l('yatirim_ve_is_modelleri_text')?></h3>
                <ul class="circle-list">
                    
                        <li><?=l('oyun_studyosunun_degerlendirilmesi_text')?></li>
                    
                        <li><?=l('oyun_studyosuna_uygun_bir_finansman_modelinin_bulunmasi')?></li>
                    
                        <li><?=l('bulunan_model_dahilinde_oyun_studyosu_ile_calismalarin_baslamasi')?></li>
                    
                        <li><?=l('is_modelinin_ve_finansal_projeksiyonlarin_optimizasyonu')?></li>
                    
                        <li><?=l('oyun_studyosu_ve_yatirimci_arasindaki_text')?></li>
                    
                </ul>
            
        </div>
        <div class="sticky">
            <div class="marq vertical">
                <p>
                    <span>Ventures</span>
                </p>
            </div>
            <div class="img">
                <img src="<?=base_url()?>assets/home/images/detail-venture-img.svg" alt="" />
            </div>
        </div>
    </div>

    <div id="detail-games">
        <div class="txt">
            <h2>Zindhu <b>Games</b></h2>
            <p><?=l('oyun_sektorunun_gelisiminin_text')?></p>
            <div class="btns">
                <button class="rbtn" onclick="sg_form_open()"><?=l('yayinci_ariyorum')?></button>
                <button class="rbtn rev" data-scroll="contact"><?=l('sorularim_var')?></button>
            </div>
            <ul class="circle-list">
                
                    <li><?=l('sektoru_ve_studyolarin_mevcut_asamalarini_degerlendirip')?></li>
                
                    <li><?=l('studyolarin_varsa_sorun_text')?></li>
                
                    <li><?=l('kisisellestirilmis_cozumlerle_zindhu_text')?></li>
                
                    <li><?=l('oyun_studyosunun_gelistirdiği_text')?></li>
                
            </ul>
            <p><?=l('icin_ozellestirilmis_hizmetler_text')?></p>
            <br>
            <p><b><?=l('turkiye_oyun_pazarında_text')?></b></p>
            <br>
            <p><?=l('aklinizda_sorularmi_var')?></p>
            <br>
            <p><?=l('bir_gorusme_ayarlayalim')?></p>

        </div>
        <div class="sticky">
            <div class="marq vertical">
                <p>
                    <span>Games</span>
                </p>
            </div>
            <div class="img">
                <img src="<?=base_url()?>assets/home/images/detail-games-img.svg" alt="" />
            </div>
        </div>
    </div>

</section>


<section id="contact" class="contact-form-area">
    <div class="fl">
        <div>
            <img src="<?=base_url()?>assets/home/images/contact.svg" alt="" />
        </div>
        <div>
            <h2><?=l('z_bize_ulasin')?></h2>
            <form action="#" id="contact-form" autocomplete="off">
                <div class="row">
                    <input type="text" name="fullname" placeholder="<?=l('z_ad_soyad')?>" />
                    <div class="invalid"></div>
                </div>
                <div class="row">
                    <input type="email" name="email" placeholder="<?=l('z_e_posta')?>" />
                    <div class="invalid"></div>
                </div>
                <div class="row">
                    <textarea name="message" rows="4" placeholder="<?=l('mesaj')?>"></textarea>
                    <div class="invalid"></div>
                </div>
                <div class="row">
                    <a href="javascript:void(0)" onclick="contact_save()" id="btn-save" class="rbtn"><?=l('gonder')?></a>
                </div>
            </form>
        </div>
    </div>
</section>


<footer class="fm">
    <img src="<?=base_url()?>assets/home/images/zindhu-logo-compact.svg" alt="Zindhu" />
    <div class="fl">
        <button type="button" data-scroll="detail-hr">Zindhu HR</button>
        <button type="button" data-scroll="detail-games">Zindhu Games</button>
        <button type="button" data-scroll="detail-venture">Zindhu Ventures</button>
        <button type="button" data-scroll="contact">İletişim</button>
    </div>
    <p><?=l('bizi_takip_edin')?></p>


    <div class="sm">
        <a href="https://www.linkedin.com/company/zindhu" target="_blank">
            <img src="<?=base_url()?>assets/home/images/sm-linkedin.svg" alt="LinkedIn" />
        </a>
        <a href="https://www.instagram.com/zindhugames/" target="_blank">
            <img src="<?=base_url()?>assets/home/images/sm-instagram.svg" alt="Instagram" />
        </a>
    </div>
    <div class="legal">
        <a href="javascript:void(0)" onclick="kvk_open()"><?=l('kisisel_veri_ve_gizlilik_politikasi')?></a>
        <p>&copy; 2021 Zindhu</p>
    </div>
</footer>

<div id="share-game-area" class="overlay share-game-area">
    <span class="bg"></span>
    <div class="wrapper">
        <div class="box">
            <button type="button" class="close"></button>
            <div class="content">
                <h2><?=l('oyununuzu_paylasin')?></h2>
                <form action="#" id="share-game-form" autocomplete="off">
                    <div class="row">
                        <label for="sg-name"><?=l('z_ad_soyad')?></label>
                        <input type="text" name="sg_fullname" />
                        <div class="invalid"></div>
                    </div>
                    <div class="row">
                        <label for="sg-email"><?=l('z_e_posta')?></label>
                        <input type="email" name="sg_email" />
                        <div class="invalid"></div>
                    </div>
                    <div class="row">
                        <label for="sg-url"><?=l('oyun_linkiniz')?></label>
                        <input type="text" name="sg_game_link" class="link" />
                        <div class="invalid"></div>
                    </div>
                    <div class="row">
                        <label for="sg-message"><?=l('oyununuzu_anlatin')?></label>
                        <textarea name="sg_message" rows="2"></textarea>
                        <div class="invalid"></div>
                    </div>
                    <div class="row">
                        <a href="javascript:void(0)" onclick="sg_contact_save()" id="btn-save-sg" class="rbtn"><?=l('gonder')?></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div id="success" class="overlay">
    <span class="bg"></span>
    <div class="wrapper">
        <div class="box">
            <button type="button" class="close"></button>
            <div class="content">
                <p></p>
                <img src="<?=base_url()?>assets/home/images/success.svg" />
                <button type="button" class="rbtn"><?=l('anladim')?></button>
            </div>
        </div>
    </div>
</div>

<div id="kvk-area" class="overlay">
    <span class="bg"></span>
    <div class="wrapper">
        <div class="box">
            <button type="button" class="close"></button>
            <div class="content">
                
                <h2 style="text-align: center; margin-bottom: 15px; border-bottom: 1px solid #efefef;"><?=l('kisisel_veri_ve_gizlilik_politikasi')?></h2>

                <p style="text-align: center;"><b><i>Zindhu Oyun Yazılım Danışmanlık Hizmetleri A.Ş</i></b></p>

                <p style="text-align: center;"><b>&ldquo;Şirket&rdquo;</b></p>

                <p style="text-align: center;"><b>KİŞİSEL VERİ İŞLEME VE GİZLİLİK POLİTİKASI</b></p>

                <p style="text-align: center;">Şirket&rsquo;imiz aşağıda detaylandırılan ve kimliği belirli veya belirlenebilir ger&ccedil;ek kişilere ilişkin her t&uuml;rl&uuml; bilgi kapsamındaki kişisel veriler a&ccedil;ısından veri sorumlusu olarak; <b><i>&Ccedil;eliktepe Mah. İsmet İn&ouml;n&uuml; Cad. Yıldırımlar İş Merkezi No:11 İ&ccedil; Kapı No: 501 Kağıthane/Istanbul</i></b><b> </b>adresinde mukim İstanbul Ticaret Sicili M&uuml;d&uuml;rl&uuml;ğ&uuml;&rsquo;ne <b><i>262674-5</i></b> sicil numarası ile kayıtlıdır.&nbsp;</p>

                <p><b>Şirket&rsquo;imiz internet sitesi &uuml;zerinden;</b></p>

                <p><strong>1.&nbsp;</strong>İnternet sitesi ziyaret&ccedil;ilerine ait ad-soyad ve elektronik posta adresi ve tarafımıza g&ouml;nderilecek mesaj iletinizde yer alabilecek kişisel verileri aşağıdaki ama&ccedil;ların yerine getirilmesi i&ccedil;in 6698 sayılı Kişisel Verilerin Korunması Kanunu&rsquo;da (&ldquo;<b>Kanun</b>&rdquo;) belirtilen ilgili kişisel veri işleme şartlarında dayanarak işlemektedir:</p>

                <ul>
                    <li>Şirket&rsquo;imiz ile ziyaret&ccedil;i arasın ger&ccedil;ekleşebilecek her t&uuml;rl&uuml; ilişki ve/veya iş birliği kapsamında veya ziyaret&ccedil;inin tercih ettiği diğer bir konuya ilişkin olarak iletişime ge&ccedil;ilmesi ve g&ouml;r&uuml;şme ger&ccedil;ekleştirilmesi,&nbsp;</li>
                    <li>internet sitesi ziyaret&ccedil;isinin taleplerinin yerine getirilmesi,&nbsp;</li>
                    <li>ziyaret&ccedil;inin talebine istinaden Şirket&rsquo;imizin y&uuml;r&uuml;tmekte olduğu faaliyetler ve bununla ilişkili diğer faaliyetlerimiz hakkında bilgi sağlanması,&nbsp;</li>
                    <li>onay verilmesi halinde ticari elektronik ileti g&ouml;nderilmesi, ve&nbsp;</li>
                    <li>mevzuattan kaynaklanan zamanaşımı s&uuml;resi doğrultusunda saklanması.</li>
                </ul>

                <p><strong>2.&nbsp;</strong>St&uuml;dyo yetkililerine ait ad-soyad, st&uuml;dyoya ait pay sahiplerinin bilgileri, linkedin dahil olmak &uuml;zrere sosyal medya hesaplarına ait bilgiler, kimliği belirli veya belirlenebilir bir ger&ccedil;ek kişiye ait nitelikte olması halinde elektronik posta adresi, telefon numarası ve adres bilgisi t&uuml;r&uuml;ndeki kişisel verileri aşağıdaki ama&ccedil;ların yerine getirilmesi i&ccedil;in Kanun&rsquo;da belirtilen ilgili kişisel veri işleme şartlarında dayanarak işlemektedir:</p>

                <ul>
                    <li>Zindhu insan kaynakları platformuna (&ldquo;<b>Platform</b>&rdquo;) st&uuml;dyo kaydının tam ve eksiksiz olarak&nbsp; tamamlanması ve kayıt s&uuml;recinin y&ouml;netilmesi,&nbsp;</li>
                    <li>Platform &uuml;zerinden st&uuml;dyoya işe alım konusunda danışmanlık verilmesi,&nbsp;</li>
                    <li>st&uuml;dyo ve oyun sektor&uuml;nde istihdam edilmek isteyen kişilerin bir araya getirilmesi,</li>
                    <li>gerekli olması veya st&uuml;dyonun talebi halinde Platform kullanımı ile ilgili olarak iletişime ge&ccedil;ilmesi,</li>
                    <li>st&uuml;dyoya sunulan istihdam y&ouml;netim hizmeti ve danışmanlık sunulması kapsamında Platform &uuml;zerindeki st&uuml;dyo kaydının y&ouml;netilmesi,&nbsp;</li>
                    <li>Platform kullanımına ilişkin eğitim verilmesi ve gerekli bilgilerin sağlanması,&nbsp;</li>
                    <li>oyun sekt&ouml;r&uuml;nde istihdam edilmek isteyen kişiler ile st&uuml;dyoları bir araya getirmek i&ccedil;in iletişime ge&ccedil;ilmesi,&nbsp;</li>
                    <li>st&uuml;dyonun istihdam etmek istediği &ccedil;alışan adaylarının bulunması ve st&uuml;dyolar ile buluşturulması hizmetlerinin y&uuml;r&uuml;t&uuml;lmesi,&nbsp;</li>
                    <li>işe alım s&uuml;re&ccedil;leri kapsamında st&uuml;dyoya hizmet sağlanması,&nbsp;</li>
                    <li>onay verilmesi halinde ticari elektronik ileti g&ouml;nderilmesi,&nbsp;</li>
                    <li>bahsi ge&ccedil;en hizmetlerin sunulması ve faaliyetlerin y&uuml;r&uuml;t&uuml;lmesi i&ccedil;in kişi ve şirketler ile iş birliği kurulması, ve&nbsp;</li>
                    <li>mevzuattan kaynaklanan zamanaşımı s&uuml;resi doğrultusunda saklanması.</li>
                </ul>

                <p><b>3. </b>&Ccedil;alışan adaylarına ait&nbsp; ad-soyad, e-posta adresi, telefon numarası, adres eğitim durumu, iş deneyimi, kullanımında yetkin olunan programlar, kimliği belirli veya belirlenebilir ger&ccedil;ek kişiye ilişkin bilgi i&ccedil;ermesi halinde portfolyodaki veriler ve &ouml;z ge&ccedil;mişinize ilişkin &ouml;zel veya genel nitelikte olabilecek kişisel verileri aşağıdaki ama&ccedil;ların yerine getirilmesi i&ccedil;in Kanun&rsquo;da belirtilen ilgili kişisel veri işleme şartlarında dayanarak işlemektedir:<b>&nbsp;</b></p>

                <ul>
                    <li>Platform kaydının tam ve eksiksiz olarak&nbsp; tamamlanması ve kayıt s&uuml;recinin y&ouml;netilmesi,&nbsp;</li>
                    <li>Platform &uuml;zerinden işe alım konusunda danışmanlık verilmesi,&nbsp;</li>
                    <li>istihdam amacıyla oyun st&uuml;dyoları ile bir araya gelinmesi,&nbsp;</li>
                    <li>gerekli olması veya st&uuml;dyonun talebi halinde Platform kullanımı ile ilgili olarak iletişime ge&ccedil;ilmesi, istihdam edilme s&uuml;recinizde &ccedil;alışan arayan oyun st&uuml;dyolarına sağladığınız verilerin iletilebilmesi, istihdam y&ouml;netim ve iş bulma hizmeti ve danışmanlık sunulması kapsamında Platform &uuml;zerindeki kaydın y&ouml;netilmesi,&nbsp;</li>
                    <li>Platform kullanımına ilişkin eğitim verilmesi ve gerekli bilgilerin sağlanması,</li>
                    <li>istihdam sağlanması kapsamında oyun st&uuml;dyoları ile bir araya gelinmesinin sağlanması i&ccedil;in iletişime ge&ccedil;ilmesi,</li>
                    <li>oyun st&uuml;dyolarının bulunması ve st&uuml;dyolar ile buluşma kapsamındaki hizmetlerin y&uuml;r&uuml;t&uuml;lmesi,</li>
                    <li>işe alım s&uuml;re&ccedil;leri kapsamında hizmet sağlanması,&nbsp;</li>
                    <li>onay verilmesi halinde&nbsp;</li>
                    <li>ticari elektronik ileti g&ouml;nderilmesi,&nbsp;</li>
                    <li>bahsi ge&ccedil;en hizmetlerin sunulması ve faaliyetlerin y&uuml;r&uuml;t&uuml;lmesi i&ccedil;in kişi ve şirketler ile iş birliği kurulması&nbsp;</li>
                    <li>ve mevzuattan kaynaklanan zamanaşımı s&uuml;resi doğrultusunda saklanması.</li>
                </ul>

                <p>İnternet sitesi ziyaret&ccedil;ileri, st&uuml;dyo yetkilileri ve &ccedil;alışan adayları tek tek &ldquo;<b>İlgili Kişi</b>&rdquo;, birlikte &ldquo;<b>İlgili Kişiler</b>&rdquo; olarak anılacaklardır.</p>

                <p><b>İlgili kişilerin, kişisel verileri ile ilgili Kanun kapsamındaki hakları aşağıdaki şekildedir:&nbsp;</b></p>

                <p>(a) Kişisel verilerinizin işlenip işlenmediğini &ouml;ğrenme, (b) Kişisel verileriniz işlenmişse buna ilişkin bilgi talep etme, (c) Kişisel verilerinizin işlenme amacını ve bunların amacına uygun kullanılıp kullanılmadığını &ouml;ğrenme, (&ccedil;) Yurt i&ccedil;inde veya yurt dışında kişisel verilerin aktarıldığı &uuml;&ccedil;&uuml;nc&uuml; kişileri bilme, (d) Kişisel verilerinizin eksik veya yanlış işlenmiş olması h&acirc;linde bunların d&uuml;zeltilmesini isteme, (e) Kişisel verilerinizin işlenmesini gerektiren sebeplerin ortadan kalkması halinde kişisel verilerinizin silinmesini veya yok edilmesini isteme, (f), (d) ve (e) bentleri uyarınca yapılan işlemlerin, kişisel verilerin aktarıldığı &uuml;&ccedil;&uuml;nc&uuml; kişilere bildirilmesini isteme, (g) İşlenen verilerin m&uuml;nhasıran otomatik sistemler vasıtasıyla analiz edilmesi suretiyle kişinin kendisi aleyhine bir sonucun ortaya &ccedil;ıkmasına itiraz etme, (ğ) Kişisel verilerin kanuna aykırı olarak işlenmesi sebebiyle zarara uğraması h&acirc;linde zararın giderilmesini talep etme.&nbsp;</p>

                <p>Bu haklar yazılı olarak veya g&uuml;venli elektronik imza, mobil imza ya da Şirket&rsquo;e daha &ouml;nce bildirilen ve Şirket&rsquo;in sisteminde kayıtlı bulunan elektronik posta adresi kullanmak suretiyle (Bu kapsamda <a href="mailto:info@zindhugames.com">info@zindhugames.com</a><b><i> </i></b>e-posta adresi &uuml;zerinden Şirket&rsquo;e ulaşabilecektir) veya başvuru amacına y&ouml;nelik geliştirilmiş bir yazılım ya da uygulama vasıtasıyla Şirket&rsquo;e iletilebilecektir.&nbsp;</p>

                <p><b>Gizlilik Politikası</b></p>

                <p>İşbu Kişisel Veri İşleme ve Gizlilik Politikası, t&uuml;m İlgili Kişiler i&ccedil;in ge&ccedil;erlidir. İlgili Kişiler tarafından Şirket&rsquo;e elektronik ortamda iletilen kişisel bilgiler, işbu Kişisel Veri İşleme ve Gizlilik Politikası ile belirlenen ama&ccedil;lar ve kapsam dışında, &uuml;&ccedil;&uuml;nc&uuml; kişilere a&ccedil;ıklanmayacaktır.&nbsp;</p>

                <p>İlgili Kişiler&rsquo;e ait her t&uuml;rl&uuml; bilgi &ldquo;<b>Gizli Bilgiler</b>&rdquo; olarak anılacaktır.</p>

                <p>İnternet Sitesi i&ccedil;erisinde referans g&ouml;sterilen veya bağlantı sağlanan diğer sitelerin/uygulamaların/reklamların i&ccedil;eriğinden ya da gizlilik uygulamalarından Şirket sorumlu tutulamaz.</p>

                <p>Şirket Gizli Bilgiler&rsquo;i, burada belirtilenler saklı kalmak &uuml;zere, kesinlikle &ouml;zel ve gizli tutmayı, bunu bir sır saklama y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml; olarak addetmeyi ve gizliliğin sağlanması ve s&uuml;rd&uuml;r&uuml;lmesi, Gizli Bilgi&rsquo;nin tamamının veya herhangi bir kısmının kamu alanına girmesini veya yetkisiz kullanımını veya &uuml;&ccedil;&uuml;nc&uuml; bir kişiye ifşasını &ouml;nlemek i&ccedil;in gerekli tedbirleri almayı ve gerekli &ouml;zeni g&ouml;stermeyi taahh&uuml;t etmektedir. Ancak İlgili Kişiler&rsquo;in bilgilerini kendi cihazı &uuml;zerinden vermesi sebebiyle ilgisiz kişiler tarafından erişilmemesi amacıyla vir&uuml;s ve benzeri zararlı uygulamalara karşı gerekli tedbirlerin İlgili Kişiler tarafından alınmaması ve Şirket&rsquo;in gerekli bilgi g&uuml;venliği &ouml;nlemlerini almasına karşın, sisteme yapılan saldırılar sonucunda gizli bilgilerin zarar g&ouml;rmesi veya &uuml;&ccedil;&uuml;nc&uuml; kişilerin eline ge&ccedil;mesi durumunda, Şirket&rsquo;in herhangi bir sorumluluğu olmayacaktır.</p>

                <p>Şirket, İlgili Kişiler&rsquo;in Gizli Bilgiler&rsquo;ini (a) y&uuml;r&uuml;rl&uuml;kte olan kanun veya d&uuml;zenlemeler ya da verilmiş olan bir yargı kararı ya da idari emir gereğince a&ccedil;ıklaması gerektiği durumlarda ve/veya (b) İlgili Kişiler&rsquo;in rızasının bulunduğu takdirde ve/veya (c) işbu Kişisel Veri İşleme ve Gizlilik Politikası&rsquo;na uygun şekilde &uuml;&ccedil;&uuml;nc&uuml; kişilere a&ccedil;ıklayabilir ve/veya kullanabilir.&nbsp;</p>

                <p>İlgili Kişiler&rsquo;in, internet sitesinde herkese a&ccedil;ık alanlarda ifşa ettiği, yorumlarda veya mesajlarda paylaştığı bilgilerinin &uuml;&ccedil;&uuml;nc&uuml; kişiler tarafından kullanılması sonucunda doğacak zarardan Şirket sorumlu değildir.&nbsp;</p>

                <p>Şirket, işbu Kişisel Veri İşleme ve Gizlilik Politikası h&uuml;k&uuml;mlerini her zaman g&uuml;ncelleyebilir, değiştirebilir veya y&uuml;r&uuml;rl&uuml;kten kaldırabilir.&nbsp; G&uuml;ncellenen, değiştirilen ya da y&uuml;r&uuml;rl&uuml;kten kaldırılan her h&uuml;k&uuml;m, yayın tarihinde İlgili Kişi i&ccedil;in h&uuml;k&uuml;m ifade edecektir. &nbsp;</p>


            </div>
        </div>
    </div>
</div>



<script src="<?=base_url()?>assets/home/scripts/vendor-54ea5d8a06.js"></script>
<script src="<?=base_url()?>assets/home/scripts/main-514f216c0b.js"></script>


<script type="text/javascript">

    $('input[type="text"], input[type="email"], textarea').keyup(function() {
        $(this).next().text('');
    });

    function kvk_open() {
        $('#kvk-area').slideDown();
    }

    function contact_form_open() {
        $('#success p').text('');
    }

    function contact_save() {
        $('#btn-save').text('<?=l('gonderiliyor')?>');
        $('#btn-save').attr('disabled', true);

        var warning_text = '';
        var formData = new FormData($('#contact-form')[0]);
        $.ajax({
            url: '/ajax/home_contact_save',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data) {
                if (data.status) {
                    $('#success p').text('<?=l('mesajiniz_basariyla_gonderildi')?>')
                    $('#success').slideDown();
                    $('#contact-form')[0].reset();
                } else {
                    for (var i = 0; i < data.inputerror.length; i++) {
                        $('#contact-form [name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                    }
                }

                $('#btn-save').text('<?=l('gonder')?>');
                $('#btn-save').attr('disabled', false);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#btn-save').text('<?=l('gonder')?>');
                $('#btn-save').attr('disabled', false);
            }
        });
    }

    function sg_form_open() {
        $('#share-game-area').slideDown();
        $('#success p').text('');
    }

    function sg_contact_save() {
        $('#btn-save-sg').text('<?=l('gonderiliyor')?>');
        $('#btn-save-sg').attr('disabled', true);

        var warning_text = '';
        var formData = new FormData($('#share-game-form')[0]);
        $.ajax({
            url: '/ajax/sg_contact_save',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data) {
                if (data.status) {
                    $('#share-game-area').slideUp();
                    $('#success p').text('<?=l('oyununuzu_bize_basariyla_gonderdiniz')?>')
                    $('#success').slideDown();
                    $('#share-game-form')[0].reset();
                } else {
                    for (var i = 0; i < data.inputerror.length; i++) {
                        $('#share-game-form [name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                    }
                }

                $('#btn-save-sg').text('<?=l('gonder')?>');
                $('#btn-save-sg').attr('disabled', false);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#btn-save-sg').text('<?=l('gonder')?>');
                $('#btn-save-sg').attr('disabled', false);
            }
        });
    }

</script>

</body>
</html>