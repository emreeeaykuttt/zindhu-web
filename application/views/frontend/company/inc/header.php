<!--Main Navigation-->
<header>

    <!-- Sidebar navigation -->
    <div id="slide-out" class="side-nav sn-bg-4 fixed">
        <ul class="custom-scrollbar">
            <!-- Logo -->
            <li class="logo-sn waves-effect py-3">
                <div>
                    <a href="<?= base_url('zindhu-hr/studyo/dashboard') ?>" class="pl-0"><img src="<?= base_url('assets/frontend/img/zindhu-hr-icon.svg') ?>" width="60"></a>
                </div>
            </li>
            <!--/. Logo -->

                <div class="user-box">
                    <div class="icon">
                        <span class="icon-company-icon"></span>
                    </div>
                    <div>
                        <div class="name"><?= $this->session->userdata('CompanyFullname') ?></div>
                        <div class="position">Stüdyo</div>
                    </div>
                </div>
                <!-- Side navigation links -->
                <li>
                    <ul class="collapsible collapsible-accordion left-menu">
                    <li id="-zindhu-hr-studyo-dashboard">
                        <a href="<?= base_url('zindhu-hr/studyo/dashboard') ?>" class="collapsible-header waves-effect arrow-r">
                            <i class="icon-dashboard-icon"></i> Dashboard
                        </a>
                    </li>
                    <li id="-zindhu-hr-studyo-profil">
                        <a href="<?= base_url('zindhu-hr/studyo/profil') ?>" class="collapsible-header waves-effect arrow-r">
                            <i class="icon-profile-2-icon"></i> Profil
                        </a>
                    </li>
                    <li id="-zindhu-hr-studyo-konusmalar">
                        <a href="<?= base_url('zindhu-hr/studyo/konusmalar') ?>" class="collapsible-header waves-effect arrow-r">
                            <i class="icon-conversations-icon"></i> 
                            Konuşmalar
                            <span class="badge badge-danger ml-auto" id="unread-count"></span>
                        </a>
                    </li>
                    <li id="-zindhu-hr-studyo-is-ilanlari">
                        <a href="<?= base_url('zindhu-hr/studyo/is-ilanlari') ?>" class="collapsible-header waves-effect arrow-r text-white">
                            <i class="icon-myads-icon"></i> İlanlarım
                        </a>
                    </li>
                    <li id="-zindhu-hr-studyo-cv-havuzu">
                        <a href="<?= base_url('zindhu-hr/studyo/cv-havuzu') ?>" class="collapsible-header waves-effect arrow-r">
                            <i class="icon-cvpool-icon"></i> CV Havuzu
                        </a>
                    </li>
                </ul>
                </li>
                <!--/. Side navigation links -->
                <div class="left-menu-create-ad">
                    <a href="<?= base_url('zindhu-hr/studyo/is-ilanlari?action=new') ?>" class="btn">İlan Oluştur <i class="la la-plus"></i></a>
                </div>
                <div class="left-menu-logout">
                    <a href="<?= base_url('zindhu-hr/studyo/cikis') ?>">
                        <span class="icon-logout-icon"></span>
                        Çıkış Yap
                    </a>
                    <div class="copyright">© 2021 Zindhu HR.</div>
                </div>
        </ul>
        
        <div class="sidenav-bg mask-strong"></div>
    </div>
    <!--/. Sidebar navigation -->

    <!-- Navbar -->

    <div class="header 
        <?= $this->uri->segment(3) == 'is-ilanlari' ? 'header-white' : '' ?>
        <?= $this->uri->segment(2) ? 'sub-header' : '' ?>">
        <div class="float-left">
            <a href="#" data-activates="slide-out" class="button-collapse black-text"><i class="icon-hamburger-menu-icon"></i></a>
        </div>
        <div></div>
        <div class="menu">

                <div class="user-menu d-flex">
                    <a class="btn contact-btn" onclick="zindhu_start_contact()"><i class="icon-support-icon"></i> Bize Ulaşın</a>
                    <div class="dropdown user-dropdown">
                        <button class="btn profile-btn" type="button" id="dropdownMenuButton" data-toggle="dropdown"  aria-expanded="true">
                            <i class="icon-profile-icon"></i>
                            <span class="name"><?= $this->session->userdata('CompanyFullname') ?></span>
                            <i class="fa fa-caret-down"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="<?= base_url('zindhu-hr/studyo/dashboard') ?>">
                                <i class="icon-dashboard-icon"></i>Dashboard
                            </a>
                            <a class="dropdown-item" href="<?= base_url('zindhu-hr/studyo/profil') ?>">
                                <i class="icon-profile-2-icon"></i>Profil
                            </a>
                            <a class="dropdown-item" href="<?= base_url('zindhu-hr/studyo/konusmalar') ?>">
                                <i class="icon-conversations-icon"></i>Konuşmalar 
                                <span class="badge badge-danger ml-auto" id="header-unread-count">0</span>
                            </a>
                            <a class="dropdown-item" href="<?= base_url('zindhu-hr/studyo/is-ilanlari') ?>">
                                <i class="icon-job-list-icon"></i>İlanlarım
                            </a>
                            <a class="dropdown-item" href="<?= base_url('zindhu-hr/studyo/cv-havuzu') ?>">
                                <i class="icon-job-list-icon"></i>CV Havuzu
                            </a>
                            <a class="dropdown-item logout" href="<?= base_url('zindhu-hr/studyo/cikis') ?>">
                                <i class="icon-logout-icon"></i>Çıkış Yap
                            </a>
                        </div>
                    </div>
                    <div class="dropdown">
                        <button class="btn lang-btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-world-icon"></i>
                            <?php if ($this->session->userdata('lang_code') == 'tr') { ?>
                                Türkçe
                            <?php } else { ?>
                                English
                            <?php } ?>
                            <i class="fa fa-caret-down"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="<?= base_url('language/change/tr?page=zindhu-hr/studyo') ?>">Türkçe</a>
                            <a class="dropdown-item" href="<?= base_url('language/change/en?page=zindhu-hr/studyo') ?>">English</a>
                        </div>
                    </div>
                </div>

        </div>
    </div>
    <!-- /.Navbar -->

</header>
<!--Main Navigation-->