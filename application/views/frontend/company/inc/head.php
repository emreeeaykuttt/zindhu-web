<meta name="google-site-verification" content="thk6a3gwobpRoIbHmnCPaA9iPWXVPx6ndUpgiYzRoQQ" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="icon" href="<?=base_url()?>assets/admin/img/favicon.png" type="image/x-icon">

<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300;400;700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="<?=base_url()?>assets/plugins/line-awesome/css/line-awesome.min.css" />

<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/bootstrap.min.css">
<!-- Material Design Bootstrap -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/mdb.css">
<link rel="stylesheet" href="<?=base_url()?>assets/plugins/swiper/swiper-bundle.min.css" />
<link rel="stylesheet" href="<?=base_url()?>assets/frontend/fonts/fonts.css" />
<link rel="stylesheet" href="<?=base_url()?>assets/frontend/fonts/icomoon/style.css" />
<link rel="stylesheet" href="<?=base_url()?>assets/frontend/css/style.css?v=2.0" />

<!-- Main Style -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/style.css?v=2.1">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-175472421-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-175472421-1');
</script>