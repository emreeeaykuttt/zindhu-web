<!-- Footer -->
<footer class="auth-footer page-footer pt-0 mt-5">

	<!-- Copyright -->
	<div class="footer-copyright py-3 text-center">
		<div class="container-fluid">
			© 2020 Copyright: <a href="<?=base_url()?>" target="_blank"> zindhu.com </a>
		</div>
	</div>
	<!-- Copyright -->

</footer>
<!-- Footer -->

<script type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/popper.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/pro/js/mdb.min.js"></script>
<script type="text/javascript">var base_url = '<?=base_url()?>'; var upload_folder = '<?=UPLOAD_FOLDER?>';</script>