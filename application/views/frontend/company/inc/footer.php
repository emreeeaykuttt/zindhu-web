<div class="footer">
    <div class="footer-top">
        <div class="footer-logo"></div>
        <div class="info">
            <h4>Zindhu</h3>
            <p>© 2021 Zindhu HR.</p>
        </div>
        <div class="address">
            <h4>İstanbul</h3>
            <p>Çeliktepe Mahallesi, İsmet İnönü Caddesi, No:11 501, 34413, Kağıthane</p>
        </div>
        <div class="social-media">
            <a href="https://www.linkedin.com/company/zindhu" target="_blank" class="social-link"><i class="la la-linkedin"></i></a>
            <a href="https://www.instagram.com/zindhugames/" target="_blank" class="social-link"><i class="la la-instagram"></i></a>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#privacy-modal" class="text-link">
                <?=l('kisisel_veri_ve_gizlilik_politikasi')?>
            </a>
        </div>
    </div>
    <hr />
    <div class="copyright d-flex align-md-items-center">
        <div class="col-md-1 col-3">
            <img src="<?=base_url()?>assets/frontend/img/iskur-logo.png" class="w-100" />
        </div>
        <div class="col-md-11 col-9">
            <p>
                Zindhu Oyun Yazılım Danışmanlık Hizmetleri A.Ş Özel İstihdam Bürosu Olarak 28.09.2020-27.09.2023
                tarihleri arasında faaliyette bulunmak üzere, Türkiye İş Kurumu tarafından 23.09.2020 tarih ve
                6761618 sayılı karar uyarınca 1186 nolu belge ile faaliyet göstermektedir. 4904 sayılı kanun
                uyarınca iş arayanlardan ücret alınması yasaktır. Şikayetleriniz için aşağıdaki telefon numaralarına
                başvurabilirsiniz. İstanbul Çalışma ve İş Kurumu İl Müdürlüğü: 0212 249 29 87 İstanbul Çalışma ve İş
                Kurumu İl Müdürlüğü Şişli Hizmet Merkezi: 02122910925
            </p>
        </div>
    </div>
</div>


<div class="modal" id="privacy-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Kişisel Veri ve Gizlilik Politikası</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="color: #000;">
                
                <p style="text-align: center;"><b><i>Zindhu Oyun Yazılım Danışmanlık Hizmetleri A.Ş</i></b></p>

                <p style="text-align: center;"><b>&ldquo;Şirket&rdquo;</b></p>

                <p style="text-align: center;"><b>KİŞİSEL VERİ İŞLEME VE GİZLİLİK POLİTİKASI</b></p>

                <p style="text-align: center;">Şirket&rsquo;imiz aşağıda detaylandırılan ve kimliği belirli veya belirlenebilir ger&ccedil;ek kişilere ilişkin her t&uuml;rl&uuml; bilgi kapsamındaki kişisel veriler a&ccedil;ısından veri sorumlusu olarak; <b><i>&Ccedil;eliktepe Mah. İsmet İn&ouml;n&uuml; Cad. Yıldırımlar İş Merkezi No:11 İ&ccedil; Kapı No: 501 Kağıthane/Istanbul</i></b><b> </b>adresinde mukim İstanbul Ticaret Sicili M&uuml;d&uuml;rl&uuml;ğ&uuml;&rsquo;ne <b><i>262674-5</i></b> sicil numarası ile kayıtlıdır.&nbsp;</p>

                <p><b>Şirket&rsquo;imiz internet sitesi &uuml;zerinden;</b></p>

                <p><strong>1.&nbsp;</strong>İnternet sitesi ziyaret&ccedil;ilerine ait ad-soyad ve elektronik posta adresi ve tarafımıza g&ouml;nderilecek mesaj iletinizde yer alabilecek kişisel verileri aşağıdaki ama&ccedil;ların yerine getirilmesi i&ccedil;in 6698 sayılı Kişisel Verilerin Korunması Kanunu&rsquo;da (&ldquo;<b>Kanun</b>&rdquo;) belirtilen ilgili kişisel veri işleme şartlarında dayanarak işlemektedir:</p>

                <ul>
                    <li>Şirket&rsquo;imiz ile ziyaret&ccedil;i arasın ger&ccedil;ekleşebilecek her t&uuml;rl&uuml; ilişki ve/veya iş birliği kapsamında veya ziyaret&ccedil;inin tercih ettiği diğer bir konuya ilişkin olarak iletişime ge&ccedil;ilmesi ve g&ouml;r&uuml;şme ger&ccedil;ekleştirilmesi,&nbsp;</li>
                    <li>internet sitesi ziyaret&ccedil;isinin taleplerinin yerine getirilmesi,&nbsp;</li>
                    <li>ziyaret&ccedil;inin talebine istinaden Şirket&rsquo;imizin y&uuml;r&uuml;tmekte olduğu faaliyetler ve bununla ilişkili diğer faaliyetlerimiz hakkında bilgi sağlanması,&nbsp;</li>
                    <li>onay verilmesi halinde ticari elektronik ileti g&ouml;nderilmesi, ve&nbsp;</li>
                    <li>mevzuattan kaynaklanan zamanaşımı s&uuml;resi doğrultusunda saklanması.</li>
                </ul>

                <p><strong>2.&nbsp;</strong>St&uuml;dyo yetkililerine ait ad-soyad, st&uuml;dyoya ait pay sahiplerinin bilgileri, linkedin dahil olmak &uuml;zrere sosyal medya hesaplarına ait bilgiler, kimliği belirli veya belirlenebilir bir ger&ccedil;ek kişiye ait nitelikte olması halinde elektronik posta adresi, telefon numarası ve adres bilgisi t&uuml;r&uuml;ndeki kişisel verileri aşağıdaki ama&ccedil;ların yerine getirilmesi i&ccedil;in Kanun&rsquo;da belirtilen ilgili kişisel veri işleme şartlarında dayanarak işlemektedir:</p>

                <ul>
                    <li>Zindhu insan kaynakları platformuna (&ldquo;<b>Platform</b>&rdquo;) st&uuml;dyo kaydının tam ve eksiksiz olarak&nbsp; tamamlanması ve kayıt s&uuml;recinin y&ouml;netilmesi,&nbsp;</li>
                    <li>Platform &uuml;zerinden st&uuml;dyoya işe alım konusunda danışmanlık verilmesi,&nbsp;</li>
                    <li>st&uuml;dyo ve oyun sektor&uuml;nde istihdam edilmek isteyen kişilerin bir araya getirilmesi,</li>
                    <li>gerekli olması veya st&uuml;dyonun talebi halinde Platform kullanımı ile ilgili olarak iletişime ge&ccedil;ilmesi,</li>
                    <li>st&uuml;dyoya sunulan istihdam y&ouml;netim hizmeti ve danışmanlık sunulması kapsamında Platform &uuml;zerindeki st&uuml;dyo kaydının y&ouml;netilmesi,&nbsp;</li>
                    <li>Platform kullanımına ilişkin eğitim verilmesi ve gerekli bilgilerin sağlanması,&nbsp;</li>
                    <li>oyun sekt&ouml;r&uuml;nde istihdam edilmek isteyen kişiler ile st&uuml;dyoları bir araya getirmek i&ccedil;in iletişime ge&ccedil;ilmesi,&nbsp;</li>
                    <li>st&uuml;dyonun istihdam etmek istediği &ccedil;alışan adaylarının bulunması ve st&uuml;dyolar ile buluşturulması hizmetlerinin y&uuml;r&uuml;t&uuml;lmesi,&nbsp;</li>
                    <li>işe alım s&uuml;re&ccedil;leri kapsamında st&uuml;dyoya hizmet sağlanması,&nbsp;</li>
                    <li>onay verilmesi halinde ticari elektronik ileti g&ouml;nderilmesi,&nbsp;</li>
                    <li>bahsi ge&ccedil;en hizmetlerin sunulması ve faaliyetlerin y&uuml;r&uuml;t&uuml;lmesi i&ccedil;in kişi ve şirketler ile iş birliği kurulması, ve&nbsp;</li>
                    <li>mevzuattan kaynaklanan zamanaşımı s&uuml;resi doğrultusunda saklanması.</li>
                </ul>

                <p><b>3. </b>&Ccedil;alışan adaylarına ait&nbsp; ad-soyad, e-posta adresi, telefon numarası, adres eğitim durumu, iş deneyimi, kullanımında yetkin olunan programlar, kimliği belirli veya belirlenebilir ger&ccedil;ek kişiye ilişkin bilgi i&ccedil;ermesi halinde portfolyodaki veriler ve &ouml;z ge&ccedil;mişinize ilişkin &ouml;zel veya genel nitelikte olabilecek kişisel verileri aşağıdaki ama&ccedil;ların yerine getirilmesi i&ccedil;in Kanun&rsquo;da belirtilen ilgili kişisel veri işleme şartlarında dayanarak işlemektedir:<b>&nbsp;</b></p>

                <ul>
                    <li>Platform kaydının tam ve eksiksiz olarak&nbsp; tamamlanması ve kayıt s&uuml;recinin y&ouml;netilmesi,&nbsp;</li>
                    <li>Platform &uuml;zerinden işe alım konusunda danışmanlık verilmesi,&nbsp;</li>
                    <li>istihdam amacıyla oyun st&uuml;dyoları ile bir araya gelinmesi,&nbsp;</li>
                    <li>gerekli olması veya st&uuml;dyonun talebi halinde Platform kullanımı ile ilgili olarak iletişime ge&ccedil;ilmesi, istihdam edilme s&uuml;recinizde &ccedil;alışan arayan oyun st&uuml;dyolarına sağladığınız verilerin iletilebilmesi, istihdam y&ouml;netim ve iş bulma hizmeti ve danışmanlık sunulması kapsamında Platform &uuml;zerindeki kaydın y&ouml;netilmesi,&nbsp;</li>
                    <li>Platform kullanımına ilişkin eğitim verilmesi ve gerekli bilgilerin sağlanması,</li>
                    <li>istihdam sağlanması kapsamında oyun st&uuml;dyoları ile bir araya gelinmesinin sağlanması i&ccedil;in iletişime ge&ccedil;ilmesi,</li>
                    <li>oyun st&uuml;dyolarının bulunması ve st&uuml;dyolar ile buluşma kapsamındaki hizmetlerin y&uuml;r&uuml;t&uuml;lmesi,</li>
                    <li>işe alım s&uuml;re&ccedil;leri kapsamında hizmet sağlanması,&nbsp;</li>
                    <li>onay verilmesi halinde&nbsp;</li>
                    <li>ticari elektronik ileti g&ouml;nderilmesi,&nbsp;</li>
                    <li>bahsi ge&ccedil;en hizmetlerin sunulması ve faaliyetlerin y&uuml;r&uuml;t&uuml;lmesi i&ccedil;in kişi ve şirketler ile iş birliği kurulması&nbsp;</li>
                    <li>ve mevzuattan kaynaklanan zamanaşımı s&uuml;resi doğrultusunda saklanması.</li>
                </ul>

                <p>İnternet sitesi ziyaret&ccedil;ileri, st&uuml;dyo yetkilileri ve &ccedil;alışan adayları tek tek &ldquo;<b>İlgili Kişi</b>&rdquo;, birlikte &ldquo;<b>İlgili Kişiler</b>&rdquo; olarak anılacaklardır.</p>

                <p><b>İlgili kişilerin, kişisel verileri ile ilgili Kanun kapsamındaki hakları aşağıdaki şekildedir:&nbsp;</b></p>

                <p>(a) Kişisel verilerinizin işlenip işlenmediğini &ouml;ğrenme, (b) Kişisel verileriniz işlenmişse buna ilişkin bilgi talep etme, (c) Kişisel verilerinizin işlenme amacını ve bunların amacına uygun kullanılıp kullanılmadığını &ouml;ğrenme, (&ccedil;) Yurt i&ccedil;inde veya yurt dışında kişisel verilerin aktarıldığı &uuml;&ccedil;&uuml;nc&uuml; kişileri bilme, (d) Kişisel verilerinizin eksik veya yanlış işlenmiş olması h&acirc;linde bunların d&uuml;zeltilmesini isteme, (e) Kişisel verilerinizin işlenmesini gerektiren sebeplerin ortadan kalkması halinde kişisel verilerinizin silinmesini veya yok edilmesini isteme, (f), (d) ve (e) bentleri uyarınca yapılan işlemlerin, kişisel verilerin aktarıldığı &uuml;&ccedil;&uuml;nc&uuml; kişilere bildirilmesini isteme, (g) İşlenen verilerin m&uuml;nhasıran otomatik sistemler vasıtasıyla analiz edilmesi suretiyle kişinin kendisi aleyhine bir sonucun ortaya &ccedil;ıkmasına itiraz etme, (ğ) Kişisel verilerin kanuna aykırı olarak işlenmesi sebebiyle zarara uğraması h&acirc;linde zararın giderilmesini talep etme.&nbsp;</p>

                <p>Bu haklar yazılı olarak veya g&uuml;venli elektronik imza, mobil imza ya da Şirket&rsquo;e daha &ouml;nce bildirilen ve Şirket&rsquo;in sisteminde kayıtlı bulunan elektronik posta adresi kullanmak suretiyle (Bu kapsamda <a href="mailto:info@zindhugames.com">info@zindhugames.com</a><b><i> </i></b>e-posta adresi &uuml;zerinden Şirket&rsquo;e ulaşabilecektir) veya başvuru amacına y&ouml;nelik geliştirilmiş bir yazılım ya da uygulama vasıtasıyla Şirket&rsquo;e iletilebilecektir.&nbsp;</p>

                <p><b>Gizlilik Politikası</b></p>

                <p>İşbu Kişisel Veri İşleme ve Gizlilik Politikası, t&uuml;m İlgili Kişiler i&ccedil;in ge&ccedil;erlidir. İlgili Kişiler tarafından Şirket&rsquo;e elektronik ortamda iletilen kişisel bilgiler, işbu Kişisel Veri İşleme ve Gizlilik Politikası ile belirlenen ama&ccedil;lar ve kapsam dışında, &uuml;&ccedil;&uuml;nc&uuml; kişilere a&ccedil;ıklanmayacaktır.&nbsp;</p>

                <p>İlgili Kişiler&rsquo;e ait her t&uuml;rl&uuml; bilgi &ldquo;<b>Gizli Bilgiler</b>&rdquo; olarak anılacaktır.</p>

                <p>İnternet Sitesi i&ccedil;erisinde referans g&ouml;sterilen veya bağlantı sağlanan diğer sitelerin/uygulamaların/reklamların i&ccedil;eriğinden ya da gizlilik uygulamalarından Şirket sorumlu tutulamaz.</p>

                <p>Şirket Gizli Bilgiler&rsquo;i, burada belirtilenler saklı kalmak &uuml;zere, kesinlikle &ouml;zel ve gizli tutmayı, bunu bir sır saklama y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml; olarak addetmeyi ve gizliliğin sağlanması ve s&uuml;rd&uuml;r&uuml;lmesi, Gizli Bilgi&rsquo;nin tamamının veya herhangi bir kısmının kamu alanına girmesini veya yetkisiz kullanımını veya &uuml;&ccedil;&uuml;nc&uuml; bir kişiye ifşasını &ouml;nlemek i&ccedil;in gerekli tedbirleri almayı ve gerekli &ouml;zeni g&ouml;stermeyi taahh&uuml;t etmektedir. Ancak İlgili Kişiler&rsquo;in bilgilerini kendi cihazı &uuml;zerinden vermesi sebebiyle ilgisiz kişiler tarafından erişilmemesi amacıyla vir&uuml;s ve benzeri zararlı uygulamalara karşı gerekli tedbirlerin İlgili Kişiler tarafından alınmaması ve Şirket&rsquo;in gerekli bilgi g&uuml;venliği &ouml;nlemlerini almasına karşın, sisteme yapılan saldırılar sonucunda gizli bilgilerin zarar g&ouml;rmesi veya &uuml;&ccedil;&uuml;nc&uuml; kişilerin eline ge&ccedil;mesi durumunda, Şirket&rsquo;in herhangi bir sorumluluğu olmayacaktır.</p>

                <p>Şirket, İlgili Kişiler&rsquo;in Gizli Bilgiler&rsquo;ini (a) y&uuml;r&uuml;rl&uuml;kte olan kanun veya d&uuml;zenlemeler ya da verilmiş olan bir yargı kararı ya da idari emir gereğince a&ccedil;ıklaması gerektiği durumlarda ve/veya (b) İlgili Kişiler&rsquo;in rızasının bulunduğu takdirde ve/veya (c) işbu Kişisel Veri İşleme ve Gizlilik Politikası&rsquo;na uygun şekilde &uuml;&ccedil;&uuml;nc&uuml; kişilere a&ccedil;ıklayabilir ve/veya kullanabilir.&nbsp;</p>

                <p>İlgili Kişiler&rsquo;in, internet sitesinde herkese a&ccedil;ık alanlarda ifşa ettiği, yorumlarda veya mesajlarda paylaştığı bilgilerinin &uuml;&ccedil;&uuml;nc&uuml; kişiler tarafından kullanılması sonucunda doğacak zarardan Şirket sorumlu değildir.&nbsp;</p>

                <p>Şirket, işbu Kişisel Veri İşleme ve Gizlilik Politikası h&uuml;k&uuml;mlerini her zaman g&uuml;ncelleyebilir, değiştirebilir veya y&uuml;r&uuml;rl&uuml;kten kaldırabilir.&nbsp; G&uuml;ncellenen, değiştirilen ya da y&uuml;r&uuml;rl&uuml;kten kaldırılan her h&uuml;k&uuml;m, yayın tarihinde İlgili Kişi i&ccedil;in h&uuml;k&uuml;m ifade edecektir. &nbsp;</p>


            </div>
        </div>
    </div>
</div>