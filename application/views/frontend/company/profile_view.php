
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Stüdyo Profil">

    <title>Stüdyo Profil</title>

    <?php include('inc/head.php'); ?>

</head>

<body class="fixed-sn white-skin">

	<?php include('inc/header.php'); ?>

	<!-- Main layout -->
	<main>
		<div class="container-fluid">

			<section>

		        <!-- Gird column -->
		        <div class="col-md-12">

		         	<h5 class="my-4 dark-grey-text font-weight-bold" id="module-title">Profil</h5>

		         

							<?php if (isset($profile_status)) : ?>
								<div class="alert <?= $profile_status['key'] ? 'alert-success' : 'alert-danger' ?>">
									<span><?= $profile_status['message'] ?></span>
								</div>
							<?php endif ?>
							
							<form action="<?= base_url('zindhu-hr/studyo/profil') ?>" method="POST" id="register-form" autocomplete="off">

								<div class="row">
									
									<div class="col-md-6">
										<div class="custom-input">
											<label for="firstname" class="mdb-main-label">İsim</label>
											<input 
												type="text" 
												name="firstname" 
												id="firstname" 
												value="<?=set_value('firstname') ? set_value('firstname') : $profile->firstname?>" 
												class="form-control">
					                		<?=form_error('firstname', '<div class="red-text small">', '</div>')?>
										</div>		
									</div>

									<div class="col-md-6">
										<div class="custom-input">
											<label for="lastname" class="mdb-main-label">Soyisim</label>
											<input 
												type="text" 
												name="lastname" 
												id="lastname" 
												value="<?=set_value('lastname') ? set_value('lastname') : $profile->lastname?>" 
												class="form-control">
					                		<?=form_error('lastname', '<div class="red-text small">', '</div>')?>
										</div>	
									</div>

									<div class="col-md-12">
										<div class="custom-input">
											<label for="company-name" class="mdb-main-label">Stüdyo İsmi</label>
											<input 
												type="text" 
												name="company_name" 
												id="company-name" 
												value="<?=set_value('company_name') ? set_value('company_name') : $profile->company_name?>" 
												class="form-control">
					                		<?=form_error('company_name', '<div class="red-text small">', '</div>')?>
										</div>
									</div>

									<div class="col-md-12">
										<div class="custom-input">
											<label for="partners" class="mdb-main-label">Kurucu Ortaklar</label>
											<input 
												type="text" 
												name="partners" 
												id="partners" 
												value="<?=set_value('partners') ? set_value('partners') : $profile->partners?>" 
												class="form-control">
					                		<?=form_error('partners', '<div class="red-text small">', '</div>')?>
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="custom-input">
											<label for="email" class="mdb-main-label">Stüdyo Mail Adresi</label>
											<input 
												type="text" 
												name="email" 
												id="email" 
												value="<?=set_value('email') ? set_value('email') : $profile->email?>" 
												class="form-control">
					                		<?=form_error('email', '<div class="red-text small">', '</div>')?>
										</div>
									</div>

									<div class="col-md-6">
										<div class="custom-input">
											<label for="phone" class="mdb-main-label">Stüdyo Telefon Numarası</label>
											<input 
												type="text" 
												name="phone" 
												id="phone" 
												value="<?=set_value('phone') ? set_value('phone') : $profile->phone?>" 
												class="form-control">
					                		<?=form_error('phone', '<div class="red-text small">', '</div>')?>
										</div>
									</div>


									<div class="col-md-6">
										<div class="custom-input">
											<label class="mdb-main-label">Stüdyo Şehir</label>
											<select 
												name="city_id" 
												id="city-id" 
												class="mdb-select" 
												searchable="Arama">
								                <option value="" disabled selected>Seçiniz</option>
								            </select>	
					                		<?=form_error('city_id', '<div class="red-text small">', '</div>')?>
										</div>
									</div>

									<div class="col-md-6">
										<div class="custom-input">
											<label class="mdb-main-label">Stüdyo İlçe</label>
											<select 
												name="district_id" 
												id="district-id" 
												class="mdb-select" 
												searchable="Arama">
								                <option value="" disabled selected>Seçiniz</option>
								            </select>
					                		<?=form_error('district_id', '<div class="red-text small">', '</div>')?>
										</div>
									</div>

									<div class="col-md-6">
										<div class="custom-input">
											<label class="mdb-main-label">Çalışan Sayısı</label>
											<input 
												type="number"
												min="1" 
												name="number_employees" 
												id="number-employees" 
												value="<?=set_value('number_employees') ? set_value('number_employees') : $profile->number_employees?>" 
												class="form-control">
					                		<?=form_error('number_employees', '<div class="red-text small">', '</div>')?>
										</div>
									</div>

									<div class="col-md-6">
										<div class="custom-input">
											<label class="mdb-main-label">Stüdyonun İlgili Olduğu Oyun Türü</label>
											<select 
												name="game_type" 
												id="game-type" 
												class="mdb-select" 
												searchable="Arama">
								                <option value="" disabled selected>Seçiniz</option>
								            </select>
					                		<?=form_error('game_type', '<div class="red-text small">', '</div>')?>
										</div>
									</div>

									<div class="col-md-6">
										<div class="custom-input">
											<label for="company-linkedin" class="mdb-main-label">Stüdyo Linkedin</label>
											<input 
												type="text" 
												name="company_linkedin" 
												id="company-linkedin" 
												value="<?=set_value('company_linkedin') ? set_value('company_linkedin') : $profile->company_linkedin?>" 
												class="form-control">
					                		<?=form_error('company_linkedin', '<div class="red-text small">', '</div>')?>
										</div>		
									</div>

									<div class="col-md-6">
										<div class="custom-input">
											<label for="founder-linkedin" class="mdb-main-label">Stüdyo Kurucu Linkedin</label>
											<input 
												type="text" 
												name="founder_linkedin" 
												id="founder-linkedin" 
												value="<?=set_value('founder_linkedin') ? set_value('founder_linkedin') : $profile->founder_linkedin?>" 
												class="form-control">
					                		<?=form_error('founder_linkedin', '<div class="red-text small">', '</div>')?>
										</div>		
									</div>

								</div>

								<div class="text-right">
									<button class="btn btn-dark btn-rounded mt-3 py-3 px-5">GÜNCELLE</button>
								</div>
							</form>



		        </div>
		        <!-- Gird column -->

		    </section>

		</div>
	</main>
	<!-- Main layout -->

    <?php include('inc/footer.php'); ?>
    <?php include('inc/script.php'); ?>

    <script type="text/javascript">
    	city_list();
    	game_list();

    	$(document).ready(function () {
	    	$('#game-type').materialSelect();
	    	
	    	if ('<?=set_value('district_id') ? set_value('district_id') : $profile->district_id?>' == '') {
	    		$('#district-id').materialSelect();
	    	}

	    	$('#city-id').change(function(){
	    		let city_id = $(this).val();
	    		district_list(city_id);
	    	});
	    });

	    function city_list()
		{
			let set_value = '<?=set_value('city_id') ? set_value('city_id') : $profile->city_id?>';
			
		    $.ajax({
		        url : base_url + 'ajax/city',
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if (data != "") 
		            {
		            	let city_id;
		            	let selected = '';
		                let count = data.length;
		                $.each(data, function(key, val){
		                	if (set_value == val.id) {
		                		selected = 'selected';
		                		city_id = val.id
		                	} else {
		                		selected = ''; 
		                	}

		                    $('#city-id').append('<option value="'+val.id+'" '+selected+'>'+val.city+'</option>');
		                    if (key+1 === count) {
		                    	setTimeout(function(){
	                            	$('#city-id').materialSelect();
	                            }, 100);
		                    }
		                });

		                if (city_id) {
		                	district_list(city_id);
		                }
		            }
		        }
		    });
		}

		function district_list(city_id)
		{
			let set_value = '<?=set_value('district_id') ? set_value('district_id') : $profile->district_id?>';
			
		    $.ajax({
		        url : base_url + 'ajax/district/' + city_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if (data != "") 
		            {
		            	let selected = ''; 
		                let count = data.length;
		                $.each(data, function(key, val){
		                	if (set_value == val.id) {
		                		selected = 'selected';
		                	} else {
		                		selected = ''; 
		                	}

		                    $('#district-id').append('<option value="'+val.id+'" '+selected+'>'+val.district+'</option>');
		                    if (key+1 === count) {
	                            $('#district-id').materialSelect();
		                    }
		                });
		            }
		        }
		    });
		}

		function game_list()
		{
			let set_value = '<?=set_value('game_type') ? set_value('game_type') : $profile->game_type?>';

			$.ajax({
		        url : base_url + 'ajax/game',
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if (data != "") 
		            {
		            	let selected = ''; 
		            	let count = data.length;
		            	$.each(data, function(key, val){
		            		if (set_value == val.id) {
		                		selected = 'selected';
		                	} else {
		                		selected = ''; 
		                	}

		            		$('#game-type').append('<option value="'+val.id+'" '+selected+'>'+val.title+'</option>');
		            		if (key+1 === count) {
		            			setTimeout(function(){
		            				$('#game-type').materialSelect();
		            			}, 100);
						    }
		            	});
		            }
		        }
		    });
		}
    </script>

</body>

</html>