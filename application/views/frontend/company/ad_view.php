<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Stüdyo İş İlanları">

    <title>Stüdyo İş İlanları</title>

    <?php include('inc/head.php'); ?>
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/addons/datatables.min.css">

</head>

<body class="fixed-sn white-skin">

	<?php include('inc/header.php'); ?>

	<!-- Main layout -->
	<main>
		<div class="container-fluid">

			<section>
				
		        <!-- Gird column -->
		        <div class="col-md-12">

		         	<h5 class="mb-3 dark-grey-text font-weight-bold" id="module-title">
		         		İlanlarım
		         	</h5>
					 <button onclick="add()" class="create-ad-btn d-flex btn btn-lg btn-green mb-3">
					 	<div>İlan Oluştur</div>
						<div class="ml-5"> <i class="la la-plus"></i></div>
					 </button>
					 <div class="d-flex justify-content-end ad-search">
					 	<div class="conversation-search">
							<i class="la la-search"></i>
							<input class="form-control" type="search" id="adSearch" placeholder="İlanlarda ara">
						</div>
					 </div>
					
					<div class="table-responsive">
						<table id="ad-table" class="table" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th width="31%" class="position-padding">Pozisyon</th>
									<th width="18%">Yayın Durumu</th>
									<th width="18%">Onay Durumu</th>
									<th width="33%"></th>
								</tr>
							</thead>
						</table>
					</div>

		        </div>
		        <!-- Gird column -->

		    </section>

		</div>
	</main>
	<!-- Main layout -->

    <?php include('inc/footer.php'); ?>

     <!-- Ad Form Modal -->
    <div class="modal fade" id="ad-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
      aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header bg-transparent border-0">
					<p class="heading lead" id="modal-ad-title">İlan</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body">
					<form action="#" id="ad-form" enctype="multipart/form-data">

						<input type="hidden" name="ad_id" value="">
						<div class="row">

							<div class="col-md-12">
								<div class="custom-input">
									<label for="title" class="mdb-main-label">Başlık:</label>
									<input type="text" class="form-control" name="title" id="title">
									<div class="red-text small error-text" id="title_error"></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="custom-input">
									<label class="mdb-main-label">Pozisyon:</label>
									<select class="mdb-select md-form disable-toggle-all" name="position_json[]" id="position-json" searchable="Arama" multiple>
										<option value="0" disabled selected>Seçiniz</option>
									</select>
									<div class="red-text small error-text" id="position_json_error"></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="custom-input">
									<label class="mdb-main-label">Çalışma Şekli:</label>
									<select class="mdb-select" name="study_type" id="study-type">
										<option value="0" disabled selected>Seçiniz</option>
										<option value="Full-Time">Full-Time</option>
										<option value="Part-Time">Part-Time</option>
										<option value="Freelance">Freelance</option>
									</select>
									<div class="red-text small error-text" id="study_type_error"></div>
								</div>
							</div>

							<div class="col-md-12">
								<div class="mt-3">
								<label for="short-description" class="mdb-main-label">Kısa Açıklama:</label>
								<textarea type="text" name="short_description" id="short-description" class="md-textarea form-control" rows="2"></textarea>
								<div class="red-text small error-text" id="short_description_error"></div>
								</div>
							</div>

						</div>

			            <div class="mt-3">
			            	<label for="job-description" class="mdb-main-label">Detay:</label>
			                <textarea type="text" name="job_description" id="job-description" class="md-textarea form-control editor" rows="6"></textarea>
			                <div class="red-text small error-text" id="job_description_error"></div>
			            </div>

			            <div class="row">
				            
				            <div class="col-md-6">
								<div class="custom-input">
									<label class="mdb-main-label">Şehir:</label>
									<select class="mdb-select" name="city_id" id="city-id" searchable="Arama">
										<option value="0" disabled selected>Seçiniz</option>
									</select>
									<div class="red-text small error-text" id="city_id_error"></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="custom-input">
									<label class="mdb-main-label">İlçe:</label>
									<select class="mdb-select" name="district_id" id="district-id" searchable="Arama">
										<option value="0" disabled selected>Seçiniz</option>
									</select>
									<div class="red-text small error-text" id="district_id_error"></div>
								</div>
							</div>

							<div class="col-md-12 d-none city-text">
								<div class="custom-input mt-4">
									<label for="city-text" class="mdb-main-label">Ülke-Şehir:</label>
									<input type="text" class="form-control" name="city_text" id="city-text">
									<div class="red-text small error-text" id="city_text_error"></div>
								</div>
							</div>

							<div class="col-12">
								<div class="my-3">
									<input type="checkbox" class="form-check-input" name="is_remote" id="is-remote" value="1">
									<label class="form-check-label" for="is-remote">Uzaktan(Remote) Çalışma</label>
									<div class="red-text small error-text" id="is_remote_error"></div>
								</div>
							</div>

						</div>

			            <div class="mt-3 publish-status">
							<label for="status" class="mdb-main-label">Yayınlama Durumu:</label><br>

							<div class="ad-status">
								<div id="ad-status-text"><!-- text --></div>
								<div class="custom-control custom-switch">
									<input 
										type="checkbox" 
										name="status"
										class="custom-control-input" 
										id="ad-status">
									<label class="custom-control-label" for="ad-status"></label>
								</div>
							</div>

							<div class="red-text small error-text" id="status_error"></div>
						</div>
					</form>
				</div>

				<!-- Footer -->
				<div class="modal-footer justify-content-end">
					<a onclick="save()" id="ad-btn" class="btn btn-dark text-center">Kaydet</a>
				</div>

			</div>
			<!-- Content -->
		</div>
    </div>
    <!-- Ad Form Modal -->

    <?php include('inc/script.php'); ?>
    <script type="text/javascript" src="<?=base_url()?>assets/admin/js/addons/datatables.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/plugins/ckeditor/ckeditor.js"></script>

    <script type="text/javascript">
    	
		var table;
		var action_param = getUrlParameter('action');
	    var params = '';

	    position_list();
	    city_list();
		
		$(document).ready(function(){

			CKEDITOR.replace('job-description',{
		        customConfig: base_url + 'assets/plugins/ckeditor/basic_config.js'
		    });

		    if (action_param == 'new') {
				add();
			}

		    table = $('#ad-table').DataTable({
				"bLengthChange" : false,
				'language': {
					"sEmptyTable": '<div class="table-info-screen"><img src="/assets/frontend/img/no-profile-found.svg"><p>Kriterler uygun ilan bulunamamıştır.</p></div>',
					"infoEmpty": '<div class="table-info-screen"><img src="/assets/frontend/img/no-profile-found.svg"><p>İlan bulunmamaktadır.</p></div>',
					"sZeroRecords": '<div class="table-info-screen"><img src="/assets/frontend/img/no-profile-found.svg"><p>Eşleşen ilan bulunamadı.</p></div>',
					"sLoadingRecords": '<div class="table-info-screen"><p>Yükleniyor..</p></div>',
					"sProcessing":     '<div class="table-info-screen"><p>İşleniyor...</p></div>',
		            'url': base_url + 'assets/admin/js/addons/language_tr.json'
		        },
		    	

		        'processing': true,
		        'serverSide': true,
		        'order': [],
		        // 'lengthMenu': [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'Tüm']],
		        'ajax': {
		            'url': base_url + 'company/ad/ajax_list' + params,
		            'type': 'POST'
		        },

		        'dom': '<"w-50 float-left"l><"w-50 float-left"fB><"table-content" tr><"w-50 float-left"i><"w-50 float-left"p>',

		  		'columns': [
		  			{ 'data': 'title' },
		  			{ 
		  				'data': 'status', 
		  				'render': function ( data, type, row, meta ) {
		                    if (data == 1) {
		                        return '<span class="status active"><i class="la la-check"></i> Aktif</span>'
		                    } else{
		                        return '<span class="status passive"><i class="la la-times"></i> Pasif</span>'
		                    }
		                }
		  			},
		            { 
		            	'data': 'admin_confirm', 
		            	'render': function ( data, type, row, meta ) {
		                    if (data == 'pending') {
		                        return '<span class="status waiting"><i class="la la-clock"></i> Beklemede</span>'
		                    } else if (data == 'approved') {
		                        return '<span class="status approved"><i class="la la-check"></i> Onaylandı</span>'
		                    } else if (data == 'rejected') {
		                        return '<span class="status rejected"><i class="la la-times"></i> Reddedildi</span>'
		                    }
		                }
		            },
		            { 'data': 'transactions', 'orderable': false}
		        ],

		    });

		    $('[data-toggle="tooltip"]').tooltip();

		    $('#ad-status').change(function() {
				if ($(this).is(":checked")) {
					$('#ad-status-text').text('Aktif');
				} else {
					$('#ad-status-text').text('Pasif');
				}
			});

			$('#city-id').change(function() {
				let city_id = $(this).val();

				if (city_id == 341) {
					$('.city-text').removeClass('d-none');
					$('.city-text').addClass('d-block');
				} else {
					$('.city-text').removeClass('d-block');
					$('.city-text').addClass('d-none');
					$('[name="city_text"]').val('').trigger("change");
				}

				district_list(city_id);
			});

			$('#adSearch').on('keyup', function () {
				table.search( this.value ).draw();
			});

		});

		function add()
		{
			$('#ad-modal').modal('show');
			$('#modal-ad-title').text('İlan Ekle');
			$('#ad-form')[0].reset();
			$('[name="ad_id"]').val('');
			$('.error-text').text('');
			$('#ad-status').attr('checked','checked');
			$('#ad-status-text').text('Aktif');
			$('#district-id').html('<option value="0" disabled selected>Seçiniz</option>');
			$('#position-json, #study-type, #city-id, #district-id').materialSelect('destroy');
    		$('#position-json, #study-type, #city-id, #district-id').materialSelect();
    		$('[name="job_description"]').val(CKEDITOR.instances['job-description'].setData(''));
		}

		function edit(ad_id)
		{
			$('#ad-modal').modal('show');
			$('#modal-ad-title').text('İlan Düzenle');
			$('#ad-form')[0].reset();
			$('.error-text').text('');

			$.ajax({
		        url : base_url + 'company/ad/ajax_view/' + ad_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	$('[name="ad_id"]').val(data.id);
		            $('[name="title"]').val(data.title).trigger("change");
		            $('[name="short_description"]').val(data.short_description).trigger("change");
		            $('[name="position_json[]"]').val(JSON.parse(data.position_json));
		            $('[name="study_type"]').val(data.study_type);
		            CKEDITOR.instances['job-description'].setData(data.job_description);
		            $('[name="city_id"]').val(data.city_id);
            		$('[name="city_text"]').val(data.city_text).trigger("change");

            		if (data.is_remote == 1) {
		                $('#is-remote').attr('checked', true);
		            }else {
		                $('#is-remote').attr('checked', false);
		            }

            		district_list(data.city_id, data.district_id);

		            $('#position-json, #study-type, #city-id').materialSelect('destroy');
            		$('#position-json, #study-type, #city-id').materialSelect();

            		if (data.city_id == 341) {
						$('.city-text').removeClass('d-none');
						$('.city-text').addClass('d-block');
					} else {
						$('.city-text').removeClass('d-block');
						$('.city-text').addClass('d-none');
						$('[name="city_text"]').val('').trigger("change");
					}

		            if (data.status == 0) {
		            	$('#ad-status-text').text('Pasif');
	                    $('#ad-status').removeAttr('checked');
	                } else if(data.status == 1) {
	                	$('#ad-status-text').text('Aktif');
	                    $('#ad-status').attr('checked','checked');
	                }
		        }
		    });
		}

		function remove(ad_id)
		{
			Swal.fire({
		        title: 'Silmek istiyor musunuz?',
		        text: 'Sil dediğinizde bu ilana ait tüm bilgiler silinir.',
		        icon: 'success',
		        showCancelButton: true,
		        confirmButtonColor: '#3085d6',
		        cancelButtonText: 'Vazgeç',
		        cancelButtonColor: '#d33',
		        confirmButtonText: 'Evet'
		    }).then((result) => {
		        if (result.value) 
		        {
		        	$.ajax({
				        url : base_url + 'company/ad/ajax_remove/' + ad_id,
				        type: 'POST',
				        dataType: 'JSON',
				        success: function(data)
				        {
				            toastr.success(data.message);
				            reload_table();
				        }
				    });
		        }
    		});
		}

		function save()
		{
			$('[name="job_description"]').val(CKEDITOR.instances['job-description'].getData());
			
			let formData = new FormData($('#ad-form')[0]);

			$('#ad-btn').text('Kaydediyor...');
		    $('#ad-btn').attr('disabled',true);

		    $.ajax({
		        url : base_url + 'company/ad/ajax_save',
		        type: 'POST',
		        data: formData,
		        contentType: false,
		        processData: false,
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	$('.error-text').text('');
		        	
		            if(data.status) 
		            {
		                $('#ad-modal').modal('hide');
		                $('#ad-form')[0].reset();
		                toastr.success(data.message);
		                reload_table();
		            }
		            else
		            {   
		            	toastr.error('Gerekli alanları doldurmadan kayıt edemezsiniz');

		                for (var i = 0; i < data.inputerror.length; i++) 
		                {
		                    $('#' + data.inputerror[i] + '_error').text(data.error_string[i]); 
		                }
		                
		                $('#ad-form').stop().animate({
		                    scrollTop:0
		                });
		            }

		            $('#ad-btn').text('Kaydet'); 
		            $('#ad-btn').attr('disabled',false);
		        }
		    });
		}
		
		function reload_table()
		{
		    table.ajax.reload(null,false);
		}

		function position_list()
		{
		    $.ajax({
		        url : base_url + 'ajax/position',
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if (data != "") 
		            {
		                let count = data.length;
		                $.each(data, function(key, val){
		                    $('#position-json').append('<option value="'+val.id+'">'+val.title+'</option>');
		                    if (key+1 === count) {
		                        setTimeout(function(){
		                            $('#position-json').materialSelect();
		                        }, 100);
		                    }
		                });
		            }
		        }
		    });
		}

		function city_list()
		{
		    $.ajax({
		        url : base_url + 'ajax/city',
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if (data != "") 
		            {
		                let count = data.length;
		                $.each(data, function(key, val){
		                    $('#city-id').append('<option value="'+val.id+'">'+val.city+'</option>');
		                    if (key+1 === count) {
		                        setTimeout(function(){
		                            $('#city-id').materialSelect();
		                        }, 100);
		                    }
		                });
		            }
		        }
		    });
		}

		function district_list(city_id, set_value = '')
		{	
			$('#district-id').html('<option value="0" disabled selected>Seçiniz</option>');

		    $.ajax({
		        url : base_url + 'ajax/district/' + city_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if (data) 
		            {
		            	let selected = ''; 
		                let count = data.length;
		                $.each(data, function(key, val){
		                	if (set_value == val.id) {
		                		selected = 'selected';
		                	} else {
		                		selected = ''; 
		                	}

		                    $('#district-id').append('<option value="'+val.id+'" '+selected+'>'+val.district+'</option>');
		                    if (key+1 === count) {
	                            $('#district-id').materialSelect();
		                    }
		                });
		            } else {
		            	$('#district-id').materialSelect();
		            }
		        }
		    });
		}

    </script>

</body>

</html>