<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?=$this->session->userdata('CompanyName')?>">
    <meta name="keywords" content="<?=$this->session->userdata('CompanyName')?>">

    <title><?=$this->session->userdata('CompanyName')?></title>

    <?php include('inc/head.php'); ?>

    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/addons/datatables.min.css">
    <style type="text/css">
    	.conversation-wrapper .dataTables_info {
    		opacity: 1
    	}
    </style>

</head>

<body class="fixed-sn white-skin">

	<?php include('inc/header.php'); ?>

	<main>
		<div class="container-fluid position-relative">

			<div class="d-flex justify-content-center page-loading">
                <div class="spinner-border" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>

			<section class="list-view d-none">

				<h5 class="mb-3 dark-grey-text font-weight-bold" id="module-title">CV Havuzu</h5>
				<!-- <div class="listing-pool">
					<p><span class="dark">İstanbul</span> içinde "<span class="dark">iOS Developer</span>” pozisyonunda <span class="dark">36</span> kişi gösteriliyor.</p>
				</div> -->
			
				<div class="conversation-wrapper">
					<div class="row s-pool-view">
						<div class="col-xl-8">
							<div class="table-responsive">
								<table id="pool-table" class="table table-borderless" cellspacing="0" width="100%">
									<tbody>
										
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-xl-4">
							<div class="list-filter-mobile-trigger"><i class="las la-filter"></i>
								<div>Filtrele<span class="black-text"></span></div>
							</div>
							<div class="card-body" id="user-filter">
								<div class="row">
									<div class="col-md-12">
										<div class="custom-input">
											<label class="mdb-main-label">Kişi ara:</label>
											<input type="search" id="pool-search" class="form-control" placeholder="Bir isim yazın"/>
										</div>
										<hr />
									</div>
									<div class="w-100 text-center d-none" id="clear-all-filters">
										<a href="javascript:void(0)" onclick="user_search_clear()" class="btn btn-rounded btn-gray mt-3">
											<i class="la la-times-circle mr-1"></i> Tüm filtreleri temizle
										</a>
									</div>
									<div class="col-md-12">
										<div class="custom-input">
											<label class="mdb-main-label">Pozisyon:</label>
											<select class="mdb-select" id="personal-information-position-id" searchable="Arama">
												<option value="" selected>Hepsi</option>
											</select>
										</div>
									</div>

									<div class="col-md-12">
										<div class="custom-input">
											<label class="mdb-main-label">Yaşadığı Şehir:</label>
											<select class="mdb-select" id="personal-information-city-id" searchable="Arama">
												<option value="" selected>Hepsi</option>
											</select>
										</div>
									</div>

									<div class="col-md-12">
										<div class="pl-3 mb-2 mt-2">
											<input type="checkbox" class="form-check-input" id="personal-information-is-remote" value="1">
											<label class="form-check-label" for="personal-information-is-remote">Uzaktan çalışma</label>
										</div>
									</div>

									<div class="col-md-12">
										<div class="custom-input">
											<label class="mdb-main-label">Yaşamak İstediği Şehir:</label>
											<select class="mdb-select" id="personal-information-city-json" searchable="Arama">
												<option value="" selected>Hepsi</option>
											</select>
										</div>
									</div>

									<div class="col-md-12">
										<div class="custom-input">
											<label class="mdb-main-label">Eğitim Seviyesi:</label>
											<select class="mdb-select" id="education-level">
												<option value="" selected>Hepsi</option>
												<option value="on-lisans">Ön Lisans</option>
												<option value="lisans">Lisans</option>
												<option value="yuksek-lisans">Yüksek Lisans</option>
												<option value="doktora">Doktora</option>
											</select>
										</div>
									</div>

									<div class="col-md-12">
										<div class="custom-input">
											<label class="mdb-main-label">Üniversite:</label>
											<select class="mdb-select" id="education-university-id" searchable="Arama">
												<option value="" selected>Hepsi</option>
											</select>
										</div>
									</div>

									<div class="col-md-12">
										<div class="custom-input">
											<label class="mdb-main-label">Kullanılan Program:</label>
											<select class="mdb-select" id="used-program-id" searchable="Arama">
												<option value="" selected>Hepsi</option>
											</select>
										</div>
									</div>

									<div class="col-md-12">
										<div class="custom-input">
											<label class="mdb-main-label">Program Kullanma Süresi:</label>
											<select class="mdb-select" id="used-program-duration" searchable="Arama">
												<option value="" selected>Hepsi</option>
												<?php for ($i=1; $i <= 30; $i++) { ?>
													<option value="<?=$i?>"><?=$i?>+ Yıl</option>
												<?php } ?>
											</select>
										</div>
									</div>

									<div class="col-md-12">
										<div class="custom-input">
											<label class="mdb-main-label">Gelecekte üretiminde bulunmak istediğiniz tür:</label>
											<select class="mdb-select" id="game-future-json" searchable="Arama">
												<option value="" selected>Hepsi</option>
											</select>
										</div>
									</div>

									<div class="col-md-12">
										<div class="custom-input">
											<label class="mdb-main-label">Geçmişte üretiminde bulunduğunuz tür:</label>
											<select class="mdb-select" id="game-past-json" searchable="Arama">
												<option value="" selected>Hepsi</option>
											</select>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>

			</section>

		</div>
	</main>

    <?php include('inc/footer.php'); ?>

    <!-- CV View Modal -->
    <div class="modal fade pool-cv-modal" id="cv-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
      aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document" >
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header bg-transparent border-0 position-absolute w-100">
					<p></p>
					<button type="button" class="close ml-auto right-0" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="cv-list">
					
				</div>
			</div>
			<!-- Content -->
		</div>
    </div>
    <!-- CV View Modal -->
    

    <?php include('inc/script.php'); ?>
    <script type="text/javascript" src="<?=base_url()?>assets/admin/js/addons/datatables.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/admin/js/cv_helper.js"></script>

    <script type="text/javascript">
    	
		var table;
	    var export_name = module_export_name('cv_havuzu');
	    var watch_status = getUrlParameter('watch');
	    var conversation_status = getUrlParameter('conversation');
	    var ad_param = getUrlParameter('ad');
	    var params = '';

	    position_list();
	    city_list();
	    university_list();
	    program_list();
	    game_list();
		
		$(document).ready(function(){

			if (watch_status) {
				params = '?watch=1';
				$('#module-title').html('İzlenilen Kişiler');
			} else if (conversation_status) {
				params = '?conversation=1';
				$('#module-title').html('Görüşülen Kişiler');
			} else if (ad_param) {
				params = '?ad=' + ad_param;
				$('#module-title').html('İlana Başvuran Kişiler <span id="ad-title"></span>');

				$.ajax({
			        url : base_url + 'company/ad/ajax_view/' + ad_param,
			        type: 'GET',
			        dataType: 'JSON',
			        success: function(data)
			        {
			        	$('#ad-title').html('(İlan: ' + data.title + ')');
			        }
			    });
			} else {
				$('#module-title').html('CV Havuzu');
				params = '?pool=1';
			}

		    table = $('#pool-table').DataTable({

		    	'language': {
					"sEmptyTable": '<div class="table-info-screen"><img src="/assets/frontend/img/no-profile-found.svg"><a href="javascript:void(0)" onclick="user_search_clear()" class="btn btn-rounded btn-gray white mt-3"><i class="la la-times-circle mr-1"></i> Tüm filtreleri temizle</a></div>',
					"infoEmpty": '<div class="table-info-screen"><img src="/assets/frontend/img/no-profile-found.svg"> <p>Filtrelerinize uygun sonuç bulunamadı.</p><a href="javascript:void(0)" onclick="user_search_clear()" class="btn btn-rounded btn-gray white mt-3"><i class="la la-times-circle mr-1"></i> Tüm filtreleri temizle</a></div>',
					"sZeroRecords": '<div class="table-info-screen"><img src="/assets/frontend/img/no-profile-found.svg"> <a href="javascript:void(0)" onclick="user_search_clear()" class="btn btn-rounded btn-gray text-white mt-3"><i class="la la-times-circle mr-1"></i> Tüm filtreleri temizle</a></div>',
					"sLoadingRecords": '<div class="table-info-screen"><p>Yükleniyor..</p></div>',
					"sProcessing":     '<div class="table-info-screen"><p>İşleniyor...</p></div>',
		            'url': base_url + 'assets/admin/js/addons/language_tr.json'
		        },
				

		        'processing': true,
		        'serverSide': true,
		        'order': [],
		        'lengthMenu': [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'Tüm']],
		        'ajax': {
		            'url': base_url + 'company/pool/ajax_list' + params,
		            'type': 'POST'
		        },

		        'dom': '<"w-50 float-left d-none"l><"w-50 float-left d-none"fB><"w-100 float-left"i><"table-content" tr><"w-100 float-left"p>',

				'buttons': [
					{	
						extend: 'colvis', 
						text: 'Sütünlar'
					},
					{
						extend: 'csv', 
						text: 'CSV', 
						title: export_name, 
						footer: true, 
						exportOptions: {columns: ':visible(:not(.not-export-col))'}
					},
					{
						extend: 'pdf', 
						text: 'PDF', 
						title: export_name, 
						footer: true, 
						exportOptions: {columns: ':visible(:not(.not-export-col))'}
					},
				],

		  		'columns': [
		            { 'data': 'transactions', 'orderable': false}
		        ]

		    });

		    $('#pool-search').on('keyup', function () {
				table.search( this.value ).draw();
			})

		    setTimeout(function() {
		    	$('[data-toggle="tooltip"]').tooltip();
		    	$('.dataTables_filter input').on('keyup', function() {
			    	setTimeout(function() {
				    	$('[data-toggle="tooltip"]').tooltip();
				    }, 700);
				});
				$('.list-view').removeClass('d-none');
		    	$('.page-loading').removeClass('d-flex');
		    	$('.page-loading').addClass('d-none');
		    }, 700);


		    $('#education-level, #used-program-duration').materialSelect();

		    $('#personal-information-position-id, #personal-information-city-id, #personal-information-city-json, #education-level, #education-university-id, #used-program-id, #used-program-duration, #game-future-json, #game-past-json, #personal-information-is-remote').change(function(){
		    	user_search();
		    });

		    $('#personal-information-is-remote').change(function(){
		    	if ($(this).is(':checked')) {
					$('.work-end-year').css('display', 'none');
				} else {
					$('.work-end-year').css('display', 'block');
				}
		    });

		});

		function user_search_clear()
		{
			window.location.href = base_url + 'zindhu-hr/studyo/cv-havuzu';
		}

		function user_search()
		{
			let position_id = $('#personal-information-position-id').val();
			let city_id = $('#personal-information-city-id').val();
			let city_json = $('#personal-information-city-json').val();
			let education_level = $('#education-level').val();
			let university_id = $('#education-university-id').val();
			let program_id = $('#used-program-id').val();
			let program_duration = $('#used-program-duration').val();
			let game_future = $('#game-future-json').val();
			let game_past = $('#game-past-json').val();
			let is_remote = 0;

			if ($('#personal-information-is-remote').is(':checked')) {
				is_remote = 1
			}

			if (position_id || city_id || city_json || education_level || university_id || program_id || program_duration || game_future || game_past) {
				$('#clear-all-filters').removeClass('d-none');
				$('#clear-all-filters').addClass('d-block');
			} else if ($('#personal-information-is-remote').is(':checked')) {
				$('#clear-all-filters').removeClass('d-none');
				$('#clear-all-filters').addClass('d-block');
			} else {
				$('#clear-all-filters').removeClass('d-block');
				$('#clear-all-filters').addClass('d-none');
			}

			table.ajax.url(base_url + 
		        'company/pool/ajax_list' + params +
		        '&position_id=' + position_id +
		        '&city_id=' + city_id +
		        '&city_json=' + city_json +
		        '&education_level=' + education_level +
		        '&university_id=' + university_id +
		        '&program_id=' + program_id + 
		        '&program_duration=' + program_duration +
		        '&game_future=' + game_future + 
		        '&game_past=' + game_past +
		        '&is_remote=' + is_remote
		    ).load();

		    setTimeout(function() {
		    	$('[data-toggle="tooltip"]').tooltip();
		    }, 500);
		}

		function add_watch(user_id)
		{
			$('#add-watch, #watch' + user_id).text('İşleniyor...');
		    $('#add-watch, #watch' + user_id).attr('disabled',true);

		    let form_data = {
		        user_id: user_id,
		    }

		    $.ajax({
		        url : base_url + 'company/ajax/add_watch',
		        type: 'POST',
		        data: form_data,
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if(data.status) 
		            {
		                toastr.success(data.message);
		                $('#add-watch, #watch' + user_id).html('<i class="las la-times mr-1"></i> Takibi bırak');
		                $('#add-watch, #watch' + user_id).attr('onclick', 'remove_watch('+user_id+')');
		                $('#add-watch, #watch' + user_id).addClass('following');
		            }
		            else
		            {
		                toastr.error(data.message);
		                $('#add-watch, #watch' + user_id).html('<i class="icon-telescope-icon mr-1"></i> Takip et');
		                $('#add-watch, #watch' + user_id).removeClass('following');
		            }

		            $('#add-watch, #watch' + user_id).attr('disabled', false);
		        }
		    });
		}

		function remove_watch(user_id)
		{
			$('#add-watch, #watch' + user_id).text('İşleniyor...');
		    $('#add-watch, #watch' + user_id).attr('disabled',true);

		    let form_data = {
		        user_id: user_id,
		    }

		    $.ajax({
		        url : base_url + 'company/ajax/remove_watch',
		        type: 'POST',
		        data: form_data,
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if(data.status) 
		            {
		                toastr.success(data.message);
		                $('#add-watch, #watch' + user_id).html('<i class="icon-telescope-icon mr-1"></i> Takip et');
		                $('#add-watch, #watch' + user_id).attr('onclick', 'add_watch('+user_id+')');
		                $('#add-watch, #watch' + user_id).removeClass('following');
		                reload_table();
		            }
		            else
		            {
		                toastr.error(data.message);
		                $('#add-watch, #watch' + user_id).html('<i class="las la-times mr-1"></i> Takibi bırak');
		                $('#add-watch, #watch' + user_id).addClass('following');
		            }

		            $('#add-watch, #watch' + user_id).attr('disabled', false);
		        }
		    });
		}

		function user_accept_invite(user_id)
		{
		    $('#user-accept-invite-btn').attr('disabled',true);

		    let form_data = {
		        user_id: user_id,
		    }

		    $.ajax({
		        url : base_url + 'company/ajax/user_accept_invite',
		        type: 'POST',
		        data: form_data,
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if(data.status) {
		                toastr.success(data.message);
		                view(user_id)
		            } else {
		                toastr.error(data.message);
		            }

		    		$('#user-accept-invite-btn').attr('disabled',false);
		        }
		    });
		}

		function user_exit(user_id)
		{
			Swal.fire({
		        title: 'İşten çıkarmak istediğinize emin misiniz?',
		        text: '',
		        icon: 'success',
		        showCancelButton: true,
		        confirmButtonColor: '#3085d6',
		        cancelButtonText: 'Vazgeç',
		        cancelButtonColor: '#d33',
		        confirmButtonText: 'Evet'
		    }).then((result) => {
		        if (result.value) 
		        {
					$('#user-exit-btn').attr('disabled',true);

				    let form_data = {
				        user_id: user_id,
				    }

				    $.ajax({
				        url : base_url + 'company/ajax/user_exit',
				        type: 'POST',
				        data: form_data,
				        dataType: 'JSON',
				        success: function(data)
				        {
				            if(data.status) {
				                toastr.success(data.message);
				                view(user_id)
				            } else {
				                toastr.error(data.message);
				            }

				    		$('#user-exit-btn').attr('disabled',false);
				        }
				    });
				}
    		});
		}

		function user_invite(user_id)
		{
		    $('#user-invite-btn').attr('disabled',true);

		    let form_data = {
		        user_id: user_id,
		    }

		    $.ajax({
		        url : base_url + 'company/ajax/user_invite',
		        type: 'POST',
		        data: form_data,
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if(data.status) {
		                toastr.success(data.message);
		                view(user_id)
		            } else {
		                toastr.error(data.message);
		            }

		    		$('#user-invite-btn').attr('disabled',false);
		        }
		    });
		}

		function view(user_id)
		{
			$('#cv-list').html(loading_render());
			$('#cv-modal').modal('show');

			$.ajax({
		        url : base_url + 'company/ajax/cv_view/' + user_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	let hires = data.company_hires;

		        	$('#cv-list').html('');

		            $('#cv-list').append(
		            	'<div id="user-top-info" class="title"></div>' +
		            	'<div id="personal-information-list" class="personal-info p-3 my-2"></div>' +
		            	'<div class="profile-section">'+
		            		'<div id="educational-background-list" class="section-item"></div>' +
			            	'<div id="work-experience-list" class="section-item"></div>' +
			            	'<div id="used-program-list" class="section-item"></div>' +
			            	'<div id="game-choice-list" class="section-item"></div>' +
		            	'</div>'
		            );

		            $('#educational-background-list').append(
		            	'<div class="title d-flex justify-content-between">' +
							'<h3 class="text"><i class="icon-cap-icon"></i>Eğitim geçmişi</h3>' +
						'</div>' +
						'<div class="content"></div>'
		            );
		            $('#work-experience-list').append(
		            	'<div class="title d-flex justify-content-between">' +
							'<h3 class="text"><i class="icon-experience-icon"></i>Geçmiş deneyimler</h3>' +
						'</div>' +
						'<div class="content"></div>'
		            );
		            $('#used-program-list').append(
		            	'<div class="title d-flex justify-content-between">' +
							'<h3 class="text"><i class="icon-program-icon"></i>Programlar</h3>' +
						'</div>' +
						'<div class="content"></div>'
		            );
		            $('#game-choice-list').append(
		            	'<div class="title d-flex justify-content-between">' +
							'<h3 class="text">İlgi duyduğun türler</h3>' +
						'</div>' +
						'<div class="content"></div>'
		            );

		            var portfolio_name = '';
	            	var portfolio_link = '-';
		            if (data.portfolio) {
		            	$.each(data.portfolio, function(key, portfolio){
		            		portfolio_name = '';
		            		portfolio_link = '-';
			            	if (portfolio.name) {
			            		portfolio_name = 
				            		'<a href="'+base_url+upload_folder+'portfolios/'+portfolio.name+'" target="_blank" class="download-portfolio">' +
									 	'<div class="icon"><i class="icon-pdf-icon"></i></div>' +
										'<div>Portfolioyu indir</div> ' +
										'<div><i class="la la-download"></i></div>' +
									'</a>'
			            	}
			            	if (portfolio.link) {
			            		portfolio_link = '<a href="'+portfolio.link+'" target="_blank"><i class="la la-link"></i> '+ portfolio.link +'</a>';
			            	}
			            });
		            }

		            $.each(data.personal_information, function(key, personal){

		            	let company_hires_status = company_hires_render(hires, user_id, data.user.firstname, data.user.lastname);
		            	let cities_text = '';
		            	let remote_text = '';
		            	let city_text = '';
		            	let lastname = data.user.lastname.substring(0,1) + '.';
		            	let watch_btn = '';
                    
	                    $.each(cities_render(personal.city_json), function(key, val){
	                        cities_text += val['city'] + ', ';
	                    });
	                    cities_text = cities_text.replace(/,\s*$/, "");

	                    if (personal.is_remote == '1') {
	                        remote_text = 'Uzaktan (remote) çalışmak istiyor';
	                    } else if (personal.is_remote == '0') {
	                        remote_text = 'Uzaktan (remote) çalışmak istemiyor';
	                    } else {
	                        remote_text = 'Uzaktan (remote) çalışma durumu belirtilmemiş';
	                    }

	                    if (personal.city_id == 341) {
	                        city_text = ' - ' + personal.city_text;
	                    }

			        	if (data.watch) {
			        		watch_btn = '<a href="javascript:void(0)" onclick="remove_watch('+user_id+')" id="add-watch" class="btn btn-gray following"><i class="las la-times mr-1"></i> Takibi bırak</a>'
			        	} else {
			        		watch_btn = '<a href="javascript:void(0)" onclick="add_watch('+user_id+')" id="add-watch" class="btn btn-gray"><i class="icon-telescope-icon mr-1"></i> Takip et</a>'
			        	}

	                    $('#user-top-info').append(
	                    	'<div>' +
								'<div class="name">'+ data.user.firstname + ' ' + lastname +'</div>' +
								'<div>' +
									'<span class="position">'+ position_render(personal.position_id) +'</span> <span class="location"><i class="la la-map-marker"></i> '+ city_render(personal.city_id) + city_text +'</span>' +
								'</div>' +
							'</div>' +
							'<div class="actions">' +
								watch_btn +
								'<a href="/zindhu-hr/studyo/konusmalar?user='+user_id+'" target="_blank" class="btn btn-white ml-2">' +
									'<i class="icon-conversations-icon"></i> Mesaj gönder' +
								'</a>' +
								'<div class="py-3 mt-2">' +
								company_hires_status +
								'</div>' +
							'</div>'
	                    );

		            	$('#personal-information-list').append(
	                        '<div class="row">' +
								'<div class="col-md-6 '+ (portfolio_link == '-' && !portfolio_name ? 'd-none' : '') +'">' +
									'<label>Portfolyo linki:</label>' +
									portfolio_link +
								'</div>' +
								'<div class="col-md-6 '+ (portfolio_link == '-' && !portfolio_name ? 'd-none' : '') +'">' +
									portfolio_name +
								'</div>' +
								'<div class="col-md-6">' +
									'<label>Ad Soyad</label>' +
									data.user.firstname + ' ' + lastname +
								'</div>' +
								'<div class="col-md-6">' +
									'<label>CV Oluşturma Tarihi</label>' +
									data.user.cv_created_at +
								'</div>' +
								'<div class="col-md-6">' +
									'<label>Yaşadığı şehir</label>' +
									city_render(personal.city_id) + city_text +
								'</div>' +
								'<div class="col-md-6">' +
									'<label>Çalışmak istediği şehir</label>' +
									cities_text +
								'</div>' +
								'<div class="col-md-6">' +
									'<label>Pozisyon</label>' +
									position_render(personal.position_id) +
								'</div>' +
								'<div class="col-md-6 d-flex align-items-center">' +
									remote_text +
								'</div>' +
							'</div>'
		            	);
		            });

		            let university_text = '';
		            let end_year_text = '';
		            $.each(data.educational_background, function(key, education){
		            	end_year_text = education.continues == 1 ? 'Devam Ediyor' : education.end_year;
		            	if (education.university_id == 0) {
	                        university_text = education.university_text;
	                    } else {
	                        university_text = university_render(education.university_id);
	                    }
	                    
		            	$('#educational-background-list .content').append(
		            		'<div class="item d-flex justify-content-between">' +
								'<div class="info">' +
									'<p class="blue-text">' + education_level_render(education.education_level) + '</p>' +
									'<h5>' + university_text + '</h5>' +
									'<p>' + education.university_section + '</p>' +
									'<p class="gray-text">' + education.start_year + ' - ' + end_year_text + '</p>' +
								'</div>' +
							'</div>'
		            	);
		            });

		            $.each(data.work_experience, function(key, work){
		            	let end_year_text = work.working == 1 ? 'Hala Çalışıyorum' : work.end_year;
	                    
		            	$('#work-experience-list .content').append(
		            		'<div class="item d-flex justify-content-between">' +
								'<div class="info">' +
									'<p class="blue-text">'+work.company_position+'</p>' +
									'<h5>'+work.company_name+' ('+work.company_sector + ')' +'</h5>' +
									'<p>'+
										date_render(work.start_month, work.start_year) +
		                                ' - ' +
		                                end_date_render(work.end_month, work.end_year) +
									'</p>' +
									'<p class="gray-text">'+work.detail +'</p>' +
								'</div>' +
							'</div>'
		            	);
		            });

		            $.each(data.used_program, function(key, program){
		            	$('#used-program-list .content').append(
		            		'<div class="item d-flex justify-content-between">' +
								'<div class="info">' +
									'<div class="d-flex align-items-center">' +
										'<div class="icon"><img src="'+ program_icon_render(program.program_id) +'" class="w-100"></div>' +
										'<div>' +
											'<h5>'+ program_render(program.program_id) +'</h5>' +
											'<p class="gray-text">'+ program.duration +' Yıl Tecrübe</p>' +
										'</div>' +
									'</div>' +
								'</div>' +
							'</div>'
		            	);
		            });

		            $.each(data.game_choice, function(key, game){
		            	let future_text = '';
	                	let past_text = '';
	                	
	                	$.each(game_render(game.future_json), function(key, val){
	                		future_text += '<div class="kind">' + val['title'] + '</div>';
	                	});

	                	$.each(game_render(game.past_json), function(key, val){
	                		past_text += '<div class="kind">' + val['title'] + '</div>';
	                	});

	                    $('#game-choice-list .content').append(
	                        '<div class="item d-flex justify-content-between">' +
	                            '<div>'+
	                                '<div>'+
	                                    '<h5 class="p-2">Gelecekte üretiminde bulunmak istediğiniz türler:</h5> '+
	                                    '<div class="d-flex flex-wrap">'+
	                                        future_text +
	                                    '</div>'+
	                                '</div>'+
	                                '<div>'+
	                                    '<h5 class="p-2">Geçmişte üretiminde bulunduğunuz türler:</h5>'+
	                                    '<div class="d-flex flex-wrap">'+
	                                        past_text +
	                                    '</div>'+
	                                '</div>'+
	                            '</div>'+
	                        '</div>'
	                    );
		            });
		        }
		    });
		}

		function company_hires_render(hires, user_id, firstname, lastname)
		{
			if (hires) {
        		if (hires.company_confirm == 0 && hires.continues == 0 && !hires.enddate) {
        			company_hires_status = 
                	'<h6><b>' + firstname + ' ' + lastname + '</b> sizinle çalışmak için istek gönderdi. Kabul ediyorsanız aşağıdaki onay butonuna tıklayabilirsiniz.</h6>' +
                	'<div>' +
	        			'<a href="javascript:void(0)" onclick="user_accept_invite('+ user_id +')" class="btn btn-dark btn-md ml-0" id="user-accept-invite-btn">EVET, KABUL EDİYORUM' +
        			'</div>';
                } else if (hires.company_confirm == 1 && hires.continues == 1 && !hires.enddate) {
        			company_hires_status = 
                	'<h6>Tebrikler! <b>' + firstname + ' ' + lastname + '</b> ile çalışmaya başladınız. İşten çıkarmak istediğiniz zaman aşağıdaki "İŞTEN ÇIKAR" butonuna tıklayabilirsiniz.</h6>' +
                	'<div class="d-flex justify-content-start">' +
	        			'<a href="javascript:void(0)" style="cursor:auto;" class="btn btn-white btn-md mr-2">' +
	        				'İŞE ALINDI' +
        				'</a>' +
	        			'<a href="javascript:void(0)" onclick="user_exit('+ user_id +')" class="btn btn-dark btn-md" id="user-exit-btn">' +
	        				'İŞTEN ÇIKAR' +
        				'</a>' +
        			'</div>';
                } else if (hires.company_confirm == 1 && hires.continues == 0 && hires.enddate) {
        			company_hires_status = 
                	'<h6> <b>' + firstname + ' ' + lastname + '</b> ile çalışma serüveni bitmiştir. Tekrar çalışmayı düşünürseniz aşağıdaki "TEKRAR İŞ TEKLİFİ GÖNDER" butonundan tekrar istek gönderebilirsiniz.</h6>' +
        			'<div class="d-flex justify-content-start">' +
	        			'<a href="javascript:void(0)" style="cursor:auto;" class="btn btn-white btn-md mr-2">ÇIKIŞ TARİHİ '+ hires.enddate +'</a>' +
	        			'<a href="javascript:void(0)" onclick="user_invite('+ user_id +')" class="btn btn-dark btn-md" id="user-invite-btn">TEKRAR İŞ TEKLİFİ GÖNDER</a>' +
        			'</div>';
                } else if (hires.user_confirm == 0) {
                	company_hires_status = 
                	'<div class="d-inline-block">' +
	        			'<a href="javascript:void(0)" style="cursor:auto;" class="btn btn-warning btn-md ml-0">' +
	        				'KULLANICIDAN DÖNÜŞ BEKLENİYOR' +
        				'</a>' +
        			'</div>';

        			company_hires_status = 
                	'<h6><b>' + firstname + ' ' + lastname + '</b> kullanıcısına çalışma isteği gönderildi.</h6>' +
                	'<div>' +
	        			'<a href="javascript:void(0)" style="cursor:auto;" class="btn btn-white btn-md ml-0">' +
	        				'KULLANICIDAN DÖNÜŞ BEKLENİYOR' +
        				'</a>' +
        			'</div>';
                }
        	} else {
        		company_hires_status = 
    			'<h6><b>' + firstname + ' ' + lastname + '</b> ile çalışmayı düşünüyorsanız iş teklifi gönderebilirsiniz.</h6>' +
    			'<div>' +
        			'<a href="javascript:void(0)" onclick="user_invite('+ user_id +')" class="btn btn-dark btn-md ml-0" id="user-invite-btn">İŞ TEKLİFİ GÖNDER</a>' +
    			'</div>';
        	}

        	return company_hires_status;
		}

		function reload_table()
		{
		    table.ajax.reload(null,false);
		    
		    setTimeout(function() {
		    	$('[data-toggle="tooltip"]').tooltip();
		    }, 700);
		}

		$('.list-filter-mobile-trigger').click(function() {
                $('#user-filter').toggleClass("opened");
            });
    </script>

</body>

</html>