<!DOCTYPE html>
<html lang="en">

<head>
    
    <title>Studyo Üye Ol  - Zindhu HR</title>

    <?php include(dirname(__DIR__) . '/../include/head.php'); ?>

</head>

<body>

    <div class="header login">
        <a class="logo logo-black" href="<?=base_url('zindhu-hr')?>" data-ani></a>
    </div>

    <div class="sub-page sign-up corporate">
        
        <div class="graph">
            <div class="slogan">Zindhu HR olarak <strong>1.000'den fazla</strong> yeteneği, oyun stüdyoları ile bir
                araya getiriyoruz.</div>
        </div>

        <div class="form">
            
            <small>Stüdyo</small>
            <h2>Kayıt Formu</h2>

            <form action="<?= base_url('zindhu-hr/studyo/kayit-ol') ?>" method="POST" id="register-form" autocomplete="off">
                <div class="row">

                    <div class="col-md-6">
                    	<label>İsim</label>
                        <input 
							type="text" 
							name="firstname" 
							id="firstname" 
							value="<?=set_value('firstname')?>" 
							class="form-control rounded <?=form_error('firstname') ? 'invalid' : ''?>">
                		<?=form_error('firstname', '<div class="invalid">', '</div>')?>
                    </div>

                    <div class="col-md-6">
                    	<label>Soyisim</label>
                        <input 
							type="text" 
							name="lastname" 
							id="lastname" 
							value="<?=set_value('lastname')?>" 
							class="form-control rounded <?=form_error('lastname') ? 'invalid' : ''?>">
                		<?=form_error('lastname', '<div class="invalid">', '</div>')?>
                    </div>

                    <div class="col-md-12">
                    	<label>Stüdyo İsmi</label>
                        <input 
							type="text" 
							name="company_name" 
							id="company-name" 
							value="<?=set_value('company_name')?>" 
							class="form-control rounded <?=form_error('company_name') ? 'invalid' : ''?>">
	            		<?=form_error('company_name', '<div class="invalid">', '</div>')?>
                    </div>

                    <div class="col-md-12">
                    	<label>Kurucu Ortaklar</label>
                        <input 
							type="text" 
							name="partners" 
							id="partners" 
							value="<?=set_value('partners')?>" 
							class="form-control rounded <?=form_error('partners') ? 'invalid' : ''?>">
                		<?=form_error('partners', '<div class="invalid">', '</div>')?>
                    </div>

                    <div class="col-md-6">
                    	<label>Stüdyo Mail Adresi</label>
                        <input 
							type="text" 
							name="email" 
							id="email" 
							value="<?=set_value('email')?>" 
							class="form-control rounded <?=form_error('email') ? 'invalid' : ''?>">
                		<?=form_error('email', '<div class="invalid">', '</div>')?>
                    </div>

                    <div class="col-md-6">
                        <label>Stüdyo Telefon Numarası</label>
                        <input 
							type="text" 
							name="phone" 
							id="phone" 
							value="<?=set_value('phone')?>" 
							class="form-control rounded <?=form_error('phone') ? 'invalid' : ''?>">
                		<?=form_error('phone', '<div class="invalid">', '</div>')?>
                    </div>

                    <div class="col-md-6">
                        <label>Stüdyo Şehir</label>
                        <select 
							name="city_id" 
							id="city-id" 
							class="mdb-select md-form <?=form_error('city_id') ? 'invalid' : ''?>" 
							searchable="Arama">
			                <option value="" disabled selected>Seçiniz</option>
			            </select>	
                		<?=form_error('city_id', '<div class="invalid">', '</div>')?>
                    </div>

                    <div class="col-md-6">
                        <label>Stüdyo İlçe</label>
                        <select 
							name="district_id" 
							id="district-id" 
							class="mdb-select md-form <?=form_error('district_id') ? 'invalid' : ''?>" 
							searchable="Arama">
			                <option value="" disabled selected>Seçiniz</option>
			            </select>
                		<?=form_error('district_id', '<div class="invalid">', '</div>')?>
                    </div>

                    <div class="col-md-6">
                    	<label>Çalışan Sayısı</label>
						<input 
							type="number"
							min="1" 
							name="number_employees" 
							id="number-employees" 
							value="<?=set_value('number_employees')?>" 
							class="form-control rounded <?=form_error('number_employees') ? 'invalid' : ''?>">
                		<?=form_error('number_employees', '<div class="invalid">', '</div>')?>
					</div>

					<div class="col-md-6">
                        <label>Stüdyonun İlgili Olduğu Oyun Türü</label>
                        <select 
							name="game_type" 
							id="game-type" 
							class="mdb-select md-form <?=form_error('game_type') ? 'invalid' : ''?>" 
							searchable="Arama">
			                <option value="" disabled selected>Seçiniz</option>
			            </select>	
                		<?=form_error('game_type', '<div class="invalid">', '</div>')?>
                    </div>

                    <div class="col-md-6">
                    	<label>Stüdyo Linkedin</label>
                        <input 
							type="text" 
							name="company_linkedin" 
							id="company-linkedin" 
							value="<?=set_value('company_linkedin')?>" 
							class="form-control rounded <?=form_error('company_linkedin') ? 'invalid' : ''?>">
	            		<?=form_error('company_linkedin', '<div class="red-text small">', '</div>')?>
                    </div>

                    <div class="col-md-6">
                    	<label>Stüdyo Kurucu Linkedin</label>
                        <input 
							type="text" 
							name="founder_linkedin" 
							id="founder-linkedin" 
							value="<?=set_value('founder_linkedin')?>" 
							class="form-control rounded <?=form_error('founder_linkedin') ? 'invalid' : ''?>">
                		<?=form_error('founder_linkedin', '<div class="red-text small">', '</div>')?>
                    </div>

                    <div class="col-md-6">
                    	<label>Şifre</label>
						<input 
							type="password" 
							name="password" 
							id="password" 
							placeholder="Şifre"
							value="<?=set_value('password')?>" 
							class="form-control rounded <?=form_error('password') ? 'invalid' : ''?>">
	            		<?=form_error('password', '<div class="invalid">', '</div>')?>
					</div>

					<div class="col-md-6">
						<label>Şifre Tekrar</label>
						<input 
							type="password" 
							name="password_again" 
							id="password-again" 
							value="<?=set_value('password_again')?>" 
							class="form-control rounded <?=form_error('password_again') ? 'invalid' : ''?>">
	            		<?=form_error('password_again', '<div class="invalid">', '</div>')?>
					</div>

                    <div class="col-md-9">
                        <div class="form-check light-form my-3">
                             <input 
						    	type="checkbox" 
						    	class="form-check-input" 
						    	name="is_accept"
						    	id="is-accept"
						    	value="1"
						    	<?= set_value('is_accept') ? 'checked="checked"' : ''?>>
                            <label class="form-check-label" for="is-accept">
		                        <a href="javascript:void(0)" data-toggle="modal" data-target="#register-modal">Aydınlatma Metni</a> ’ni okudum, kabul ediyorum.
		                    </label>
		                    <?=form_error('is_accept', '<div class="invalid">', '</div>')?>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <button class="btn btn-green text-center btn-block mt-2">Kayıt Ol</button>
                    </div>

                </div>
            </form>
        </div>

    </div>

    <!-- register-modal -->
	<div class="modal fade" id="register-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	  aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<h4 class="m-0" id="kvkk-title">Stüdyo Aydınlatma Metni</h4>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="kvkk-content">
					
					<div class="row pl-3 pr-3">

						<p><b><i>Zindhu Oyun Yazılım Danışmanlık Hizmetleri A.Ş</i></b><b> </b>(&ldquo;<b>Şirket</b>&rdquo;) olarak, işbu Aydınlatma Metni ile, Kişisel Verilerin Korunması Kanunu (&ldquo;<b>Kanun</b>&rdquo;) kapsamında aydınlatma y&uuml;k&uuml;ml&uuml;ğ&uuml;m&uuml;z&uuml;n yerine getirilmesi ama&ccedil;lanmaktadır. Bu kapsamda sizlere bilgi vermekle y&uuml;k&uuml;ml&uuml; olduğumuz konular aşağıdaki şekildedir:&nbsp;</p>

						<p><b>1. Veri sorumlusunun ve varsa temsilcisinin kimliği&nbsp;</b></p>

						<p>Veri sorumlusu; <b><i>&Ccedil;eliktepe Mah. İsmet İn&ouml;n&uuml; Cad. Yıldırımlar İş Merkezi No:11 İ&ccedil; Kapı No: 501 Kağıthane/Istanbul</i></b><b> </b>adresinde mukim İstanbul Ticaret Sicili M&uuml;d&uuml;rl&uuml;ğ&uuml;&rsquo;ne <b><i>262674-5</i></b> sicil numarası ile kayıtlı <b><i>Zindhu Oyun Yazılım Danışmanlık Hizmetleri A.Ş</i></b><b>&rsquo;</b>dir.&nbsp;</p>

						<p><b>2. Kişisel verilerin hangi ama&ccedil;la işleneceği&nbsp;</b></p>

						<p>St&uuml;dyo yetkilisi olarak Şirket&rsquo;imize sağladığınız ad-soyad, st&uuml;dyonuza ait pay sahiplerinin kim olduğu, st&uuml;dyo yetkilisi olarak sağladığınız linkedin dahil olmak &uuml;zrere sosyal medya hesaplarınıza ait bilgiler, kimliği belirli veya belirlenebilir bir ger&ccedil;ek kişiye ait nitelikte olması halinde elektronik posta adresi, telefon numarası ve adres bilgisi <b>t&uuml;r ve kategorilerindeki </b>kişisel verileriniz; Zindhu insan kaynakları platformuna (&ldquo;<b>Platform</b>&rdquo;) st&uuml;dyo kaydının tam ve eksiksiz olarak&nbsp; tamamlanması ve kayıt s&uuml;recinin y&ouml;netilmesi, Platform &uuml;zerinden st&uuml;dyonuza işe alım konusunda danışmanlık verilmesi, st&uuml;dyo ve oyun sektor&uuml;nde istihdam edilmek isteyen kişilerin bir araya getirilmesi, gerekli olması veya st&uuml;dyonun talebi halinde Platform kullanımı ile ilgili olarak tarafınız ile iletişime ge&ccedil;ilmesi, st&uuml;dyoya sunulan istihdam y&ouml;netim hizmeti ve danışmanlık sunulması kapsamında Platform &uuml;zerindeki st&uuml;dyo kaydınızın y&ouml;netilmesi, tarafınıza ve/veya st&uuml;dyoya Platform kullanımına ilişkin eğitim verilmesi ve gerekli bilgilerin sağlanması, oyun sekt&ouml;r&uuml;nde istihdam edilmek isteyen kişiler ile st&uuml;dyoları bir araya getirmek i&ccedil;in sizlerle iletişime ge&ccedil;ilmesi, st&uuml;dyonun istihdam etmek istediği &ccedil;alışan adaylarının bulunması ve st&uuml;dyolar ile buluşturulması hizmetlerinin y&uuml;r&uuml;t&uuml;lmesi, işe alım s&uuml;re&ccedil;leriniz kapsamında yetkilisi olduğunuz st&uuml;dyoya hizmet sağlanması, onay vermeniz halinde tarafınıza ticari elektronik ileti g&ouml;nderilmesi, burada bahsi ge&ccedil;en hizmetlerin sunulması ve faaliyetlerin y&uuml;r&uuml;t&uuml;lmesi i&ccedil;in iş ortaklarımız ve mevzuattan kaynaklanan zamanaşımı s&uuml;resi doğrultusunda saklanması <b>amacı ile </b>işlenmektedir.&nbsp; &nbsp;</p>

						<p><b>3. Şirket tarafından işlenen kişisel verilerin kimlere ve hangi ama&ccedil;la aktarılabileceği&nbsp;</b></p>

						<p>Şirket&rsquo;imize sağladığınız kişisel verileriniz, st&uuml;dyoya insan kaynakları danışmanlık hizmetleri sunulması ve oyun sekt&ouml;r&uuml;nde istihdam edilmek istenen kişiler ile st&uuml;dyonun bir araya getirilmesi faaliyetleri kapsamında ve bu faaliyetleri ile doğrudan ilgili olarak bu &ccedil;er&ccedil;evede iş birliği i&ccedil;inde olduğumuz Rollic Games Oyun Yazılım ve Pazarlama A.Ş.&rsquo;ye aktarılabilecek ve resmi makamların d&uuml;zenleyici veya icrai işlemleri veya yargı kararlarının gereklerini yerine getirmek amacıyla ve taleple sınırlı olarak resmi makamlar ile paylaşabilecektir.&nbsp;</p>

						<p><b>4. Kişisel veri toplamanın y&ouml;ntemi ve hukuki sebebi&nbsp;</b></p>

						<p>Kişisel verileriniz, Platform &uuml;zerinden, elektronik ortam aracılığıyla elde edilmekte ve a&ccedil;ık rızanız hukuki sebepleri ile toplanmaktadır.&nbsp;</p>

						<p><b>5. Kişisel verileriniz ile ilgili Kanun kapsamındaki haklarınız aşağıdaki şekildedir:&nbsp;</b></p>

						<p>(a) Kişisel verilerinizin işlenip işlenmediğini &ouml;ğrenme, (b) Kişisel verileriniz işlenmişse buna ilişkin bilgi talep etme, (c) Kişisel verilerinizin işlenme amacını ve bunların amacına uygun kullanılıp kullanılmadığını &ouml;ğrenme, (&ccedil;) Yurt i&ccedil;inde veya yurt dışında kişisel verilerin aktarıldığı &uuml;&ccedil;&uuml;nc&uuml; kişileri bilme, (d) Kişisel verilerinizin eksik veya yanlış işlenmiş olması h&acirc;linde bunların d&uuml;zeltilmesini isteme, (e) Kişisel verilerinizin işlenmesini gerektiren sebeplerin ortadan kalkması halinde kişisel verilerinizin silinmesini veya yok edilmesini isteme, (f), (d) ve (e) bentleri uyarınca yapılan işlemlerin, kişisel verilerin aktarıldığı &uuml;&ccedil;&uuml;nc&uuml; kişilere bildirilmesini isteme, (g) İşlenen verilerin m&uuml;nhasıran otomatik sistemler vasıtasıyla analiz edilmesi suretiyle kişinin kendisi aleyhine bir sonucun ortaya &ccedil;ıkmasına itiraz etme, (ğ) Kişisel verilerin kanuna aykırı olarak işlenmesi sebebiyle zarara uğraması h&acirc;linde zararın giderilmesini talep etme.&nbsp;</p>

						<p>Bu haklarınızı yazılı olarak veya g&uuml;venli elektronik imza, mobil imza ya da Şirket&rsquo;e daha &ouml;nce bildirilen ve Şirket&rsquo;in sisteminde kayıtlı bulunan elektronik posta adresinizi kullanmak suretiyle (Bu kapsamda <a href="mailto:info@zindhugames.com">info@zindhugames.com</a><b><i> </i></b>e-posta adresi &uuml;zerinden Şirket&rsquo;e ulaşabilirsiniz) veya başvuru amacına y&ouml;nelik geliştirilmiş bir yazılım ya da uygulama vasıtasıyla Şirket&rsquo;e iletebilirsiniz.&nbsp;</p>

						<p>Bilginize sunarız.&nbsp;</p>

						<p><b><i>Zindhu Oyun Yazılım Danışmanlık Hizmetleri A.Ş</i></b></p>


					</div>

				</div>
			</div>
			<!-- Content -->
		</div>
	</div>
	<!-- register-modal -->
	<?php include(dirname(__DIR__) . '/../include/footer.php'); ?>

	<div class="scroll-top" id="scrollTop">
	</div>
	<?php include(dirname(__DIR__) . '/../include/script.php'); ?>

    <script type="text/javascript">
    	city_list();
    	game_list();
    	
    	

    	$(document).ready(function () {
	    	$('#game-type').materialSelect();
	    	
	    	if ('<?=set_value('district_id')?>' == '') {
	    		$('#district-id').materialSelect();
	    	}

	    	$('#city-id').change(function(){
	    		let city_id = $(this).val();
	    		district_list(city_id);
	    	});
	    });

	    function city_list()
		{
			let set_value = '<?=set_value('city_id')?>';
			
		    $.ajax({
		        url : base_url + 'ajax/city',
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if (data != "") 
		            {
		            	let city_id;
		            	let selected = '';
		                let count = data.length;
		                $.each(data, function(key, val){
		                	if (set_value == val.id) {
		                		selected = 'selected';
		                		city_id = val.id
		                	} else {
		                		selected = ''; 
		                	}

		                    $('#city-id').append('<option value="'+val.id+'" '+selected+'>'+val.city+'</option>');
		                    if (key+1 === count) {
		                    	setTimeout(function(){
	                            	$('#city-id').materialSelect();
	                            }, 100);
		                    }
		                });

		                if (city_id) {
		                	district_list(city_id);
		                }
		            }
		        }
		    });
		}

		function district_list(city_id)
		{
			let set_value = '<?=set_value('district_id')?>';
			
		    $.ajax({
		        url : base_url + 'ajax/district/' + city_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if (data != "") 
		            {
		            	let selected = ''; 
		                let count = data.length;
		                $.each(data, function(key, val){
		                	if (set_value == val.id) {
		                		selected = 'selected';
		                	} else {
		                		selected = ''; 
		                	}

		                    $('#district-id').append('<option value="'+val.id+'" '+selected+'>'+val.district+'</option>');
		                    if (key+1 === count) {
	                            $('#district-id').materialSelect();
		                    }
		                });
		            }
		        }
		    });
		}

		function game_list()
		{
			let set_value = '<?=set_value('game_type')?>';

			$.ajax({
		        url : base_url + 'ajax/game',
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if (data != "") 
		            {
		            	let selected = ''; 
		            	let count = data.length;
		            	$.each(data, function(key, val){
		            		if (set_value == val.id) {
		                		selected = 'selected';
		                	} else {
		                		selected = ''; 
		                	}

		            		$('#game-type').append('<option value="'+val.id+'" '+selected+'>'+val.title+'</option>');
		            		if (key+1 === count) {
		            			setTimeout(function(){
		            				$('#game-type').materialSelect();
		            			}, 100);
						    }
		            	});
		            }
		        }
		    });
		}
    </script>

</body>

</html>