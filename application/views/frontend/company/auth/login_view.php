<!DOCTYPE html>
<html lang="en">

<head>
    
    <title>Studyo Giriş Yap - Zindhu HR</title>

    <?php include(dirname(__DIR__) . '/../include/head.php'); ?>

</head>

<body>

	<?php
		$create_status = $this->session->flashdata('create_status');
		$reset_status = $this->session->flashdata('reset_status');
		$forgot_status = $this->session->flashdata('forgot_status');
		$account_enabled = $this->session->flashdata('account_enabled');
		$login_status = $this->session->flashdata('login_status');
		$flashdata = null;
		if (isset($create_status)) {
			$flashdata = $create_status;
		} elseif (isset($reset_status)) {
			$flashdata = $reset_status;
		} elseif (isset($forgot_status)) {
			$flashdata = $forgot_status;
		} elseif (isset($account_enabled)) {
			$flashdata = $account_enabled;
		} elseif (isset($login_status)) {
			$flashdata = $login_status;
		}

		if (isset($flashdata)) {
			if ($flashdata['status']) {
				$flashdata['img'] = base_url('assets/frontend/img/modal-images/success.svg');
			} else {
				$flashdata['img'] = base_url('assets/frontend/img/modal-images/error.svg');
			}
		}
	?>

	<div class="header login">
        <a class="logo logo-black" href="<?=base_url('zindhu-hr')?>" data-ani></a>
    </div>

    <div class="sub-page sign-up corporate-login">
        <div class="form">
            
            <div class="custom-dropdown">
                <ul>
                    <li>
                        <a href="#">
                            Studyo
                            <i class="la la-caret-down"></i>
                        </a>
                        <ul class="dropdown">
                            <li><a href="<?=base_url('zindhu-hr/kullanici/giris')?>">Bireysel</a></li>
                        </ul>
                    </li>
                </ul>
            </div>

            <h2>Giriş Yap</h2>

            <form action="<?= base_url('zindhu-hr/studyo/giris') ?>" method="POST" id="login-form" autocomplete="off">

            	<div>
	            	<input 
	            		type="text" 
						name="email" 
						id="email" 
						placeholder="E-Posta"
						value="<?= set_value('email') ? set_value('email') : $this->session->tempdata('RememberCompanyEmail') ?>" 
						class="form-control rounded <?=form_error('email') ? 'invalid' : ''?>">
					<?=form_error('email', '<div class="invalid">', '</div>')?>
				</div>
                
                <div>
					<input 
						type="password" 
						name="password" 
						id="password" 
						placeholder="Şifre"
						value="<?= set_value('password') ? '' : $this->session->tempdata('RememberCompanyPassword') ?>" 
						class="form-control rounded <?=form_error('password') ? 'invalid' : ''?>">
            		<?=form_error('password', '<div class="invalid">', '</div>')?>
				</div>

                <div class="d-flex justify-content-between align-items-center">
                    <div class="form-check my-3">
                    	<input 
					    	type="checkbox" 
					    	class="form-check-input" 
					    	name="remember"
					    	id="remember"
					    	<?= $this->session->tempdata('RememberCompanyEmail') ? 'checked="checked"' : ''?>>
					    <label class="form-check-label" for="remember">
                            Beni hatırla
                        </label>	
                    </div>
                    <div class="forgot-pass"><a href="<?=base_url()?>zindhu-hr/studyo/sifrenizi-mi-unuttunuz">Şifremi unuttum</a></div>
                </div>

                <button class="btn btn-dark btn-block text-center mt-2" type="submit">Giriş Yap</button>
                <div class="already">Hesabınız yok mu? <a href="<?=base_url()?>zindhu-hr/studyo/kayit-ol">Kayıt olun</a></div>

            </form>

        </div>
        
        <?php include('right.php'); ?>

    </div>
	<?php include(dirname(__DIR__) . '/../include/footer.php'); ?>

	<div class="scroll-top" id="scrollTop">
	</div>
	<?php include(dirname(__DIR__) . '/../include/script.php'); ?>

	<script type="text/javascript">
        
        <?php if ($flashdata) : ?>
        
        modal_open('<?= $flashdata['message'] ?>', '<?=$flashdata['img']?>');

        <?php endif ?>

    </script>

</body>

</html>