<!DOCTYPE html>
<html lang="en">

<head>

    <title>Studyo Şifremi Yenile - Zindhu HR</title>

    <?php include(dirname(__DIR__) . '/../include/head.php'); ?>

</head>

<body>

	<div class="header login">
        <a class="logo logo-black" href="<?=base_url('zindhu-hr')?>" data-ani></a>
    </div>

    <div class="sub-page sign-up forgot">
        <div class="form">

            <h2>Şifremi Yenile</h2>
            <small>
				Şifrenizin güçlü ve hatırlanması kolay olduğundan emin olun.
			</small>

            <form method="POST" id="forgot-form" autocomplete="off">

            	<div>
					<input 
						type="text" 
						name="password" 
						id="password" 
						placeholder="Şifre"
						value="<?=set_value('password')?>" 
						class="form-control rounded <?=form_error('password') ? 'invalid' : ''?>">
            		<?=form_error('password', '<div class="invalid">', '</div>')?>
				</div>

				<div>
					<input 
						type="text" 
						name="password_again" 
						id="password-again" 
						placeholder="Şifre Tekrar"
						value="<?=set_value('password_again')?>" 
						class="form-control rounded <?=form_error('password_again') ? 'invalid' : ''?>">
            		<?=form_error('password_again', '<div class="invalid">', '</div>')?>
				</div>

                <button class="btn btn-dark btn-block text-center mt-2">Şifremi Sıfırla</button>
            </form>

        </div>

        <?php include('right.php'); ?>

    </div>

    <?php include(dirname(__DIR__) . '/../include/script.php'); ?>

</body>

</html>