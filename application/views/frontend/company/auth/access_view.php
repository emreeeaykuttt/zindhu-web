<!DOCTYPE html>
<html lang="en">

<head>

    <title>Stüdyo Kayıt Formuna Erişim Ekranı - Zindhu HR</title>

    <?php include(dirname(__DIR__) . '/../include/head.php'); ?>

</head>

<body>

	<?php
		$access_status = $this->session->flashdata('access_status');
		$flashdata = null;
		if (isset($access_status)) {
			$flashdata = $access_status;
		}

		if (isset($flashdata)) {
            if ($flashdata['status']) {
                $flashdata['img'] = base_url('assets/frontend/img/modal-images/success.svg');
            } else {
                $flashdata['img'] = base_url('assets/frontend/img/modal-images/error.svg');
            }
        }
	?>

	<div class="header login">
        <a class="logo logo-black" href="<?=base_url('zindhu-hr')?>" data-ani></a>
    </div>

    <div class="sub-page sign-up corporate-login">
        <div class="form">

            <h2>Stüdyo Kayıt Formuna Erişim Ekranı</h2>

            <form action="<?= base_url('zindhu-hr/studyo/kayit-ol') ?>" method="POST" id="access-form" autocomplete="off">

            	<div>
	            	<input 
	            		type="text" 
						name="username" 
						id="username" 
						placeholder="Kullanıcı Adı"
						class="form-control rounded <?=form_error('username') ? 'invalid' : ''?>">
					<?=form_error('username', '<div class="invalid">', '</div>')?>
				</div>
                
                <div>
					<input 
						type="password" 
						name="password" 
						id="password" 
						placeholder="Şifre"
						class="form-control rounded <?=form_error('password') ? 'invalid' : ''?>">
            		<?=form_error('password', '<div class="invalid">', '</div>')?>
				</div>

				<div class="grey-text mb-3"><small>Kayıt formuna erişebilmek için size verilen bilgiler ile giriş yapmalısınız.</small></div>

                <button class="btn btn-dark btn-block text-center mt-2" type="submit">Sisteme Giriş Yap</button>

            </form>

        </div>
        
        <?php include('right.php'); ?>

    </div>
	<?php include(dirname(__DIR__) . '/../include/footer.php'); ?>

	<div class="scroll-top" id="scrollTop">
	</div>
	<?php include(dirname(__DIR__) . '/../include/script.php'); ?>

	<script type="text/javascript">
        
        <?php if ($flashdata) : ?>
        
        modal_open('<?= $flashdata['message'] ?>', '<?=$flashdata['img']?>');

        <?php endif ?>

    </script>

</body>

</html>