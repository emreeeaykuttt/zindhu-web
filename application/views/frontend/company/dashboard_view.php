<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="Stüdyo Dashboard">
    <meta name="keywords" content="Stüdyo Dashboard">

    <title>Stüdyo Dashboard</title>

    <?php include('inc/head.php'); ?>

</head>

<body class="fixed-sn white-skin">

	<?php include('inc/header.php'); ?>

	<!-- Main layout -->
	<main>
		<div class="container-fluid">

			<section>
			<div class="row">
				<div class="col-12">
					<h5 class="mb-0 dark-grey-text font-weight-bold" id="module-title">Dashboard</h5>
					<div class="d-flex align-items-center mb-5">
						<span><?= $this->session->userdata('CompanyFullname') ?></span>
						<span class="badge badge-blue ml-2">Stüdyo</span>
					</div>
				</div>
				<div class="col-md-6">
				<div class="row">
					
					<div class="col-6">
						
						<a href="<?=base_url('zindhu-hr/studyo/konusmalar?unread=1')?>" class="card classic-admin-card unread">
							<div class="card-body text-black">
								<div class="pull-right">
									<i class="icon-mail-icon"></i>
								</div>
								<p>Okunmamış Mesajlar</p>
								<h4 class="check" id="dashboard-unread-count">0</h4>
								<div class="arrow">
									<i class="las la-arrow-right"></i>
								</div>
							</div>
						</a>

					</div>

					<div class="col-6">

						<a href="<?=base_url('zindhu-hr/studyo/cv-havuzu?conversation=1')?>" class="card classic-admin-card  follow">
							<div class="card-body text-black">
								<div class="pull-right">
									<i class="icon-handshake-icon"></i>
								</div>
								<p>Görüşülen Kişiler</p>
								<h4 class="check" id="dashboard-conversation-count">0</h4>
								<div class="arrow">
									<i class="las la-arrow-right"></i>
								</div>
							</div>
						</a>

					</div>

					<div class="col-6">

						<a href="<?=base_url('zindhu-hr/studyo/cv-havuzu?watch=1')?>" class="card classic-admin-card interview" >
							<div class="card-body text-black">
								<div class="pull-right">
									<i class="icon-telescope-icon"></i>
								</div>
								<p>İzlediğin Kişiler</p>
								<h4 class="check" id="dashboard-watch-count">0</h4>
								<div class="arrow">
									<i class="las la-arrow-right"></i>
								</div>
							</div>
						</a>

					</div>

					<div class="col-6">

						<a href="<?=base_url('zindhu-hr/studyo/cv-havuzu')?>" class="card classic-admin-card shown">
							<div class="card-body text-black">
								<div class="pull-right">
									<i class="icon-cvpool-icon"></i>
								</div>
								<p>CV Havuzu</p>
								<h4 class="check" id="dashboard-pool-count">0</h4>
								<div class="arrow">
									<i class="las la-arrow-right"></i>
								</div>
							</div>
						</a>

					</div>



				</div>
				</div>
				<div class="col-md-6">
					<div class="right-create-ad">
						<img src="/assets/frontend/img/create-ad.svg" class="w-100" />
						<div class="text">
							<h4><strong>Zindhu HR</strong> ile oyun dünyasındaki yetenekleri keşfetmek için</h4>
							<a href="<?= base_url('zindhu-hr/studyo/is-ilanlari?action=new') ?>" class="btn btn-dark" >İlan Oluştur + </a>
						</div>
					</div>
				</div>

			</div>
		
			</section>

		</div>
	</main>
	<!-- Main layout -->

    <?php include('inc/footer.php'); ?>
    <?php include('inc/script.php'); ?>

    <script type="text/javascript">
    	
    	$(document).ready(function(){

			all_statistics();

		});

		function all_statistics()
		{
			$.ajax({
		        url : base_url + 'company/ajax/all_statistics',
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            $('#dashboard-watch-count').text(data.watch_count);
		            $('#dashboard-conversation-count').text(data.conversation_count);
		            $('#dashboard-pool-count').text(data.user_count);
		        }
		    });
		}

    </script>

</body>

</html>