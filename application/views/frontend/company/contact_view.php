<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Zindhu HR'a ulaşın">
    <meta name="keywords" content="Zindhu HR'a ulaşın">

    <title>Zindhu HR'a ulaşın</title>

    <?php include('inc/head.php'); ?>

</head>

<body class="fixed-sn white-skin">

	<?php include('inc/header.php'); ?>

	<!-- Main layout -->
	<main>
		<div class="container-fluid">

			<section>

		        <!-- Gird column -->
		        <div class="col-md-12">

		         	<h5 class="my-4 dark-grey-text font-weight-bold" id="module-title">ZİNDHU HR'A ULAŞIN</h5>

			        <div class="card">
			            <div class="card-body">

							<div class="refresh-btn text-right">
								<i class="fas fa-sync-alt" id="refresh-btn" onclick="start_contact()" data-toggle="tooltip" title="Yenile"></i>
							</div>

							<div class="list-group-item mb-3 conversation-area" id="contact-list">
								<!-- contact list -->
							</div>

							<div class="list-group-item">
								<div class="md-form m-0">
				                	<textarea type="text" name="message" id="message" class="md-textarea form-control" rows="1"></textarea>
				                	<label for="message">Mesaj At</label>
				                	<div class="red-text small error-text" id="message_error"></div>
				                	<a href="javascript:void(0)" onclick="send_message()" id="message-btn" class="btn btn-warning ml-0 w-100">
				                		<i class="las la-paper-plane"></i>
				                	</a>
				              	</div>
							</div>

			            </div>
			         </div>

		        </div>
		        <!-- Gird column -->

		    </section>

		</div>
	</main>
	<!-- Main layout -->

    <?php include('inc/footer.php'); ?>
    <?php include('inc/script.php'); ?>

    <script type="text/javascript">
    	
    	start_contact();
		
		$(document).ready(function(){


		});

		function send_message()
		{
			$('#message-btn').html(small_loading_render());
		    $('#message-btn').attr('disabled',true);

		    let form_data = {
		        message: $('[name="message"]').val(),
		    }

		    $.ajax({
		        url : base_url + 'company/contact/send_message',
		        type: 'POST',
		        data: form_data,
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if(data.status) {
		                toastr.success(data.message);

		                $('#contact-list').append(
			        		'<div class="card mb-3 text-right success-color">' +
				              	'<div class="card-body">' +
				                	'<p class="card-text white-text">'+ data.record.content + '<br />' +
				                	'<small class="text-muted white-text">'+ time_render(data.record.created_at) +'</small></p>' +
				              	'</div>' +
				            '</div>'
			        	);

	                    $('#contact-list').animate({scrollTop : $('#contact-list').get(0).scrollHeight }, 1000);
		            } else {
		                toastr.error(data.message);
		            }

		            $('[name="message"]').val('').trigger('change');
		            $('#message-btn').html('<i class="las la-paper-plane"></i>');
		            $('#message-btn').attr('disabled', false);
		        }
		    });
		}

		function start_contact()
		{
			$('#contact-list').html('');
			$('#contact-list').html(loading_render());

			$('#message-btn').attr('onclick', 'send_message()');
			$('#refresh-btn').attr('onclick', 'start_contact()');
			
			$.ajax({
		        url : base_url + 'company/contact/start_contact',
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	$('#contact-list').html('');

		        	if (!data.messages) {
		        		$('#contact-list').append(
			        		'<div class="card mb-3">' +
				              	'<div class="card-body">' +
				                	'<p class="card-text"><i class="fas fa-info-circle"></i> Görüşmeye başlamak için aşağıdaki "Mesaj At" alanından mesaj yollayabilirsiniz.</p>' +
				              	'</div>' +
				            '</div>'
			        	);
		        	}

		        	let card_direction;
		        	let card_bg_color;
		        	let card_text_color;
		        	$.each(data.messages, function(key, val){
		        		card_direction = '';
		        		card_bg_color = 'border-success';
		        		card_text_color = 'dark-grey-text'
		        		if (val.sender_type == 'company') {
		        			card_direction = 'text-right';
		        			card_bg_color = 'success-color';
		        			card_text_color = 'white-text';
		        		}

		        		$('#contact-list').append(
			        		'<div class="card mb-3 '+ card_direction + ' ' + card_bg_color +'">' +
				              	'<div class="card-body">' +
				                	'<p class="card-text '+ card_text_color +'">'+ val.content + '<br />' +
				                	'<small class="text-muted '+ card_text_color +'">'+ time_render(val.created_at) +'</small></p>' +
				              	'</div>' +
				            '</div>'
			        	);
		        	});

		        	setTimeout(function(){
                        $('#contact-list').animate({scrollTop : $('#contact-list').get(0).scrollHeight }, 50);
                    }, 100);

		        	unread_contact();
		        }
		    });
		}

    </script>

</body>

</html>