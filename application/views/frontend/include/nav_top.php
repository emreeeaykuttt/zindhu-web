<div id="preloader">
    <div>
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80.8 78" style="enable-background:new 0 0 80.8 78;" xml:space="preserve">
            <style type="text/css">
                .st0 {
                    fill: #7FB642;
                }
            </style>
            <path class="st0" d="M78,11.3c-4.3-5-12.3-5.9-22.8-2.5c-0.9,0.3-1.8,0.6-2.8,1C47,3.5,39.1,0,30.8,0C15.2-0.1,2.5,12.5,2.3,28.1c0,6,2,11.9,5.6,16.7c-4.5,6.4-7.3,12.6-7.8,17.9c-0.5,5,1.2,8.2,2.8,10c3,3.5,7.6,5.3,13.9,5.3H25c2.6,0,4.7-2.1,4.7-4.7s-2.1-4.7-4.7-4.7h-8.2c-2.3,0-5.3-0.3-6.6-1.9c-0.6-0.9-0.8-2-0.6-3c0.8-9,12.5-22.3,22.9-30.8c19.6-16,35.5-18.5,38.2-15.3c0.9,1.1,1.1,4.3-1.6,9.7c-1.1,2.4-0.1,5.2,2.3,6.3c2.3,1.1,5.1,0.1,6.3-2.2C82.9,20.7,80.7,14.6,78,11.3z M26.5,25.4c-5.3,4.3-10.2,9-14.6,14.2l-0.2-0.3c1-1.4,1.4-3.1,1.2-4.9l0,0c-0.8-2.1-1.2-4.3-1.2-6.5c0.1-10.4,8.6-18.7,19-18.6c3.6,0,7.1,1,10.2,2.9c1.6,0.7,3.4,0.7,5,0l0.2,0.2C39.2,16.1,32.6,20.4,26.5,25.4z" />
        </svg>

    </div>
</div>

<div class="header 
        <?= $this->uri->segment(3) == 'is-ilanlari' ? 'header-white' : '' ?>
        <?= $this->uri->segment(2) ? 'sub-header' : '' ?>">
    <a class="logo" href="<?= base_url('zindhu-hr') ?>" data-ani></a>
    <div class="hamburger" data-ani>
        <div class="open-hamburger">
            <svg width="36" height="26" viewBox="0 0 36 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect width="36" height="3" rx="1.5" fill="#050522" />
                <rect x="9" y="23" width="27" height="3" rx="1.5" fill="#050522" />
                <rect x="23" y="12" width="13" height="3" rx="1.5" fill="#050522" />
            </svg>
        </div>
        <div class="close-hamburger">
            <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M23.3851 25.5507C23.9833 26.1489 24.9531 26.1489 25.5513 25.5507C26.1496 24.9525 26.1496 23.9826 25.5513 23.3844L15.1666 12.9999L25.5511 2.61557C26.1493 2.01738 26.1493 1.04752 25.5511 0.44933C24.9529 -0.14886 23.9831 -0.14886 23.3849 0.44933L13.0003 10.8337L2.61514 0.448643C2.01693 -0.149549 1.04706 -0.149547 0.448858 0.448643C-0.149343 1.04683 -0.149345 2.01669 0.448858 2.61488L10.8341 12.9999L0.448652 23.3851C-0.149552 23.9833 -0.149549 24.9532 0.448652 25.5514C1.04685 26.1495 2.01673 26.1495 2.61493 25.5514L13.0003 15.1661L23.3851 25.5507Z" fill="#050522" />
            </svg>
        </div>
    </div>
    <div class="menu">
        <?php if ($this->session->userdata('UserAdminConfirm') == 'approved') { ?>
            
            <div class="user">
                <div class="custom-dropdown">
                    <ul>
                        <li>
                            <a href="#">
                                <i class="fas fa-user-alt"></i>
                                <?= $this->session->userdata('UserFullname') ?>
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown">
                                <li><a href="<?= base_url('zindhu-hr/kullanici/dashboard') ?>"><i class="fas fa-columns"></i> Dashboard</a></li>
                                <li><a href="<?= base_url('zindhu-hr/kullanici/profil') ?>"><i class="fas fa-user-alt"></i> Profil</a></li>
                                <li><a href="<?= base_url('zindhu-hr/kullanici/is-ilanlari') ?>"><i class="fas fa-list"></i> İş İlanları</a></li>
                                <li><a href="<?= base_url('zindhu-hr/kullanici/is-ilanlari?transaction[]=apply') ?>"><i class="fas fa-list"></i> Başvurduğum İlanlar</a></li>
                                <li><a href="<?= base_url('zindhu-hr/kullanici/is-ilanlari?transaction[]=follow') ?>"><i class="fas fa-list"></i> Takip Ettiğim İlanlar</a></li>
                                <li>
                                    <a href="<?= base_url('zindhu-hr/kullanici/konusmalar') ?>">
                                        <i class="fas fa-comments"></i> Konuşmalar <span class="badge badge-primary ml-auto" id="header-unread-count">0</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= base_url('zindhu-hr/kullanici/zindhu-hr-ulasin') ?>">
                                        <i class="fas fa-comments"></i> Zindhu HR'a Ulaşın <span class="badge badge-primary ml-auto" id="header-unread-admin-count">0</span>
                                    </a>
                                </li>
                                <li class="logout"><a href="<?= base_url('user/auth/logout') ?>"><i class="fas fa-power-off"></i> Çıkış Yap</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="language" data-ani>

                <div class="custom-dropdown">
                    <ul>
                        <li>
                            <a href="#">
                                <i class="la la-globe"></i>
                                <?php if ($this->session->userdata('lang_code') == 'tr') { ?>
                                    Türkçe
                                <?php } else { ?>
                                    English
                                <?php } ?>
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown">
                                <li><a href="<?= base_url('language/change/tr?page=zindhu-hr') ?>">Türkçe</a></li>
                                <li><a href="<?= base_url('language/change/en?page=zindhu-hr') ?>">English</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

        <?php } else { ?>
            <div class="language" data-ani>

                <div class="custom-dropdown">
                    <ul>
                        <li>
                            <a href="#">
                                <i class="la la-globe"></i>
                                <?php if ($this->session->userdata('lang_code') == 'tr') { ?>
                                    Türkçe
                                <?php } else { ?>
                                    English
                                <?php } ?>
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown">
                                <li><a href="<?= base_url('language/change/tr?page=zindhu-hr') ?>">Türkçe</a></li>
                                <li><a href="<?= base_url('language/change/en?page=zindhu-hr') ?>">English</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                </div>
            <div class="login" data-ani>
                <a href="#" class="btn btn-green" data-toggle="modal" data-target="#loginModal"><?= l('giris_yap') ?></a>
            </div>

        <?php } ?>

    </div>
</div>

<div class="mobile-menu">
    <?php if ($this->session->userdata('UserAdminConfirm') == 'approved') { ?>
        <h5>
            <i class="fas fa-user-alt"></i> <?= $this->session->userdata('UserFullname') ?>
        </h5>
    
        <ul class="mobile-menu-links">
            <li><a href="<?= base_url('zindhu-hr/kullanici/dashboard') ?>"><i class="fas fa-columns"></i> Dashboard</a></li>
            <li><a href="<?= base_url('zindhu-hr/kullanici/profil') ?>"><i class="fas fa-user-alt"></i> Profil</a></li>
            <li><a href="<?= base_url('zindhu-hr/kullanici/is-ilanlari') ?>"><i class="fas fa-list"></i> İş İlanları</a></li>
            <li><a href="<?= base_url('zindhu-hr/kullanici/is-ilanlari?transaction[]=apply') ?>"><i class="fas fa-list"></i> Başvurduğum İlanlar</a></li>
            <li><a href="<?= base_url('zindhu-hr/kullanici/is-ilanlari?transaction[]=follow') ?>"><i class="fas fa-list"></i> Takip Ettiğim İlanlar</a></li>
            <li>
                <a href="<?= base_url('zindhu-hr/kullanici/konusmalar') ?>">
                    <i class="fas fa-comments"></i> Konuşmalar <span class="badge badge-primary ml-auto" id="header-unread-count">0</span>
                </a>
            </li>
            <li>
                <a href="<?= base_url('zindhu-hr/kullanici/zindhu-hr-ulasin') ?>">
                    <i class="fas fa-comments"></i> Zindhu HR'a Ulaşın <span class="badge badge-primary ml-auto" id="header-unread-admin-count">0</span>
                </a>
            </li>
        </ul>
        <div class="login">
            <a href="<?= base_url('user/auth/logout') ?>" class="btn btn-green" >Çıkış Yap</a>
        </div>

    <?php } else { ?>

        <div class="social-media">
            <a href="https://www.linkedin.com/company/zindhu" target="_blank" class="social-link"><i class="la la-linkedin"></i></a>
            <a href="https://www.instagram.com/zindhugames/" target="_blank" class="social-link"><i class="la la-instagram"></i></a>
        </div>
        <div class="info">
            <h4>Zindhu</h3>
                <p>© 2021 Zindhu HR.</p>
        </div>
        <div class="address">
            <h4>İstanbul</h3>
                <p>Çeliktepe Mahallesi, İsmet İnönü Caddesi, No:11 501, 34413, Kağıthane</p>
        </div>
        <a href="javascript:void(0)" data-toggle="modal" data-target="#privacy-modal" class="text-link">
            <?= l('kisisel_veri_ve_gizlilik_politikasi') ?>
        </a>
        <div class="login">
            <a href="#" class="btn btn-green" data-toggle="modal" data-target="#loginModal">Giriş Yap</a>
            <a href="#" class="green">Kayıt Ol</a>
        </div>
        
    <?php } ?>
</div>