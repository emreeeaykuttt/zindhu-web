<div class="modal information" id="general-modal" tabindex="-1" role="dialog" aria-labelledby="general-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="logo"></div>
                <p class="text-center" id="modal-content">
                    <!-- Content -->
                </p>
                <p>
                    <img src="" id="modal-image" />
                </p>
                <p class="text-center">
                    <button href="#" class="btn btn-dark" data-dismiss="modal">Tamam</button>
                </p>
            </div>
        </div>
    </div>
</div>

<script src="<?=base_url()?>assets/plugins/jquery/jquery-3.5.1.min.js"></script>
<script src="<?=base_url()?>assets/plugins/swiper/swiper-bundle.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap/bootstrap.bundle.min.js"></script>
<script src="<?=base_url()?>assets/plugins/mdb/mdb.min.js"></script>
<script src="<?=base_url()?>assets/plugins/gsap/gsap.min.js"></script>
<script src="<?=base_url()?>assets/plugins/gsap/ScrollTrigger.min.js"></script>
<script type="text/javascript">var base_url = '<?=base_url()?>';</script>
<script src="<?=base_url()?>assets/frontend/js/common.js"></script>