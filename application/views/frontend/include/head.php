    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="google-site-verification" content="thk6a3gwobpRoIbHmnCPaA9iPWXVPx6ndUpgiYzRoQQ" />

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?=base_url()?>assets/frontend/img/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=base_url()?>assets/frontend/img/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=base_url()?>assets/frontend/img/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=base_url()?>assets/frontend/img/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?=base_url()?>assets/frontend/img/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?=base_url()?>assets/frontend/img/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="<?=base_url()?>assets/frontend/img/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?=base_url()?>assets/frontend/img/favicon-16x16.png" sizes="16x16" />
    <meta name="application-name" content="Zindhu HR" />
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="<?=base_url()?>assets/frontend/img/mstile-144x144.png" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/plugins/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/plugins/line-awesome/css/line-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/frontend/fonts/fonts.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/frontend/fonts/icomoon/style.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/plugins/mdb/mdb.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/plugins/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/frontend/css/style.css?v=2.0" />
    <style>
        #preloader {
            background-color: #fff;
            color: black;
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100%;
            height: 100%;
            position: fixed;
            z-index: 999;
            left: 0;
            top: 0;
            pointer-events: none;
        }

        #preloader svg {
            width: 100px;
            height: 100px;
            animation: preloaderAni 1s infinite;
            position: relative;
            display: inline-block;
            object-fit: contain;
        }

        @keyframes preloaderAni {
            0% {
                left: -20px
            }

            50% {
                left: 40px;
            }

            100% {
                left: -20px
            }
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-175472421-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-175472421-1');
    </script>