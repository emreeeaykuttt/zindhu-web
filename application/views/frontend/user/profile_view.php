<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="Profil">
	<meta name="keywords" content="Profil">

	<title>Profil</title>

	<?php include('inc/head.php'); ?>
	<style type="text/css">
		#personal-information-view .personal-delete {
			display: none;
		}

		.cv-status {
			width: 120px;
		}
	</style>

</head>

<body class="fixed-sn white-skin">

	<?php include('inc/header.php'); ?>

	<!-- Main layout -->
	<main>
		<div class="container-fluid">

			<div class="d-flex justify-content-center page-loading">
                <div class="spinner-border" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>

			<div class="row list-view d-none">

				<div class="col-md-12">
					<h2 class="font-weight-bold" id="module-title">Profil</h2>
				</div>

				<div class="col-12 mt-2">
					<div id="cv-status-view list-group mb-1">
						<div class="dashbord-user-info">
							<b class="gray-text">Profil durumum çalışma fırsatları için:</b>
							<div class="user-status">
								<div><i class="las la-info-circle" data-toggle="tooltip" title="Stüdyoların profilinizi incelemesini istiyorsanız durumu açık, istemiyorsanız kapalı yapınız."></i></div>
								<div id="cv-status-text"><?= $this->session->userdata('UserCvStatus') == 1 ? 'Açık' : 'Kapalı' ?></div>
								<div class="custom-control custom-switch ">
									<input type="checkbox" <?= $this->session->userdata('UserCvStatus') == 1 ? 'checked' : '' ?> class="custom-control-input" id="cv-status">
									<label class="custom-control-label" for="cv-status"></label>
								</div>
							</div>
						</div>
						<hr class="mt-0" />
					</div>

					<div class="profile-section">
						<div class="section-item personal-information" id="personal-information">
							<div class="title d-flex justify-content-between">
								<h3 class="text"><i class="icon-profile-2-icon"></i>Kişisel Bilgiler</h3>
							</div>
							<div class="user-status" id="working-status-view">
								<!-- working-status -->
							</div>
							<div class="content" id="personal-information-view">
								<!-- personal-information -->
							</div>
						</div>
						<div class="section-item" id="educational-background">
							<div class="title d-flex justify-content-between">
								<h3 class="text"><i class="icon-cap-icon"></i>Eğitim geçmişi</h3>
								<div class="add" onclick="educational_background_add()">
									Ekle <i class="la la-plus"></i>
								</div>
							</div>
							<div class="p-3">
								<button class="add-new" onclick="educational_background_add()"> <i class="icon-cap-icon"></i>Eğitim Ekle <i class="la la-plus"></i></button>
							</div>
							<div class="content" id="educational-background-view">
								<!-- educational-background -->
							</div>
						</div>

						<div class="section-item" id="work-experience">
							<div class="title d-flex justify-content-between">
								<h3 class="text"><i class="icon-experience-icon"></i>Geçmiş deneyimler</h3>
								<div class="add" onclick="work_experience_add()">
									Ekle <i class="la la-plus"></i>
								</div>
							</div>
							<div class="p-3">
								<button class="add-new" onclick="work_experience_add()"> <i class="icon-experience-icon"></i>Deneyim Ekle <i class="la la-plus"></i></button>
							</div>
							<div class="content" id="work-experience-view">
								<!-- work-experience -->
							</div>
						</div>

						<div class="section-item" id="used-program">
							<div class="title d-flex justify-content-between">
								<h3 class="text"><i class="icon-program-icon"></i>Programlar</h3>
								<div class="add" onclick="used_program_add()">
									Ekle <i class="la la-plus"></i>
								</div>
							</div>
							<div class="p-3">
								<button class="add-new" onclick="used_program_add()"> <i class="icon-program-icon"></i>Program Ekle <i class="la la-plus"></i></button>
							</div>
							<div class="content" id="used-program-view">
								<!-- used-program -->
							</div>
						</div>

						<div class="section-item" id="game-choice">
							<div class="title d-flex justify-content-between">
								<h3 class="text"><i class="las la-gamepad"></i> İlgi duyduğun türler</h3>
								<!-- <div class="add" onclick="game_choice_add()">
									Ekle <i class="la la-plus"></i>
								</div> -->
							</div>
							<div class="p-3">
								<button class="add-new" onclick="game_choice_add()"> <i class="las la-gamepad"></i>Tür Ekle <i class="la la-plus"></i></button>
							</div>
							<div class="content" id="game-choice-view">
								<!-- game-choice -->
							</div>
						</div>

						<div class="section-item portfolio" id="portfolio">
							<div class="title d-flex justify-content-between">
								<h3 class="text">Portfolyo</h3>
								<div class="add" onclick="portfolio_add()">
									Ekle <i class="la la-plus"></i>
								</div>
							</div>
							<div class="p-3">
								<button class="add-new" onclick="portfolio_add()"> <i></i>Portfolyo Ekle <i class="la la-plus"></i></button>
							</div>
							<div class="content" id="portfolio-view">
								<!-- portfolio -->
							</div>
						</div>

					</div>
				</div>
			</div>

		</div>
	</main>
	<!-- Main layout -->

	<?php include('inc/footer.php'); ?>

	<!-- working-status-modal -->
	<div class="modal fade profile-modal" id="working-status-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-xs" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead" id="working-status-title">Çalışma Durumu</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="working-status-content">

					<div class="row">
						<form action="#" id="working-status-from" class="col-12">

							<div class="row">

								<div class="col-6 col-md-2">
									<div class="custom-control custom-checkbox mb-3">
										<input type="checkbox" class="custom-control-input" name="working_status_is_working" id="is-working" value="1">
										<label class="custom-control-label" for="is-working">Çalışıyorum</label>
									</div>
								</div>

								<div class="md-form col-12 mb-0 working-status-company-id">
									<select class="mdb-select" name="working_status_company_id" id="working-status-company-id" searchable="Arama">
										<option value="0" disabled selected>Seçiniz</option>
									</select>
									<label class="mdb-main-label">Stüdyo Adı:</label>
									<div class="red-text small error-text" id="working_status_company_id_error"></div>
								</div>

								<div class="col-12 col-md-12 working-status-company-text">
									<div class="md-form">
										<input type="text" class="form-control" name="working_status_company_text" id="working-status-company-text">
										<label for="working-status-company-text" class="mdb-main-label">Stüdyo Adı:</label>
										<div class="red-text small error-text" id="working_status_company_text_error"></div>
									</div>
								</div>

								<div class="col-12 text-center mt-3">
									<a onclick="working_status_save()" id="working-status-btn" class="btn btn-primary btn-md-lg w-50">Kaydet</a>
								</div>
							</div>

						</form>
					</div>

				</div>
			</div>
			<!-- Content -->
		</div>
	</div>
	<!-- working-status-modal -->

	<!-- personal-information-modal -->
	<div class="modal fade profile-modal" id="personal-information-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead" id="personal-information-title">Kişisel Bilgiler</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="personal-information-content">
					<div class="row">
						<form action="#" id="personal-information-from" class="col-12">

							<input type="hidden" value="" name="personal_information_id" />

							<div class="row">

								<div class="col-6 col-md-6">
									<div class="custom-input">
										<label for="user-firstname" class="mdb-main-label">Ad:</label>
										<input type="text" class="form-control" name="user_firstname" id="user-firstname">
										<div class="red-text small error-text" id="user_firstname_error"></div>
									</div>
								</div>

								<div class="col-6 col-md-6">
									<div class="custom-input">
										<label for="user-lastname" class="mdb-main-label">Soyad:</label>
										<input type="text" class="form-control" name="user_lastname" id="user-lastname">
										<div class="red-text small error-text" id="user_lastname_error"></div>
									</div>
								</div>

								<div class="col-12 col-md-6">
									<div class="custom-input">
										<label for="user-email" class="mdb-main-label">Email:</label>
										<input type="text" class="form-control" name="user_email" id="user-email">
										<div class="red-text small error-text" id="user_email_error"></div>
									</div>
								</div>

								<div class="col-6 col-md-6">
									<div class="custom-input">
										<label for="personal-information-phone" class="mdb-main-label">Telefon:</label>
										<input type="text" class="form-control" name="personal_information_phone" id="personal-information-phone">
										<div class="red-text small error-text" id="personal_information_phone_error"></div>
									</div>
								</div>
								<div class="col-6 col-md-6">
									<div class="custom-input">
										<label class="mdb-main-label">Pozisyon:</label>
										<select class="mdb-select" name="personal_information_position_id" id="personal-information-position-id" searchable="Arama">
											<option value="0" disabled selected>Seçiniz</option>
										</select>
										<div class="red-text small error-text" id="personal_information_position_id_error"></div>
									</div>
								</div>

								<div class="col-12 col-md-6">
									<div class="custom-input">
										<label class="mdb-main-label">Yaşadığın Şehir:</label>
										<select class="mdb-select" name="personal_information_city_id" id="personal-information-city-id" searchable="Arama">
											<option value="0" disabled selected>Seçiniz</option>
										</select>
										<div class="red-text small error-text" id="personal_information_city_id_error"></div>
									</div>
								</div>

								<div class="col-12 col-md-6">
									<div class="custom-input">
										<label class="mdb-main-label">Çalışmak İstediğiniz Şehirler:</label>
										<select class="mdb-select md-form disable-toggle-all" name="personal_information_city_json" id="personal-information-city-json" searchable="Arama" multiple>
											<option value="0" disabled selected>Seçiniz</option>
										</select>
										<div class="red-text small error-text" id="personal_information_city_json_error"></div>
									</div>
								</div>

								<div class="col-12 col-md-6 d-none city-text">
									<div class="custom-input mt-4">
										<label for="personal-information-city-text" class="mdb-main-label">Ülke-Şehir:</label>
										<input type="text" class="form-control" name="personal_information_city_text" id="personal-information-city-text">
										<div class="red-text small error-text" id="personal_information_city_text_error"></div>
									</div>
								</div>

								<div class="col-12">
									<div class="my-3">
										<input type="checkbox" class="form-check-input" name="personal_information_is_remote" id="personal-information-is-remote" value="1">
										<label class="form-check-label" for="personal-information-is-remote">Uzaktan(remote) çalışmak istiyorum.</label>
										<div class="red-text small error-text" id="personal_information_is_remote_error"></div>
									</div>
								</div>


							</div>

						</form>
					</div>
				</div>
				<div class="modal-footer d-flex">
					<button type="button" class="btn btn-md-lg btn-dark-outline col-md-4" data-dismiss="modal">Kapat</button>
					<button onclick="personal_information_save()" id="personal-information-btn" class="btn btn-primary btn-md-lg col-md-8">Kaydet</button>
				</div>
			</div>
			<!-- Content -->
		</div>
	</div>
	<!-- personal-information-modal -->

	<!-- educational-background-modal -->
	<div class="modal fade profile-modal" id="educational-background-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead" id="education-title">Üniversite Ekle</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="educational-background-content">
					<div class="row">
						<form action="#" id="educational-background-from" class="col-12">

							<input type="hidden" value="" name="education_id" />

							<div class="row">
								<div class="col-12 col-md-6">
									<div class="custom-input">
										<label class="mdb-main-label">Eğitim Seviyesi:</label>
										<select class="mdb-select" name="education_level" id="education-level">
											<option value="0" disabled selected>Seçiniz</option>
											<option value="on-lisans">Ön Lisans</option>
											<option value="lisans">Lisans</option>
											<option value="yuksek-lisans">Yüksek Lisans</option>
											<option value="doktora">Doktora</option>
										</select>
										<div class="red-text small error-text" id="education_level_error"></div>
									</div>
								</div>

								<div class="col-12 col-md-6">
									<div class="custom-input">
										<label class="mdb-main-label">Üniversite:</label>
										<select class="mdb-select" name="education_university_id" id="education-university-id" searchable="Arama">
											<option value="" disabled selected>Seçiniz</option>
										</select>
										<div class="red-text small error-text" id="education_university_id_error"></div>
									</div>
								</div>

								<div class="col-12 col-md-12">
									<div class="custom-input">
										<label for="education-university-section" class="mdb-main-label">Bölüm:</label>
										<input type="text" class="form-control" name="education_university_section" id="education-university-section">
										<div class="red-text small error-text" id="education_university_section_error"></div>
									</div>
								</div>

								<div class="col-12 d-none university-other">
									<div class="custom-input mt-0">
										<label for="education-university-text" class="mdb-main-label">Üniversite:</label>
										<input type="text" class="form-control" name="education_university_text" id="education-university-text">
										<div class="red-text small error-text" id="education_university_text_error"></div>
									</div>
								</div>

								<div class="col-md-4">
									<div class="custom-input">
										<label class="mdb-main-label">Başlangıç Yılı:</label>
										<select class="mdb-select" name="education_start_year" id="education-start-year">
											<option value="0" disabled selected>Seçiniz</option>
											<?php for ($i = 2021; $i >= 1970; $i--) { ?>
												<option value="<?= $i ?>"><?= $i ?></option>
											<?php } ?>
										</select>
										<div class="red-text small error-text" id="education_start_year_error"></div>
									</div>
								</div>

								<div class="col-md-4 education-end-year">
									<div class="custom-input">
										<label class="mdb-main-label">Bitiş Yılı:</label>
										<select class="mdb-select" name="education_end_year" id="education-end-year">
											<option value="0" disabled selected>Seçiniz</option>
											<?php for ($i = 2021; $i >= 1970; $i--) { ?>
												<option value="<?= $i ?>"><?= $i ?></option>
											<?php } ?>
										</select>
										<div class="red-text small error-text" id="education_end_year_error"></div>
									</div>
								</div>

								<div class="col-md-4 d-flex align-items-end">
									<div class="form-check my-3">
										<input type="checkbox" class="form-check-input" name="education_continues" id="continues">
										<label class="form-check-label" for="continues">
											Devam Ediyor
										</label>
									</div>
									<!-- <div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" name="education_continues" id="continues" value="1">
										<label class="custom-control-label" for="continues">Devam Ediyor</label>
									</div> -->
								</div>
							</div>

						</form>
					</div>
				</div>
				<div class="modal-footer d-flex ">
					<button type="button" class="btn btn-md-lg btn-dark-outline col-md-2" data-dismiss="modal">Kapat</button>
					<button onclick="educational_background_save()" id="personal-information-btn " class="btn btn-primary btn-md-lg col-md-4">Kaydet</button>
				</div>
			</div>
			<!-- Content -->
		</div>
	</div>
	<!-- educational-background-modal -->

	<!-- work-experience-modal -->
	<div class="modal fade profile-modal" id="work-experience-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead" id="work-title">Deneyim Ekle</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="work-experience-content">

					<div class="row">
						<form action="#" id="work-experience-from" class="col-12">

							<input type="hidden" value="" name="work_id" />
							<input type="hidden" value="0" name="work_status" />

							<div class="row">

								<div class="col-12 col-md-4">
									<div class="custom-input">
										<label for="work-company-name" class="mdb-main-label">Firma Adı:</label>
										<input type="text" class="form-control" name="work_company_name" id="work-company-name">
										<div class="red-text small error-text" id="work_company_name_error"></div>
									</div>
								</div>

								<div class="col-6 col-md-4">
									<div class="custom-input">
										<label for="work-company-sector" class="mdb-main-label">Firma Sektorü:</label>
										<input type="text" class="form-control" name="work_company_sector" id="work-company-sector">
										<div class="red-text small error-text" id="work_company_sector_error"></div>
									</div>
								</div>

								<div class="col-6 col-md-4">
									<div class="custom-input">
										<label for="work-company-position" class="mdb-main-label">Firmadaki Pozisyon:</label>
										<input type="text" class="form-control" name="work_company_position" id="work-company-position">
										<div class="red-text small error-text" id="work_company_position_error"></div>
									</div>
								</div>

								<div class="col-6 col-md-2">
									<div class="custom-input">
										<label class="mdb-main-label">Başlangıç Ayı:</label>
										<select class="mdb-select" name="work_start_month" id="work-start-month">
											<option value="0" disabled selected>Seçiniz</option>
											<option value="1">Ocak</option>
											<option value="2">Şubat</option>
											<option value="3">Mart</option>
											<option value="4">Nisan</option>
											<option value="5">Mayıs</option>
											<option value="6">Haziran</option>
											<option value="7">Temmuz</option>
											<option value="8">Ağustos</option>
											<option value="9">Eylül</option>
											<option value="10">Ekim</option>
											<option value="11">Kasım</option>
											<option value="12">Aralık</option>
										</select>
										<div class="red-text small error-text" id="work_start_month_error"></div>
									</div>
								</div>

								<div class="col-6 col-md-3">
									<div class="custom-input">
										<label class="mdb-main-label">Başlangıç Yılı:</label>
										<select class="mdb-select" name="work_start_year" id="work-start-year">
											<option value="0" disabled selected>Seçiniz</option>
											<?php for ($i = 2021; $i >= 1970; $i--) { ?>
												<option value="<?= $i ?>"><?= $i ?></option>
											<?php } ?>
										</select>
										<div class="red-text small error-text" id="work_start_year_error"></div>
									</div>
								</div>

								<div class="col-6 col-md-2 work-end-year">
									<div class="custom-input">
										<label class="mdb-main-label">Bitiş Ayı:</label>
										<select class="mdb-select" name="work_end_month" id="work-end-month">
											<option value="0" disabled selected>Seçiniz</option>
											<option value="1">Ocak</option>
											<option value="2">Şubat</option>
											<option value="3">Mart</option>
											<option value="4">Nisan</option>
											<option value="5">Mayıs</option>
											<option value="6">Haziran</option>
											<option value="7">Temmuz</option>
											<option value="8">Ağustos</option>
											<option value="9">Eylül</option>
											<option value="10">Ekim</option>
											<option value="11">Kasım</option>
											<option value="12">Aralık</option>
										</select>
										<div class="red-text small error-text" id="work_end_month_error"></div>
									</div>
								</div>

								<div class="col-6 col-md-3 work-end-year">
									<div class="custom-input">
										<label class="mdb-main-label">Bitiş Yılı:</label>
										<select class="mdb-select" name="work_end_year" id="work-end-year">
											<option value="0" disabled selected>Seçiniz</option>
											<?php for ($i = 2021; $i >= 1970; $i--) { ?>
												<option value="<?= $i ?>"><?= $i ?></option>
											<?php } ?>
										</select>
										<div class="red-text small error-text" id="work_end_year_error"></div>
									</div>
								</div>

								<div class="col-12 col-md-2 d-flex align-items-end">
									<div class="form-check my-3">
										<input type="checkbox" class="form-check-input" name="work_working" id="working">
										<label class="form-check-label" for="working">
											Hala Çalışıyorum
										</label>
									</div>
								</div>

								<div class="col-12">
									<div class="custom-input">
										<label for="work-detail" class="mdb-main-label">Açıklama:</label>
										<input type="text" class="form-control" name="work_detail" id="work-detail">
										<div class="red-text small error-text" id="work_detail_error"></div>
									</div>
								</div>
							</div>

						</form>
					</div>

				</div>
				<div class="modal-footer d-flex">
					<button type="button" class="btn btn-md-lg btn-dark-outline col-md-2" data-dismiss="modal">Kapat</button>
					<button onclick="work_experience_save()" id="work-btn" class="btn btn-primary btn-md-lg col-md-4">Kaydet</button>
				</div>
			</div>
			<!-- Content -->

		</div>
	</div>
	<!-- work-experience-modal -->

	<!-- used-program-modal -->
	<div class="modal fade profile-modal" id="used-program-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead" id="used-program-title">Program Ekle</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="used-program-content">

					<div class="row">
						<form action="#" id="used-program-from" class="col-12">

							<input type="hidden" value="" name="program_id" />

							<div class="row">
								<div class="col-md-6">
									<div class="custom-input">
										<label class="mdb-main-label">Program:</label>
										<select class="mdb-select" name="used_program_id" id="used-program-id" searchable="Arama">
											<option value="0" disabled selected>Seçiniz</option>
										</select>
										<div class="red-text small error-text" id="used_program_id_error"></div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="custom-input">
										<label class="mdb-main-label">Kullanma Süresi:</label>
										<select class="mdb-select" name="used_program_duration" id="used-program-duration" searchable="Arama">
											<option value="0" disabled selected>Seçiniz</option>
											<?php for ($i = 1; $i <= 30; $i++) { ?>
												<option value="<?= $i ?>"><?= $i ?> Yıl</option>
											<?php } ?>
										</select>
										<div class="red-text small error-text" id="used_program_duration_error"></div>
									</div>
								</div>
							</div>
						</form>
					</div>

				</div>
				<div class="modal-footer d-flex">
					<button type="button" class="btn btn-md-lg btn-dark-outline col-md-2" data-dismiss="modal">Kapat</button>
					<button onclick="used_program_save()"  id="used-program-btn" class="btn btn-primary btn-md-lg col-md-4">Kaydet</button>
				</div>
			</div>
			<!-- Content -->
		</div>
	</div>
	<!-- used-program-modal -->

	<!-- game-choice-modal -->
	<div class="modal fade profile-modal" id="game-choice-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead" id="game-choice-title">Tür Ekle</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="game-choice-content">

					<div class="row">
						<form action="#" id="game-choice-from" class="col-12">

							<input type="hidden" value="" name="game_id" />

							<div class="row">
								<div class="col-12 col-md-6">
									<div class="custom-input">
										<label class="mdb-main-label">Gelecekte üretiminde bulunmak istediğiniz türler:</label>
										<select class="mdb-select md-form disable-toggle-all" name="game_future_json" id="game-future-json" searchable="Arama" multiple>
											<option value="0" disabled selected>Seçiniz</option>
										</select>
										<div class="red-text small error-text" id="game_future_json_error"></div>
									</div>
								</div>

								<div class="col-12 col-md-6">
									<div class="custom-input">
										<label class="mdb-main-label">Geçmişte üretiminde bulunduğunuz türler:</label>
										<select class="mdb-select md-form disable-toggle-all" name="game_past_json" id="game-past-json" searchable="Arama" multiple>
											<option value="0" disabled selected>Seçiniz</option>
										</select>
										<div class="red-text small error-text" id="game_past_json_error"></div>
									</div>
								</div>

								
							</div>

						</form>
					</div>
					
				</div>
				<div class="modal-footer d-flex">
					<button type="button" class="btn btn-md-lg btn-dark-outline col-md-2" data-dismiss="modal">Kapat</button>
					<button onclick="game_choice_save()"   id="game-choice-btn" class="btn btn-primary btn-md-lg col-md-4">Kaydet</button>
				</div>
			</div>
			<!-- Content -->
		</div>
	</div>
	<!-- game-choice-modal -->

	<!-- portfolio-modal -->
	<div class="modal fade profile-modal" id="portfolio-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead" id="portfolio-title">Portfolyo Ekle</p>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="portfolio-content">
					<div class="row">
						<form action="#" id="portfolio-form" class="col-12" enctype="multipart/form-data">
							<input type="hidden" value="" name="portfolio_id" />
							<div class="row">
								<div class="col-12 mb-3">
									<div class="custom-input">
										<label for="portfolio-link" class="mdb-main-label">Portfolyo Linki:</label>
										<input type="text" class="form-control" name="portfolio_link" id="portfolio-link">
										<div class="red-text small error-text" id="portfolio_link_error"></div>
									</div>
								</div>
								<div class="col-12">
									<div class="custom-input">
										<label id="portfolio-name-text"></label>
										<input type="hidden" name="portfolio_name_text" value="">
									</div>
								</div>
								<div class="col-12">
									<div class="custom-input mt-0">
									<div class="file-field">
											<div class="file-drag">
												<img src="/assets/frontend/img/file-icon.svg" />
												<p>Dosyanızı buraya sürükleyin veya tıklayın.</p>
												<p class="weak">(Maks. 10mb, PDF, PPT )</p>
												<input type="file" name="portfolio_name" id="portfolio-name">
											</div>
											<div class="file-path-wrapper">
												<input class="file-path validate" disabled type="text" placeholder="Dosya Adı">
											</div>
										</div>
										<div class="red-text small error-text" id="portfolio_name_error"></div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>

				<div class="modal-footer d-flex">
					<button type="button" class="btn btn-md-lg btn-dark-outline col-md-2" data-dismiss="modal">Kapat</button>
					<button onclick="portfolio_save()" id="used-program-btn" class="btn btn-primary btn-md-lg col-md-4">Kaydet</button>
				</div>

			</div>
		</div>
	</div>
	<!-- portfolio-modal -->

	<?php include('inc/script.php'); ?>
	<script src="<?= base_url() ?>assets/admin/js/cv_helper.js"></script>
	<script src="<?= base_url() ?>assets/admin/js/cv.js?v=2.3"></script>

	<script type="text/javascript">
		let cv_status = 0;
		company_list();

		$(document).ready(function() {

			$('#cv-status').change(function() {

				if ($(this).is(":checked")) {
					cv_status = 1;
					$('#cv-status-text').text('Açık');
				} else {
					cv_status = 0;
					$('#cv-status-text').text('Kapalı');
				}

				$.ajax({
					url: base_url + 'user/cv/cv_status/' + cv_status,
					type: 'GET',
					dataType: 'JSON',
					success: function(data) {
						if (data.cv_status) {
							toastr.success(data.message);
						} else {
							toastr.warning(data.message);
						}
					}
				});

			});

			$('[data-toggle="tooltip"]').tooltip();

			$('input[type="file"]').change(function(e) {
				var $this = $(e.target);
				var $fileField = $this.closest('.file-field');
				var $pathInput = $fileField.find('input.file-path');
				var fileNames = [];

				fileNames.push(e.target.files[0].name);

				$pathInput.val(fileNames.join(', '));
				$pathInput.trigger('change');
			});

			$('#is-working').change(function() {
				if ($(this).is(':checked')) {
					$('.working-status-company-id').css('display', 'block');
				} else {
					$('.working-status-company-id, .working-status-company-text').css('display', 'none');
				}
			});

			$('#continues').change(function() {
				if ($(this).is(':checked')) {
					$('.education-end-year').css('display', 'none');
				} else {
					$('.education-end-year').css('display', 'block');
				}
			});

			$('#working').change(function() {
				if ($(this).is(':checked')) {
					$('.work-end-year').css('display', 'none');
				} else {
					$('.work-end-year').css('display', 'block');
				}
			});

			$('#education-level, #education-start-year, #education-end-year, #work-start-month, #work-start-year, #work-end-month, #work-end-year, #used-program-duration, #working-status-company-id').materialSelect();

			$('#education-university-id').change(function() {
				let university_id = $(this).val();
				if (university_id == 0) {
					$('.university-other').attr('style', 'display: block !important');
				} else {
					$('.university-other').attr('style', 'display: none !important');
				}
			});

			$('#personal-information-city-id').change(function() {
				let city_id = $(this).val();
				if (city_id == 341) {
					$('.city-text').removeClass('d-none');
					$('.city-text').addClass('d-block');
				} else {
					$('.city-text').removeClass('d-block');
					$('.city-text').addClass('d-none');
					$('[name="personal_information_city_text"]').val('').trigger("change");
				}
			});

			$('#working-status-company-id').change(function() {
				let company_id = $(this).val();
				if (company_id == 0) {
					$('.working-status-company-text').attr('style', 'display: block !important');
				} else {
					$('.working-status-company-text').attr('style', 'display: none !important');
				}
			});
		});
	</script>

</body>

</html>