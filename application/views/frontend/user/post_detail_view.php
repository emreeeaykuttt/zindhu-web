<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Dashboard">
    <meta name="keywords" content="Dashboard">
	<meta name="description" content="Oyun sektörünü oluşturan oyun geliştiricilerden,  yayıncılara, oyun monetizasyonu  yöntemlerinden insan kaynaklarına kadar oyun sektöründe 360 çözümler sunan tüm hizmetler Zindhu çatısı altında!" />
    <meta property="og:title" content="Zindhu: Oyun sektörünün kalbi" />
    <meta property="og:description" content="Oyun sektörünü oluşturan oyun geliştiricilerden,  yayıncılara, oyun monetizasyonu  yöntemlerinden insan kaynaklarına kadar oyun sektöründe 360 çözümler sunan tüm hizmetler Zindhu çatısı altında!" />
    <meta property="og:image" content="<?=base_url()?>assets/home/og.jpg">
    <meta property="og:url" content="<?=base_url()?>">
    <meta property="og:type" content="website">
    <meta name="twitter:card" content="summary" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="theme-color" content="#3d00bf" />
    <title>Dashboard</title>

    <?php include('inc/head.php'); ?>

</head>

<body class="fixed-sn white-skin">

	<?php include('inc/header.php'); ?>

	<!-- Main layout -->
	<main>
		<div class="container-fluid">

			<section>
				<div class="row">

					<div class="col-8 offset-md-2">
						<div class="card">
				            <div class="card-body">

				            	<h5><?=$post_detail->title?></h5>

				            	<?php if ($post_detail->photo): ?>
				            	<div class="view view-cascade overlay">
					                <img class="card-img-top" src="<?=base_url(UPLOAD_FOLDER . $post_detail->photo)?>">
					                <div>
					                	<div class="mask rgba-black-slight waves-effect waves-light"></div>
					                </div>
					             </div>
					             <?php endif ?>

					            <p class="mt-3">
					            	<?=$post_detail->detail?>
					            </p>

				            </div>
				        </div>
					</div>

				</div>
			</section>

		</div>
	</main>
	<!-- Main layout -->

    <?php include('inc/footer.php'); ?>
    <?php include('inc/script.php'); ?>

    <script type="text/javascript">
    	
    	$(document).ready(function(){

			

		});

    </script>

</body>

</html>