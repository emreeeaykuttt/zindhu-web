<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="Dashboard">
	<meta name="keywords" content="Dashboard">

	<title>Dashboard</title>
	<style type="text/css">
		#first-login-content i {
			font-size: 25px;
			color: green
		}
	</style>

	<?php include('inc/head.php'); ?>

</head>

<body class="fixed-sn white-skin">

	<?php include('inc/header.php'); ?>

	<!-- Main layout -->
	<main>

		<div class="container-fluid">
			<section>
				<div class="row">
					<div class="col-12">
						<h2 class="font-weight-bold" id="module-title">Dashboard</h2>
						<div class="dashbord-user-info">
							<div class="title">
								<p class="name"><?= $this->session->userdata('UserFullname') ?></p>
								<p class="position"><?= $this->session->userdata('UserPosition') ?></p>
							</div>
							<div class="user-status">
								<div><i class="las la-info-circle" data-toggle="tooltip" title="Stüdyoların profilinizi incelemesini istiyorsanız durumu açık, istemiyorsanız kapalı yapınız."></i></div>
								<div id="cv-status-text"><?= $this->session->userdata('UserCvStatus') == 1 ? 'Açık' : 'Kapalı' ?></div>
								<div class="custom-control custom-switch">
									<input type="checkbox" <?= $this->session->userdata('UserCvStatus') == 1 ? 'checked' : '' ?> class="custom-control-input" id="cv-status">
									<label class="custom-control-label" for="cv-status"></label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="dashboard-grid">
					<div class="row ">
						
						<div class="col-xl-3 col-md-6 mb-xl-0 mb-4">

							<a href="<?= base_url('zindhu-hr/kullanici/konusmalar?unread=1') ?>" class="card classic-admin-card unread">
								<div class="card-body text-black">
									<div class="pull-right">
										<i class="icon-mail-icon"></i>
									</div>
									<p>Okunmamış mesajlar</p>
									<h4 class="check" id="dashboard-unread-count">0</h4>
									<div class="arrow">
										<i class="las la-arrow-right"></i>
									</div>
								</div>
							</a>

						</div>

						<div class="col-xl-3 col-md-6 mb-xl-0 mb-4">

							<a href="<?= base_url('zindhu-hr/kullanici/konusmalar') ?>" class="card classic-admin-card interview">
								<div class="card-body text-black">
									<div class="pull-right">
										<i class="icon-handshake-icon"></i>
									</div>
									<p>Görüşülen şirketler</p>
									<h4 class="check" id="dashboard-conversation-count">0</h4>
									<div class="arrow">
										<i class="las la-arrow-right"></i>
									</div>
								</div>
							</a>

						</div>

						<div class="col-xl-3 col-md-6 mb-xl-0 mb-4">

							<div class="card classic-admin-card follow" onclick="watch_companies()" data-toggle="modal" data-target="#follow-views-modal">
								<div class="card-body text-black">
									<div class="pull-right">
										<i class="icon-telescope-icon"></i>
									</div>
									<p>Sizi takip eden stüdyolar</p>
									<h4 class="check" id="dashboard-watch-count">0</h4>
									<div class="arrow">
										<i class="las la-arrow-right"></i>
									</div>
								</div>
							</div>

						</div>

						<div class="col-xl-3 col-md-6 mb-xl-0 mb-4">

							<div class="card classic-admin-card shown" onclick="views_companies()" data-toggle="modal" data-target="#profile-views-modal">
								<div class="card-body text-black">
									<div class="pull-right">
										<i class="icon-eye-icon"></i>
									</div>
									<p>Profil görüntülenmesi</p>
									<h4 class="check" id="dashboard-views-count">0</h4>
									<div class="arrow">
										<i class="las la-arrow-right"></i>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				
				<div class="dasboard-bottom">
					<div class="saved">
						<div class="title"><i class="la la-bookmark"></i> Kaydedilen ilanlar</div>
						<div class="content">

							<?php if ($followed_ads) { ?>
								<div class="list">
									<ul>
										<?php foreach ($followed_ads as $key => $ad) : ?>
											<li class="ad-item-<?= $ad['ad_id'] ?>">
												<div class="icon"></div>
												<div class="title"><?= $ad['title'] ?> <?=is_remote_render($ad['is_remote'])?></div>
												<div class="actions">
													<div class="dropdown">
														<button class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															<i class="las la-ellipsis-h"></i>
														</button>
														<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
															<a class="dropdown-item" href="<?= base_url('zindhu-hr/kullanici/is-ilani/' . $ad['slug'] . '-' . $ad['ad_id']) ?>">İlan detayına git</a>
															<a class="dropdown-item" href="javascript:void(0)" onclick="ad_unfollow(<?= $ad['ad_id'] ?>)">Kaydetmeyi geri al</a>
														</div>
													</div>
												</div>
											</li>
										<?php endforeach ?>
									</ul>
								</div>
							<?php } else { ?>
								<!-- If no saved -->
								<div class="no-content">Kaydettiğiniz ilanları buradan takip edebilirsiniz.</div>
								<!-- If no saved end -->
							<?php } ?>

						</div>
					</div>
					<div class="swiper-container dashboard-carousel">
						<div class="swiper-pagination"></div>
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<div class="item">
									<div>
										<div class="title">Unity Developer</div>
										<p>İlanlarına göz at, hayalindeki işi birlikte bulalım.</p>
										<a href="<?= base_url('zindhu-hr/kullanici/is-ilanlari?position[]=22&position[]=23&position[]=24') ?>" class="btn btn-outline">İlanlara git</a>
									</div>
									<div class="image" style="background-image: url(/assets/frontend/img/dashboard-carousel-02.svg);">
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div>
										<div class="title">3D Artist</div>
										<p>İlanlarına göz at, hayalindeki işi birlikte bulalım.</p>
										<a href="<?= base_url('zindhu-hr/kullanici/is-ilanlari?position[]=20') ?>" class="btn btn-outline">İlanlara git</a>
									</div>
									<div class="image" style="background-image: url(/assets/frontend/img/3d-artist.svg);">
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div>
										<div class="title">Game Designer</div>
										<p>İlanlarına göz at, hayalindeki işi birlikte bulalım.</p>
										<a href="<?= base_url('zindhu-hr/kullanici/is-ilanlari?position[]=15') ?>" class="btn btn-outline">İlanlara git</a>
									</div>
									<div class="image" style="background-image: url(/assets/frontend/img/game-designer.svg);">
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div>
										<div class="title">Blog</div>
										<?php if (!empty($blogs)) { ?>
											<p><?= $blogs[0]['title'] ?></p>
											<a href="<?= base_url('zindhu-hr/kullanici/post/detay/' . seo_url($blogs[0]['title']) . '-' . $blogs[0]['id']) ?>" class="btn btn-outline">Devamını oku</a>
										<?php } ?>
									</div>
									<div class="image" style="background-image: url(/assets/frontend/img/dashboard-carousel-01.svg);">
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div>
										<div class="title">Duyurular</div>
										<?php if (!empty($announcements)) { ?>
											<p><?= $announcements[0]['title'] ?></p>
											<a href="<?= base_url('zindhu-hr/kullanici/post/detay/' . seo_url($announcements[0]['title']) . '-' . $announcements[0]['id']) ?>" class="btn btn-outline">Devamını oku</a>
										<?php } ?>
									</div>
									<div class="image" style="background-image: url(/assets/frontend/img/dashboard-carousel-01.svg);">
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div>
										<div class="title">Eğitimler</div>
										<?php if (!empty($educations)) { ?>
											<p><?= $educations[0]['title'] ?></p>
											<a href="<?= base_url('zindhu-hr/kullanici/post/detay/' . seo_url($educations[0]['title']) . '-' . $educations[0]['id']) ?>" class="btn btn-outline">Devamını oku</a>
										<?php } ?>
									</div>
									<div class="image" style="background-image: url(/assets/frontend/img/dashboard-carousel-01.svg);">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</section>

		</div>
	</main>
	<!-- Main layout -->

	<?php include('inc/footer.php'); ?>

	<!-- first-login-modal -->
	<div class="modal fade information"  id="first-login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-md" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->

				<div class="modal-header">
					<p class="heading lead" id="kvkk-title">Zindhu HR’a Hoşgeldiniz!</p>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="first-login-content">

					<div class="pl-3 pr-3 text-center">
						<img src="<?=base_url()?>assets/frontend/img/modal-images/success.svg" />
						<br />

						<h5 class="text-center">Kayıt ve onay süreciniz tamamlandı.</h5>
						<br />
						<p>
							Size özel hazırlanmış panelinizden konuşmalarınızı takip edebilir, başvurularınızı ve kaydettiğiniz ilanları görüntüleyebilirsiniz. Oyun şirketleri size mesaj attığında hem panelinizde görüntüleyebilir hem de mail adresinize yapılan bilgilendirmeyi kontrol edebilirsiniz.
							<br>
							Her türlü sorunda «Bize Ulaşın» butonu ile Zindhu ekibine ulaşabilirsiniz.
						</p>
						<br />
						<p class="text-center">
                        	<button href="#" class="btn btn-dark" data-dismiss="modal">Tamam</button>
                    	</p>

					</div>

				</div>
			</div>
			<!-- Content -->
		</div>
	</div>
	<!-- first-login-modal -->

	<!-- profile-views-modal -->
	<div class="modal fade views" id="profile-views-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header" style="background:none">
					<h3> <i class="icon-eye-icon"></i> Profilini görüntüleyenler</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body pb-5">

					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="views-companies-all-tab" data-toggle="tab" href="#views-companies-all" role="tab" aria-controls="home" aria-selected="true">Tüm Zamanlar</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="views-companies-last30days-tab" data-toggle="tab" href="#views-companies-last30days" role="tab" aria-controls="profile" aria-selected="false">Son 30 gün</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="views-companies-last7days-tab" data-toggle="tab" href="#views-companies-last7days" role="tab" aria-controls="contact" aria-selected="false">Bu hafta</a>
						</li>
					</ul>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="views-companies-all" role="tabpanel" aria-labelledby="views-companies-all-tab">
							<!-- all -->
						</div>
						<div class="tab-pane fade" id="views-companies-last30days" role="tabpanel" aria-labelledby="views-companies-last30days-tab">
							<!-- last30days -->
						</div>
						<div class="tab-pane fade" id="views-companies-last7days" role="tabpanel" aria-labelledby="views-companies-last7days-tab">
							<!-- last7days -->
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- profile-views-modal -->

	<!-- follow-views-modal -->
	<div class="modal fade views" id="follow-views-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header" style="background:none">
					<h3> <i class="icon-telescope-icon"></i> Sizi takip eden stüdyolar</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body pb-5">

					<div class="tab-pane fade show active" id="watch-companies" role="tabpanel" aria-labelledby="watch-companies-tab">

					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- follow-views-modal -->

	<?php include('inc/script.php'); ?>
	<script src="https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js"></script>

	<script type="text/javascript">
		var is_first_login = '<?= $this->session->userdata("UserFirstLogin") ?>';
		let cv_status = 0;

		$(document).ready(function() {

			$('#cv-status').change(function() {

				if ($(this).is(":checked")) {
					cv_status = 1;
					$('#cv-status-text').text('Açık');
				} else {
					cv_status = 0;
					$('#cv-status-text').text('Kapalı');
				}

				$.ajax({
					url: base_url + 'user/cv/cv_status/' + cv_status,
					type: 'GET',
					dataType: 'JSON',
					success: function(data) {
						if (data.cv_status) {
							toastr.success(data.message);
						} else {
							toastr.warning(data.message);
						}
					}
				});

			});

			$('[data-toggle="tooltip"]').tooltip();

			all_statistics();

			if (Cookies.get('UserFirstLogin') == null) {

				if (is_first_login == 1) {
					$('#first-login-modal').modal('show');
					Cookies.set('UserFirstLogin', '1', {
						expires: 1
					});
				}
			}

		});

		var dashboardCarousel = new Swiper('.dashboard-carousel', {
			loop: false,
			slidesPerView: 1,
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
			autoplay: {
				delay: 5000,
			},
		});

		function all_statistics() {
			$.ajax({
				url: base_url + 'user/ajax/all_statistics',
				type: 'GET',
				dataType: 'JSON',
				success: function(data) {
					$('#dashboard-watch-count').text(data.watch_count);
					$('#dashboard-conversation-count').text(data.conversation_count);
					$('#dashboard-views-count').text(data.views_count);
				}
			});
		}

		function watch_companies() {
			$('#watch-companies').html('');

			$.ajax({
				url: base_url + 'user/ajax/watch_companies',
				type: 'GET',
				dataType: 'JSON',
				success: function(data) {
					if (data.companies) {
						$.each(data.companies, function(key, value) {
							$('#watch-companies').append(
								'<div class="row">' +
								'<div class="col-md-12">' +
								'<span class="hr">HR</span> <strong>' + value.company_name + '</strong>' +
								'</div>' +
								'</div>'
							);
						});
					} else {
						$('#watch-companies').append('<div class="text-center">Kayıt bulunamadı</div>');
					}
				}
			});
		}

		function views_companies() {
			$('#views-companies-all, #views-companies-last30days, #views-companies-last7days').html('');

			$.ajax({
				url: base_url + 'user/ajax/views_companies',
				type: 'GET',
				dataType: 'JSON',
				success: function(data) {
					if (data.companies_all) {
						$.each(data.companies_all, function(key, value) {
							$('#views-companies-all').append(
								'<div class="row">' +
								'<div class="col-8">' +
								'<span class="hr">HR</span> <strong>' + value.company_name + '</strong>' +
								'</div>' +
								'<div class="col-4 text-right">' +
								date_convert(value.created_at) + ' önce' +
								'</div>' +
								'</div>'
							);
						});
					} else {
						$('#views-companies-all').append('Kayıt bulunamadı');
					}

					if (data.companies_last30days) {
						$.each(data.companies_last30days, function(key, value) {
							$('#views-companies-last30days').append(
								'<div class="row">' +
								'<div class="col-8">' +
								'<span class="hr">HR</span> <strong>' + value.company_name + '</strong>' +
								'</div>' +
								'<div class="col-4 text-right">' +
								date_convert(value.created_at) + ' önce' +
								'</div>' +
								'</div>'
							);
						});
					} else {
						$('#views-companies-last30days').append('Kayıt bulunamadı');
					}

					if (data.companies_last7days) {
						$.each(data.companies_last7days, function(key, value) {
							$('#views-companies-last7days').append(
								'<div class="row">' +
								'<div class="col-8">' +
								'<span class="hr">HR</span> <strong>' + value.company_name + '</strong>' +
								'</div>' +
								'<div class="col-4 text-right">' +
								date_convert(value.created_at) + ' önce' +
								'</div>' +
								'</div>'
							);
						});
					} else {
						$('#views-companies-last7days').append('Kayıt bulunamadı');
					}
				}
			});
		}

		function ad_unfollow(ad_id = '') {
			let alert_icon = '';
			let form_data = {
				ad_id: ad_id
			}

			$.ajax({
				url: base_url + 'user/ad/ajax_unfollow',
				type: 'POST',
				data: form_data,
				dataType: 'JSON',
				success: function(data) {
					if (data.status) {
						alert_icon = '/assets/frontend/img/modal-images/success.svg';
					} else {
						alert_icon = '/assets/frontend/img/modal-images/error.svg';
					}

					modal_open(data.message, alert_icon);

					if (data.referral_link) {
						setTimeout(function() {
							window.location.href = base_url + data.referral_link
						}, 1500);
					} else {
						if (data.status) {
							$('.ad-item-' + ad_id).remove();

							if (!$('.dasboard-bottom .saved .content .list ul li')[0]) {
								$('.dasboard-bottom .saved .content').html('<div class="no-content">Kaydettiğiniz ilanları buradan takip edebilirsiniz.</div>');
							}
						}
					}
				}
			});
		}
	</script>

</body>

</html>