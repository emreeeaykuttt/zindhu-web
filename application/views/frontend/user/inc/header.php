<!--Main Navigation-->
<header>

    <?php if ($this->uri->segment(3) != 'is-ilanlari' && $this->uri->segment(3) != 'is-ilani' && !empty($this->uri->segment(2))): ?>
    <!-- Sidebar navigation -->
    <div id="slide-out" class="side-nav sn-bg-4 fixed">
        <ul class="custom-scrollbar">
            <!-- Logo -->
            <li class="logo-sn waves-effect py-3">
                <div>
                    <a href="<?= base_url('zindhu-hr/kullanici/dashboard') ?>" class="pl-0"><img src="<?= base_url('assets/frontend/img/zindhu-hr-icon.svg') ?>" width="60"></a>
                </div>
            </li>
            <!--/. Logo -->

            <?php if ($this->session->userdata('UserAdminConfirm') == 'approved') { ?>
                <div class="user-box">
                    <div class="icon">
                        <span class="icon-profile-icon"></span>
                    </div>
                    <div>
                        <div class="name"><?= $this->session->userdata('UserFullname') ?></div>
                        <div class="position"><?= $this->session->userdata('UserPosition') ?></div>
                    </div>
                </div>
                <!-- Side navigation links -->
                <li>
                    <ul class="collapsible collapsible-accordion left-menu">
                        
                        <li id="-zindhu-hr-kullanici-dashboard">
                            <a href="<?= base_url('zindhu-hr/kullanici/dashboard') ?>" class="collapsible-header waves-effect arrow-r">
                                <span class="icon-dashboard-icon"></span> Dashboard
                            </a>
                        </li>
                        <li id="-zindhu-hr-kullanici-profil">
                            <a href="<?= base_url('zindhu-hr/kullanici/profil') ?>" class="collapsible-header waves-effect arrow-r">
                                <span class="icon-profile-2-icon"></span> Profil
                            </a>
                        </li>
                        <li id="-zindhu-hr-kullanici-konusmalar">
                            <a href="<?= base_url('zindhu-hr/kullanici/konusmalar') ?>" class="collapsible-header waves-effect arrow-r">
                                <span class="icon-conversations-icon"></span> 
                                Konuşmalar
                                <span class="badge badge-danger ml-auto" id="unread-count"></span>
                            </a>
                        </li>
                        <li id="-zindhu-hr-kullanici-is-ilanlari">
                            <a href="<?= base_url('zindhu-hr/kullanici/is-ilanlari') ?>" class="collapsible-header waves-effect arrow-r text-white">
                                <span class="icon-job-list-icon"></span> İş İlanları
                            </a>
                        </li>
                    </ul>
                </li>
                <!--/. Side navigation links -->

            <?php } ?>

        </ul>
        <div class="left-menu-logout">
            <a href="<?= base_url('zindhu-hr/kullanici/cikis') ?>">
                <span class="icon-logout-icon"></span>
                Çıkış Yap
            </a>
            <div class="copyright">© 2021 Zindhu HR.</div>
        </div>
        <div class="sidenav-bg mask-strong"></div>
    </div>
    <!--/. Sidebar navigation -->
    <?php endif ?>

    <!-- Navbar -->

    <div class="header 
        <?= $this->uri->segment(3) == 'is-ilanlari' ? 'header-white' : '' ?>
        <?= $this->uri->segment(2) ? 'sub-header' : '' ?>">

        <?php if ($this->uri->segment(3) == 'is-ilanlari' || $this->uri->segment(3) == 'is-ilani' || empty($this->uri->segment(2))): ?>
        <a class="logo" href="<?= base_url('zindhu-hr') ?>" data-ani></a>
        <?php endif ?>

        <?php if ($this->uri->segment(3) != 'is-ilanlari' && $this->uri->segment(3) != 'is-ilani' && !empty($this->uri->segment(2))): ?>
        <div class="float-left">
            <a href="#" data-activates="slide-out" class="button-collapse black-text"><i class="icon-hamburger-menu-icon"></i></a>
        </div>
        <?php endif ?>

        <div class="menu">
            <?php if ($this->session->userdata('UserAdminConfirm') == 'approved') { ?>

                <div class="user-menu d-flex">
                    <a class="btn contact-btn" onclick="zindhu_start_contact()"><i class="icon-support-icon"></i> Bize Ulaşın</a>
                    <div class="dropdown user-dropdown">
                        <button class="btn profile-btn" type="button" id="dropdownMenuButton" data-toggle="dropdown"  aria-expanded="true">
                            <i class="icon-profile-icon"></i>
                            <span class="name"><?= $this->session->userdata('UserFirstname') ?></span>
                            <i class="fa fa-caret-down"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="<?= base_url('zindhu-hr/kullanici/dashboard') ?>">
                                <i class="icon-dashboard-icon"></i>Dashboard
                            </a>
                            <a class="dropdown-item" href="<?= base_url('zindhu-hr/kullanici/profil') ?>">
                                <i class="icon-profile-2-icon"></i>Profil
                            </a>
                            <a class="dropdown-item" href="<?= base_url('zindhu-hr/kullanici/konusmalar') ?>">
                                <i class="icon-conversations-icon"></i>Konuşmalar 
                                <span class="badge badge-danger ml-auto" id="header-unread-count">0</span>
                            </a>
                            <a class="dropdown-item" href="<?= base_url('zindhu-hr/kullanici/is-ilanlari') ?>">
                                <i class="icon-job-list-icon"></i>İş İlanları
                            </a>
                            <a class="dropdown-item logout" href="<?= base_url('zindhu-hr/kullanici/cikis') ?>">
                                <i class="icon-logout-icon"></i>Çıkış Yap
                            </a>
                        </div>
                    </div>
                    <div class="dropdown">
                        <button class="btn lang-btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-world-icon"></i>
                            <?php if ($this->session->userdata('lang_code') == 'tr') { ?>
                                Türkçe
                            <?php } else { ?>
                                English
                            <?php } ?>
                            <i class="fa fa-caret-down"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="<?= base_url('language/change/tr?page=zindhu-hr/kullanici') ?>">Türkçe</a>
                            <a class="dropdown-item" href="<?= base_url('language/change/en?page=zindhu-hr/kullanici') ?>">English</a>
                        </div>
                    </div>
                </div>

            <?php } else { ?>
                <div class="language">

                    <div class="custom-dropdown">
                        <ul>
                            <li>
                                <a href="#">
                                    <i class="la la-globe"></i>
                                    <?php if ($this->session->userdata('lang_code') == 'tr') { ?>
                                        Türkçe
                                    <?php } else { ?>
                                        English
                                    <?php } ?>
                                    <i class="fa fa-caret-down"></i>
                                </a>
                                <ul class="dropdown">
                                    <li><a href="<?= base_url('language/change/tr?page=zindhu-hr') ?>">Türkçe</a></li>
                                    <li><a href="<?= base_url('language/change/en?page=zindhu-hr') ?>">English</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="login">
                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#loginModal"><?= l('giris_yap') ?></a>
                </div>

            <?php } ?>

        </div>

    </div>
    <!-- /.Navbar -->

</header>

<!--Main Navigation-->
