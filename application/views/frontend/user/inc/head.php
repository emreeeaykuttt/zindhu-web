<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="google-site-verification" content="thk6a3gwobpRoIbHmnCPaA9iPWXVPx6ndUpgiYzRoQQ" />
<meta name="description" content="Zindhu HR, ülkemizdeki oyun sektörü odaklı ilk ve tek insan kaynakları platformudur. Zindhu HR portalı süre gelen işe alım imkanı sunan diğer insan kaynakları platformlarına kıyasla Türkiye'deki oyun sektörünü tek bir çatı altında toplamayı hedefleyen, işe alımın ötesinde, eğitim ve iletişim imkanlarının ücretsiz bulunabildiği yegane platformdur.">
<meta name="keywords" content="Oyun, mobil oyun, oyun stüdyosu, oyun yayıncısı, gaming, mobile gaming, indie gaming, insan kaynakları, ik, hr, human resources, platform, kariyer, eğitim">
<meta name="description" content="Oyun sektörünü oluşturan oyun geliştiricilerden,  yayıncılara, oyun monetizasyonu  yöntemlerinden insan kaynaklarına kadar oyun sektöründe 360 çözümler sunan tüm hizmetler Zindhu çatısı altında!" />
<meta property="og:title" content="Zindhu: Oyun sektörünün kalbi" />
<meta property="og:description" content="Oyun sektörünü oluşturan oyun geliştiricilerden,  yayıncılara, oyun monetizasyonu  yöntemlerinden insan kaynaklarına kadar oyun sektöründe 360 çözümler sunan tüm hizmetler Zindhu çatısı altında!" />
<meta property="og:image" content="<?=base_url()?>assets/home/og.jpg">
<meta property="og:url" content="<?=base_url()?>">
<meta property="og:type" content="website">
<meta name="twitter:card" content="summary" />
<meta name="theme-color" content="#3d00bf" />
<link rel="icon" href="<?=base_url()?>assets/admin/img/favicon.png" type="image/x-icon">

<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300;400;700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="<?=base_url()?>assets/plugins/line-awesome/css/line-awesome.min.css" />
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/bootstrap.min.css">
<!-- Material Design Bootstrap -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/mdb.css">
<link rel="stylesheet" href="<?=base_url()?>assets/plugins/swiper/swiper-bundle.min.css" />
<link rel="stylesheet" href="<?=base_url()?>assets/frontend/fonts/fonts.css" />
<link rel="stylesheet" href="<?=base_url()?>assets/frontend/fonts/icomoon/style.css" />
<link rel="stylesheet" href="<?=base_url()?>assets/frontend/css/style.css?v=2.0" />

<!-- Main Style -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/style.css?v=2.1">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-175472421-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-175472421-1');
</script>