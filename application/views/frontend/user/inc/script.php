<div class="modal information" id="general-modal" tabindex="-1" role="dialog" aria-labelledby="general-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="logo"></div>
                <p class="text-center" id="modal-content">
                    <!-- Content -->
                </p>
                <p>
                    <img src="" id="modal-image" />
                </p>
                <p class="text-center">
                    <button class="btn btn-dark" id="modal-btn-text" data-dismiss="modal">Tamam</button>
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Contact modal -->
<div class="modal fade contact" id="contact-us-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header border-0" style="background:none">
                <h3> <i class="icon-support-icon"></i> Zindhu HR Destek</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
                <div class="card">
                    <div class="card-body">

                        <div class="mb-3 conversation-area" id="contact-list">
                            <!-- contact list -->
                        </div>

                        <div>
                            <div class="m-0">
                                <div class="send-message-area">
                                    <div>
                                        <div class="custom-input m-0">
                                            <textarea type="text" placeholder="Bir mesaj yazın..." name="z-message" id="z-message" class="md-textarea form-control" rows="1"></textarea>
                                        </div>
                                    </div>
                                    <div>
                                        <a href="javascript:void(0)" onclick="zindhu_send_message()" id="z-message-btn" class="btn btn-send ml-2">
                                            <i class="las la-paper-plane"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
					    </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- Contact modal -->

<script type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/popper.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/swiper/swiper-bundle.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/pro/js/mdb.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/modules/sidenav.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script> -->
<!-- <script type="text/javascript" src="<?=base_url()?>assets/admin/js/mdb.js"></script> -->
<script type="text/javascript"src="<?=base_url()?>assets/plugins/gsap/gsap.min.js"></script>
<script type="text/javascript"src="<?=base_url()?>assets/plugins/gsap/ScrollTrigger.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/sweetalert2.all.min.js"></script>
<script type="text/javascript">
    var base_url = '<?=base_url()?>'; 
    var upload_folder = '<?=UPLOAD_FOLDER?>'; 
    var user_fullname = '<?=$this->session->userdata('UserFullname')?>';
</script>
<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/common.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/script.js?v=2.2"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/user.js?v=2.1"></script>

<script>

    var zindhu_control_conversation;

    toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "md-toast-bottom-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
    //   "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }

	// SideNav Initialization
	$('.button-collapse').sideNav();

	// var container = document.querySelector('.custom-scrollbar');
	// var ps = new PerfectScrollbar(container, {
	// 	wheelSpeed: 2,
	// 	wheelPropagation: true,
	// 	minScrollbarLength: 20
	// });

    // menu active class 
    var pageActiveUrl = window.location.pathname;
    pageActiveUrl = pageActiveUrl.replace(/\//g,'-');

    $('#' + pageActiveUrl).addClass('current');

    // zindhu send message
    function zindhu_send_message()
    {
        $('#z-message-btn').html(small_loading_render());
        $('#z-message-btn').attr('disabled',true);

        let form_data = {
            message: $('[name="z-message"]').val(),
        }

        $.ajax({
            url : base_url + 'user/contact/send_message',
            type: 'POST',
            data: form_data,
            dataType: 'JSON',
            success: function(data)
            {
                if(data.status) {
                    toastr.success(data.message);

                    $('#info-card').remove();

                    $('#contact-list').append(
                        '<div class="card mb-3 blue-conversation">' +
                            '<div class="card-body">' +
                                '<p class="card-text white-text">'+ data.record.content + '<br />' +
                                '<small class="text-muted white-text">'+ time_render(data.record.created_at) +'</small></p>' +
                            '</div>' +
                        '</div>'
                    );

                    $('#contact-list').animate({scrollTop : $('#contact-list').get(0).scrollHeight }, 1000);
                } else {
                    toastr.error(data.message);
                }

                $('[name="z-message"]').val('').trigger('change');
                $('#z-message-btn').html('<i class="las la-paper-plane"></i>');
                $('#z-message-btn').attr('disabled', false);
            }
        });
    }

    // zindhu start contact
    function zindhu_start_contact()
    {
        $('#contact-list').html('');
        $('#contact-list').html(loading_render());

        $('#contact-us-modal').modal('show');
        
        $.ajax({
            url : base_url + 'user/contact/start_contact',
            type: 'GET',
            dataType: 'JSON',
            success: function(data)
            {
                $('#contact-list').html('');

                if (!data.messages) {
                    $('#contact-list').append(
                        '<div class="card mb-3" id="info-card">' +
                            '<div class="card-body">' +
                                '<p class="card-text"><i class="fas fa-info-circle"></i> Görüşmeye başlamak için aşağıdaki "Mesaj At" alanından mesaj yollayabilirsiniz.</p>' +
                            '</div>' +
                        '</div>'
                    );
                }

                let card_direction;
                let card_bg_color;
                let card_text_color;
                $.each(data.messages, function(key, val){
                    card_direction = 'text-right';
                    card_bg_color = 'gray-conversation';
                    card_text_color = ''
                    if (val.sender_type == 'user') {
                        card_direction = '';
                        card_bg_color = 'blue-conversation';
                        card_text_color = 'white-text';
                    }

                    $('#contact-list').append(
                        '<div class="card mb-3 '+ card_direction + ' ' + card_bg_color +'">' +
                            '<div class="card-body">' +
                                '<p class="card-text '+ card_text_color +'">'+ val.content + '<br />' +
                                '<small class="text-muted '+ card_text_color +'">'+ time_render(val.created_at) +'</small></p>' +
                            '</div>' +
                        '</div>'
                    );
                });

                unread_contact();

                setTimeout(function(){
                    $('#contact-list').animate({scrollTop : $('#contact-list').get(0).scrollHeight }, 50);
                }, 100);

                // control_conversation (start)
                zindhu_control_conversation = setInterval(function(){
            
                    $.ajax({
                        url : base_url + 'user/contact/control_conversation',
                        type: 'GET',
                        dataType: 'JSON',
                        success: function(data)
                        {
                            if (data.messages) {
                                $.each(data.messages, function(key, value){
                                    $('#contact-list').append(
                                        '<div class="card mb-3 text-right gray-conversation">' +
                                            '<div class="card-body">' +
                                                '<p class="card-text">'+ value.content + '<br />' +
                                                '<small class="text-muted">'+ time_render(value.created_at) +'</small></p>' +
                                            '</div>' +
                                        '</div>'
                                    );
                                });
                                $('#contact-list').animate({scrollTop : $('#contact-list').get(0).scrollHeight }, 50);
                            }
                        }
                    });

                }, 1000);
                // control_conversation (end)
            }
        });
    }

    $(document).ready(function() {

        if ($(document).scrollTop() > 0) {
            $(".header").addClass("scrolled");
        }

        $('#contact-us-modal').on('hidden.bs.modal', function(e) {
            clearInterval(zindhu_control_conversation);
        });

    });

    $(window).scroll(function() {
        var scroll = $(document).scrollTop();

        if (scroll == 0) {
            $(".header").removeClass("scrolled");
        } else {
            $(".header").addClass("scrolled");
        }
    });
</script>