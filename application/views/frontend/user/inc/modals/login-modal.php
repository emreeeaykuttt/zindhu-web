<div class="modal show login" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content ">
            <div class="modal-header border-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="login-wrapper">
                    <div class="logo">
                        <img src="<?=base_url()?>assets/frontend/img/zindhu-hr-logo.svg" />
                    </div>
                    <h3><?=l('giris_yap')?></h3>
                    <p><?=l('zindhu_hr_100e_yakin_studyo_ve_is_ilani_seni_bekliyor')?></p>

                    <a href="<?=base_url('zindhu-hr/kullanici/giris')?>" class="btn btn-login">
                        <div><?=l('bireysel')?> <span class="weak"><?=l('giris')?></span></div> <i class="la la-angle-right"></i>
                    </a>
                    <a href="<?=base_url('zindhu-hr/studyo/giris')?>" class="btn btn-login">
                        <div><?=l('kurumsal')?> <span class="weak"><?=l('giris')?></span></div> <i class="la la-angle-right"></i>
                    </a>
                    <div class="signup-text">
                        <span class="weak"><?=l('hesabiniz_yok_mu')?></span> <a href="<?=base_url('zindhu-hr/kullanici/kayit-ol')?>"><u><?=l('kayit_olun')?>.</u></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>