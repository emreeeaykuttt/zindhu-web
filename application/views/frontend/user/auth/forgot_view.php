<!DOCTYPE html>
<html lang="en">

<head>

    <title>Bireysel Şifremi Unuttum - Zindhu HR</title>

    <?php include(dirname(__DIR__) . '/../include/head.php'); ?>

</head>

<body>

	<?php
		$forgot_status = $this->session->flashdata('forgot_status');
		$flashdata = null;
		if (isset($forgot_status)) {
			$flashdata = $forgot_status;
		}

        if (isset($flashdata)) {
            if ($flashdata['status']) {
                $flashdata['img'] = base_url('assets/frontend/img/modal-images/success.svg');
            } else {
                $flashdata['img'] = base_url('assets/frontend/img/modal-images/error.svg');
            }
        }
	?>

	<div class="header login">
        <a class="logo" href="<?=base_url('zindhu-hr')?>" data-ani></a>
    </div>

    <div class="sub-page sign-up forgot">
        <div class="form">

            <h2>Şifremi Unuttum</h2>

            <p>Şifrenizi sıfırlamak için sisteme kayıtlı e-posta adresinizi girin.</p>

            <form action="<?= base_url('zindhu-hr/kullanici/sifrenizi-mi-unuttunuz') ?>" method="POST" id="forgot-form" autocomplete="off">

            	<div>
					<input 
						type="text" 
						name="email" 
						id="email" 
						placeholder="E-Posta"
						value="<?=set_value('email')?>" 
						class="form-control rounded <?=form_error('email') ? 'invalid' : ''?>">
            		<?=form_error('email', '<div class="invalid">', '</div>')?>
				</div>

                <button class="btn btn-dark btn-block text-center mt-2">Şifremi Sıfırla</button>
            </form>

        </div>

        <?php include('right.php'); ?>
        
    </div>
    <?php include(dirname(__DIR__) . '/../include/footer.php'); ?>

<div class="scroll-top" id="scrollTop">
</div>
    <?php include(dirname(__DIR__) . '/../include/script.php'); ?>

    <script type="text/javascript">
        
        <?php if ($flashdata) : ?>
        
        modal_open('<?= $flashdata['message'] ?>', '<?=$flashdata['img']?>');

        <?php endif ?>

    </script>

</body>

</html>