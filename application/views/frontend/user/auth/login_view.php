<!DOCTYPE html>
<html lang="en">

<head>

	<title>Bireysel Giriş Yap - Zindhu HR</title>
	<meta name="description" content="Oyun sektörünü oluşturan oyun geliştiricilerden,  yayıncılara, oyun monetizasyonu  yöntemlerinden insan kaynaklarına kadar oyun sektöründe 360 çözümler sunan tüm hizmetler Zindhu çatısı altında!" />
    <meta property="og:title" content="Zindhu: Oyun sektörünün kalbi" />
    <meta property="og:description" content="Oyun sektörünü oluşturan oyun geliştiricilerden,  yayıncılara, oyun monetizasyonu  yöntemlerinden insan kaynaklarına kadar oyun sektöründe 360 çözümler sunan tüm hizmetler Zindhu çatısı altında!" />
    <meta property="og:image" content="<?=base_url()?>assets/home/og.jpg">
    <meta property="og:url" content="<?=base_url()?>">
    <meta property="og:type" content="website">
    <meta name="twitter:card" content="summary" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="theme-color" content="#3d00bf" />

	<?php include(dirname(__DIR__) . '/../include/head.php'); ?>

</head>

<body>

	<?php
	$create_status = $this->session->flashdata('create_status');
	$reset_status = $this->session->flashdata('reset_status');
	$forgot_status = $this->session->flashdata('forgot_status');
	$account_enabled = $this->session->flashdata('account_enabled');
	$login_status = $this->session->flashdata('login_status');
	$flashdata = null;
	if (isset($create_status)) {
		$flashdata = $create_status;
	} elseif (isset($reset_status)) {
		$flashdata = $reset_status;
	} elseif (isset($forgot_status)) {
		$flashdata = $forgot_status;
	} elseif (isset($account_enabled)) {
		$flashdata = $account_enabled;
	} elseif (isset($login_status)) {
		$flashdata = $login_status;
	}

	if (isset($flashdata)) {
		if ($flashdata['status']) {
			$flashdata['img'] = base_url('assets/frontend/img/modal-images/success.svg');
		} else {
			$flashdata['img'] = base_url('assets/frontend/img/modal-images/error.svg');
		}
	}
	?>

	<div class="header login">
		<a class="logo" href="<?= base_url('zindhu-hr') ?>" data-ani></a>
	</div>

	<div class="sub-page sign-up personal">
		<div class="form">
			<div class="custom-dropdown">
				<ul>
					<li>
						<a href="#">
							Bireysel
							<i class="la la-caret-down"></i>
						</a>
						<ul class="dropdown">
							<li><a href="<?= base_url('zindhu-hr/studyo/giris') ?>">Studyo</a></li>
						</ul>
					</li>
				</ul>
			</div>

			<h2>Giriş Yap</h2>

			<?php
			$referral_link = $this->input->get('referral-link');
			if ($referral_link) {
				$referral_link = '?referral-link=' . $referral_link;
			}
			?>

			<form action="<?= base_url('zindhu-hr/kullanici/giris' . $referral_link) ?>" method="POST" id="login-form" autocomplete="off">

				<div>
					<input type="text" name="email" id="email" placeholder="E-Posta" value="<?= set_value('email') ? set_value('email') : $this->session->tempdata('RememberUserEmail') ?>" class="form-control rounded <?= form_error('email') ? 'invalid' : '' ?>">
					<?= form_error('email', '<div class="invalid">', '</div>') ?>
				</div>

				<div>
					<input type="password" name="password" id="password" placeholder="Şifre" value="<?= set_value('password') ? '' : $this->session->tempdata('RememberUserPassword') ?>" class="form-control rounded <?= form_error('password') ? 'invalid' : '' ?>">
					<?= form_error('password', '<div class="invalid">', '</div>') ?>
				</div>

				<div class="d-flex justify-content-between align-items-center">
					<div class="form-check my-3">
						<input type="checkbox" class="form-check-input" name="remember" id="remember" <?= $this->session->tempdata('RememberUserEmail') ? 'checked="checked"' : '' ?>>
						<label class="form-check-label" for="remember">
							Beni hatırla
						</label>
					</div>
					<div class="forgot-pass"><a href="<?= base_url() ?>zindhu-hr/kullanici/sifrenizi-mi-unuttunuz">Şifremi unuttum</a></div>
				</div>

				<button class="btn btn-dark btn-block text-center mt-2" type="submit">Giriş Yap</button>
				<div class="already">Hesabınız yok mu? <a href="<?= base_url() ?>zindhu-hr/kullanici/kayit-ol">Kayıt olun</a></div>

			</form>

		</div>

		<?php include('right.php'); ?>

	</div>


	<?php include(dirname(__DIR__) . '/../include/footer.php'); ?>

	<div class="scroll-top" id="scrollTop">
	</div>
    <?php include(dirname(__DIR__) . '/../include/script.php'); ?>

	<script type="text/javascript">
		<?php if ($flashdata) : ?>

			modal_open('<?= $flashdata['message'] ?>', '<?= $flashdata['img'] ?>');

		<?php endif ?>
	</script>

</body>

</html>