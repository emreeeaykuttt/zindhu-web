<!DOCTYPE html>
<html lang="en">

<head>

	<title>Bireysel Üye Ol - Zindhu HR</title>

	<?php include(dirname(__DIR__) . '/../include/head.php'); ?>

</head>

<body class="white d-flex flex-column align-items-stretch hr-bg-register prelogin">

	<div class="header login">
		<a class="logo" href="<?= base_url('zindhu-hr') ?>" data-ani></a>
	</div>

	<div class="sub-page sign-up personal">
		<div class="form">
			<h2>Kayıt Ol</h2>

			<form action="<?= base_url('zindhu-hr/kullanici/kayit-ol') ?>" method="POST" id="user-register-form" autocomplete="off">

				<div>
					<input type="text" name="firstname" id="firstname" placeholder="İsim" value="<?= set_value('firstname') ?>" class="form-control rounded <?= form_error('firstname') ? 'invalid' : '' ?>">
					<?= form_error('firstname', '<div class="invalid">', '</div>') ?>
				</div>

				<div>
					<input type="text" name="lastname" id="lastname" placeholder="Soyisim" value="<?= set_value('lastname') ?>" class="form-control rounded <?= form_error('lastname') ? 'invalid' : '' ?>">
					<?= form_error('lastname', '<div class="invalid">', '</div>') ?>
				</div>

				<div>
					<input type="text" name="email" id="email" placeholder="Email" value="<?= set_value('email') ? set_value('email') : ($this->input->get('email') ? $this->input->get('email') : '' ) ?>" class="form-control rounded <?= form_error('email') ? 'invalid' : '' ?>">
					<?= form_error('email', '<div class="invalid">', '</div>') ?>
				</div>

				<div>
					<input type="password" name="password" id="password" placeholder="Şifre" value="<?= set_value('password') ?>" class="form-control rounded <?= form_error('password') ? 'invalid' : '' ?>">
					<?= form_error('password', '<div class="invalid">', '</div>') ?>
				</div>

				<div>
					<input type="password" name="password_again" id="password-again" placeholder="Şifre Tekrar" value="<?= set_value('password_again') ?>" class="form-control rounded <?= form_error('password_again') ? 'invalid' : '' ?>">
					<?= form_error('password_again', '<div class="invalid">', '</div>') ?>
				</div>

				<div class="form-check my-3">
					<input type="checkbox" class="form-check-input" name="is_accept" id="is-accept" value="1" <?= set_value('is_accept') ? 'checked="checked"' : '' ?>>
					<label class="form-check-label" for="is-accept">
						<a href="javascript:void(0)" data-toggle="modal" data-target="#register-modal">Aydınlatma Metni</a> ’ni okudum, kabul ediyorum.
					</label>
					<?= form_error('is_accept', '<div class="invalid">', '</div>') ?>
				</div>

				<button class="btn btn-dark btn-block text-center mt-2" type="submit">Kayıt Ol</button>

				<div class="already">Hesabınız var mı? <a href="<?= base_url() ?>zindhu-hr/kullanici/giris">Giriş yapın.</a></div>

			</form>

		</div>

		<?php include('right.php'); ?>

	</div>


	<!-- register-modal -->
	<div class="modal information" id="register-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<h4 id="kvkk-title" class="m-0">Çalışan Adayı Aydınlatma Metni</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="kvkk-content">

					<div class="row pl-3 pr-3">

						<p><b><i>Zindhu Oyun Yazılım Danışmanlık Hizmetleri A.Ş </i></b>(&ldquo;<b>Şirket</b>&rdquo;) olarak, işbu Aydınlatma Metni ile, Kişisel Verilerin Korunması Kanunu (&ldquo;<b>Kanun</b>&rdquo;) kapsamında aydınlatma y&uuml;k&uuml;ml&uuml;ğ&uuml;m&uuml;z&uuml;n yerine getirilmesi ama&ccedil;lanmaktadır. Bu kapsamda sizlere bilgi vermekle y&uuml;k&uuml;ml&uuml; olduğumuz konular aşağıdaki şekildedir:&nbsp;</p>

						<p><b>1. Veri sorumlusunun ve varsa temsilcisinin kimliği&nbsp;</b></p>

						<p>Veri sorumlusu; <b><i>&Ccedil;eliktepe Mah. İsmet İn&ouml;n&uuml; Cad. Yıldırımlar İş Merkezi No:11 İ&ccedil; Kapı No: 501 Kağıthane/Istanbul</i></b><b> </b>adresinde mukim İstanbul Ticaret Sicili M&uuml;d&uuml;rl&uuml;ğ&uuml;&rsquo;ne <b><i>262674-5</i></b> sicil numarası ile kayıtlı <b><i>Zindhu Oyun Yazılım Danışmanlık Hizmetleri A.Ş</i></b><b>&rsquo;</b>dir.&nbsp;</p>

						<p><b>2. Kişisel verilerin hangi ama&ccedil;la işleneceği&nbsp;</b></p>

						<p>Şirket&rsquo;imize sağladığınız ad-soyad ve e-posta adresi <b>t&uuml;r ve kategorilerindeki </b>kişisel verileriniz; Zindhu insan kaynakları platformuna (&ldquo;<b>Platform</b>&rdquo;) kaydınızın tam ve eksiksiz olarak&nbsp; tamamlanması ve kayıt s&uuml;recinin y&ouml;netilmesi, onay vermeniz halinde tarafınıza ticari elektronik ileti g&ouml;nderilmesi, tarafınıza sağlanan istihdam edilme danışmanlık ve aracılık faaliyetlerinin y&uuml;r&uuml;t&uuml;lmesi i&ccedil;in kişi ve şirketler ile iş birliği kurulması ve mevzuattan kaynaklanan zamanaşımı s&uuml;resi doğrultusunda saklanması <b>amacı ile </b>işlenmektedir.&nbsp; &nbsp;</p>

						<p><b>3. Şirket tarafından işlenen kişisel verilerin kimlere ve hangi ama&ccedil;la aktarılabileceği&nbsp;</b></p>

						<p>Şirket&rsquo;imize sağladığınız kişisel verileriniz, insan kaynakları danışmanlık hizmetleri sunulması, istihdam edilmek isteyebileceğiniz oyun st&uuml;dyoları ile bir araya getirilmeniz, uygun iş pozisyonlarının tarafınıza sağlanması faaliyetleri kapsamında ve bu faaliyetler ile doğrudan ilgili olarak iş birliği i&ccedil;inde olduğumuz Rollic Games Oyun Yazılım ve Pazarlama A.Ş.&rsquo;ye aktarılabilecek ve resmi makamların d&uuml;zenleyici veya icrai işlemleri veya yargı kararlarının gereklerini yerine getirmek amacıyla ve taleple sınırlı olarak resmi makamlar ile paylaşabilecektir. İş birliği yaptığımız &uuml;&ccedil;&uuml;nc&uuml; kişilerin verilerinizi yurt dışında tutması halinde verileriniz yurt dışına aktarılabilecektir.</p>

						<p><b>4. Kişisel veri toplamanın y&ouml;ntemi ve hukuki sebebi&nbsp;</b></p>

						<p>Kişisel verileriniz, Platform &uuml;zerinden, elektronik ortam aracılığıyla elde edilmekte ve a&ccedil;ık rızanız hukuki sebepleri ile toplanmaktadır.&nbsp;</p>

						<p><b>5. Kişisel verileriniz ile ilgili Kanun kapsamındaki haklarınız aşağıdaki şekildedir:&nbsp;</b></p>

						<p>(a) Kişisel verilerinizin işlenip işlenmediğini &ouml;ğrenme, (b) Kişisel verileriniz işlenmişse buna ilişkin bilgi talep etme, (c) Kişisel verilerinizin işlenme amacını ve bunların amacına uygun kullanılıp kullanılmadığını &ouml;ğrenme, (&ccedil;) Yurt i&ccedil;inde veya yurt dışında kişisel verilerin aktarıldığı &uuml;&ccedil;&uuml;nc&uuml; kişileri bilme, (d) Kişisel verilerinizin eksik veya yanlış işlenmiş olması h&acirc;linde bunların d&uuml;zeltilmesini isteme, (e) Kişisel verilerinizin işlenmesini gerektiren sebeplerin ortadan kalkması halinde kişisel verilerinizin silinmesini veya yok edilmesini isteme, (f), (d) ve (e) bentleri uyarınca yapılan işlemlerin, kişisel verilerin aktarıldığı &uuml;&ccedil;&uuml;nc&uuml; kişilere bildirilmesini isteme, (g) İşlenen verilerin m&uuml;nhasıran otomatik sistemler vasıtasıyla analiz edilmesi suretiyle kişinin kendisi aleyhine bir sonucun ortaya &ccedil;ıkmasına itiraz etme, (ğ) Kişisel verilerin kanuna aykırı olarak işlenmesi sebebiyle zarara uğraması h&acirc;linde zararın giderilmesini talep etme.&nbsp;</p>

						<p>Bu haklarınızı yazılı olarak veya g&uuml;venli elektronik imza, mobil imza ya da Şirket&rsquo;e daha &ouml;nce bildirilen ve Şirket&rsquo;in sisteminde kayıtlı bulunan elektronik posta adresinizi kullanmak suretiyle (Bu kapsamda <b><i><a href="mailto:info@zindhugames.com" target="_blank">info@zindhugames.com</a></i></b> e-posta adresi &uuml;zerinden Şirket&rsquo;e ulaşabilirsiniz) veya başvuru amacına y&ouml;nelik geliştirilmiş bir yazılım ya da uygulama vasıtasıyla Şirket&rsquo;e iletebilirsiniz.&nbsp;</p>

						<p>Bilginize sunarız.&nbsp;</p>

						<p><b><i>Zindhu Oyun Yazılım Danışmanlık Hizmetleri A.Ş</i></b></p>

					</div>

				</div>
			</div>
			<!-- Content -->
		</div>
	</div>
	<!-- register-modal -->
	<?php include(dirname(__DIR__) . '/../include/footer.php'); ?>

	<div class="scroll-top" id="scrollTop">
	</div>
	<?php include(dirname(__DIR__) . '/../include/script.php'); ?>


</body>

</html>