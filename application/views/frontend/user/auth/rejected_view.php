<!DOCTYPE html>
<html lang="en">

<head>

    <title>Hesabınız Yönetici Tarafından Onay Beklemektedir</title>

    <?php include(dirname(__DIR__) . '/../include/head.php'); ?>

</head>

<body>

	<div class="header login">
        <a class="logo" href="<?=base_url('zindhu-hr')?>" data-ani></a>
    </div>

    <div class="sub-page sign-up forgot">
        <div class="form">
            
            <!-- Card -->
            <div class="card card-cascade info">

              <!-- Card image -->
              <div class="view view-cascade gradient-card-header">
                <h3 class="card-header-title mb-2">
                	Merhaba <?=$this->session->userdata('UserFullname')?>, <br />
                	Hesabınız Yönetici Tarafından Reddedilmiştir
                </h3>
                <div class="mt-3">
                	<a class="text-white btn btn-dark" href="<?=base_url('user/auth/logout')?>">Çıkış Yap</a>
                </div>
              </div>
              <!-- Card image -->

            </div>
            <!-- Card -->

        </div>

        <?php include('right.php'); ?>

    </div>

    <?php include(dirname(__DIR__) . '/../include/script.php'); ?>

</body>

</html>
