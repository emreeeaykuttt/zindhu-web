<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="Konuşmalar">
	<meta name="keywords" content="Konuşmalar">

	<title>Konuşmalar</title>

	<?php include('inc/head.php'); ?>
	<link rel="stylesheet" href="<?= base_url() ?>assets/admin/css/addons/datatables.min.css">

</head>

<body class="fixed-sn white-skin">

	<?php include('inc/header.php'); ?>

	<!-- Main layout -->
	<main>
		<div class="container-fluid position-relative">

			<div class="d-flex justify-content-center page-loading">
                <div class="spinner-border" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>

			<section class="list-view d-none">

				<h5 class="mb-3 dark-grey-text font-weight-bold" id="module-title">Konuşmalar</h5>
				<div class="conversation-search">
					<i class="la la-search"></i>
					<input class="form-control" type="search" id="conversationSearch" placeholder="Konuşmalarda ara">
					<div class="dropdown">

						<button class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="true">
							<i class="las la-filter"></i>
						</button>
						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="<?= base_url('zindhu-hr/kullanici/konusmalar') ?>">
								Tüm konuşmalar
							</a>
							<a class="dropdown-item" href="<?= base_url('zindhu-hr/kullanici/konusmalar?unread=1') ?>">
								Okunmamış mesajlar
							</a>
						</div>
					</div>
				</div>
				<div class="conversation-wrapper">
					<div class="table-responsive">
						<table id="conversations-table" class="table table-borderless" cellspacing="0" width="100%">
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>

			</section>

		</div>
	</main>
	<!-- Main layout -->

	<?php include('inc/footer.php'); ?>

	<!-- Company View Modal -->
	<div class="modal fade" id="company-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header bg-transparent border-0">
					<p class="heading lead">Görüntüleme</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="company-info">

				</div>

				<!-- Footer -->
				<div class="modal-footer justify-content-center">
					<a href="javascript:void(0)" onclick="" id="start-conversation" class="btn btn-warning">MESAJ AT</a>
				</div>
			</div>
			<!-- Content -->
		</div>
	</div>
	<!-- Company View Modal -->

	<!-- Conversation View Modal -->
	<div class="modal fade" id="conversation-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header bg-transparent border-0">
					<p class="heading lead" id="conversation-company">
						<!-- company name -->
					</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body pt-0" id="conversation-view">

					<div class="mb-1 conversation-info" id="conversation-info">
						<!-- conversation-info -->
					</div>

					<div class="d-flex justify-content-end" style="display: none !important;">
						<div class="refresh-btn text-right">
							<i class="fas fa-sync-alt" id="refresh-btn" onclick="start_conversation(0)" data-toggle="tooltip" title="Yenile"></i>
						</div>
					</div>

					<div class="mb-3 conversation-area" id="conversation-list">
						<!-- conversation list -->
					</div>

					<div>
						<div class=" m-0">
							<div class="send-message-area">
								<div>
									<div class="custom-input m-0">
										<textarea type="text" placeholder="Bir cevap yazın..." name="message" id="message" class="md-textarea form-control" rows="1"></textarea>
										<div class="red-text small error-text" id="message_error"></div>
									</div>
								</div>
								<div>
									<a href="javascript:void(0)" onclick="send_message(0)" id="message-btn" class="btn btn-send ml-2">
										<i class="las la-paper-plane"></i>
									</a>
								</div>
							</div>

						</div>
					</div>

				</div>
			</div>
			<!-- Content -->
		</div>
	</div>
	<!-- Conversation View Modal -->

	<?php include('inc/script.php'); ?>
	<script type="text/javascript" src="<?= base_url() ?>assets/admin/js/addons/datatables.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/admin/js/cv_helper.js"></script>

	<script type="text/javascript">
    	
    	var control_conversation;
		var table;
		var unread_status = getUrlParameter('unread');
		var company_param = getUrlParameter('company');
	    var params = '';
		
		$(document).ready(function(){

			if (company_param) {
				start_conversation(company_param);
			}

			if (unread_status) {
				params = '?unread=1';
				$('#module-title').html('Konuşmalar <small>Okunmamışlar</small>');
			} else {
				$('#module-title').html('Konuşmalar');
			}

		    table = $('#conversations-table').DataTable({

		    	'language': {
					"sEmptyTable": '<div class="table-info-screen"><img src="/assets/frontend/img/no-conversations.svg"><p>Henüz yeni bir konuşmanız bulunmamaktadır.</p></div>',
					"infoEmpty": '<div class="table-info-screen"><img src="/assets/frontend/img/no-conversations.svg"><p>Konuşma bulunmamaktadır.</p></div>',
					"sZeroRecords": '<div class="table-info-screen"><img src="/assets/frontend/img/no-conversations.svg"><p>Eşleşen konuşma bulunamadı.</p></div>',
					"sLoadingRecords": '<div class="table-info-screen"><p>Yükleniyor..</p></div>',
					"sProcessing":     '<div class="table-info-screen"><p>İşleniyor...</p></div>',
		            'url': base_url + 'assets/admin/js/addons/language_tr.json',
		        },
				

		        'processing': true,
		        'serverSide': true,
		        'order': [],
		        'lengthMenu': [[25, 50, 100, -1], [25, 50, 100, 'Tüm']],
		        'ajax': {
		            'url': base_url + 'user/conversations/ajax_list' + params,
		            'type': 'POST'
		        },

		        'dom': '<"w-50 float-left d-none"l><"w-50 float-left d-none"fB><"table-content" tr><"w-50 float-left"i><"w-50 float-left"p>',

		  		'columns': [
		            { 'data': 'transactions', 'orderable': false}
		        ]

		    });

			$('#conversationSearch').on('keyup', function () {
				table.search( this.value ).draw();
			});

		    $('.list-view').removeClass('d-none');
	    	$('.page-loading').removeClass('d-flex');
	    	$('.page-loading').addClass('d-none');

	    	$('[data-toggle="tooltip"]').tooltip();
	    	$('.dataTables_filter input').on('keyup', function() {
		    	setTimeout(function() {
			    	$('[data-toggle="tooltip"]').tooltip();
			    }, 700);
			});

		    $('#conversation-modal').on('hidden.bs.modal', function(e) {
				clearInterval(control_conversation);
	        });

		});

		function send_message(company_id)
		{
			$('#message-btn').html(small_loading_render());
		    $('#message-btn').attr('disabled',true);

		    let form_data = {
		        company_id: company_id,
		        message: $('[name="message"]').val(),
		    }

		    $.ajax({
		        url : base_url + 'user/ajax/send_message',
		        type: 'POST',
		        data: form_data,
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if(data.status) {
		                toastr.success(data.message);

		                $('#info-card').remove();

		                $('#conversation-list').append(
			        		'<div class="card mb-3 text-right blue-conversation">' +
				              	'<div class="card-body">' +
				                	'<p class="card-text white-text">'+ data.record.content + '<br />' +
				                	'<small class="text-muted white-text">'+ time_render(data.record.created_at) +'</small></p>' +
				              	'</div>' +
				            '</div>'
			        	);

	                    $('#conversation-list').animate({scrollTop : $('#conversation-list').get(0).scrollHeight }, 1000);
		            } else {
		                toastr.error(data.message);
		            }

		            $('[name="message"]').val('').trigger('change');
		            $('#message-btn').html('<i class="las la-paper-plane"></i>');
		            $('#message-btn').attr('disabled', false);
		        }
		    });
		}

		function start_conversation(company_id)
		{
			$('#conversation-list, #conversation-info, #conversation-company').html('');
			$('#conversation-list, #conversation-info, #conversation-company').html(loading_render());

			$('#company-modal').modal('hide');
			$('#conversation-modal').modal('show');
			$('#message-btn').attr('onclick', 'send_message('+ company_id +')');
			$('#refresh-btn').attr('onclick', 'start_conversation('+ company_id +')');
			
			$.ajax({
		        url : base_url + 'user/ajax/start_conversation/' + company_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	let hires = data.company_hires;
		        	let company_hires_status = company_hires_render(hires, company_id, data.company.company_name);

		        	$('#conversation-list, #conversation-info, #conversation-company').css('opacity', '0');
		        	$('#conversation-list, #conversation-info, #conversation-company').html('');

		        	$('#conversation-company').html(data.company.company_name);

		        	$('#conversation-info').html(
						company_hires_status
		        	);

		        	if (!data.conversations) {
		        		$('#conversation-list').append(
			        		'<div class="card mb-3" id="info-card">' +
				              	'<div class="card-body">' +
				                	'<p class="card-text"><i class="fas fa-info-circle"></i> Görüşmeye başlamak için aşağıdaki "Mesaj At" alanından mesaj yollayabilirsiniz.</p>' +
				              	'</div>' +
				            '</div>'
			        	);
		        	}

		        	let card_direction;
		        	let card_bg_color;
		        	let card_text_color;
		        	let system_message;
		        	let system_message_float;
		        	$.each(data.conversations, function(key, val){
		        		card_direction = '';
		        		card_bg_color = 'gray-conversation';
		        		card_text_color = 'dark-grey-text'
		        		system_message = '';
		        		system_message_float = 'float-left mr-1';

		        		if (val.sender_type == 'user') {
		        			card_direction = 'text-right';
		        			card_bg_color = 'blue-conversation';
		        			card_text_color = 'white-text';
		        			system_message_float = 'float-right ml-1';
		        		}

		        		if (val.is_system_message == 1) {
		        			if (val.sender_type == 'user') {
		        				system_message = user_system_message();
			        		} else {
			        			system_message = company_system_message();
			        		}
		        		}

		        		$('#conversation-list').append(
			        		'<div class="card mb-3 '+ card_direction + ' ' + card_bg_color +'">' +
				              	'<div class="card-body">' +
				                	'<p class="card-text '+ card_text_color +'">'+ val.content + ' ' + '<span class="'+ system_message_float+ '">' + system_message + '</span><br />' +
				                	'<small class="text-muted '+ card_text_color +'">'+ time_render(val.created_at) +'</small></p>' +
				              	'</div>' +
				            '</div>'
			        	);
		        	});

		        	unread_conversations();

		        	setTimeout(function(){
                        $('#conversation-list').animate({scrollTop : $('#conversation-list').get(0).scrollHeight }, 50);
                        $('#conversation-list, #conversation-info, #conversation-company').css('opacity', '1');
                        reload_table();
                    }, 100);

		        	// control_conversation (start)
                    control_conversation = setInterval(function(){
				
						$.ajax({
					        url : base_url + 'user/ajax/control_conversation/' + company_id,
					        type: 'GET',
					        dataType: 'JSON',
					        success: function(data)
					        {
					        	if (data.messages) {
					        		$.each(data.messages, function(key, value){
					        			$('#conversation-list').append(
							        		'<div class="card mb-3 gray-conversation">' +
								              	'<div class="card-body">' +
								                	'<p class="card-text dark-grey-text">'+ value.content + '<br />' +
								                	'<small class="text-muted dark-grey-text">'+ time_render(value.created_at) +'</small></p>' +
								              	'</div>' +
								            '</div>'
							        	);
					        		});
					        		$('#conversation-list').animate({scrollTop : $('#conversation-list').get(0).scrollHeight }, 50);
					        	}
					       	}
					    });

					}, 1000);
					// control_conversation (end)
		        }
		    });
		}

		function company_accept_invite(company_id)
		{
		    $('#company-accept-invite-btn').attr('disabled',true);

		    let form_data = {
		        company_id: company_id,
		    }

		    $.ajax({
		        url : base_url + 'user/ajax/company_accept_invite',
		        type: 'POST',
		        data: form_data,
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if(data.status) {
		                toastr.success(data.message);
		                start_conversation(company_id)
		            } else {
		                toastr.error(data.message);
		            }

		    		$('#company-accept-invite-btn').attr('disabled',false);
		        }
		    });
		}

		function company_exit(company_id)
		{
			Swal.fire({
		        title: 'İşten ayrılmak istediğinize emin misiniz?',
		        text: '',
		        icon: 'success',
		        showCancelButton: true,
		        confirmButtonColor: '#3085d6',
		        cancelButtonText: 'Vazgeç',
		        cancelButtonColor: '#d33',
		        confirmButtonText: 'Evet'
		    }).then((result) => {
		        if (result.value) 
		        {
					$('#company-exit-btn').attr('disabled',true);

				    let form_data = {
				        company_id: company_id,
				    }

				    $.ajax({
				        url : base_url + 'user/ajax/company_exit',
				        type: 'POST',
				        data: form_data,
				        dataType: 'JSON',
				        success: function(data)
				        {
				            if(data.status) {
				                toastr.success(data.message);
				                start_conversation(company_id)
				            } else {
				                toastr.error(data.message);
				            }

				    		$('#company-exit-btn').attr('disabled',false);
				        }
				    });
				}
    		});
		}

		function company_invite(company_id)
		{
			$('#company-invite-btn').attr('disabled',true);

		    let form_data = {
		        company_id: company_id,
		    }

		    $.ajax({
		        url : base_url + 'user/ajax/company_invite',
		        type: 'POST',
		        data: form_data,
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if(data.status) {
		                toastr.success(data.message);
		                start_conversation(company_id)
		            } else {
		                toastr.error(data.message);
		            }

		    		$('#company-invite-btn').attr('disabled',false);
		        }
		    });
		}

		function view(company_id)
		{
			$('#company-info').html(loading_render());
			$('#company-modal').modal('show');

			$.ajax({
		        url : base_url + 'user/ajax/company_view/' + company_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	let hires = data.company_hires;
		        	let company_hires_status = company_hires_render(hires, company_id, data.company.company_name);

		        	$('#company-info').html('');
		        	$('#start-conversation').attr('onclick', 'start_conversation('+company_id+')');

		        	let partners = '';
		        	if (data.company.partners) {
		        		partners = '<tr><td>Kurucu Ortaklar: </td><td><b>'+ data.company.partners +'</b></td></tr>';
		        	}

		        	let number_employees = '';
		        	if (data.company.number_employees) {
		        		number_employees = '<tr><td>Stüdyo Çalışan Sayısı: </td><td><b>'+ data.company.number_employees +'</b></td></tr>';
		        	}

		        	let game_type = '';
		        	if (data.company.game_type) {
		        		game_type = '<tr><td>Stüdyonun İlgili Olduğu Oyun Türü: </td><td><b>'+ game_single_render(data.company.game_type) +'</b></td></tr>';
		        	}

		        	let company_linkedin = '';
		        	if (data.company.company_linkedin) {
		        		company_linkedin = '<tr><td>Stüdyo Linkedin: </td><td><b>'+ data.company.company_linkedin +'</b></td></tr>';
		        	}

		        	let founder_linkedin = '';
		        	if (data.company.founder_linkedin) {
		        		founder_linkedin = '<tr><td>Stüdyo Kurucu Linkedin: </td><td><b>'+ data.company.founder_linkedin +'</b></td></tr>';
		        	}

		            $('#company-info').append(
		            	'<div>' +
	            			'<table class="table ">' +
	            				'<div class="text-right">' +
	            					company_hires_status +
            					'</div>' +
			            		'<tr><td>Ad Soyad: </td><td><b>'+ data.company.firstname + ' ' + data.company.lastname +'</b></td></tr>' +
			            		'<tr><td>Stüdyo İsmi: </td><td><b>'+ data.company.company_name +'</b></td></tr>' +
			            		partners +
			            		'<tr><td>Stüdyo Lokasyon Şehir-İlçe: </td><td><b>'+ city_render(data.company.city_id) + ' - ' + district_render(data.company.district_id) +'</b></td></tr>' +
			            		number_employees +
			            		game_type +
			            		company_linkedin +
			            		founder_linkedin +
		            		'</table>' +
                        '</div>'
		            );
		        }
		    });
		}

		function company_hires_render(hires, company_id, company_name)
		{
			if (hires) {
        		if (hires.user_confirm == 0 && hires.continues == 0 && !hires.enddate) {
                	company_hires_status = 
                	'<h6><b>' + company_name + '</b> studyosu size çalışma isteği gönderdi. Kabul ediyorsanız aşağıdaki onay butonuna tıklayabilirsiniz.</h6>' +
                	'<div>' +
	        			'<a href="javascript:void(0)" onclick="company_accept_invite('+ company_id +')" class="btn btn-dark btn-md ml-0" id="company-accept-invite-btn">EVET, KABUL EDİYORUM' +
        			'</div>';
                } else if (hires.user_confirm == 1 && hires.continues == 1 && !hires.enddate) {
                	company_hires_status = 
                	'<h6>Tebrikler! <b>' + company_name + '</b> studyosu ile çalışmaya başladınız. İşten ayrıldınığınız zaman aşağıdaki "İŞTEN AYRILDIM" butonuna tıklayabilirsiniz. <a href="/zindhu-hr/kullanici/profil" target="_blank"><u>Profil</u></a> sayfasına gidip deneyimlerinize de studyoyu kayıt edebilirsiniz.</h6>' +
                	'<div class="d-flex justify-content-start">' +
	        			'<a href="javascript:void(0)" style="cursor:auto;" class="btn btn-dark btn-md mr-2">' +
	        				company_name +' İLE ÇALIŞIYORUM' +
        				'</a>' +
	        			'<a href="javascript:void(0)" onclick="company_exit('+ company_id +')" class="btn btn-dark btn-md" id="company-exit-btn">İŞTEN AYRILDIM</a>' +
        			'</div>';
                } else if (hires.user_confirm == 1 && hires.continues == 0 && hires.enddate) {
                	company_hires_status = 
                	'<h6> <b>' + company_name + '</b> studyosundan ayrıldınız. Tekrar çalışmayı düşünürseniz aşağıdaki "TEKRAR ŞİRKETE İŞ İSTEĞİ GÖNDER" butonundan tekrar istek gönderebilirsiniz.</h6>' +
        			'<div class="d-flex justify-content-start">' +
	        			'<a href="javascript:void(0)" style="cursor:auto;" class="btn btn-dark btn-md mr-2">ÇIKIŞ TARİHİ '+ hires.enddate +'</a>' +
	        			'<a href="javascript:void(0)" onclick="company_invite('+ company_id +')" class="btn btn-dark btn-md" id="company-invite-btn">TEKRAR ŞİRKETE İŞ İSTEĞİ GÖNDER</a>' +
        			'</div>';
                } else if (hires.company_confirm == 0) {
                	company_hires_status = 
                	'<h6><b>' + company_name + '</b> studyosuna çalışma isteği gönderildi.</h6>' +
                	'<div>' +
	        			'<a href="javascript:void(0)" style="cursor:auto;" class="btn btn-dark btn-md ml-0">' +
	        				'STÜDYODAN DÖNÜŞ BEKLENİYOR' +
        				'</a>' +
        			'</div>';
                }
        	} else {
        		company_hires_status = 
        		'<h6><b>' + company_name + '</b> studyosu ile çalışmayı düşünüyorsan çalışma isteği gönderebilirsin.</h6>' +
    			'<div>' +
        			'<a href="javascript:void(0)" onclick="company_invite('+ company_id +')" class="btn btn-dark btn-md ml-0" id="company-invite-btn">ŞİRKETE İŞ İSTEĞİ GÖNDER</a>' +
    			'</div>';
        	}

        	return company_hires_status;
		}

		function reload_table()
		{
		    table.ajax.reload(null,false);

		    setTimeout(function() {
		    	$('[data-toggle="tooltip"]').tooltip();
		    }, 700);
		}

    </script>

</body>

</html>