<!DOCTYPE html>
<html lang="tr">

<head>

    <title>İş İlanları - Zindhu HR</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>

    <?php include(dirname(__DIR__) . '/inc/header.php'); ?>

    <div>

        <div class="sub-page job-list">
            <div class="cover">
                <h2>Merhaba, ekibe katılır mısın?</h2>
            </div>
            <div class="list-filter-mobile-trigger"><i class="las la-filter"></i>
                <div>Filtrele<span class="black-text"></span></div>
            </div>
            <div class="web-filter">
                <div class="listing">

                    <?php if (
                        (isset($search['position']) && is_array($search['position'])) ||
                        (isset($search['city']) && is_array($search['city'])) ||
                        (isset($search['study_type']) && is_array($search['study_type'])) ||
                        (isset($search['transaction']) && is_array($search['transaction'])) ||
                        isset($search['keywords'])
                    ) : ?>

                       

                        <div class="d-block mt-2"></div>

                        <a class="btn btn-dark" href="?clear=1"><i class="la la-times-circle mr-1"></i>Tüm filtreleri temizle</a>

                    <?php endif ?>

                </div>
                <form action="<?= base_url('zindhu-hr/kullanici/is-ilanlari') ?>" method="get">
                    <div class="list-filter">
                        <div class="search">
                            <div class="custom-input">
                                <label>Aranacak Kelime</label>
                                <input type="text" class="form-control rounded" name="keywords" id="keywords" placeholder="Anahtar Kelime" value="<?= $search['keywords'] ?>">
                            </div>
                        </div>
                        <div>
                            
                            <div class="custom-input">
                                <label class="mdb-main-label">Pozisyonlar</label>
                                <select class="mdb-select md-form" name="position[]" multiple>
                                    <option value="" disabled selected>Seçiniz</option>

                                    <?php foreach ($positions as $position) : ?>
                                        <?php $position_count_by_ad = position_count_by_ad($position['id'], $search); ?>
                                        <option value="<?= $position['id'] ?>" id="position-<?= $position['id'] ?>" <?= select_find($search['position'], $position['id']) ?>><?= $position['title'] ?></option>
                                    <?php endforeach ?>

                                </select>
                            </div>
                        </div>
                        <div>
                            <div class="custom-input">
                                <label class="mdb-main-label">Lokasyon</label>
                                <select class="mdb-select md-form" name="city[]" multiple>

                                    <option value="" disabled selected>Seçiniz</option>
                                    <?php foreach ($cities as $city) : ?>
                                        <option value="<?= $city['id'] ?>" id="city-<?= $city['id'] ?>" <?= select_find($search['city'], $city['id']) ?>><?= $city['city'] ?></option>
                                    <?php endforeach ?>

                                </select>
                            </div>
                            
                        </div>
                        <div class="work-type">
                            <div class="custom-input">
                                <label class="mdb-main-label">Çalışma Türü</label>
                                <select class="mdb-select md-form" name="study_type[]" multiple>

                                    <option value="" disabled selected>Seçiniz</option>
                                    <?php foreach ($study_types as $study_type) : ?>
                                        <?php $study_type_count_by_ad = study_type_count_by_ad($study_type, $search); ?>
                                        <option id="study-type-<?= $study_type ?>" value="<?= $study_type ?>" <?= select_find($search['study_type'], $study_type) ?>><?= $study_type ?></option>
                                    <?php endforeach ?>

                                </select>
                            </div>
                            
                        </div>
                    </div>
                    <div class="list-filter">
                        <button type="submit" id="filter-btn" class="btn btn-black">ARAMA</button>
                    </div>
                </form>
            </div>


            <div class="results">
                <div class="listing">
                <?php if (isset($search['keywords'])) : ?>
                            <p>
                                “<span class="black-text"><?= $search['keywords'] ?></span>” arama sonucu için
                                <span class="black-text"><?= $total_rows ?></span> ilan gösteriliyor.
                            </p>
                        <?php endif ?>

                        <?php if (isset($search['position']) && is_array($search['position'])) : ?>
                            <?php foreach ($search['position'] as $position) : ?>
                                <h4 class="d-inline">
                                    <a href="javascript:void(0)" class="checkbox-filter badge p-2 mb-2" data-id="position-<?= $position ?>">
                                        <small><?= position_render($position) ?></small> <i class="la la-times-circle"></i>
                                    </a>
                                </h4>
                            <?php endforeach ?>
                        <?php endif ?>

                        <?php if (isset($search['city']) && is_array($search['city'])) : ?>
                            <?php foreach ($search['city'] as $city) : ?>
                                <h4 class="d-inline">
                                    <a href="javascript:void(0)" class="checkbox-filter badge p-2 mb-2" data-id="city-<?= $city ?>">
                                        <small><?= city_render($city) ?></small> <i class="la la-times-circle"></i>
                                    </a>
                                </h4>
                            <?php endforeach ?>
                        <?php endif ?>

                        <?php if (isset($search['study_type']) && is_array($search['study_type'])) : ?>
                            <?php foreach ($search['study_type'] as $study_type) : ?>
                                <h4 class="d-inline">
                                    <a href="javascript:void(0)" class="checkbox-filter badge p-2 mb-2" data-id="study-type-<?= $study_type ?>">
                                        <small><?= $study_type ?></small> <i class="la la-times-circle"></i>
                                    </a>
                                </h4>
                            <?php endforeach ?>
                        <?php endif ?>

                        <?php if (isset($search['transaction'])) : ?>
                            <?php foreach ($search['transaction'] as $transaction) : ?>
                                <h4 class="d-inline">
                                    <a href="javascript:void(0)" class="checkbox-filter badge bg-black p-2 mb-2" data-id="transaction-<?= $transaction ?>">
                                        <small class="black-text"><?= transaction_render($transaction) ?></small> <i class="la la-times-circle black-text"></i>
                                    </a>
                                </h4>
                            <?php endforeach ?>
                        <?php endif ?>

                </div>
           
                <div class="open-positions">

                    <div class="jobs-list">

                        <?php if ($ads) { ?>

                            <?php foreach ($ads as $key => $ad) : ?>
                                <?php $ad_user = ad_user_control($ad['id'], $this->session->userdata('UserID')); ?>
                                <a class="job-detail mt-0 <?= isset($ad_user->views_date) ? 'weak-job' : '' ?>" href="<?= base_url('zindhu-hr/kullanici/is-ilani/' . $ad['slug'] . '-' . $ad['id']) ?>">
                                    <div class="icon">
                                        <i class="icon-zindhu-icon"></i>
                                    </div>
                                    <div class="title">
                                        <div class="name">

                                            <h3><?= $ad['title'] ?> <?=is_remote_render($ad['is_remote'])?></h3>
                                            <span class="badge badge-dark"><?= $ad['study_type'] ?></span>

                                            <?php if (isset($ad_user->apply_date)) : ?>
                                                <span class="badge badge-green">Başvuruldu</span>
                                            <?php endif ?>

                                            <?php if (isset($ad_user->follow_date)) : ?>
                                                <span class="badge badge-red">Takip Ediliyor</span>
                                            <?php endif ?>

                                        </div>
                                        <div class="location">
                                            <i class="las la-map-marker"></i> 
                                            <?=city_district_render($ad['city'], $ad['district'], $ad['city_text'])?>
                                            <span class="weak"><?= $ad['number_employees'] ?> Kişi Çalışıyor</span>
                                        </div>
                                    </div>
                                    <div class="link">
                                        <i class="la la-angle-right"></i>
                                    </div>
                                </a>
                            <?php endforeach ?>

                        <?php } else { ?>

                            <div class="card mt-3">
                                <div class="card-body pb-0">
                                    <p class="text-dark">
                                        Aradığınız kriterlere uygun ilan şu an bulunmamaktadır.<br />
                                        <small>Seçtiğiniz kriterleri değiştirerek tekrar arama yapabilirsiniz.</small>
                                    </p>
                                </div>
                            </div>

                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>

        <div class="prev-next-btn-group mt-3 text-right">
            <?php foreach ($links as $link) {
                echo $link;
            } ?>

            <?php if (@$links[0] != '') : ?>
                <div class="count-area text-center">
                    Toplam <?= $total_rows ?> ilan<br />
                    Toplam <?= $total_page ?> sayfa
                </div>
            <?php endif ?>
        </div>
    </div>

    <?php include(dirname(__DIR__) . '/inc/modals/login-modal.php'); ?>

    <?php include(dirname(__DIR__) . '/inc/footer.php'); ?>

    <div class="scroll-top" id="scrollTop">
    </div>

    <?php include(dirname(__DIR__) . '/inc/script.php'); ?>

    <script type="text/javascript">
        $(document).ready(function() {

            let is_keywords_get = getUrlParameter('keywords');
            let is_transaction_get = getUrlParameter('transaction[]');
            let is_position_get = getUrlParameter('position[]');

            if (is_keywords_get != null || is_transaction_get != null || is_position_get != null) {
                $('html, body').stop().animate({
                    scrollTop: $('.sub-page.job-list .cover').height()
                });
            }

            $('.checkbox-filter').click(function() {
                var data_id = $(this).attr('data-id');
                $('#' + data_id).removeAttr('selected');
                $('#filter-btn').click();
            });

            $('.text-filter').click(function() {
                $('#keywords').val('');
                $('#filter-btn').click();
            });

            $('.list-filter-mobile-trigger').click(function() {
                $('.web-filter').toggleClass("opened");
            });

        });
    </script>

</body>

</html>