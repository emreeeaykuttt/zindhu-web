<!DOCTYPE html>
<html lang="tr">

<head>
    
    <title><?=$ad->title?> <?=is_remote_render($ad->is_remote)?> - Zindhu HR</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>

    <?php include(dirname(__DIR__) . '/inc/header.php'); ?>

    <div class="sub-page job-detail">
        
        <div class="left">
            <div class="icon">
                <i class="icon-zindhu-icon"></i>
            </div>
            <h2><?=$ad->title?> <?=is_remote_render($ad->is_remote)?></h2>
            <div class="sub-info">
                <div class="badge"><?=$ad->study_type?></div>
                <div class="time">Yayınlanma tarihi: <span class="weak"><?=$ad->created_date?></span></div>
            </div>
            <div class="actions">

                <a 
                    href="javascript:void(0)" 
                    id="ad-apply" 
                    onclick="ad_apply(<?=$ad->id?>)" 
                    class="btn btn-green <?=isset($ad_user->apply_date) ? 'd-none' : 'd-flex'?>">
                    Başvuru Yap
                </a>
                <a 
                    href="javascript:void(0)" 
                    id="ad-applied" 
                    style="cursor: default;"
                    class="btn btn-green disabled <?=isset($ad_user->apply_date) ? 'd-flex' : 'd-none'?>">
                    Başvuruldu
                </a>

                <a 
                    href="javascript:void(0)" 
                    id="ad-follow" 
                    onclick="ad_follow(<?=$ad->id?>)" 
                    class="btn btn-green-outline <?=isset($ad_user->follow_date) ? 'd-none' : 'd-flex'?>">
                    <i class="la la-bookmark"></i>
                </a>
                <a 
                    href="javascript:void(0)" 
                    id="ad-unfollow" 
                    onclick="ad_unfollow(<?=$ad->id?>)" 
                    class="btn btn-green <?=isset($ad_user->follow_date) ? 'd-flex' : 'd-none'?>">
                    <i class="la la-bookmark"></i>
                </a>

            </div>
        </div>

        <div class="right">
            <div class="go-back">
                <a href="<?=base_url('zindhu-hr/kullanici/is-ilanlari')?>">
                    <i class="la la-arrow-left"></i> Tüm pozisyonlar
                </a>
            </div>
            <div class="tags">
                <?=positions_render(json_decode($ad->position_json))?>
            </div>
            <div class="info">
                <div>
                    <span class="weak">Çalışma Türü</span>
                    <?=$ad->study_type?>
                </div>
                <div>
                    <span class="weak">Lokasyon</span>
                    <?=city_district_render($ad->city, $ad->district, $ad->city_text)?>
                </div>
                <div>
                    <span class="weak">Çalışan Sayısı</span>
                    <?=$ad->number_employees?>
                </div>
            </div>
            <hr>
            <div class="detail job-definition">
                <h3>İş tanımı</h3>
                
                <?php if ($this->session->userdata('UserAdminConfirm') == 'approved'){ ?>
                    <?=$ad->job_description?>
                <?php } else { ?>
                    <?=$ad->short_description?>
                    <a href="<?=base_url('zindhu-hr/kullanici/giris')?>" class="btn btn-green mt-4 w-100">
                        İlanı detaylı görmek için giriş yapınız.
                    </a>
                <?php } ?>

            </div>
            
            <div class="similar-jobs">
                <?php if ($related_records): ?>
                <h3>Benzer İlanlar</h3>
                <div class="open-positions">
                    <div class="jobs-list">
                        <?php foreach ($related_records as $key => $ad): ?>
                            <?php $ad_user = ad_user_control($ad['id'], $this->session->userdata('UserID')); ?>
                            <a 
                                class="job-detail <?=isset($ad_user->views_date) ? 'weak-job' : ''?>" 
                                href="<?=base_url('zindhu-hr/kullanici/is-ilani/'. $ad['slug'] . '-' . $ad['id'])?>">
                                <div class="icon">
                                    <i class="icon-zindhu-icon"></i>
                                </div>
                                <div class="title">
                                    <div class="name">
                                        <h3><?=$ad['title']?> <?=is_remote_render($ad['is_remote'])?></h3>
                                    </div>
                                    <div class="location">
                                        <img src="<?=base_url()?>assets/frontend/img/map-marker.svg" > 
                                        <?=city_district_render($ad['city'], $ad['district'], $ad['city_text'])?>
                                        <span class="weak"><?=$ad['number_employees']?> Kişi Çalışıyor</span>
                                    </div>
                                    <span class="badge badge-dark">Full Time</span>
                                    
                                    <?php if (isset($ad_user->apply_date)): ?>
                                    <span class="badge badge-green">Başvuruldu</span>
                                    <?php endif ?>

                                    <?php if (isset($ad_user->follow_date)): ?>
                                    <span class="badge badge-red">Takip Ediliyor</span>
                                    <?php endif ?>

                                </div>
                            </a> 
                        <?php endforeach ?>
                    </div>
                </div>
                <?php endif ?>
            </div>

        </div>
    </div>
    <?php /*
    <div class="sub-page testimonials">
        <div class="text">
            <div>
                <h2 data-ani>
                    Hayalini oyuna <br>dönüştürenler <br>anlatıyor.
                </h2>
            </div>
        </div>
        <div class="carousel" data-ani>
            <div class="swiper-pagination"></div>
            <div class="swiper-container t-thumbs">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img src="/assets/frontend/img/profile_1.jpg" />
                    </div>
                    <div class="swiper-slide">
                        <img src="/assets/frontend/img/profile_2.jpg" />
                    </div>
                    <div class="swiper-slide">
                        <img src="/assets/frontend/img/profile_3.jpg" />
                    </div>
                </div>
            </div>
            <div class="swiper-container t-carousel">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="quotes">
                            <p>"Hayallerimde her zaman büyük oyun projelerinde yer almak vardı, şimdi bu ekibin önemli
                                bir parçası olduğumu kanıtladım."</p>
                            <div class="title">
                                <p class="name">Lorem Ipsum</p>
                                <p class="position">Unity Developer</p>
                            </div>
                            <div class="big-text">Unity Developer</div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="quotes">
                            <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis id dolorem enim
                                quaerat eligendi repellat necessitatibus molestias ex ea eaque."</p>
                            <div class="title">
                                <p class="name">Joe Doe</p>
                                <p class="position">Game Designer</p>
                            </div>
                            <div class="big-text">Game Designer</div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="quotes">
                            <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, dignissimos."</p>
                            <div class="title">
                                <p class="name">Lorem Ipsum</p>
                                <p class="position">3D Artist</p>
                            </div>
                            <div class="big-text">3D Artist</div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- <div class="big-text">Unity Developer</div> -->
    </div>
    */ ?>
    <?php include(dirname(__DIR__) . '/inc/modals/login-modal.php'); ?>

    <?php include(dirname(__DIR__) . '/inc/footer.php'); ?>

    <div class="scroll-top" id="scrollTop">
    </div>

    <?php include(dirname(__DIR__) . '/inc/script.php'); ?>

    <script type="text/javascript">

        var testimonialThumbs = new Swiper('.t-thumbs', {
            stopOnLastSlide: true,
            touchRatio: 0,
            slidesPerView: 5
        });
        var testimonialSlide = new Swiper('.t-carousel', {
            pagination: {
                el: '.swiper-pagination',
                type: 'custom',
                renderCustom: function (swiper, current, total) {
                    return ('0' + current).slice(-2);
                }
            },
            slidesPerView: 1,
            thumbs: {
                swiper: testimonialThumbs
            },
            autoplay: {
                delay: 5000,
            },
            effect: 'fade',
            fadeEffect: {
                crossFade: true
            },
        });

        ScrollTrigger.matchMedia({
            "(min-width: 800px)": function () {
                gsap.to(".left", {
                    scrollTrigger: {
                        trigger: ".left",
                        start: "top top",
                        endTrigger: ".similar-jobs",
                        end:"top top",
                        pin: true,
                    }
                });
            }, 
        });

        function ad_apply(ad_id = '')
        {
            let alert_icon = '';
            let form_data = {
                ad_id: ad_id
            }

            $.ajax({
                url : base_url + 'user/ad/ajax_apply',
                type: 'POST',
                data: form_data,
                dataType: 'JSON',
                success: function(data)
                {
                    if (data.status) {
                        alert_icon = '/assets/frontend/img/modal-images/success.svg';
                    } else {
                        alert_icon = '/assets/frontend/img/modal-images/error.svg';
                    }

                    modal_open(data.message, alert_icon);

                    if (data.referral_link) {
                        setTimeout(function(){ 
                            window.location.href = base_url + data.referral_link
                        }, 1500);
                    } else {
                        if (data.status) {
                            $('#ad-apply').removeClass('d-flex');
                            $('#ad-apply').addClass('d-none');
                            $('#ad-applied').removeClass('d-none');
                            $('#ad-applied').addClass('d-flex');
                        }
                    }
                }
            });
        }
        
        function ad_follow(ad_id = '')
        {
            let alert_icon = '';
            let form_data = {
                ad_id: ad_id
            }

            $.ajax({
                url : base_url + 'user/ad/ajax_follow',
                type: 'POST',
                data: form_data,
                dataType: 'JSON',
                success: function(data)
                {
                    if (data.status) {
                        alert_icon = '/assets/frontend/img/modal-images/success.svg';
                    } else {
                        alert_icon = '/assets/frontend/img/modal-images/error.svg';
                    }

                    modal_open(data.message, alert_icon);

                    if (data.referral_link) {
                        setTimeout(function(){ 
                            window.location.href = base_url + data.referral_link
                        }, 1500);
                    } else {
                        if (data.status) {
                            $('#ad-follow').removeClass('d-flex');
                            $('#ad-follow').addClass('d-none');
                            $('#ad-unfollow').removeClass('d-none');
                            $('#ad-unfollow').addClass('d-flex');
                        }
                    }
                }
            });
        }

        function ad_unfollow(ad_id = '')
        {
            let alert_icon = '';
            let form_data = {
                ad_id: ad_id
            }

            $.ajax({
                url : base_url + 'user/ad/ajax_unfollow',
                type: 'POST',
                data: form_data,
                dataType: 'JSON',
                success: function(data)
                {
                    if (data.status) {
                        alert_icon = '/assets/frontend/img/modal-images/success.svg';
                    } else {
                        alert_icon = '/assets/frontend/img/modal-images/error.svg';
                    }

                    modal_open(data.message, alert_icon);

                    if (data.referral_link) {
                        setTimeout(function(){ 
                            window.location.href = base_url + data.referral_link
                        }, 1500);
                    } else {
                        if (data.status) {
                            $('#ad-follow').removeClass('d-none');
                            $('#ad-follow').addClass('d-flex');
                            $('#ad-unfollow').removeClass('d-flex');
                            $('#ad-unfollow').addClass('d-none');
                        }
                    }
                }
            });
        }

    </script>
   
</body>

</html>