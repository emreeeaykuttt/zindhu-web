<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="CV">
	<meta name="keywords" content="CV">

	<title>CV</title>

	<?php include('inc/head.php'); ?>

</head>

<body class="stepper">
	<div class="header 
		<?= $this->uri->segment(3) == 'is-ilanlari' ? 'header-white' : '' ?>
        <?= $this->uri->segment(2) ? 'sub-header' : '' ?>">
		<a href="<?= base_url('zindhu-hr/kullanici/dashboard') ?>" class="pl-0"><img src="<?= base_url('assets/frontend/img/zindhu-hr-icon.svg') ?>" width="60"></a>
		<div></div>
		<div class="menu">

			<div class="user-menu d-flex">
				<a class="btn contact-btn" data-toggle="modal" data-target="#contact-us-modal"><i class="icon-support-icon"></i> Bize Ulaşın</a>
				<div class="dropdown user-dropdown">
					<button class="btn profile-btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="true">
						<i class="icon-profile-icon"></i>
						<span class="name"><?= $this->session->userdata('UserFirstname') ?></span>
						<i class="fa fa-caret-down"></i>
					</button>
					<div class="dropdown-menu dropdown-menu-start " aria-labelledby="dropdownMenuButton">
						<a class="dropdown-item" href="<?= base_url('zindhu-hr/kullanici/dashboard') ?>">
							<i class="icon-dashboard-icon"></i>Dashboard
						</a>
						<a class="dropdown-item" href="<?= base_url('zindhu-hr/kullanici/profil') ?>">
							<i class="icon-profile-2-icon"></i>Profil
						</a>
						<a class="dropdown-item" href="<?= base_url('zindhu-hr/kullanici/konusmalar') ?>">
							<i class="icon-conversations-icon"></i>Konuşmalar
							<span class="badge badge-danger ml-auto" id="header-unread-count">0</span>
						</a>
						<a class="dropdown-item" href="<?= base_url('zindhu-hr/kullanici/is-ilanlari') ?>">
							<i class="icon-job-list-icon"></i>İş İlanları
						</a>
						<a class="dropdown-item logout" href="<?= base_url('zindhu-hr/kullanici/cikis') ?>">
							<i class="icon-logout-icon"></i>Çıkış Yap
						</a>
					</div>
				</div>
				<div class="dropdown">
					<button class="btn lang-btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="icon-world-icon"></i>
						<?php if ($this->session->userdata('lang_code') == 'tr') { ?>
							Türkçe
						<?php } else { ?>
							English
						<?php } ?>
						<i class="fa fa-caret-down"></i>
					</button>
					<div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton">
						<a class="dropdown-item" href="<?= base_url('language/change/tr?page=zindhu-hr/kullanici') ?>">Türkçe</a>
						<a class="dropdown-item" href="<?= base_url('language/change/en?page=zindhu-hr/kullanici') ?>">English</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="stepper-wrapper">
		<div class="stepper-menu">
			<ul class="nav" role="tablist">
				<li>
					<a id="personal-information-tab" data-toggle="tab" href="#personal-information" role="tab" class="active" aria-selected="true">
						<span>1</span>Kişisel Bilgiler <i class="la la-arrow-right"></i>
					</a>
				</li>
				<li>
					<a id="educational-background-tab" data-toggle="tab" href="#educational-background" role="tab" aria-selected="false">
						<span>2</span>Eğitim Geçmişi <i class="la la-arrow-right"></i>
					</a>
				</li>
				<li>
					<a id="work-experience-tab" data-toggle="tab" href="#work-experience" role="tab" aria-selected="false">
						<span>3</span>Geçmiş Deneyimler <i class="la la-arrow-right"></i>
					</a>
				</li>
				<li>
					<a id="used-program-tab" data-toggle="tab" href="#used-program" role="tab" aria-selected="false" href="#">
						<span>4</span>Kullanılan Programlar <i class="la la-arrow-right"></i>
					</a>
				</li>
				<li>
					<a id="game-choice-tab" data-toggle="tab" href="#game-choice" role="tab" aria-selected="false" href="#">
						<span>5</span>Üretimine İlgi Duyduğun Türler <i class="la la-arrow-right"></i>
					</a>
				</li>
				<li>
					<a id="portfolio-tab" data-toggle="tab" href="#portfolio" role="tab" aria-selected="false" href="#">
						<span>6</span>Portfolyo
					</a>
				</li>
				<li>
					<a id="confirm-tab" data-toggle="tab" href="#confirm-area" role="tab" aria-selected="false" href="#">
						<span>7</span>Onaylama
					</a>
				</li>
			</ul>
			<div class="zindhu">© 2021 Zindhu HR.</div>
		</div>
		<div class="tab-content">

			<div class="tab-pane show active" id="personal-information" role="tabpanel">
				<div class="profile-section">
					<div class="section-item personal-information">
						<div class="title d-flex justify-content-between">
							<h3 class="text"><i class="icon-profile-2-icon"></i>Kişisel Bilgiler</h3>
						</div>
						<div class="py-3">
							<p class="f18">Tüm adımları eksiksiz bir şekilde doldurduktan sonra özgeçmişiniz oluşacaktır. Özgeçmişiniz onaylandıktan sonra sistemizi kullanmaya başlayabilirsiniz.</p>
							<button class="add-new" style="display: none;" onclick="personal_information_add()">
								<i class="icon-profile-2-icon"></i> Kişisel Bilgiler <i class="la la-plus"></i>
							</button>
						</div>
						<div class="content" id="personal-information-view">
							<!-- personal-information -->
						</div>
						<div class="actions d-flex justify-content-between">
							<button class="btn btn-dark" disabled>Önceki adım</button>
							<button class="btn btn-dark" onclick="go_tab('educational-background-tab')">Devam et <i class="la la-angle-right"></i></button>
						</div>
					</div>
				</div>
			</div>

			<div class="tab-pane" id="educational-background" role="tabpanel">
				<div class="profile-section">
					<div class="section-item">
						<div class="title d-flex justify-content-between">
							<h3 class="text"><i class="icon-cap-icon"></i>Eğitim geçmişi</h3>
							<div class="add" onclick="educational_background_add()">
								Üniversite Ekle <i class="la la-plus"></i>
							</div>
						</div>
						<div class="p-3">
							<button class="add-new" onclick="educational_background_add()"><i class="icon-profile-2-icon"></i> Üniversite Ekle <i class="la la-plus"></i></button>
						</div>
						<div class="content" id="educational-background-view">
							<!-- educational-background -->
						</div>
						<div class="actions d-flex justify-content-between">
							<button class="btn btn-dark" onclick="go_tab('personal-information-tab')">Önceki adım</button>
							<button class="btn btn-dark" onclick="go_tab('work-experience-tab')">Devam et <i class="la la-angle-right"></i></button>
						</div>
					</div>
				</div>
			</div>

			<div class="tab-pane" id="work-experience" role="tabpanel">
				<div class="profile-section">
					<div class="section-item">
						<div class="title d-flex justify-content-between">
							<h3 class="text"><i class="icon-experience-icon"></i>Geçmiş Deneyimler</h3>
							<div class="add" onclick="work_experience_add()">
								Deneyim Ekle <i class="la la-plus"></i>
							</div>
						</div>
						<div class="p-3">
							<button class="add-new" onclick="work_experience_add()"><i class="icon-experience-icon"></i> Deneyim Ekle <i class="la la-plus"></i></button>
						</div>
						<div class="content" id="work-experience-view">
							<!-- work-experience -->
						</div>
						<div class="actions d-flex justify-content-between">
							<button class="btn btn-dark" onclick="go_tab('educational-background-tab')">Önceki adım</button>
							<button class="btn btn-dark" onclick="go_tab('used-program-tab')">Devam et <i class="la la-angle-right"></i></button>
						</div>
					</div>
				</div>
			</div>

			<div class="tab-pane" id="used-program" role="tabpanel">
				<div class="profile-section">
					<div class="section-item">
						<div class="title d-flex justify-content-between">
							<h3 class="text"><i class="icon-program-icon"></i>Kullanılan Programlar</h3>
							<div class="add" onclick="used_program_add()">
								Program Ekle <i class="la la-plus"></i>
							</div>
						</div>
						<div class="p-3">
							<button class="add-new" onclick="used_program_add()"><i class="icon-program-icon"></i> Program Ekle <i class="la la-plus"></i></button>
						</div>
						<div class="content" id="used-program-view">
							<!-- used-program -->
						</div>
						<div class="actions d-flex justify-content-between">
							<button class="btn btn-dark" onclick="go_tab('work-experience-tab')">Önceki adım</button>
							<button class="btn btn-dark" onclick="go_tab('game-choice-tab')">Devam et <i class="la la-angle-right"></i></button>
						</div>
					</div>
				</div>
			</div>

			<div class="tab-pane" id="game-choice" role="tabpanel">
				<div class="profile-section">
					<div class="section-item">
						<div class="title d-flex justify-content-between">
							<h3 class="text"><i class="las la-gamepad"></i> Üretimine İlgi Duyduğun Türler</h3>
							<!-- <div class="add" onclick="game_choice_add()">
							 Tür Ekle <i class="la la-plus"></i>
							</div> -->
						</div>
						<div class="p-3">
							<button class="add-new" onclick="game_choice_add()"> <i class="las la-gamepad"></i>Tür Ekle <i class="la la-plus"></i></button>
						</div>
						<div class="content" id="game-choice-view">
							<!-- game-choice -->
						</div>
						<div class="actions d-flex justify-content-between">
							<button class="btn btn-dark" onclick="go_tab('used-program-tab')">Önceki adım</button>
							<button class="btn btn-dark" onclick="go_tab('portfolio-tab')">Devam et <i class="la la-angle-right"></i></button>
						</div>
					</div>
				</div>
			</div>

			<div class="tab-pane" id="portfolio" role="tabpanel">
				<div class="profile-section">
					<div class="section-item">
						<div class="title d-flex justify-content-between">
							<h3 class="text">Portfolyo</h3>
							<div class="add" onclick="portfolio_add()">
								Portfolyo Ekle <i class="la la-plus"></i>
							</div>
						</div>
						<div class="p-3">
							<button class="add-new" onclick="portfolio_add()"> <i></i> Portfolyo Ekle <i class="la la-plus"></i></button>
						</div>
						<div class="content" id="portfolio-view">
							<!-- portfolio -->
						</div>
						<div class="actions d-flex justify-content-between">
							<button class="btn btn-dark" onclick="go_tab('game-choice-tab')">Önceki adım</button>
							<button class="btn btn-dark" onclick="go_tab('confirm-tab')">Devam et <i class="la la-angle-right"></i></button>
						</div>
					</div>
				</div>
			</div>

			<div class="tab-pane" id="confirm-area" role="tabpanel">
				<div class="profile-section">
					<div class="section-item">
						<div class="title d-flex justify-content-between">
							<h3 class="text">Artık hazır sayılırsın, onayla ve gönder!</h3>
						</div>
						<div class="content" id="confirm-view">

							<div class="col-12">
								<div class="form-check mb-2">
									<input type="checkbox" class="form-check-input" name="user_privacy" id="privacy" value="1">
									<label class="form-check-label" for="privacy"><u><b data-toggle="modal" data-target="#privacy-modal">Kişisel Veri ve Gizlilik Politikası'nı</b></u> okudum, kabul ediyorum</label>
									<div class="red-text small error-text" id="user_privacy_error"></div>
								</div>
							</div>

							<div class="col-12">
								<div class="form-check mb-2">
									<input type="checkbox" class="form-check-input" name="user_kvkk" id="kvkk" value="1">
									<label class="form-check-label" for="kvkk"><u><b onclick="kvkk_open()">Aydınlatma Metni'ni</b></u> okudum ve onayladım.</label>
									<div class="red-text small error-text" id="user_kvkk_error"></div>
								</div>
							</div>

							<div class="col-12">
								<div class="form-check">
									<input type="checkbox" class="form-check-input" name="user_confirm" id="confirm" value="1">
									<label class="form-check-label" for="confirm">Verdiğim bilgilerin doğru olduğunu onaylıyorum.</label>
									<div class="red-text small error-text" id="user_confirm_error"></div>
								</div>
							</div>
							
						</div>
						<div class="actions d-flex justify-content-between">
							<button class="btn btn-dark" onclick="go_tab('portfolio-tab')">Önceki adım</button>
							<button class="btn btn-dark" onclick="confirm_save()">Tamamla</button>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<?php include('inc/footer.php'); ?>

	<!-- kvkk-modal -->
	<div class="modal fade" id="kvkk-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead" id="kvkk-title">Aydınlatma Metni</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="kvkk-content">

					<div class="row pl-3 pr-3">
						<p><b>&Ccedil;ALIŞAN ADAYI AYDINLATMA METNİ</b></p>

						<p><b>&nbsp;</b><b><i>Zindhu Oyun Yazılım Danışmanlık Hizmetleri A.Ş</i></b>(&ldquo;<b>Şirket</b>&rdquo;) olarak, işbu Aydınlatma Metni ile, Kişisel Verilerin Korunması Kanunu (&ldquo;<b>Kanun</b>&rdquo;) kapsamında aydınlatma y&uuml;k&uuml;ml&uuml;ğ&uuml;m&uuml;z&uuml;n yerine getirilmesi ama&ccedil;lanmaktadır. Bu kapsamda sizlere bilgi vermekle y&uuml;k&uuml;ml&uuml; olduğumuz konular aşağıdaki şekildedir:&nbsp;</p>

						<p><b>1.Veri sorumlusunun ve varsa temsilcisinin kimliği&nbsp;</b></p>

						<p>Veri sorumlusu; <b><i>&Ccedil;eliktepe Mah. İsmet İn&ouml;n&uuml; Cad. Yıldırımlar İş Merkezi No:11 İ&ccedil; Kapı No: 501 Kağıthane/Istanbul</i></b><b> </b>adresinde mukim İstanbul Ticaret Sicili M&uuml;d&uuml;rl&uuml;ğ&uuml;&rsquo;ne <b><i>[262674-5</i></b> sicil numarası ile kayıtlı <b><i>Zindhu Oyun Yazılım Danışmanlık Hizmetleri A.Ş</i></b><b>&rsquo;</b>dir.&nbsp;</p>

						<p><b>2. Kişisel verilerin hangi ama&ccedil;la işleneceği&nbsp;</b></p>

						<p>Şirket&rsquo;imize sağladığınız ad-soyad, e-posta adresi, telefon numarası, adres eğitim durumu, iş deneyimi, kullanımında yetkin olduğunuz programlar <b>t&uuml;r ve kategorilerindeki </b>kişisel verileriniz, kimliği belirli veya belirlenebilir ger&ccedil;ek kişiye ilişkin bilgi i&ccedil;ermesi halinde sağladığınız portfolyodaki verileriniz ve &ouml;z ge&ccedil;mişinize ilişkin &ouml;zel veya genel nitelikte olabilecek kişisel verileriniz; Zindhu insan kaynakları platformuna (&ldquo;<b>Platform</b>&rdquo;) kaydınızın tam ve eksiksiz olarak&nbsp; tamamlanması ve kayıt s&uuml;recinin y&ouml;netilmesi, Platform &uuml;zerinden işe alımınız konusunda danışmanlık verilmesi, istihdam edilmeniz amacıyla oyun st&uuml;dyoları ile bir araya getirilmeniz, gerekli olması veya st&uuml;dyonun talebi halinde Platform kullanımı ile ilgili olarak tarafınız ile iletişime ge&ccedil;ilmesi, istihdam edilme s&uuml;recinizde &ccedil;alışan arayan oyun st&uuml;dyolarına tarafımıza sağladığınız verilerin iletilebilmesi, tarafınıza sunulan istihdam y&ouml;netim ve iş bulma hizmeti ve danışmanlık sunulması kapsamında Platform &uuml;zerindeki kaydınızın y&ouml;netilmesi, tarafınıza Platform kullanımına ilişkin eğitim verilmesi ve gerekli bilgilerin sağlanması, istihdamınızın sağlanması kapsamında oyun st&uuml;dyoları ile bir araya getirilmenizin sağlanması i&ccedil;in sizlerle iletişime ge&ccedil;ilmesi, &ccedil;alışmak istediğiniz oyun st&uuml;dyolarının bulunması ve st&uuml;dyolar ile buluşturulmanıza kapsamındaki hizmetlerin y&uuml;r&uuml;t&uuml;lmesi, işe alım s&uuml;re&ccedil;leriniz kapsamında tarafınıza hizmet sağlanması, onay vermeniz halinde tarafınıza ticari elektronik ileti g&ouml;nderilmesi, burada bahsi ge&ccedil;en hizmetlerin sunulması ve faaliyetlerin y&uuml;r&uuml;t&uuml;lmesi i&ccedil;in kişi ve şirketler ile iş birliği kurulması ve mevzuattan kaynaklanan zamanaşımı s&uuml;resi doğrultusunda saklanması <b>amacı ile </b>işlenmektedir.&nbsp; &nbsp;</p>

						<p><b>3. Şirket tarafından işlenen kişisel verilerin kimlere ve hangi ama&ccedil;la aktarılabileceği&nbsp;</b></p>

						<p>Şirket&rsquo;imize sağladığınız kişisel verileriniz, insan kaynakları danışmanlık hizmetleri sunulması, istihdam edilmek isteyebileceğiniz oyun st&uuml;dyoları ile bir araya getirilmeniz, uygun iş pozisyonlarının tarafınıza sağlanması faaliyetleri kapsamında ve bu faaliyetler ile doğrudan ilgili olarak iş birliği i&ccedil;inde olduğumuz Rollic Games Oyun Yazılım ve Pazarlama A.Ş.&rsquo;ye ve istihdamınızın sağlanması kapsamında oyun st&uuml;dyolarına aktarılabilecek ve resmi makamların d&uuml;zenleyici veya icrai işlemleri veya yargı kararlarının gereklerini yerine getirmek amacıyla ve taleple sınırlı olarak resmi makamlar ile paylaşabilecektir. İş birliği yaptığımız &uuml;&ccedil;&uuml;nc&uuml; kişilerin ve oyun st&uuml;dyolarının verilerinizi yurt dışında tutması halinde verileriniz yurt dışına aktarılabilecektir.</p>

						<p><b>4. Kişisel veri toplamanın y&ouml;ntemi ve hukuki sebebi&nbsp;</b></p>

						<p>Kişisel verileriniz, Platform &uuml;zerinden, elektronik ortam aracılığıyla elde edilmekte ve a&ccedil;ık rızanız hukuki sebepleri ile toplanmaktadır.&nbsp;</p>

						<p><b>5. Kişisel verileriniz ile ilgili Kanun kapsamındaki haklarınız aşağıdaki şekildedir:&nbsp;</b></p>

						<p>(a) Kişisel verilerinizin işlenip işlenmediğini &ouml;ğrenme, (b) Kişisel verileriniz işlenmişse buna ilişkin bilgi talep etme, (c) Kişisel verilerinizin işlenme amacını ve bunların amacına uygun kullanılıp kullanılmadığını &ouml;ğrenme, (&ccedil;) Yurt i&ccedil;inde veya yurt dışında kişisel verilerin aktarıldığı &uuml;&ccedil;&uuml;nc&uuml; kişileri bilme, (d) Kişisel verilerinizin eksik veya yanlış işlenmiş olması h&acirc;linde bunların d&uuml;zeltilmesini isteme, (e) Kişisel verilerinizin işlenmesini gerektiren sebeplerin ortadan kalkması halinde kişisel verilerinizin silinmesini veya yok edilmesini isteme, (f), (d) ve (e) bentleri uyarınca yapılan işlemlerin, kişisel verilerin aktarıldığı &uuml;&ccedil;&uuml;nc&uuml; kişilere bildirilmesini isteme, (g) İşlenen verilerin m&uuml;nhasıran otomatik sistemler vasıtasıyla analiz edilmesi suretiyle kişinin kendisi aleyhine bir sonucun ortaya &ccedil;ıkmasına itiraz etme, (ğ) Kişisel verilerin kanuna aykırı olarak işlenmesi sebebiyle zarara uğraması h&acirc;linde zararın giderilmesini talep etme.&nbsp;</p>

						<p>Bu haklarınızı yazılı olarak veya g&uuml;venli elektronik imza, mobil imza ya da Şirket&rsquo;e daha &ouml;nce bildirilen ve Şirket&rsquo;in sisteminde kayıtlı bulunan elektronik posta adresinizi kullanmak suretiyle (Bu kapsamda <b><i><a href="mailto:info@zindhugames.com" target="_blank">info@zindhugames.com</a></i></b> e-posta adresi &uuml;zerinden Şirket&rsquo;e ulaşabilirsiniz) veya başvuru amacına y&ouml;nelik geliştirilmiş bir yazılım ya da uygulama vasıtasıyla Şirket&rsquo;e iletebilirsiniz.&nbsp;</p>

						<p class="w-100">Bilginize sunarız.&nbsp;</p><br />

						<p><b><i>Zindhu Oyun Yazılım Danışmanlık Hizmetleri A.Ş</i></b></p>

					</div>

				</div>
			</div>
			<!-- Content -->
		</div>
	</div>
	<!-- kvkk-modal -->

	<!-- working-status-modal -->
	<div class="modal fade profile-modal" id="working-status-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-xs" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead" id="working-status-title">Çalışma Durumu</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="working-status-content">

					<div class="row">
						<form action="#" id="working-status-from" class="col-12">

							<div class="row">

								<div class="col-6 col-md-2">
									<div class="custom-control custom-checkbox mb-3">
										<input type="checkbox" class="custom-control-input" name="working_status_is_working" id="is-working" value="1">
										<label class="custom-control-label" for="is-working">Çalışıyorum</label>
									</div>
								</div>

								<div class="md-form col-12 mb-0 working-status-company-id">
									<select class="mdb-select" name="working_status_company_id" id="working-status-company-id" searchable="Arama">
										<option value="0" disabled selected>Seçiniz</option>
									</select>
									<label class="mdb-main-label">Stüdyo Adı:</label>
									<div class="red-text small error-text" id="working_status_company_id_error"></div>
								</div>

								<div class="col-12 col-md-12 working-status-company-text">
									<div class="md-form">
										<input type="text" class="form-control" name="working_status_company_text" id="working-status-company-text">
										<label for="working-status-company-text" class="mdb-main-label">Stüdyo Adı:</label>
										<div class="red-text small error-text" id="working_status_company_text_error"></div>
									</div>
								</div>

								<div class="col-12 text-center mt-3">
									<a onclick="working_status_save()" id="working-status-btn" class="btn btn-primary btn-lg w-50">Kaydet</a>
								</div>
							</div>

						</form>
					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- working-status-modal -->

	<!-- personal-information-modal -->
	<div class="modal fade profile-modal" id="personal-information-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead" id="personal-information-title">Kişisel Bilgiler</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="personal-information-content">
					<div class="row">
						<form action="#" id="personal-information-from" class="col-12">

							<input type="hidden" value="" name="personal_information_id" />

							<div class="row">

								<div class="col-6 col-md-6">
									<div class="custom-input">
										<label for="user-firstname" class="mdb-main-label">Ad:</label>
										<input type="text" class="form-control" name="user_firstname" id="user-firstname">
										<div class="red-text small error-text" id="user_firstname_error"></div>
									</div>
								</div>

								<div class="col-6 col-md-6">
									<div class="custom-input">
										<label for="user-lastname" class="mdb-main-label">Soyad:</label>
										<input type="text" class="form-control" name="user_lastname" id="user-lastname">
										<div class="red-text small error-text" id="user_lastname_error"></div>
									</div>
								</div>

								<div class="col-12 col-md-6">
									<div class="custom-input">
										<label for="user-email" class="mdb-main-label">Email:</label>
										<input type="text" class="form-control" name="user_email" id="user-email">
										<div class="red-text small error-text" id="user_email_error"></div>
									</div>
								</div>

								<div class="col-6 col-md-6">
									<div class="custom-input">
										<label for="personal-information-phone" class="mdb-main-label">Telefon:</label>
										<input type="text" class="form-control" name="personal_information_phone" id="personal-information-phone">
										<div class="red-text small error-text" id="personal_information_phone_error"></div>
									</div>
								</div>
								<div class="col-6 col-md-6">
									<div class="custom-input">
										<label class="mdb-main-label">Pozisyon:</label>
										<select class="mdb-select" name="personal_information_position_id" id="personal-information-position-id" searchable="Arama">
											<option value="0" disabled selected>Seçiniz</option>
										</select>
										<div class="red-text small error-text" id="personal_information_position_id_error"></div>
									</div>
								</div>

								<div class="col-12 col-md-6">
									<div class="custom-input">
										<label class="mdb-main-label">Yaşadığın Şehir:</label>
										<select class="mdb-select" name="personal_information_city_id" id="personal-information-city-id" searchable="Arama">
											<option value="0" disabled selected>Seçiniz</option>
										</select>
										<div class="red-text small error-text" id="personal_information_city_id_error"></div>
									</div>
								</div>

								<div class="col-12 col-md-6">
									<div class="custom-input">
										<label class="mdb-main-label">Çalışmak İstediğiniz Şehirler:</label>
										<select class="mdb-select md-form disable-toggle-all" name="personal_information_city_json" id="personal-information-city-json" searchable="Arama" multiple>
											<option value="0" disabled selected>Seçiniz</option>
										</select>
										<div class="red-text small error-text" id="personal_information_city_json_error"></div>
									</div>
								</div>

								<div class="col-12 col-md-6 d-none city-text">
									<div class="custom-input mt-4">
										<label for="personal-information-city-text" class="mdb-main-label">Ülke-Şehir:</label>
										<input type="text" class="form-control" name="personal_information_city_text" id="personal-information-city-text">
										<div class="red-text small error-text" id="personal_information_city_text_error"></div>
									</div>
								</div>

								<div class="col-12">
									<div class="my-3">
										<input type="checkbox" class="form-check-input" name="personal_information_is_remote" id="personal-information-is-remote" value="1">
										<label class="form-check-label" for="personal-information-is-remote">Uzaktan(remote) çalışmak istiyorum.</label>
										<div class="red-text small error-text" id="personal_information_is_remote_error"></div>
									</div>
								</div>


							</div>

						</form>
					</div>
				</div>
				<div class="modal-footer d-flex">
					<button type="button" class="btn btn-md-lg btn-dark-outline col-md-2" data-dismiss="modal">Kapat</button>
					<button onclick="personal_information_save()" id="personal-information-btn" class="btn btn-primary btn-md-lg col-md-4">Kaydet</button>
				</div>

			</div>
		</div>
	</div>
	<!-- personal-information-modal -->

	<!-- educational-background-modal -->
	<div class="modal fade profile-modal" id="educational-background-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead" id="education-title">Üniversite Ekle</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="educational-background-content">
					<div class="row">
						<form action="#" id="educational-background-from" class="col-12">

							<input type="hidden" value="" name="education_id" />

							<div class="row">
								<div class="col-12 col-md-6">
									<div class="custom-input">
										<label class="mdb-main-label">Eğitim Seviyesi:</label>
										<select class="mdb-select" name="education_level" id="education-level">
											<option value="0" disabled selected>Seçiniz</option>
											<option value="on-lisans">Ön Lisans</option>
											<option value="lisans">Lisans</option>
											<option value="yuksek-lisans">Yüksek Lisans</option>
											<option value="doktora">Doktora</option>
										</select>
										<div class="red-text small error-text" id="education_level_error"></div>
									</div>
								</div>

								<div class="col-12 col-md-6">
									<div class="custom-input">
										<label class="mdb-main-label">Üniversite:</label>
										<select class="mdb-select" name="education_university_id" id="education-university-id" searchable="Arama">
											<option value="" disabled selected>Seçiniz</option>
										</select>
										<div class="red-text small error-text" id="education_university_id_error"></div>
									</div>
								</div>

								<div class="col-12 col-md-12">
									<div class="custom-input">
										<label for="education-university-section" class="mdb-main-label">Bölüm:</label>
										<input type="text" class="form-control" name="education_university_section" id="education-university-section">
										<div class="red-text small error-text" id="education_university_section_error"></div>
									</div>
								</div>

								<div class="col-12 d-none university-other">
									<div class="custom-input mt-0">
										<label for="education-university-text" class="mdb-main-label">Üniversite:</label>
										<input type="text" class="form-control" name="education_university_text" id="education-university-text">
										<div class="red-text small error-text" id="education_university_text_error"></div>
									</div>
								</div>

								<div class="col-md-4">
									<div class="custom-input">
										<label class="mdb-main-label">Başlangıç Yılı:</label>
										<select class="mdb-select" name="education_start_year" id="education-start-year">
											<option value="0" disabled selected>Seçiniz</option>
											<?php for ($i = 2021; $i >= 1970; $i--) { ?>
												<option value="<?= $i ?>"><?= $i ?></option>
											<?php } ?>
										</select>
										<div class="red-text small error-text" id="education_start_year_error"></div>
									</div>
								</div>

								<div class="col-md-4 education-end-year">
									<div class="custom-input">
										<label class="mdb-main-label">Bitiş Yılı:</label>
										<select class="mdb-select" name="education_end_year" id="education-end-year">
											<option value="0" disabled selected>Seçiniz</option>
											<?php for ($i = 2021; $i >= 1970; $i--) { ?>
												<option value="<?= $i ?>"><?= $i ?></option>
											<?php } ?>
										</select>
										<div class="red-text small error-text" id="education_end_year_error"></div>
									</div>
								</div>

								<div class="col-md-4 d-flex align-items-end">
									<div class="form-check my-3">
										<input type="checkbox" class="form-check-input" name="education_continues" id="continues">
										<label class="form-check-label" for="continues">
											Devam Ediyor
										</label>
									</div>
								</div>
							</div>

						</form>
					</div>
				</div>
				<div class="modal-footer d-flex">
					<button type="button" class="btn btn-md-lg btn-dark-outline col-md-2" data-dismiss="modal">Kapat</button>
					<button onclick="educational_background_save()" id="personal-information-btn " class="btn btn-primary btn-md-lg col-md-4">Kaydet</button>
				</div>

			</div>
		</div>
	</div>
	<!-- educational-background-modal -->

	<!-- work-experience-modal -->
	<div class="modal fade profile-modal" id="work-experience-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog  modal-dialog-centered modal-xl" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead" id="work-title">Deneyim Ekle</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="work-experience-content">

					<div class="row">
						<form action="#" id="work-experience-from" class="col-12">

							<input type="hidden" value="" name="work_id" />
							<input type="hidden" value="0" name="work_status" />

							<div class="row">

								<div class="col-12 col-md-4">
									<div class="custom-input">
										<label for="work-company-name" class="mdb-main-label">Firma Adı:</label>
										<input type="text" class="form-control" name="work_company_name" id="work-company-name">
										<div class="red-text small error-text" id="work_company_name_error"></div>
									</div>
								</div>

								<div class="col-6 col-md-4">
									<div class="custom-input">
										<label for="work-company-sector" class="mdb-main-label">Firma Sektorü:</label>
										<input type="text" class="form-control" name="work_company_sector" id="work-company-sector">
										<div class="red-text small error-text" id="work_company_sector_error"></div>
									</div>
								</div>

								<div class="col-6 col-md-4">
									<div class="custom-input">
										<label for="work-company-position" class="mdb-main-label">Firmadaki Pozisyon:</label>
										<input type="text" class="form-control" name="work_company_position" id="work-company-position">
										<div class="red-text small error-text" id="work_company_position_error"></div>
									</div>
								</div>

								<div class="col-6 col-md-2">
									<div class="custom-input">
										<label class="mdb-main-label">Başlangıç Ayı:</label>
										<select class="mdb-select" name="work_start_month" id="work-start-month">
											<option value="0" disabled selected>Seçiniz</option>
											<option value="1">Ocak</option>
											<option value="2">Şubat</option>
											<option value="3">Mart</option>
											<option value="4">Nisan</option>
											<option value="5">Mayıs</option>
											<option value="6">Haziran</option>
											<option value="7">Temmuz</option>
											<option value="8">Ağustos</option>
											<option value="9">Eylül</option>
											<option value="10">Ekim</option>
											<option value="11">Kasım</option>
											<option value="12">Aralık</option>
										</select>
										<div class="red-text small error-text" id="work_start_month_error"></div>
									</div>
								</div>

								<div class="col-6 col-md-3">
									<div class="custom-input">
										<label class="mdb-main-label">Başlangıç Yılı:</label>
										<select class="mdb-select" name="work_start_year" id="work-start-year">
											<option value="0" disabled selected>Seçiniz</option>
											<?php for ($i = 2021; $i >= 1970; $i--) { ?>
												<option value="<?= $i ?>"><?= $i ?></option>
											<?php } ?>
										</select>
										<div class="red-text small error-text" id="work_start_year_error"></div>
									</div>
								</div>

								<div class="col-6 col-md-2 work-end-year">
									<div class="custom-input">
										<label class="mdb-main-label">Bitiş Ayı:</label>
										<select class="mdb-select" name="work_end_month" id="work-end-month">
											<option value="0" disabled selected>Seçiniz</option>
											<option value="1">Ocak</option>
											<option value="2">Şubat</option>
											<option value="3">Mart</option>
											<option value="4">Nisan</option>
											<option value="5">Mayıs</option>
											<option value="6">Haziran</option>
											<option value="7">Temmuz</option>
											<option value="8">Ağustos</option>
											<option value="9">Eylül</option>
											<option value="10">Ekim</option>
											<option value="11">Kasım</option>
											<option value="12">Aralık</option>
										</select>
										<div class="red-text small error-text" id="work_end_month_error"></div>
									</div>
								</div>

								<div class="col-6 col-md-3 work-end-year">
									<div class="custom-input">
										<label class="mdb-main-label">Bitiş Yılı:</label>
										<select class="mdb-select" name="work_end_year" id="work-end-year">
											<option value="0" disabled selected>Seçiniz</option>
											<?php for ($i = 2021; $i >= 1970; $i--) { ?>
												<option value="<?= $i ?>"><?= $i ?></option>
											<?php } ?>
										</select>
										<div class="red-text small error-text" id="work_end_year_error"></div>
									</div>
								</div>

								<div class="col-6 col-md-2 d-flex align-items-end">
									<div class="form-check my-3">
										<input type="checkbox" class="form-check-input" name="work_working" id="working">
										<label class="form-check-label" for="working">
											Hala Çalışıyorum
										</label>
									</div>
								</div>

								<div class="col-12">
									<div class="custom-input">
										<label for="work-detail" class="mdb-main-label">Açıklama:</label>
										<input type="text" class="form-control" name="work_detail" id="work-detail">
										<div class="red-text small error-text" id="work_detail_error"></div>
									</div>
								</div>
							</div>

						</form>
					</div>

				</div>
				<div class="modal-footer d-flex">
					<button type="button" class="btn btn-md-lg btn-dark-outline col-md-2" data-dismiss="modal">Kapat</button>
					<button onclick="work_experience_save()" id="work-btn" class="btn btn-primary btn-md-lg col-md-4">Kaydet</button>
				</div>

			</div>
		</div>
	</div>
	<!-- work-experience-modal -->

	<!-- used-program-modal -->
	<div class="modal fade profile-modal" id="used-program-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog  modal-dialog-centered modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead" id="used-program-title">Program Ekle</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="used-program-content">

					<div class="row">
						<form action="#" id="used-program-from" class="col-12">

							<input type="hidden" value="" name="program_id" />

							<div class="row">
								<div class="col-6 col-md-6">
									<div class="custom-input">
										<label class="mdb-main-label">Program:</label>
										<select class="mdb-select" name="used_program_id" id="used-program-id" searchable="Arama">
											<option value="0" disabled selected>Seçiniz</option>
										</select>
										<div class="red-text small error-text" id="used_program_id_error"></div>
									</div>
								</div>

								<div class="col-6 col-md-6">
									<div class="custom-input">
										<label class="mdb-main-label">Kullanma Süresi:</label>
										<select class="mdb-select" name="used_program_duration" id="used-program-duration" searchable="Arama">
											<option value="0" disabled selected>Seçiniz</option>
											<?php for ($i = 1; $i <= 30; $i++) { ?>
												<option value="<?= $i ?>"><?= $i ?> Yıl</option>
											<?php } ?>
										</select>
										<div class="red-text small error-text" id="used_program_duration_error"></div>
									</div>
								</div>
							</div>
						</form>
					</div>

				</div>
				<div class="modal-footer d-flex">
					<button type="button" class="btn btn-md-lg btn-dark-outline col-md-2" data-dismiss="modal">Kapat</button>
					<button onclick="used_program_save()" id="used-program-btn" class="btn btn-primary btn-md-lg col-md-4">Kaydet</button>
				</div>

			</div>
		</div>
	</div>
	<!-- used-program-modal -->

	<!-- game-choice-modal -->
	<div class="modal fade profile-modal" id="game-choice-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog  modal-dialog-centered modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead" id="game-choice-title">Tür Ekle</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="game-choice-content">

					<div class="row">
						<form action="#" id="game-choice-from" class="col-12">

							<input type="hidden" value="" name="game_id" />

							<div class="row">
								<div class="col-12 col-md-6">
									<div class="custom-input">
										<label class="mdb-main-label">Gelecekte üretiminde bulunmak istediğiniz türler:</label>
										<select class="mdb-select md-form disable-toggle-all" name="game_future_json" id="game-future-json" searchable="Arama" multiple>
											<option value="0" disabled selected>Seçiniz</option>
										</select>
										<div class="red-text small error-text" id="game_future_json_error"></div>
									</div>
								</div>

								<div class="col-12 col-md-6">
									<div class="custom-input">
										<label class="mdb-main-label">Geçmişte üretiminde bulunduğunuz türler:</label>
										<select class="mdb-select md-form disable-toggle-all" name="game_past_json" id="game-past-json" searchable="Arama" multiple>
											<option value="0" disabled selected>Seçiniz</option>
										</select>
										<div class="red-text small error-text" id="game_past_json_error"></div>
									</div>
								</div>
							</div>

						</form>
					</div>
				</div>
				<div class="modal-footer d-flex">
					<button type="button" class="btn btn-md-lg btn-dark-outline col-md-2" data-dismiss="modal">Kapat</button>
					<button onclick="game_choice_save()" id="used-program-btn" class="btn btn-primary btn-md-lg col-md-4">Kaydet</button>
				</div>

			</div>
		</div>
	</div>
	<!-- game-choice-modal -->

	<!-- portfolio-modal -->
	<div class="modal fade profile-modal" id="portfolio-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead" id="portfolio-title">Portfolyo Ekle</p>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="portfolio-content">
					<div class="row">
						<form action="#" id="portfolio-form" class="col-12" enctype="multipart/form-data">
							<input type="hidden" value="" name="portfolio_id" />
							<div class="row">
								<div class="col-12 mb-3">
									<div class="custom-input">
										<label for="portfolio-link" class="mdb-main-label">Portfolyo Linki:</label>
										<input type="text" class="form-control" name="portfolio_link" id="portfolio-link">
										<div class="red-text small error-text" id="portfolio_link_error"></div>
									</div>
								</div>
								<div class="col-12">
									<div class="custom-input">
										<label id="portfolio-name-text"></label>
										<input type="hidden" name="portfolio_name_text" value="">
									</div>
								</div>
								<div class="col-12">
									<div class="custom-input mt-0">
										<div class="file-field">
											<div class="file-drag">
												<img src="/assets/frontend/img/file-icon.svg" />
												<p>Dosyanızı buraya sürükleyin veya tıklayın.</p>
												<p class="weak">(Maks. 10mb, PDF, PPT )</p>
												<input type="file" name="portfolio_name" id="portfolio-name">
											</div>
											<div class="file-path-wrapper">
												<input class="file-path validate" disabled type="text" placeholder="Dosya Adı">
											</div>
										</div>
										<div class="red-text small error-text" id="portfolio_name_error"></div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>

				<div class="modal-footer d-flex">
					<button type="button" class="btn btn-md-lg btn-dark-outline col-md-2" data-dismiss="modal">Kapat</button>
					<button onclick="portfolio_save()" id="used-program-btn" class="btn btn-md-lg btn-primary col-md-4">Kaydet</button>
				</div>

			</div>
		</div>
	</div>
	<!-- portfolio-modal -->

	<?php include('inc/script.php'); ?>
	<script type="text/javascript">var completed = false;</script>
	<script src="<?= base_url() ?>assets/admin/js/cv_helper.js"></script>
	<script src="<?= base_url() ?>assets/admin/js/cv.js?v=2.3"></script>

	<script type="text/javascript">
		$(document).ready(function() {

			$('#general-modal').on('hidden.bs.modal', function(e) {
				if (completed) {
					window.location.href = base_url + 'user/dashboard'
				}
	        });

			$('input[type="file"]').change(function(e) {
				var $this = $(e.target);
				var $fileField = $this.closest('.file-field');
				var $pathInput = $fileField.find('input.file-path');
				var fileNames = [];

				fileNames.push(e.target.files[0].name);

				$pathInput.val(fileNames.join(', '));
				$pathInput.trigger('change');
			});

			$('#continues').change(function() {
				if ($(this).is(':checked')) {
					$('.education-end-year').css('display', 'none');
				} else {
					$('.education-end-year').css('display', 'block');
				}
			});

			$('#working').change(function() {
				if ($(this).is(':checked')) {
					$('.work-end-year').css('display', 'none');
				} else {
					$('.work-end-year').css('display', 'block');
				}
			});

			$('#education-level, #education-start-year, #education-end-year, #work-start-month, #work-start-year, #work-end-month, #work-end-year, #used-program-duration').materialSelect();

			$('#education-university-id').change(function() {
				let university_id = $(this).val();
				if (university_id == 0) {
					$('.university-other').attr('style', 'display: block !important');
				} else {
					$('.university-other').attr('style', 'display: none !important');
				}
			});

			$('#personal-information-city-id').change(function() {
				let city_id = $(this).val();
				if (city_id == 341) {
					$('.city-text').removeClass('d-none');
					$('.city-text').addClass('d-block');
				} else {
					$('.city-text').removeClass('d-block');
					$('.city-text').addClass('d-none');
					$('[name="personal_information_city_text"]').val('').trigger("change");
				}
			});
		});

		function go_tab(tab_id = '') {
			$('#' + tab_id).click();
		}

		function next_step() {
			setTimeout(function() {
				$('#horizontal-stepper').nextStep();
			}, 1000);
		}

		function kvkk_open() {
			$('#kvkk-modal').modal('show')
		}


		
	</script>
</body>

</html>