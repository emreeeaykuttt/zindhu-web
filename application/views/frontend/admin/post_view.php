<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Paylaşımlar">
    <meta name="keywords" content="Paylaşımlar">

    <title>Paylaşımlar</title>

    <?php include('inc/head.php'); ?>
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/addons/datatables.min.css">

</head>

<body class="fixed-sn navy-blue-skin dark-bg-admin">

	<?php include('inc/header.php'); ?>

	<!-- Main layout -->
	<main>
		<div class="container-fluid">

			<section>

		        <!-- Gird column -->
		        <div class="col-md-12">

		         	<h5 class="my-4 white-text font-weight-bold" id="module-title">
		         		<span>Paylaşımlar</span>
		         		<a href="javascript:void(0)" onclick="add()" class="btn blue-gradient btn-sm">YENİ EKLE</a>
		         	</h5>

			        <div class="card">
			            <div class="card-body">
			            	<div class="table-responsive">
				              	<table id="post-table" class="table table-striped" cellspacing="0" width="100%">
				                	<thead>
					                 	<tr>
					                 		<th>Paylaşım Tipi</th>
											<th>Başlık</th>
											<th width="290">İşlemler</th>
										</tr>
				                	</thead>
			              		</table>
			              	</div>
			            </div>
			         </div>

		        </div>
		        <!-- Gird column -->

		    </section>

		</div>
	</main>
	<!-- Main layout -->

    <?php include('inc/footer.php'); ?>

    <!-- Post Form Modal -->
    <div class="modal fade" id="post-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
      aria-hidden="true">
		<div class="modal-dialog modal-notify modal-success modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead" id="modal-post-title">Paylaşım</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="white-text">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body">
					<form action="#" id="post-form" enctype="multipart/form-data">

						<input type="hidden" name="post_id" value="">
						
						<div class="md-form">
				            <select class="mdb-select" name="post_type" id="post-type">
				                <option value="0" disabled selected>Seçiniz</option>
				                <option value="duyuru">Duyuru</option>
				                <option value="blog">Blog</option>
				                <option value="egitim">Eğitim</option>
				            </select>
				            <label class="mdb-main-label">İçerik Tipi:</label>
				            <div class="red-text small error-text" id="post_type_error"></div>
				        </div>

						<div class="md-form mt-3">
			                <input type="text" class="form-control" name="post_title" id="post-title">
			                <label for="post-title" class="mdb-main-label">Başlık:</label>
			                <div class="red-text small error-text" id="post_title_error"></div>
			            </div>

			            <div class="md-form mt-3">
			                <textarea type="text" name="post_detail" id="post-detail" class="md-textarea form-control" rows="6"></textarea>
			                <label for="post-title" class="mdb-main-label">Detay:</label>
			                <div class="red-text small error-text" id="post_detail_error"></div>
			            </div>

			            <div class="mt-3" id="photo-preview">
		                	<label>Fotoğraf:</label>
		                	<div></div>
		                </div>

			            <div class="md-form mt-3">
		                  	<div class="file-field">
		                    	<div class="btn btn-primary btn-sm float-left">
		                      		<span>FOTOĞRAF YÜKLE</span>
		                      		<input type="file" name="post_photo" id="post-photo">
		                    	</div>
		                    	<div class="file-path-wrapper">
		                      		<input class="file-path validate" disabled type="text" placeholder="Dosya Adı">
		                    	</div>
		                  	</div>
		                  	<div class="red-text small error-text" id="post_photo_error"></div>
		                </div>

					</form>
				</div>

				<!-- Footer -->
				<div class="modal-footer justify-content-end">
					<a onclick="save()" id="post-btn" class="btn btn-dark">Kaydet</a>
				</div>

			</div>
			<!-- Content -->
		</div>
    </div>
    <!-- Post Form Modal -->

    <?php include('inc/script.php'); ?>
    <script type="text/javascript" src="<?=base_url()?>assets/admin/js/addons/datatables.min.js"></script>

    <script type="text/javascript">
    	
		var table;
	    var params = '';
		
		$(document).ready(function(){

		    table = $('#post-table').DataTable({

		    	'language': {
		            'url': base_url + 'assets/admin/js/addons/language_tr.json'
		        },

		        'processing': true,
		        'serverSide': true,
		        'order': [],
		        'lengthMenu': [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'Tüm']],
		        'ajax': {
		            'url': base_url + 'admin/post/ajax_list' + params,
		            'type': 'POST'
		        },

		        'dom': '<"w-50 float-left"l><"w-50 float-left"fB><"table-content" tr><"w-50 float-left"i><"w-50 float-left"p>',

		  		'columns': [
		  			{ 'data': 'post_type' },
		            { 'data': 'title' },
		            { 'data': 'transactions', 'orderable': false}
		        ]

		    });

		    $('[data-toggle="tooltip"]').tooltip();

		    $('input[type="file"]').change(function (e) {
		        var $this = $(e.target);
		        var $fileField = $this.closest('.file-field');
		        var $pathInput = $fileField.find('input.file-path');
		        var fileNames = [];

		        fileNames.push(e.target.files[0].name);

		        $pathInput.val(fileNames.join(', '));
		        $pathInput.trigger('change');
		   });

		    $('#post-type').materialSelect();
		});

		function add()
		{
			$('#post-modal').modal('show');
			$('#modal-post-title').text('Paylaşım Ekle');
			$('#post-form')[0].reset();
			$('[name="post_id"]').val('');
			$('#post-type').val(0);
			$('#photo-preview div').html('');
			$('#photo-preview').hide();
			$('.error-text').text('');
		}

		function edit(post_id)
		{
			$('#post-modal').modal('show');
			$('#modal-post-title').text('Paylaşımı Düzenle');
			$('#post-form')[0].reset();
			$('#photo-preview').show();
			$('.error-text').text('');

			$.ajax({
		        url : base_url + 'admin/post/ajax_view/' + post_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	$('[name="post_id"]').val(data.id);
		            $('#post-type').val(data.post_type);
		            $('#post-title').val(data.title).trigger("change");
		            $('#post-detail').val(data.detail).trigger("change");

		            $('#post-type').materialSelect('destroy');
    				$('#post-type').materialSelect();

    				if(data.photo) {
	                    $('#photo-preview div').html('<img src="'+base_url+upload_folder+data.photo+'" style="width: 200px;" class="img-responsive">'); 
	                    $('#photo-preview div').append(
	                    	'<div class="custom-control custom-checkbox">' +
							    '<input type="checkbox" name="remove_photo" value="'+data.photo+'" class="custom-control-input" id="remove-photo">' +
							    '<label class="custom-control-label" for="remove-photo">Kaydederken fotoğrafı kaldır</label>' +
							'</div>'
						); 
	                } else {
	                    $('#photo-preview div').text('(Fotoğraf Yok)');
	                }
		        }
		    });
		}

		function remove(post_id)
		{
			Swal.fire({
		        title: 'Silmek istiyor musunuz?',
		        icon: 'success',
		        showCancelButton: true,
		        confirmButtonColor: '#3085d6',
		        cancelButtonText: 'Vazgeç',
		        cancelButtonColor: '#d33',
		        confirmButtonText: 'Evet'
		    }).then((result) => {
		        if (result.value) 
		        {
		        	$.ajax({
				        url : base_url + 'admin/post/ajax_remove/' + post_id,
				        type: 'POST',
				        dataType: 'JSON',
				        success: function(data)
				        {
				            toastr.success(data.message);
				            reload_table();
				        }
				    });
		        }
    		});
		}

		function save()
		{
			let formData = new FormData($('#post-form')[0]);

			$('#post-btn').text('Kaydediyor...');
		    $('#post-btn').attr('disabled',true);

		    $.ajax({
		        url : base_url + 'admin/post/ajax_save',
		        type: 'POST',
		        data: formData,
		        contentType: false,
		        processData: false,
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	$('.error-text').text('');
		        	
		            if(data.status) 
		            {
		                $('#post-modal').modal('hide');
		                $('#post-form')[0].reset();
		                toastr.success(data.message);
		                reload_table();
		            }
		            else
		            {   
		                for (var i = 0; i < data.inputerror.length; i++) 
		                {
		                    $('#' + data.inputerror[i] + '_error').text(data.error_string[i]); 
		                }
		                
		                $('#post-form').stop().animate({
		                    scrollTop:0
		                });
		            }

		            $('#post-btn').text('Kaydet'); 
		            $('#post-btn').attr('disabled',false);
		        }
		    });
		}

		function reload_table()
		{
		    table.ajax.reload(null,false);
		}

    </script>

</body>

</html>