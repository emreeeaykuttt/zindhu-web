<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Panel Giriş Yap">
    <meta name="keywords" content="Panel Giriş Yap">

    <title>Panel Giriş Yap</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>
    <style>
	body {
		background:#140e56
	}
    html,
    body,
    header,
    .view {
      height: 100%;
    }
    @media (min-width: 560px) and (max-width: 740px) {
      html,
      body,
      header,
      .view {
        height: 650px;
      }
    }
    @media (min-width: 800px) and (max-width: 850px) {
      html,
      body,
      header,
      .view  {
        height: 650px;
      }
    }
  </style>

</head>

<body class="login-page hr-bg d-flex flex-column align-items-stretch">

	<?php
		$login_status = $this->session->flashdata('login_status');
		$flashdata = null;
		if (isset($login_status)) {
			$flashdata = $login_status;
		}
	?>
	
	<section class="view  my-auto">
     	<div class="mask  h-100 d-flex justify-content-center align-items-center">
	
			<div class="container">
					<div class="logo">
				<img src="/assets/hr/img/zindhu-hr-logo.svg" class="mx-auto" />
			</div>
				<div class="row">
					<div class="col-md-6 col-sm-12 mx-auto">

						<?php if ($flashdata) : ?>
							<div class="alert <?= $flashdata['status'] ? 'alert-success' : 'alert-danger' ?>">
								<span><?= $flashdata['message'] ?></span>
							</div>
						<?php endif ?>

						<div class="card mt-5 wow fadeIn" data-wow-delay="0.3s">
			                <div class="card-body">

								<div class="form-header blue-gradient">
			                    	<h3 class="font-weight-500 my-2 py-1"><i class="fas fa-user"></i> Panel Giriş Yap</h3>
			                  	</div>
								
								<form action="<?= base_url('admin/auth/login') ?>" method="POST" id="login-form" autocomplete="off">

									<div class="md-form">
										<input 
											type="text" 
											name="email" 
											id="email" 
											value="<?= set_value('email') ? set_value('email') : $this->session->tempdata('RememberAdminEmail') ?>" 
											class="form-control <?=form_error('email') ? 'invalid' : ''?>">
				                		<label for="email">Email</label>
				                		<?=form_error('email', '<div class="red-text small">', '</div>')?>
									</div>

									<div class="md-form">
										<input 
											type="password" 
											name="password" 
											id="password" 
											value="<?= set_value('password') ? '' : $this->session->tempdata('RememberAdminPassword') ?>" 
											class="form-control <?=form_error('password') ? 'invalid' : ''?>">
				                		<label for="password">Şifre</label>
				                		<?=form_error('password', '<div class="red-text small">', '</div>')?>
									</div>

									<div class="custom-control custom-checkbox">
									    <input 
									    	type="checkbox" 
									    	class="custom-control-input" 
									    	name="remember"
									    	id="remember"
									    	<?= $this->session->tempdata('RememberAdminEmail') ? 'checked="checked"' : ''?>>
									    <label class="custom-control-label text-white" for="remember">Beni hatırla</label>
									</div>

									<div class="text-center">
										<button class="btn blue-gradient mt-4"><i class="fa fa-lock"></i> GİRİŞ YAP</button>
									</div>
								</form>

							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>

    <?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
    <?php include(dirname(__DIR__) . '/inc/script.php'); ?>

    <script>

    	new WOW().init();

  	</script>

</body>

</html>