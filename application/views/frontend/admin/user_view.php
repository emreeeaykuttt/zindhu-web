
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Kullanıcılar">
    <meta name="keywords" content="Kullanıcılar">

    <title>Kullanıcılar</title>

    <?php include('inc/head.php'); ?>
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/addons/datatables.min.css">

</head>

<body class="fixed-sn navy-blue-skin dark-bg-admin">

	<?php include('inc/header.php'); ?>

	<!-- Main layout -->
	<main>
		<div class="container-fluid">

			<section>

		        <!-- Gird column -->
		        <div class="col-md-12">

		         	<h5 class="my-4 white-text font-weight-bold" id="module-title"></h5>

		         	<div class="card mb-2">
		         		<div class="card-body" style="display: none;" id="user-filter">
		         			<div class="row">
		         				
		         				<div class="col-md-3 mt-4">
		         					<select class="mdb-select" id="personal-information-position-id" searchable="Arama">
						                <option value="" selected>Hepsi</option>
						            </select>
						            <label class="mdb-main-label">Pozisyon:</label>
		         				</div>

		         				<div class="col-md-2 mt-4">
		         					<select class="mdb-select" id="personal-information-city-id" searchable="Arama">
						                <option value="" selected>Hepsi</option>
						            </select>
						            <label class="mdb-main-label">Yaşadığı Şehir:</label>
		         				</div>

		         				<div class="col-md-2 mt-4">
		         					<select class="mdb-select" id="personal-information-city-json" searchable="Arama">
						                <option value="" selected>Hepsi</option>
						            </select>
						            <label class="mdb-main-label">Yaşamak İstediği Şehir:</label>
		         				</div>

		         				<div class="col-md-2 mt-4">
		         					<select class="mdb-select" id="education-level">
						                <option value="" selected>Hepsi</option>
						                <option value="on-lisans">Ön Lisans</option>
						                <option value="lisans">Lisans</option>
						                <option value="yuksek-lisans">Yüksek Lisans</option>
						                <option value="doktora">Doktora</option>
						            </select>
						            <label class="mdb-main-label">Eğitim Seviyesi:</label>
		         				</div>

		         				<div class="col-md-3 mt-4">
		         					<select class="mdb-select" id="education-university-id" searchable="Arama">
						                <option value="" selected>Hepsi</option>
						            </select>
						            <label class="mdb-main-label">Üniversite:</label>
		         				</div>

		         				<div class="col-md-3 mt-4">
		         					<select class="mdb-select" id="used-program-id" searchable="Arama">
						                <option value="" selected>Hepsi</option>
						            </select>
						            <label class="mdb-main-label">Kullanılan Program:</label>
		         				</div>

		         				<div class="col-md-3 mt-4">
		         					<select class="mdb-select" id="used-program-duration" searchable="Arama">
						                <option value="" selected>Hepsi</option>
						                <?php for ($i=1; $i <= 30; $i++) { ?>
	                                    	<option value="<?=$i?>"><?=$i?>+ Yıl</option>
	                                    <?php } ?>
						            </select>
						            <label for="used-program-duration" class="mdb-main-label">Program Kullanma Süresi:</label>
		         				</div>

		         				<div class="col-md-3 mt-4">
		         					<select class="mdb-select" id="game-future-json" searchable="Arama">
						                <option value="" selected>Hepsi</option>
						            </select>
						            <label class="mdb-main-label">Gelecekte üretiminde bulunmak istediğiniz tür:</label>
		         				</div>

		         				<div class="col-md-3 mt-4">
		         					<select class="mdb-select" id="game-past-json" searchable="Arama">
						                <option value="" selected>Hepsi</option>
						            </select>
						            <label class="mdb-main-label">Geçmişte üretiminde bulunduğunuz tür:</label>
		         				</div>

		         			</div>
		         		</div>
		         	</div>

			        <div class="card">
			            <div class="card-body">
			            	<div class="table-responsive">
				              	<table id="user-table" class="table table-striped" cellspacing="0" width="100%">
				                	<thead>
					                 	<tr>
											<th>Ad Soyad</th>
											<th>Pozisyon</th>
											<th>Şehir</th>
											<?php  
												if ($role == 'superadmin' && $this->input->get('cv') == 'confirm') {
													$th_width = '580';
												} else if ($role == 'superadmin' && $this->input->get('contact')) {
													$th_width = '480';
												} else if ($this->input->get('hires')) {
													$th_width = '485';
												} else if ($role == 'superadmin') {
													$th_width = '390';
												} else {
													$th_width = '320';
												}
											?>
											<th width="<?=$th_width?>">İşlemler</th>
										</tr>
				                	</thead>
			              		</table>
			              	</div>
			            </div>
			         </div>

		        </div>
		        <!-- Gird column -->

		    </section>

		</div>
	</main>
	<!-- Main layout -->

    <?php include('inc/footer.php'); ?>

    <!-- CV View Modal -->
    <div class="modal fade" id="cv-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
      aria-hidden="true">
		<div class="modal-dialog modal-notify modal-success modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead">Görüntüleme</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="white-text">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="cv-list">

				</div>
			</div>
			<!-- Content -->
		</div>
    </div>
    <!-- CV View Modal -->

    <!-- Contact View Modal -->
    <div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
      aria-hidden="true">
		<div class="modal-dialog modal-top modal-notify modal-warning modal-xl" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead">Mesajlaşma Ekranı</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="white-text">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="contact-view">

					<div class="list-group-item mb-1 conversation-info" id="contact-info">
						
					</div>

					<div class="refresh-btn text-right">
						<i class="fas fa-sync-alt" id="refresh-btn" onclick="start_contact(0)" data-toggle="tooltip" title="Yenile"></i>
					</div>

					<div class="list-group-item mb-3 conversation-area" id="contact-list">
						<!-- contact list -->
					</div>

					<div class="list-group-item">
						<div class="md-form m-0">
		                	<textarea type="text" name="message" id="message" class="md-textarea form-control" rows="1"></textarea>
		                	<label for="message">Mesaj At</label>
		                	<div class="red-text small error-text" id="message_error"></div>
		                	<a href="javascript:void(0)" onclick="send_message(0)" id="message-btn" class="btn btn-warning ml-0 w-100">GÖNDER</a>
		              	</div>
					</div>

				</div>
			</div>
			<!-- Content -->
		</div>
    </div>
    <!-- Contact View Modal -->

    <!-- Note View Modal -->
    <div class="modal fade" id="note-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
      aria-hidden="true">
		<div class="modal-dialog modal-notify modal-info modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead">Not</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="white-text">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="note-list">

					<div class="list-group-item mb-1" id="note-user-info">
						
					</div>

					<div class="list-group-item">
						<div class="md-form m-0">
		                	<textarea type="text" name="note" id="note" class="md-textarea form-control" rows="3"></textarea>
		                	<label for="note">Not Alanı</label>
		                	<div class="red-text small error-text" id="note_error"></div>
		                	<a href="javascript:void(0)" onclick="add_note(0)" id="note-btn" class="btn btn-info ml-0 w-100">KAYDET</a>
		              	</div>
					</div>

				</div>
			</div>
			<!-- Content -->
		</div>
    </div>
    <!-- Note View Modal -->

    <!-- Hires View Modal -->
    <div class="modal fade" id="hires-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
      aria-hidden="true">
		<div class="modal-dialog modal-notify modal-warning modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead">Kullanıcının Zindhu Üzerinden Çalıştığı Stüdyolar</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="white-text">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="hires-list">

					<div class="list-group-item mb-1" id="hires-user-info">
						
					</div>

					<div class="list-group-item" id="hires-companies">
						
					</div>

				</div>
			</div>
			<!-- Content -->
		</div>
    </div>
    <!-- Hires View Modal -->

    <?php include('inc/script.php'); ?>
    <script type="text/javascript" src="<?=base_url()?>assets/admin/js/addons/datatables.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/admin/js/cv_helper.js"></script>

    <script type="text/javascript">
    	
		var table;
	    var export_name = module_export_name('cv_havuzu');
	    var cv_new_status = getUrlParameter('cv');
	    var contact_status = getUrlParameter('contact');
	    var hires_status = getUrlParameter('hires');
	    var ad_param = getUrlParameter('ad');
	    var params = '';

	    position_list();
	    city_list();
	    university_list();
	    program_list();
	    game_list();
		
		$(document).ready(function(){

			if (cv_new_status) {
				params = '?cv=' + cv_new_status;
				if (cv_new_status == 'new') {
					$('#module-title').html('Yeni CV’ler');
				} else if (cv_new_status == 'confirm') {
					$('#module-title').html('Onaya Düşen Üyeleri Onaylama/Reddetme');
				}
			} else if (contact_status) {
				params = '?cv=old&contact=1';
				$('#module-title').html('Kullanıcılardan Gelen Mesajlar');
			} else if (hires_status) {
				params = '?hires=1';
				$('#module-title').html('Kullanıcıların Zindhu Üzerinden Çalıştığı Stüdyolar');
			} else if (ad_param) {
				params = '?ad=' + ad_param;
				$('#module-title').html('İlana Başvuran Kişiler <span id="ad-title"></span>');

				$.ajax({
			        url : base_url + 'admin/ad/ajax_view/' + ad_param,
			        type: 'GET',
			        dataType: 'JSON',
			        success: function(data)
			        {
			        	$('#ad-title').html('(İlan: ' + data.title + ')');
			        }
			    });
			} else {
				$('#module-title').html('Kullanıcılar');
				params = '?cv=old';
			}

			$('#module-title').append(' <small id="filter-btn">(Filtreler <i class="fas fa-angle-down"></i>)</small>')

		    table = $('#user-table').DataTable({

		    	'language': {
		            'url': base_url + 'assets/admin/js/addons/language_tr.json'
		        },

		        'processing': true,
		        'serverSide': true,
		        'order': [],
		        'lengthMenu': [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'Tüm']],
		        'ajax': {
		            'url': base_url + 'admin/user/ajax_list' + params,
		            'type': 'POST'
		        },

		        'dom': '<"w-50 float-left"l><"w-50 float-left"fB><"table-content" tr><"w-50 float-left"i><"w-50 float-left"p>',

				'buttons': [
					{	
						extend: 'colvis', 
						text: 'Sütünlar'
					},
					{
						extend: 'csv', 
						text: 'CSV', 
						title: export_name, 
						footer: true, 
						exportOptions: {columns: ':visible(:not(.not-export-col))'}
					},
					{
						extend: 'pdf', 
						text: 'PDF', 
						title: export_name, 
						footer: true, 
						exportOptions: {columns: ':visible(:not(.not-export-col))'}
					},
				],

		  		'columns': [
		            { 'data': 'firstname' },
		            { 'data': 'position' },
		            { 'data': 'city' },
		            { 'data': 'transactions', 'orderable': false}
		        ]

		    });

		    setTimeout(function() {
		    	$('[data-toggle="tooltip"]').tooltip();
		    	$('.dataTables_filter input').on('keyup', function() {
			    	setTimeout(function() {
				    	$('[data-toggle="tooltip"]').tooltip();
				    }, 700);
				});
		    }, 700);

		    $('#education-level, #used-program-duration').materialSelect();

		    $('#personal-information-position-id, #personal-information-city-id, #personal-information-city-json, #education-level, #education-university-id, #used-program-id, #used-program-duration, #game-future-json, #game-past-json').change(function(){
		    	user_search();
		    });

		    $('#filter-btn').click(function(){
		    	$('#user-filter').toggle();
		    });

		});

		function user_search()
		{
			let position_id = $('#personal-information-position-id').val();
			let city_id = $('#personal-information-city-id').val();
			let city_json = $('#personal-information-city-json').val();
			let education_level = $('#education-level').val();
			let university_id = $('#education-university-id').val();
			let program_id = $('#used-program-id').val();
			let program_duration = $('#used-program-duration').val();
			let game_future = $('#game-future-json').val();
			let game_past = $('#game-past-json').val();

			table.ajax.url(base_url + 
		        'admin/user/ajax_list' + params +
		        '&position_id=' + position_id +
		        '&city_id=' + city_id +
		        '&city_json=' + city_json +
		        '&education_level=' + education_level +
		        '&university_id=' + university_id +
		        '&program_id=' + program_id + 
		        '&program_duration=' + program_duration +
		        '&game_future=' + game_future + 
		        '&game_past=' + game_past
		    ).load();

		    setTimeout(function() {
		    	$('[data-toggle="tooltip"]').tooltip();
		    }, 700);
		}

		function view(user_id)
		{
			$('#cv-list').html(loading_render());
			$('#cv-modal').modal('show');

			$.ajax({
		        url : base_url + 'admin/ajax/cv_view/' + user_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	$('#cv-list').html('');
		            $('#cv-list').append(
		            	'<div id="personal-information-list"></div>' +
		            	'<div id="educational-background-list"></div>' +
		            	'<div id="work-experience-list"></div>' +
		            	'<div id="used-program-list"></div>' +
		            	'<div id="game-choice-list"></div>' +
		            	'<div id="portfolio-list"></div>'
		            );

		            $('#personal-information-list').append('<h6>Kişisel Bilgiler</h6>');
		            $('#educational-background-list').append('<h6 class="mt-4">Eğitim Geçmişi</h6>');
		            $('#work-experience-list').append('<h6 class="mt-4">Geçmiş Deneyimler</h6>');
		            $('#used-program-list').append('<h6 class="mt-4">Kullanılan Programlar</h6>');
		            $('#game-choice-list').append('<h6 class="mt-4">Üretimine İlgi Duyduğun Türler</h6>');
		            $('#portfolio-list').append('<h6 class="mt-4">Portfolyo</h6>');

		            $.each(data.personal_information, function(key, personal){

		            	let cities_text = '';
		            	let remote_text = '';
		            	let city_text = '';
                    
	                    $.each(cities_render(personal.city_json), function(key, val){
	                        cities_text += val['city'] + ', ';
	                    });
	                    cities_text = cities_text.replace(/,\s*$/, "");

	                    if (personal.is_remote == '1') {
	                        remote_text = 'İstiyor';
	                    } else if (personal.is_remote == '0') {
	                        remote_text = 'İstemiyor';
	                    } else {
	                        remote_text = 'Belirtilmemiş';
	                    }

	                    if (personal.city_id == 341) {
	                        city_text = ' - ' + personal.city_text;
	                    }

		            	$('#personal-information-list').append(
		            		'<div class="list-group-item">' +
	                            '<div>Ad Soyad: <b>'+ data.user.firstname + ' ' + data.user.lastname +'</b></div>' +
	                            '<div>CV Oluşturma Tarihi: <b>'+ data.user.cv_created_at +'</b></div>' +
	                            '<div>Email: <b>'+ data.user.email +'</b></div>' +
	                            '<div>Telefon: <b>'+ personal.phone +'</b></div>' +
	                            '<div>Pozisyon: <b>'+ position_render(personal.position_id) +'</b></div>' +
	                            '<div>Yaşadığın Şehir: <b>'+ city_render(personal.city_id) + city_text +'</b></div>' +
	                            '<div>Çalışmak İstediğiniz Şehirler: <b>'+ cities_text +'</b></div>' +
	                            '<div>Uzaktan Çalışma Durumu: <b>'+ remote_text +'</b></div>' +
	                        '</div>'
		            	);
		            });

		            let university_text = '';
		            let end_year_text = '';
		            $.each(data.educational_background, function(key, education){
		            	end_year_text = education.continues == 1 ? 'Devam Ediyor' : education.end_year;
		            	if (education.university_id == 0) {
	                        university_text = education.university_text;
	                    } else {
	                        university_text = university_render(education.university_id);
	                    }
	                    
		            	$('#educational-background-list').append(
		            		'<div class="list-group-item">' +
	                            '<div><b>' + education_level_render(education.education_level) + '</b> - <b>' + university_text + '</b> (' + education.university_section + ')</div>' +
	                            '<div>Başlangıç Yılı: ' + education.start_year + ' - Bitiş Yılı: ' + end_year_text + '</div>' +
	                        '</div>'
		            	);
		            });

		            $.each(data.work_experience, function(key, work){
		            	let end_year_text = work.working == 1 ? 'Hala Çalışıyorum' : work.end_year;
	                    
		            	$('#work-experience-list').append(
		            		'<div class="list-group-item">' +
	                            '<div><b>'+work.company_position+'</b></div>' +
	                            '<div>'+work.company_name+' ('+work.company_sector + ')' +'</div>' +
	                            '<div>' +
	                                date_render(work.start_month, work.start_year) +
	                                ' - ' +
	                                end_date_render(work.end_month, work.end_year) +
	                            '</div>' +
	                            '<div class="work-detail">'+work.detail +'</div>' +
	                        '</div>'
		            	);
		            });

		            $.each(data.used_program, function(key, program){
		            	$('#used-program-list').append(
		            		'<div class="list-group-item">' +
	                            '<div>Program: <b>'+ program_render(program.program_id) +'</b></div>' +
                        		'<div>Kullanma Süresi: <b>'+ program.duration +' Yıl</b></div>' +
	                        '</div>'
		            	);
		            });

		            $.each(data.game_choice, function(key, game){
		            	let future_text = '';
	                	let past_text = '';
	                	
	                	$.each(game_render(game.future_json), function(key, val){
	                		future_text += val['title'] + ', ';
	                	});
	                	future_text = future_text.replace(/,\s*$/, "");

	                	$.each(game_render(game.past_json), function(key, val){
	                		past_text += val['title'] + ', ';
	                	});
	                	past_text = past_text.replace(/,\s*$/, "");

	                    $('#game-choice-list').append(
	                        '<div class="list-group-item">' +
	                            '<div>Gelecekte üretiminde bulunmak istediğiniz türler: <b>'+ future_text +'</b></div>' +
	                            '<div>Geçmişte üretiminde bulunduğunuz türler: <b>'+ past_text +'</b></div>' +
	                        '</div>'
	                    );
		            });

		            if (data.portfolio) {
		            	let portfolio_name;
		            	let portfolio_link;
		            	$.each(data.portfolio, function(key, portfolio){
		            		portfolio_name = '';
		            		portfolio_link = '';
			            	if (portfolio.name) {
			            		portfolio_name = '<div>Dosya: <a href="'+base_url+upload_folder+'portfolios/'+portfolio.name+'" target="_blank"><b>'+ portfolio.name +'</b></a></div>';
			            	}
			            	if (portfolio.link) {
			            		portfolio_link = '<div>Link: <a href="'+portfolio.link+'" target="_blank"><b>'+ portfolio.link +'</b></a></div>';
			            	}

			            	$('#portfolio-list').append(
			            		'<div class="list-group-item">' +
		                            portfolio_name +
		                            portfolio_link +
		                        '</div>'
			            	);
			            });
		            }

		        }
		    });
		}

		function hires(user_id)
		{
			$('#hires-user-info').html(loading_render());
			$('#hires-modal').modal('show');

			$.ajax({
		        url : base_url + 'admin/ajax/hires_user/' + user_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	$('#hires-user-info, #hires-companies').html('');

		        	let hires_total = data.hires.length;
		        	$.each(data.hires, function(key, val){
		        		$('#hires-companies').append('<div>Stüdyo: <b>'+ val.company_name + ' ' + time_difference_find(val.startdate, val.enddate) + '</b></div>');
		        		$('#hires-companies').append('<div>İşe Başlangıç Tarihi: <b>'+ val.startdate +'</b></div>');

		        		if (val.enddate) {
		        			$('#hires-companies').append('<div>İşten Ayrılma Tarihi: <b>'+ val.enddate +'</b></div>');
		        		} else {
		        			$('#hires-companies').append('<div>İşten Ayrılma Tarihi: <b>Çalışmaya devam ediyor</b></div>');
		        		}

		        		if (key < (hires_total-1)) {
		        			$('#hires-companies').append('<hr>');
		        		}
		        	});

		        	let cities_text = '';
                    $.each(cities_render(data.personal_information[0].city_json), function(key, val){
                        cities_text += val['city'] + ', ';
                    });
                    cities_text = cities_text.replace(/,\s*$/, "");

		        	$('#hires-user-info').html(
		        		'<div>Ad Soyad: <b>'+ data.user.firstname + ' ' + data.user.lastname +'</b></div>' +
						'<div>Pozisyon: <b>'+ position_render(data.personal_information[0].position_id) +'</b></div>' +
						'<div>Email: <b>'+ data.user.email +'</b>  / <span id="c-phone">Telefon: <b>'+ data.personal_information[0].phone +'</b></span></div>' +
						'<div>Yaşadığı Şehir: <b>'+ city_render(data.personal_information[0].city_id)+'</b> / <span id="c-cities">Çalışmak İstediği Şehirler: <b>'+ cities_text +'</b></span></div>'
		        	);
		        }
		    });
		}

		function view_note(user_id)
		{
			$('#note-user-info').html(loading_render());
			$('#note-modal').modal('show');
			$('#note-btn').attr('onclick', 'add_note('+ user_id +')');

			$.ajax({
		        url : base_url + 'admin/ajax/view_note/' + user_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	$('#note-user-info').html('');

		        	$('[name="note"]').val(data.user.note).trigger('change');

		        	let cities_text = '';
                    $.each(cities_render(data.personal_information[0].city_json), function(key, val){
                        cities_text += val['city'] + ', ';
                    });
                    cities_text = cities_text.replace(/,\s*$/, "");

		        	$('#note-user-info').html(
		        		'<div>Ad Soyad: <b>'+ data.user.firstname + ' ' + data.user.lastname +'</b></div>' +
						'<div>Pozisyon: <b>'+ position_render(data.personal_information[0].position_id) +'</b></div>' +
						'<div>Email: <b>'+ data.user.email +'</b>  / <span id="c-phone">Telefon: <b>'+ data.personal_information[0].phone +'</b></span></div>' +
						'<div>Yaşadığı Şehir: <b>'+ city_render(data.personal_information[0].city_id)+'</b> / <span id="c-cities">Çalışmak İstediği Şehirler: <b>'+ cities_text +'</b></span></div>'
		        	);
		        }
		    });
		}

		function add_note(user_id)
		{
			$('#note-btn').text('KAYDEDİLİYOR...');
		    $('#note-btn').attr('disabled',true);

		    let form_data = {
		        user_id: user_id,
		        note: $('[name="note"]').val(),
		    }

		    $.ajax({
		        url : base_url + 'admin/ajax/add_note',
		        type: 'POST',
		        data: form_data,
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	$('#note-modal').modal('hide');

		            if(data.status) {
		                toastr.success(data.message);
		                reload_table();
		            } else {
		                toastr.error(data.message);
		            }

		            $('[name="note"]').val('').trigger('change');
		            $('#note-btn').text('KAYDET');
		            $('#note-btn').attr('disabled', false);
		        }
		    });
		}

		function remove(user_id)
		{
			Swal.fire({
		        title: 'Silmek istiyor musunuz?',
		        text: 'Sil dediğinizde kullanıcıya ait tüm bilgiler silinir',
		        icon: 'success',
		        showCancelButton: true,
		        confirmButtonColor: '#3085d6',
		        cancelButtonText: 'Vazgeç',
		        cancelButtonColor: '#d33',
		        confirmButtonText: 'Sil'
		    }).then((result) => {
		        if (result.value) 
		        {
		        	let form_data = {
				        user_id: user_id
				    }

				    $.ajax({
				        url : base_url + 'admin/ajax/user_remove',
				        type: 'POST',
				        data: form_data,
				        dataType: 'JSON',
				        success: function(data)
				        {
				            toastr.success(data.message);
				            reload_table();
				        }
				    });
		        }
    		});
		}

		function approve(user_id)
		{
			Swal.fire({
		        title: 'Onaylıyor musunuz?',
		        icon: 'success',
		        showCancelButton: true,
		        confirmButtonColor: '#3085d6',
		        cancelButtonText: 'Vazgeç',
		        cancelButtonColor: '#d33',
		        confirmButtonText: 'Onayla'
		    }).then((result) => {
		        if (result.value) 
		        {
		        	let form_data = {
				        user_id: user_id
				    }

				    $.ajax({
				        url : base_url + 'admin/ajax/user_approve',
				        type: 'POST',
				        data: form_data,
				        dataType: 'JSON',
				        success: function(data)
				        {
				            toastr.success(data.message);
				            reload_table();
				        }
				    });
		        }
    		});
		}

		function rejected(user_id)
		{
			Swal.fire({
		        title: 'Reddetmek istediğinize emin misiniz?',
		        icon: 'warning',
		        showCancelButton: true,
		        confirmButtonColor: '#3085d6',
		        cancelButtonText: 'Vazgeç',
		        cancelButtonColor: '#d33',
		        confirmButtonText: 'Reddet'
		    }).then((result) => {
		        if (result.value) 
		        {
		        	let form_data = {
				        user_id: user_id
				    }

				    $.ajax({
				        url : base_url + 'admin/ajax/user_rejected',
				        type: 'POST',
				        data: form_data,
				        dataType: 'JSON',
				        success: function(data)
				        {
				            toastr.success(data.message);
				            reload_table();
				        }
				    });
		        }
    		});
		}

		function send_message(user_id)
		{
			$('#message-btn').text('GÖNDERİLİYOR...');
		    $('#message-btn').attr('disabled',true);

		    let form_data = {
		        user_id: user_id,
		        message: $('[name="message"]').val(),
		    }

		    $.ajax({
		        url : base_url + 'admin/contact/send_message_user',
		        type: 'POST',
		        data: form_data,
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if(data.status) {
		                toastr.success(data.message);

		                $('#contact-list').append(
			        		'<div class="card mb-3 text-right success-color">' +
				              	'<div class="card-body">' +
				                	'<p class="card-text white-text">'+ data.record.content + '<br />' +
				                	'<small class="text-muted white-text">'+ time_render(data.record.created_at) +'</small></p>' +
				              	'</div>' +
				            '</div>'
			        	);

	                    $('#contact-list').animate({scrollTop : $('#contact-list').get(0).scrollHeight }, 1000);
		            } else {
		                toastr.error(data.message);
		            }

		            $('[name="message"]').val('').trigger('change');
		            $('#message-btn').text('GÖNDER');
		            $('#message-btn').attr('disabled', false);
		        }
		    });
		}

		function start_contact(user_id)
		{
			$('#contact-list, #contact-info').html('');
			$('#contact-list, #contact-info').html(loading_render());

			$('#contact-modal').modal('show');
			$('#message-btn').attr('onclick', 'send_message('+ user_id +')');
			$('#refresh-btn').attr('onclick', 'start_contact('+ user_id +')');
			
			$.ajax({
		        url : base_url + 'admin/contact/start_contact_user/' + user_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	$('#contact-list, #contact-info').html('');

		        	let cities_text = '';
                    $.each(cities_render(data.personal_information[0].city_json), function(key, val){
                        cities_text += val['city'] + ', ';
                    });
                    cities_text = cities_text.replace(/,\s*$/, "");

		        	$('#contact-info').html(
		        		'<div id="c-fullname">Ad Soyad: <b>'+ data.user.firstname + ' ' + data.user.lastname +'</b></div>' +
						'<div id="c-position">Pozisyon: <b>'+ position_render(data.personal_information[0].position_id) +'</b></div>' +
						'<div id="c-email">Email: <b>'+ data.user.email +'</b>  / <span id="c-phone">Telefon: <b>'+ data.personal_information[0].phone +'</b></span></div>' +
						'<div id="c-city">Yaşadığı Şehir: <b>'+ city_render(data.personal_information[0].city_id)+'</b> / <span id="c-cities">Çalışmak İstediği Şehirler: <b>'+ cities_text +'</b></span></div>'
		        	);

		        	if (!data.messages) {
		        		$('#contact-list').append(
			        		'<div class="card mb-3">' +
				              	'<div class="card-body">' +
				                	'<p class="card-text"><i class="fas fa-info-circle"></i> Görüşmeye başlamak için aşağıdaki "Mesaj At" alanından mesaj yollayabilirsiniz.</p>' +
				              	'</div>' +
				            '</div>'
			        	);
		        	}

		        	let card_direction;
		        	let card_bg_color;
		        	let card_text_color;
		        	$.each(data.messages, function(key, val){
		        		card_direction = '';
		        		card_bg_color = 'border-success';
		        		card_text_color = 'dark-grey-text'
		        		if (val.sender_type == 'admin') {
		        			card_direction = 'text-right';
		        			card_bg_color = 'success-color';
		        			card_text_color = 'white-text';
		        		}

		        		$('#contact-list').append(
			        		'<div class="card mb-3 '+ card_direction + ' ' + card_bg_color +'">' +
				              	'<div class="card-body">' +
				                	'<p class="card-text '+ card_text_color +'">'+ val.content + '<br />' +
				                	'<small class="text-muted '+ card_text_color +'">'+ time_render(val.created_at) +'</small></p>' +
				              	'</div>' +
				            '</div>'
			        	);
		        	});

		        	setTimeout(function(){
                        $('#contact-list').animate({scrollTop : $('#contact-list').get(0).scrollHeight }, 50);
                    }, 100);

		        	unread_contact();
                    reload_table();
		        }
		    });
		}

		function reload_table()
		{
		    table.ajax.reload(null,false);

		    setTimeout(function() {
		    	$('[data-toggle="tooltip"]').tooltip();
		    }, 700);
		}

    </script>

</body>

</html>