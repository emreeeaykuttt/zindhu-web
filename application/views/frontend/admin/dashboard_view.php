<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Panel Dashboard">
    <meta name="keywords" content="Panel Dashboard">

    <title>Panel Dashboard</title>

    <?php include('inc/head.php'); ?>

</head>

<body class="fixed-sn navy-blue-skin dark-bg-admin">

	<?php include('inc/header.php'); ?>

	<!-- Main layout -->
	<main>
		<div class="container-fluid">

			<section style="margin-bottom: 330px !important;">
				<div class="row">

					<div class="col-12">
						<h5 class="my-4 white-text font-weight-bold" id="module-title">DASHBOARD</h5>
					</div>
					
					<div class="col-xl-3 col-md-6 mb-4">

						<div class="card classic-admin-card red lighten-1">
							<div class="card-body">
								<div class="pull-right">
									<i class="fas fa-user-friends"></i>
								</div>
								<p class="white-text">TOPLAM STÜDYO SAYISI</p>
								<h4 class="check" id="dashboard-company-count">0</h4>
							</div>
							<div class="progress">
								<div class="progress-bar bg-white" role="progressbar" style="width: 100%"></div>
							</div>
							<div class="card-body">
								<a href="<?=base_url('admin/company')?>"><p class="white-text">İncele</p></a>
							</div>
						</div>

					</div>

					<div class="col-xl-3 col-md-6 mb-4">

						<div class="card classic-admin-card red lighten-1">
							<div class="card-body">
								<div class="pull-right">
									<i class="fas fa-users"></i>
								</div>
								<p class="white-text">TOPLAM BİREYSEL ÜYELER</p>
								<h4 class="check" id="dashboard-user-count">0</h4>
							</div>
							<div class="progress">
								<div class="progress-bar bg-white" role="progressbar" style="width: 100%"></div>
							</div>
							<div class="card-body">
								<a href="<?=base_url('admin/user')?>"><p class="white-text">İncele</p></a>
							</div>
						</div>

					</div>

					<div class="col-xl-3 col-md-6 mb-4">

						<div class="card classic-admin-card red lighten-1">
							<div class="card-body">
								<div class="pull-right">
									<i class="fas fa-concierge-bell"></i>
								</div>
								<p class="white-text">YENİ CVLER</p>
								<h4 class="check" id="dashboard-new-cv-count">0</h4>
							</div>
							<div class="progress">
								<div class="progress-bar bg-white" role="progressbar" style="width: 100%"></div>
							</div>
							<div class="card-body">
								<a href="<?=base_url('admin/user?cv=new')?>"><p class="white-text">İncele</p></a>
							</div>
						</div>

					</div>

					<div class="col-xl-3 col-md-6 mb-4">

						<div class="card classic-admin-card red lighten-1">
							<div class="card-body">
								<div class="pull-right">
									<i class="fas fa-poll-h"></i>
								</div>
								<p class="white-text">TÜM GÖRÜŞMELER</p>
								<h4 class="check" id="dashboard-conversations-count">0</h4>
							</div>
							<div class="progress">
								<div class="progress-bar bg-white" role="progressbar" style="width: 100%"></div>
							</div>
							<div class="card-body">
								<a href="<?=base_url('admin/company?conversations=1')?>"><p class="white-text">İncele</p></a>
							</div>
						</div>

					</div>

					<div class="col-xl-3 col-md-6 mb-4">

						<div class="card classic-admin-card red lighten-1">
							<div class="card-body">
								<div class="pull-right">
									<i class="fas fa-poll-h"></i>
								</div>
								<p class="white-text">İŞE GİRENLERLER SAYISI</p>
								<h4 class="check" id="dashboard-hires-count">0</h4>
							</div>
							<div class="progress">
								<div class="progress-bar bg-white" role="progressbar" style="width: 100%"></div>
							</div>
							<div class="card-body">
								&nbsp;
							</div>
						</div>

					</div>

					<div class="col-xl-3 col-md-6 mb-4">

						<div class="card classic-admin-card red lighten-1">
							<div class="card-body">
								<div class="pull-right">
									<i class="fas fa-poll-h"></i>
								</div>
								<p class="white-text">İŞTEN ÇIKANLARIN SAYISI</p>
								<h4 class="check" id="dashboard-layoffs-count">0</h4>
							</div>
							<div class="progress">
								<div class="progress-bar bg-white" role="progressbar" style="width: 100%"></div>
							</div>
							<div class="card-body">
								&nbsp;
							</div>
						</div>

					</div>

					<div class="col-xl-3 col-md-6 mb-4">

						<div class="card classic-admin-card red lighten-1">
							<div class="card-body">
								<div class="pull-right">
									<i class="fas fa-poll-h"></i>
								</div>
								<p class="white-text">İLANLAR</p>
								<h4 class="check" id="dashboard-ad-count">0</h4>
							</div>
							<div class="progress">
								<div class="progress-bar bg-white" role="progressbar" style="width: 100%"></div>
							</div>
							<div class="card-body">
								<a href="<?=base_url('admin/ad')?>"><p class="white-text">İncele</p></a>
							</div>
						</div>

					</div>

					<div class="col-xl-3 col-md-6 mb-4">

						<div class="card classic-admin-card red lighten-1">
							<div class="card-body">
								<div class="pull-right">
									<i class="fas fa-poll-h"></i>
								</div>
								<p class="white-text">ONAY BEKLEYEN İLANLAR</p>
								<h4 class="check" id="dashboard-ad-pending-count">0</h4>
							</div>
							<div class="progress">
								<div class="progress-bar bg-white" role="progressbar" style="width: 100%"></div>
							</div>
							<div class="card-body">
								<a href="<?=base_url('admin/ad?confirm=pending')?>"><p class="white-text">İncele</p></a>
							</div>
						</div>

					</div>

				</div>
			</section>

		</div>
	</main>
	<!-- Main layout -->

    <?php include('inc/footer.php'); ?>
    <?php include('inc/script.php'); ?>

    <script type="text/javascript">
    	
    	$(document).ready(function(){

			all_statistics();

		});

		function all_statistics()
		{
			$.ajax({
		        url : base_url + 'admin/ajax/all_statistics',
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            $('#dashboard-company-count').text(data.company_count);
		            $('#dashboard-user-count').text(data.user_count);
		            $('#dashboard-new-cv-count').text(data.new_user_count);
		            $('#dashboard-conversations-count').text(data.conversations_count);
		            $('#dashboard-hires-count').text(data.hires_count);
		            $('#dashboard-layoffs-count').text(data.layoffs_count);
		            $('#dashboard-ad-count').text(data.ad_count);
		            $('#dashboard-ad-pending-count').text(data.ad_pending_count);
		        }
		    });
		}

    </script>

</body>

</html>