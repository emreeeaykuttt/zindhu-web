<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Stüdyolar">
    <meta name="keywords" content="Stüdyolar">

    <title>Stüdyolar</title>

    <?php include('inc/head.php'); ?>
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/addons/datatables.min.css">

</head>

<body class="fixed-sn navy-blue-skin dark-bg-admin">

	<?php include('inc/header.php'); ?>

	<!-- Main layout -->
	<main>
		<div class="container-fluid">

			<section>

		        <!-- Gird column -->
		        <div class="col-md-12">

		         	<h5 class="my-4 white-text font-weight-bold" id="module-title">Stüdyolar</h5>

			        <div class="card">
			            <div class="card-body">
			            	<div class="table-responsive">
				              	<table id="company-table" class="table table-striped" cellspacing="0" width="100%">
				                	<thead>
					                 	<tr>
					                 		<th>Stüdyo Adı</th>
											<th>Ad Soyad</th>
											<th>Email</th>
											<th>Telefon</th>
											<?php  
												if ($this->input->get('hires')) {
													$th_width = '270';
												} else {
													$th_width = '100';
												}
											?>
											<th width="<?=$th_width?>">İşlemler</th>
										</tr>
				                	</thead>
			              		</table>
			              	</div>
			            </div>
			         </div>

		        </div>
		        <!-- Gird column -->

		    </section>

		</div>
	</main>
	<!-- Main layout -->

    <?php include('inc/footer.php'); ?>

    <!-- Company View Modal -->
    <div class="modal fade" id="company-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
      aria-hidden="true">
		<div class="modal-dialog modal-notify modal-success modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead">Görüntüleme</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="white-text">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="company-info">

				</div>
			</div>
			<!-- Content -->
		</div>
    </div>
    <!-- Company View Modal -->

    <!-- Conversations View Modal -->
    <div class="modal fade" id="conversations-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
      aria-hidden="true">
		<div class="modal-dialog modal-notify modal-warning modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead">Stüdyonun Görüştüğü Kişiler</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="white-text">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="conversations-info">
					<div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
					</div>
				</div>
			</div>
			<!-- Content -->
		</div>
    </div>
    <!-- Conversations View Modal -->

    <!-- Contact View Modal -->
    <div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
      aria-hidden="true">
		<div class="modal-dialog modal-top modal-notify modal-warning modal-xl" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead">Mesajlaşma Ekranı</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="white-text">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="contact-view">

					<div class="list-group-item mb-1 conversation-info" id="contact-info">
						
					</div>

					<div class="refresh-btn text-right">
						<i class="fas fa-sync-alt" id="refresh-btn" onclick="start_contact(0)" data-toggle="tooltip" title="Yenile"></i>
					</div>

					<div class="list-group-item mb-3 conversation-area" id="contact-list">
						<!-- contact list -->
					</div>

					<div class="list-group-item">
						<div class="md-form m-0">
		                	<textarea type="text" name="message" id="message" class="md-textarea form-control" rows="1"></textarea>
		                	<label for="message">Mesaj At</label>
		                	<div class="red-text small error-text" id="message_error"></div>
		                	<a href="javascript:void(0)" onclick="send_message(0)" id="message-btn" class="btn btn-warning ml-0 w-100">GÖNDER</a>
		              	</div>
					</div>

				</div>
			</div>
			<!-- Content -->
		</div>
    </div>
    <!-- Contact View Modal -->

    <!-- Hires View Modal -->
    <div class="modal fade" id="hires-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
      aria-hidden="true">
		<div class="modal-dialog modal-notify modal-warning modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead">Stüdyonun Zindhu Üzerinden Çalıştığı Kişiler</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="white-text">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body" id="hires-list">

					<div class="list-group-item mb-1" id="hires-company-info">
						
					</div>

					<div class="list-group-item" id="hires-users">
						
					</div>

				</div>
			</div>
			<!-- Content -->
		</div>
    </div>
    <!-- Hires View Modal -->

    <?php include('inc/script.php'); ?>
    <script type="text/javascript" src="<?=base_url()?>assets/admin/js/addons/datatables.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/admin/js/cv_helper.js"></script>

    <script type="text/javascript">
    	
		var table;
		var conversations_status = getUrlParameter('conversations');
		var contact_status = getUrlParameter('contact');
		var hires_status = getUrlParameter('hires');
	    var params = '';
	    var cache_user_id = 0;
		
		$(document).ready(function(){

			if (conversations_status) {
				params = '?conversations=1';
				$('#module-title').html('Tüm Görüşmeler <small>(Stüdyolar sıralanıyor ve tıklanan stüdyonun görüştüğü kişiler listelenir)</small>');
			} else if (contact_status) {
				params = '?contact=1';
				$('#module-title').html('Stüdyolardan Gelen Mesajlar');
			} else if (hires_status) {
				params = '?hires=1';
				$('#module-title').html('Stüdyoların Zindhu Üzerinden Çalıştığı Kişiler');
			}

		    table = $('#company-table').DataTable({

		    	'language': {
		            'url': base_url + 'assets/admin/js/addons/language_tr.json'
		        },

		        'processing': true,
		        'serverSide': true,
		        'order': [],
		        'lengthMenu': [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'Tüm']],
		        'ajax': {
		            'url': base_url + 'admin/company/ajax_list' + params,
		            'type': 'POST'
		        },

		        'dom': '<"w-50 float-left"l><"w-50 float-left"fB><"table-content" tr><"w-50 float-left"i><"w-50 float-left"p>',

		  		'columns': [
		  			{ 'data': 'company_name' },
		            { 'data': 'firstname' },
		            { 'data': 'email' },
		            { 'data': 'phone' },
		            { 'data': 'transactions', 'orderable': false}
		        ]

		    });

		    $('[data-toggle="tooltip"]').tooltip();
		});

		function hires(company_id)
		{
			$('#hires-company-info').html(loading_render());
			$('#hires-modal').modal('show');

			$.ajax({
		        url : base_url + 'admin/ajax/hires_company/' + company_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	$('#hires-company-info, #hires-users').html('');

		        	let hires_total = data.hires.length;
		        	$.each(data.hires, function(key, val){
		        		$('#hires-users').append('<div>Kullanıcı: <b>'+ val.firstname + ' ' + val.lastname + ' - ' + val.user_email + ' ' + time_difference_find(val.startdate, val.enddate) + '</b></div>');
		        		$('#hires-users').append('<div>İşe Başlangıç Tarihi: <b>'+ val.startdate +'</b></div>');

		        		if (val.enddate) {
		        			$('#hires-users').append('<div>İşten Ayrılma Tarihi: <b>'+ val.enddate +'</b></div>');
		        		} else {
		        			$('#hires-users').append('<div>İşten Ayrılma Tarihi: <b>Çalışmaya devam ediyor</b></div>');
		        		}

		        		if (key < (hires_total-1)) {
		        			$('#hires-users').append('<hr>');
		        		}
		        	});

		        	$('#hires-company-info').html(
		        		'<div>Stüdyo İsmi: <b>'+ data.company.company_name +'</b></div>' +
		        		'<div>Ad Soyad: <b>'+ data.company.firstname + ' ' + data.company.lastname +'</b></div>'
		        	);
		        }
		    });
		}

		function conversations(company_id)
		{
			$('#conversations-info .accordion').html(loading_render());
			$('#conversations-modal').modal('show');

			$.ajax({
		        url : base_url + 'admin/ajax/conversations/' + company_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	$('#conversations-info .accordion').html('');

		        	let count = 0;
		        	$.each(data.users_by_conversation, function(key, val){
		        		count++;
		        		$('#conversations-info .accordion').append(
		        			'<div class="card">' +
		        				'<div class="card-header special-color-dark" role="tab" id="headingOne'+count+'">' +
		        					'<a onclick="conversation_by_user_id('+val.company_id+','+val.id+')" class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne'+count+'" aria-expanded="false" aria-controls="collapseTwo'+count+'">' +
		        						'<h6 class="mb-0 text-white">' +
		        							val.firstname + ' ' + val.lastname + ' <i class="fas fa-angle-down rotate-icon"></i>' +
		        						'</h6>' +
		        					'</a>' +
		        				'</div>' +	
		        				'<div id="collapseOne'+count+'" class="collapse grey lighten-3" role="tabpanel" aria-labelledby="headingOne'+count+'" data-parent="#accordionEx">' +
		        					'<div class="card-body conversation-area conversation-detail-'+val.id+'">' +
		        					'</div>' +		
		        				'</div>' +	
		        			'</div>'
		        		);
		        	});
		        }
		    });
		}

		function conversation_by_user_id(company_id, user_id)
		{
			if (cache_user_id != user_id) {
				$('.conversation-detail-' + user_id).html(loading_render());

				$.ajax({
			        url : base_url + 'admin/ajax/conversation_by_user_id/' + company_id + '/' + user_id,
			        type: 'GET',
			        dataType: 'JSON',
			        success: function(data)
			        {
			        	$('.conversation-detail-' + user_id).html('');

			        	let card_direction;
			        	let card_bg_color;
			        	let card_text_color;
			        	let system_message;
		        		let system_message_float;
			        	$.each(data.conversations, function(key, val){
			        		card_direction = '';
			        		card_bg_color = 'border-success';
			        		card_text_color = 'dark-grey-text'
			        		system_message = '';
		        			system_message_float = 'float-left';

			        		if (val.sender_type == 'user') {
			        			card_direction = 'text-right';
			        			card_bg_color = 'success-color';
			        			card_text_color = 'white-text';
			        			system_message_float = 'float-right';
			        		}

			        		if (val.is_system_message == 1) {
			        			if (val.sender_type == 'user') {
			        				system_message = user_system_message();
				        		} else {
				        			system_message = company_system_message();
				        		}
			        		}

			        		$('.conversation-detail-' + user_id).append(
				        		'<div class="card mb-3 '+ card_direction + ' ' + card_bg_color +'">' +
					              	'<div class="card-body">' +
					                	'<p class="card-text '+ card_text_color +'">'+ val.content + ' ' + '<span class="'+ system_message_float+ '">' + system_message + '</span><br />' +
					                	'<small class="text-muted '+ card_text_color +'">'+ time_render(val.created_at) +'</small></p>' +
					              	'</div>' +
					            '</div>'
				        	);
			        	});

			        	$('.conversation-detail-' + user_id).append(
			        		'<div><small>-Beyaz alan: Stüdyo / -Yeşil alan: Kullanıcı</small></div>'
	        			);

			        	setTimeout(function(){
	                        $('.conversation-detail-' + user_id).animate({scrollTop : $('.conversation-detail-' + user_id).get(0).scrollHeight }, 50);
	                        $('[data-toggle="tooltip"]').tooltip();
	                    }, 100);

			        }
			    });
			}

			cache_user_id = user_id;
		}

		function view(company_id)
		{
			$('#company-info').html(loading_render());
			$('#company-modal').modal('show');

			$.ajax({
		        url : base_url + 'admin/ajax/company_view/' + company_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	$('#company-info').html('');
		        	$('#start-conversation').attr('onclick', 'start_conversation('+company_id+')');

		        	let partners = '';
		        	if (data.company.partners) {
		        		partners = '<tr><td>Kurucu Ortaklar: </td><td><b>'+ data.company.partners +'</b></td></tr>';
		        	}

		        	let number_employees = '';
		        	if (data.company.number_employees) {
		        		number_employees = '<tr><td>Stüdyo Çalışan Sayısı: </td><td><b>'+ data.company.number_employees +'</b></td></tr>';
		        	}

		        	let game_type = '';
		        	if (data.company.game_type) {
		        		game_type = '<tr><td>Stüdyonun İlgili Olduğu Oyun Türü: </td><td><b>'+ game_single_render(data.company.game_type) +'</b></td></tr>';
		        	}

		        	let company_linkedin = '';
		        	if (data.company.company_linkedin) {
		        		company_linkedin = '<tr><td>Stüdyo Linkedin: </td><td><b>'+ data.company.company_linkedin +'</b></td></tr>';
		        	}

		        	let founder_linkedin = '';
		        	if (data.company.founder_linkedin) {
		        		founder_linkedin = '<tr><td>Stüdyo Kurucu Linkedin: </td><td><b>'+ data.company.founder_linkedin +'</b></td></tr>';
		        	}

		            $('#company-info').append(
		            	'<div>' +
	            			'<table class="table ">' +
			            		'<tr><td>Ad Soyad: </td><td><b>'+ data.company.firstname + ' ' + data.company.lastname +'</b></td></tr>' +
			            		'<tr><td>Stüdyo İsmi: </td><td><b>'+ data.company.company_name +'</b></td></tr>' +
			            		partners +
			            		'<tr><td>Stüdyo Mail Adres: </td><td><b>'+ data.company.email +'</b></td></tr>' +
	                            '<tr><td>Stüdyo Telefon Numarası: </td><td><b>'+ data.company.phone +'</b></td></tr>' +
			            		'<tr><td>Stüdyo Lokasyon Şehir-İlçe: </td><td><b>'+ city_render(data.company.city_id) + ' - ' + district_render(data.company.district_id) +'</b></td></tr>' +
			            		number_employees +
			            		game_type +
			            		company_linkedin +
			            		founder_linkedin +
		            		'</table>' +
                        '</div>'
		            );
		        }
		    });
		}

		function send_message(company_id)
		{
			$('#message-btn').text('GÖNDERİLİYOR...');
		    $('#message-btn').attr('disabled',true);

		    let form_data = {
		        company_id: company_id,
		        message: $('[name="message"]').val(),
		    }

		    $.ajax({
		        url : base_url + 'admin/contact/send_message_company',
		        type: 'POST',
		        data: form_data,
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if(data.status) {
		                toastr.success(data.message);

		                $('#contact-list').append(
			        		'<div class="card mb-3 text-right success-color">' +
				              	'<div class="card-body">' +
				                	'<p class="card-text white-text">'+ data.record.content + '<br />' +
				                	'<small class="text-muted white-text">'+ time_render(data.record.created_at) +'</small></p>' +
				              	'</div>' +
				            '</div>'
			        	);

	                    $('#contact-list').animate({scrollTop : $('#contact-list').get(0).scrollHeight }, 1000);
		            } else {
		                toastr.error(data.message);
		            }

		            $('[name="message"]').val('').trigger('change');
		            $('#message-btn').text('GÖNDER');
		            $('#message-btn').attr('disabled', false);
		        }
		    });
		}

		function start_contact(company_id)
		{
			$('#contact-list, #contact-info').html('');
			$('#contact-list, #contact-info').html(loading_render());

			$('#contact-modal').modal('show');
			$('#message-btn').attr('onclick', 'send_message('+ company_id +')');
			$('#refresh-btn').attr('onclick', 'start_contact('+ company_id +')');
			
			$.ajax({
		        url : base_url + 'admin/contact/start_contact_company/' + company_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	$('#contact-list, #contact-info').html('');

		        	$('#contact-info').html(
		        		'<div>Stüdyo Adı: <b>'+ data.company.company_name +'</b></div>' +
                        '<div>Ad Soyad: <b>'+ data.company.firstname + ' ' + data.company.lastname +'</b></div>' +
                        '<div>Email: <b>'+ data.company.email +'</b></div>' +
                        '<div>Telefon: <b>'+ data.company.phone +'</b></div>'
		        	);

		        	if (!data.messages) {
		        		$('#contact-list').append(
			        		'<div class="card mb-3">' +
				              	'<div class="card-body">' +
				                	'<p class="card-text"><i class="fas fa-info-circle"></i> Görüşmeye başlamak için aşağıdaki "Mesaj At" alanından mesaj yollayabilirsiniz.</p>' +
				              	'</div>' +
				            '</div>'
			        	);
		        	}

		        	let card_direction;
		        	let card_bg_color;
		        	let card_text_color;
		        	$.each(data.messages, function(key, val){
		        		card_direction = '';
		        		card_bg_color = 'border-success';
		        		card_text_color = 'dark-grey-text'
		        		if (val.sender_type == 'admin') {
		        			card_direction = 'text-right';
		        			card_bg_color = 'success-color';
		        			card_text_color = 'white-text';
		        		}

		        		$('#contact-list').append(
			        		'<div class="card mb-3 '+ card_direction + ' ' + card_bg_color +'">' +
				              	'<div class="card-body">' +
				                	'<p class="card-text '+ card_text_color +'">'+ val.content + '<br />' +
				                	'<small class="text-muted '+ card_text_color +'">'+ time_render(val.created_at) +'</small></p>' +
				              	'</div>' +
				            '</div>'
			        	);
		        	});

		        	setTimeout(function(){
                        $('#contact-list').animate({scrollTop : $('#contact-list').get(0).scrollHeight }, 50);
                    }, 100);

		        	unread_contact();
                    reload_table();
		        }
		    });
		}

		function reload_table()
		{
		    table.ajax.reload(null,false);
		}

    </script>

</body>

</html>