<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="İş İlanları">

    <title>İş İlanları</title>

    <?php include('inc/head.php'); ?>
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/addons/datatables.min.css">

</head>

<body class="fixed-sn navy-blue-skin dark-bg-admin">

	<?php include('inc/header.php'); ?>

	<!-- Main layout -->
	<main>
		<div class="container-fluid">

			<section>

		        <!-- Gird column -->
		        <div class="col-md-12">

		         	<h5 class="my-4 white-text font-weight-bold" id="module-title">
		         		İş İlanları <a href="?confirm=pending" class="btn blue-gradient btn-sm">ONAY BEKLEYENLER</a>
		         	</h5>

			        <div class="card">
			            <div class="card-body">
			            	<div class="table-responsive">
				              	<table id="ad-table" class="table table-striped" cellspacing="0" width="100%">
				                	<thead>
					                 	<tr>
					                 		<th>Başlık</th>
					                 		<th>Studyo</th>
					                 		<th width="180">Yayınlanma Durumu</th>
											<th width="180">Admin Onay Durumu</th>
											<?php  
												if ($this->input->get('confirm') == 'pending') {
													$th_width = '440';
												} else {
													$th_width = '330';
												}
											?>
											<th width="<?=$th_width?>">İşlemler</th>
										</tr>
				                	</thead>
			              		</table>
			              	</div>
			            </div>
			         </div>

		        </div>
		        <!-- Gird column -->

		    </section>

		</div>
	</main>
	<!-- Main layout -->

    <?php include('inc/footer.php'); ?>

     <!-- Ad Form Modal -->
    <div class="modal fade" id="ad-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
      aria-hidden="true">
		<div class="modal-dialog modal-notify modal-success modal-lg" role="document">
			<!-- Content -->
			<div class="modal-content">
				<!-- Header -->
				<div class="modal-header">
					<p class="heading lead" id="modal-ad-title">İlan</p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="white-text">&times;</span>
					</button>
				</div>

				<!-- Body -->
				<div class="modal-body">
					<form action="#" id="ad-form" enctype="multipart/form-data">

						<input type="hidden" name="ad_id" value="">

						<div class="mb-4">
			                <h5 id="company-name">Stüdyo: <span></span></h5>
			            </div>

						<div class="md-form mt-3">
			                <input type="text" class="form-control" name="title" id="title">
			                <label for="title" class="mdb-main-label">Başlık:</label>
			                <div class="red-text small error-text" id="title_error"></div>
			            </div>

			            <div class="md-form mt-5">
				            <select class="mdb-select md-form disable-toggle-all" name="position_json[]" id="position-json" searchable="Arama" multiple>
				                <option value="0" disabled selected>Seçiniz</option>
				            </select>
				            <label class="mdb-main-label">Pozisyon:</label>
				            <div class="red-text small error-text" id="position_json_error"></div>
				        </div>

				        <div class="md-form mt-5">
				             <select class="mdb-select" name="study_type" id="study-type">
				                <option value="0" disabled selected>Seçiniz</option>
				                <option value="Full-Time">Full-Time</option>
				                <option value="Part-Time">Part-Time</option>
				                <option value="Freelance">Freelance</option>
				            </select>
				            <label class="mdb-main-label">Çalışma Şekli:</label>
				            <div class="red-text small error-text" id="study_type_error"></div>
				        </div>

				        <div class="mt-3">
			            	<label for="short-description" class="mdb-main-label">Kısa Açıklama:</label>
			                <textarea type="text" name="short_description" id="short-description" class="md-textarea form-control" rows="2"></textarea>
			                <div class="red-text small error-text" id="short_description_error"></div>
			            </div>

			            <div class="mt-3">
			            	<label for="job-description" class="mdb-main-label">Detay:</label>
			                <textarea type="text" name="job_description" id="job-description" class="md-textarea form-control editor" rows="6"></textarea>
			                <div class="red-text small error-text" id="job_description_error"></div>
			            </div>

			            <div class="row">
				            
				            <div class="col-md-6">
								<div class="custom-input">
									<label class="mdb-main-label">Şehir:</label>
									<select class="mdb-select" name="city_id" id="city-id" searchable="Arama">
										<option value="0" disabled selected>Seçiniz</option>
									</select>
									<div class="red-text small error-text" id="city_id_error"></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="custom-input">
									<label class="mdb-main-label">İlçe:</label>
									<select class="mdb-select" name="district_id" id="district-id" searchable="Arama">
										<option value="0" disabled selected>Seçiniz</option>
									</select>
									<div class="red-text small error-text" id="district_id_error"></div>
								</div>
							</div>

							<div class="col-md-12 d-none city-text">
								<div class="custom-input mt-4">
									<label for="city-text" class="mdb-main-label">Ülke-Şehir:</label>
									<input type="text" class="form-control" name="city_text" id="city-text">
									<div class="red-text small error-text" id="city_text_error"></div>
								</div>
							</div>

							<div class="col-12">
								<div class="my-3">
									<input type="checkbox" class="form-check-input" name="is_remote" id="is-remote" value="1">
									<label class="form-check-label" for="is-remote">Uzaktan(Remote) Çalışma</label>
									<div class="red-text small error-text" id="is_remote_error"></div>
								</div>
							</div>

						</div>

			            <div class="mt-3">
			            	<label for="status" class="mdb-main-label">Yayınlama Durumu:</label>
							<div class="switch">
							  	<label>
							    	Kapat
							    	<input type="checkbox" name="status" id="status" value="1" checked>
							    	<span class="lever"></span> Açık
							 	</label>
							</div>
			                <div class="red-text small error-text" id="status_error"></div>
						</div>

					</form>
				</div>

				<!-- Footer -->
				<div class="modal-footer justify-content-end">
					<a onclick="save()" id="ad-btn" class="btn btn-dark">Kaydet</a>
				</div>

			</div>
			<!-- Content -->
		</div>
    </div>
    <!-- Ad Form Modal -->

    <?php include('inc/script.php'); ?>
    <script type="text/javascript" src="<?=base_url()?>assets/admin/js/addons/datatables.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/plugins/ckeditor/ckeditor.js"></script>

    <script type="text/javascript">
    	
		var table;
	    var params = '';
	    var confirm = getUrlParameter('confirm');
	    
	    position_list();
	    city_list();
		
		$(document).ready(function(){

		    if (confirm == 'pending') {
				params = '?confirm=' + confirm;
				$('#module-title').html(
					'Onay Bekleyen İş İlanları' +
					'<a href="'+base_url+'admin/ad" class="btn blue-default btn-sm">GERİ DÖN</a>'
				);
			} 

		    table = $('#ad-table').DataTable({

		    	'language': {
		            'url': base_url + 'assets/admin/js/addons/language_tr.json'
		        },

		        'processing': true,
		        'serverSide': true,
		        'order': [],
		        'lengthMenu': [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'Tüm']],
		        'ajax': {
		            'url': base_url + 'admin/ad/ajax_list' + params,
		            'type': 'POST'
		        },

		        'dom': '<"w-50 float-left"l><"w-50 float-left"fB><"table-content" tr><"w-50 float-left"i><"w-50 float-left"p>',

		  		'columns': [
		  			{ 'data': 'title' },
		  			{ 'data': 'company_name' },
		  			{ 
		  				'data': 'status', 
		  				'render': function ( data, type, row, meta ) {
		                    if (data == 1) {
		                        return '<span class="badge badge-success p-2">Aktif</span>'
		                    } else{
		                        return '<span class="badge badge-danger p-2">Pasif</span>'
		                    }
		                }
		  			},
		            { 
		            	'data': 'admin_confirm', 
		            	'render': function ( data, type, row, meta ) {
		                    if (data == 'pending') {
		                        return '<span class="badge badge-info p-2">Bekliyor</span>'
		                    } else if (data == 'approved') {
		                        return '<span class="badge badge-success p-2">Onaylandı</span>'
		                    } else if (data == 'rejected') {
		                        return '<span class="badge badge-danger p-2">Reddedildi</span>'
		                    }
		                }
		            },
		            { 'data': 'transactions', 'orderable': false}
		        ],

		    });

		    $('[data-toggle="tooltip"]').tooltip();

		    CKEDITOR.replace('job-description',{
		        customConfig: base_url + 'assets/plugins/ckeditor/basic_config.js'
		    });

		    $('#city-id').change(function() {
				let city_id = $(this).val();

				if (city_id == 341) {
					$('.city-text').removeClass('d-none');
					$('.city-text').addClass('d-block');
				} else {
					$('.city-text').removeClass('d-block');
					$('.city-text').addClass('d-none');
					$('[name="city_text"]').val('').trigger("change");
				}

				district_list(city_id);
			});

		});

		function add()
		{
			$('#ad-modal').modal('show');
			$('#modal-ad-title').text('İlan Ekle');
			$('#ad-form')[0].reset();
			$('[name="ad_id"]').val('');
			$('.error-text').text('');
			$('#status').attr('checked','checked');
			$('#district-id').html('<option value="0" disabled selected>Seçiniz</option>');
			$('#position-json, #study-type, #city-id, #district-id').materialSelect('destroy');
    		$('#position-json, #study-type, #city-id, #district-id').materialSelect();
    		$('[name="job_description"]').val(CKEDITOR.instances['job-description'].setData(''));
		}

		function edit(ad_id)
		{
			$('#ad-modal').modal('show');
			$('#modal-ad-title').text('İlan Düzenle');
			$('#ad-form')[0].reset();
			$('.error-text').text('');

			$.ajax({
		        url : base_url + 'admin/ad/ajax_view/' + ad_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	$('[name="ad_id"]').val(data.id);
		            $('[name="title"]').val(data.title).trigger("change");
		            $('[name="short_description"]').val(data.short_description).trigger("change");
		            $('[name="position_json[]"]').val(JSON.parse(data.position_json));
		            $('[name="study_type"]').val(data.study_type);
		            CKEDITOR.instances['job-description'].setData(data.job_description);
		            $('#company-name span').text(data.company_name)

		            $('[name="city_id"]').val(data.city_id);
            		$('[name="city_text"]').val(data.city_text).trigger("change");

            		if (data.is_remote == 1) {
		                $('#is-remote').attr('checked', true);
		            }else {
		                $('#is-remote').attr('checked', false);
		            }

            		district_list(data.city_id, data.district_id);

		            $('#position-json, #study-type, #city-id').materialSelect('destroy');
            		$('#position-json, #study-type, #city-id').materialSelect();

            		if (data.city_id == 341) {
						$('.city-text').removeClass('d-none');
						$('.city-text').addClass('d-block');
					} else {
						$('.city-text').removeClass('d-block');
						$('.city-text').addClass('d-none');
						$('[name="city_text"]').val('').trigger("change");
					}

		            if (data.status == 0) {
	                    $('#status').removeAttr('checked');
	                } else if(data.status == 1) {
	                    $('#status').attr('checked','checked');
	                }
		        }
		    });
		}

		function remove(ad_id)
		{
			Swal.fire({
		        title: 'Silmek istiyor musunuz?',
		        text: 'Sil dediğinizde bu ilana ait tüm bilgiler silinir.',
		        icon: 'success',
		        showCancelButton: true,
		        confirmButtonColor: '#3085d6',
		        cancelButtonText: 'Vazgeç',
		        cancelButtonColor: '#d33',
		        confirmButtonText: 'Evet'
		    }).then((result) => {
		        if (result.value) 
		        {
		        	$.ajax({
				        url : base_url + 'admin/ad/ajax_remove/' + ad_id,
				        type: 'POST',
				        dataType: 'JSON',
				        success: function(data)
				        {
				            toastr.success(data.message);
				            reload_table();
				        }
				    });
		        }
    		});
		}

		function save()
		{
			$('[name="job_description"]').val(CKEDITOR.instances['job-description'].getData());
			
			let formData = new FormData($('#ad-form')[0]);

			$('#ad-btn').text('Kaydediyor...');
		    $('#ad-btn').attr('disabled',true);

		    $.ajax({
		        url : base_url + 'admin/ad/ajax_save',
		        type: 'POST',
		        data: formData,
		        contentType: false,
		        processData: false,
		        dataType: 'JSON',
		        success: function(data)
		        {
		        	$('.error-text').text('');
		        	
		            if(data.status) 
		            {
		                $('#ad-modal').modal('hide');
		                $('#ad-form')[0].reset();
		                toastr.success(data.message);
		                reload_table();
		            }
		            else
		            {   
		                for (var i = 0; i < data.inputerror.length; i++) 
		                {
		                    $('#' + data.inputerror[i] + '_error').text(data.error_string[i]); 
		                }
		                
		                $('#ad-form').stop().animate({
		                    scrollTop:0
		                });
		            }

		            $('#ad-btn').text('Kaydet'); 
		            $('#ad-btn').attr('disabled',false);
		        }
		    });
		}

		function approve(ad_id)
		{
			Swal.fire({
		        title: 'Onaylıyor musunuz?',
		        icon: 'success',
		        showCancelButton: true,
		        confirmButtonColor: '#3085d6',
		        cancelButtonText: 'Vazgeç',
		        cancelButtonColor: '#d33',
		        confirmButtonText: 'Onayla'
		    }).then((result) => {
		        if (result.value) 
		        {
		        	let form_data = {
				        ad_id: ad_id
				    }

				    $.ajax({
				        url : base_url + 'admin/ad/ajax_approve',
				        type: 'POST',
				        data: form_data,
				        dataType: 'JSON',
				        success: function(data)
				        {
				            toastr.success(data.message);
				            reload_table();
				        }
				    });
		        }
    		});
		}

		function rejected(ad_id)
		{
			Swal.fire({
		        title: 'Reddetmek istediğinize emin misiniz?',
		        icon: 'warning',
		        showCancelButton: true,
		        confirmButtonColor: '#3085d6',
		        cancelButtonText: 'Vazgeç',
		        cancelButtonColor: '#d33',
		        confirmButtonText: 'Reddet'
		    }).then((result) => {
		        if (result.value) 
		        {
		        	let form_data = {
				        ad_id: ad_id
				    }

				    $.ajax({
				        url : base_url + 'admin/ad/ajax_rejected',
				        type: 'POST',
				        data: form_data,
				        dataType: 'JSON',
				        success: function(data)
				        {
				            toastr.success(data.message);
				            reload_table();
				        }
				    });
		        }
    		});
		}

		function reload_table()
		{
		    table.ajax.reload(null,false);
		}

		function position_list()
		{
		    $.ajax({
		        url : base_url + 'ajax/position',
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if (data != "") 
		            {
		                let count = data.length;
		                $.each(data, function(key, val){
		                    $('#position-json').append('<option value="'+val.id+'">'+val.title+'</option>');
		                    if (key+1 === count) {
		                        setTimeout(function(){
		                            $('#position-json').materialSelect();
		                        }, 100);
		                    }
		                });
		            }
		        }
		    });
		}

		function city_list()
		{
		    $.ajax({
		        url : base_url + 'ajax/city',
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if (data != "") 
		            {
		                let count = data.length;
		                $.each(data, function(key, val){
		                    $('#city-id').append('<option value="'+val.id+'">'+val.city+'</option>');
		                    if (key+1 === count) {
		                        setTimeout(function(){
		                            $('#city-id').materialSelect();
		                        }, 100);
		                    }
		                });
		            }
		        }
		    });
		}

		function district_list(city_id, set_value = '')
		{	
			$('#district-id').html('<option value="0" disabled selected>Seçiniz</option>');

		    $.ajax({
		        url : base_url + 'ajax/district/' + city_id,
		        type: 'GET',
		        dataType: 'JSON',
		        success: function(data)
		        {
		            if (data) 
		            {
		            	let selected = ''; 
		                let count = data.length;
		                $.each(data, function(key, val){
		                	if (set_value == val.id) {
		                		selected = 'selected';
		                	} else {
		                		selected = ''; 
		                	}

		                    $('#district-id').append('<option value="'+val.id+'" '+selected+'>'+val.district+'</option>');
		                    if (key+1 === count) {
	                            $('#district-id').materialSelect();
		                    }
		                });
		            } else {
		            	$('#district-id').materialSelect();
		            }
		        }
		    });
		}

    </script>

</body>

</html>