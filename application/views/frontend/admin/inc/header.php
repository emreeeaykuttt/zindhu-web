<?php  
    $role = $this->session->userdata('AdminRole');
?>

<!--Main Navigation-->
<header>

  <!-- Sidebar navigation -->
    <div id="slide-out" class="side-nav sn-bg-3 fixed">
        <ul class="custom-scrollbar">
            <!-- Logo -->
            <li class="logo-sn waves-effect py-3">
                <div class="text-center">
                    <a href="<?= base_url('zindhu-hr/') ?>" class="pl-0"><img src="<?=base_url('assets/admin/img/zindhu-hr-logo.svg')?>" width="160"></a>
                </div>
            </li>
            <!--/. Logo -->

            <!-- Side navigation links -->
            <li>
                <ul class="collapsible collapsible-accordion">
                    <li>
                        <a href="<?=base_url('admin/dashboard')?>" class="collapsible-header waves-effect arrow-r">
                            <i class="fas fa-tachometer-alt"></i> Dashboard
                        </a>
                    </li>
                    <li class="badge badge-primary pr-2 rounded-0">
                        <a href="<?=base_url('admin/ad')?>" class="collapsible-header waves-effect arrow-r text-white">
                            <i class="fas fa-list"></i> İş İlanları
                        </a>
                    </li>
                    <li>
                        <a href="<?=base_url('admin/company')?>" class="collapsible-header waves-effect arrow-r">
                            <i class="fas fa-user-friends"></i> Stüdyolar
                        </a>
                    </li>
                    <li>
                        <a href="<?=base_url('admin/user')?>" class="collapsible-header waves-effect arrow-r">
                            <i class="fas fa-users"></i> Bireysel Üyeler 
                        </a>
                    </li>
                    <li>
                        <a href="<?=base_url('admin/company?conversations=1')?>" class="collapsible-header waves-effect arrow-r">
                            <i class="fas fa-poll-h"></i> Tüm Görüşmeler
                        </a>
                    </li>
                    <li>
                        <a href="<?=base_url('admin/user?cv=new')?>" class="collapsible-header waves-effect arrow-r">
                            <i class="fas fa-concierge-bell"></i> Yeni CV’ler
                        </a>
                    </li>
                    <li>
                        <a href="<?=base_url('admin/company?hires=1')?>" class="collapsible-header waves-effect arrow-r">
                            <i class="fas fa-people-carry"></i> Stüdyoların Çalıştığı Kişiler
                        </a>
                    </li>
                    <li>
                        <a href="<?=base_url('admin/user?hires=1')?>" class="collapsible-header waves-effect arrow-r">
                            <i class="fas fa-people-carry"></i> Kullanıcıların Çalıştıkları Stüdyolar
                        </a>
                    </li>
                    <?php if ($role == 'superadmin'): ?>
                    <li>
                        <a href="<?=base_url('admin/user?cv=confirm')?>" class="collapsible-header waves-effect arrow-r">
                            <i class="fas fa-calendar-check"></i> Onaya Düşen Üyeler
                        </a>
                    </li>
                    <li>
                        <a href="<?=base_url('admin/post')?>" class="collapsible-header waves-effect arrow-r">
                            <i class="fas fa-clipboard-list"></i> Paylaşımlar
                        </a>
                    </li>
                    <li>
                        <a href="<?=base_url('admin/company?contact=1')?>" class="collapsible-header waves-effect arrow-r">
                            <i class="fas fa-poll-h"></i> Stüdyolardan Gelen Mesajlar
                        </a>
                    </li>
                    <li>
                        <a href="<?=base_url('admin/user?contact=1')?>" class="collapsible-header waves-effect arrow-r">
                            <i class="fas fa-poll-h"></i> Kullanıcılardan Gelen Mesajlar
                        </a>
                    </li>
                    <?php endif ?>
                </ul>
            </li>
            <!--/. Side navigation links -->

        </ul>
        <div class="sidenav-bg mask-strong"></div>
    </div>
    <!--/. Sidebar navigation -->

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg scrolling-navbar double-nav">

        <!-- SideNav slide-out button -->
        <div class="float-left">
            <a href="#" data-activates="slide-out" class="button-collapse text-white"><i class="icon-hamburger-menu-icon"></i></a>
        </div>
        <!-- Breadcrumb-->
        <div class="breadcrumb-dn mr-auto">
            <p>PANEL</p>
        </div>

      <!--Navbar links-->
      <ul class="nav navbar-nav nav-flex-icons ml-auto">

        <!-- Dropdown -->
        <?php if ($role == 'superadmin'): ?>
        <li class="nav-item dropdown notifications-nav">
            <a href="<?=base_url('admin/company?contact=1')?>" class="nav-link waves-effect" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-envelope"></i>
                <span class="d-none d-md-inline-block">Stüdyolar</span>
                <span class="badge red unread-count" id="unread-company-count"></span>
            </a>
        </li>
        <li class="nav-item dropdown notifications-nav">
            <a href="<?=base_url('admin/user?contact=1')?>" class="nav-link waves-effect" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-envelope"></i>
                <span class="d-none d-md-inline-block">Kullanıcılar</span>
                <span class="badge red unread-count" id="unread-user-count"></span>
            </a>
        </li>
        <?php endif ?>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle waves-effect" href="#" id="userDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user"></i> 
                <span class="clearfix d-none d-sm-inline-block">
                    <?=$this->session->userdata('AdminFullname')?>
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="<?=base_url('admin/auth/logout')?>">Çıkış Yap</a>
            </div>
        </li>

      </ul>
      <!--/Navbar links-->
      
    </nav>
    <!-- /.Navbar -->

</header>
<!--Main Navigation-->