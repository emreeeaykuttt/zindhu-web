<script type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/popper.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/mdb.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/pro/js/mdb.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/sweetalert2.all.min.js"></script>
<script type="text/javascript">var base_url = '<?=base_url()?>'; var upload_folder = '<?=UPLOAD_FOLDER?>';</script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/script.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/admin.js"></script>

<script>
	// SideNav Initialization
	$('.button-collapse').sideNav();

	var container = document.querySelector('.custom-scrollbar');
	var ps = new PerfectScrollbar(container, {
		wheelSpeed: 2,
		wheelPropagation: true,
		minScrollbarLength: 20
	});
</script>
