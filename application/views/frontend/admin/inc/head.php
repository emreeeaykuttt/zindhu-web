	<meta name="google-site-verification" content="thk6a3gwobpRoIbHmnCPaA9iPWXVPx6ndUpgiYzRoQQ" />
	<meta name="robots" content="noindex">

	<link rel="icon" href="<?=base_url()?>assets/admin/img/favicon.png" type="image/x-icon">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
	<link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/font-awesome-line-awesome/css/all.min.css">
	<!-- Font CSS -->
	<link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300;400;700&display=swap" rel="stylesheet">
	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/bootstrap.min.css">
	<!-- Material Design Bootstrap -->
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/mdb.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/frontend/fonts/fonts.css" />

	<!-- Main Style -->
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/style.css">
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-175472421-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-175472421-1');
	</script>