<!DOCTYPE html>
<html lang="tr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-site-verification" content="thk6a3gwobpRoIbHmnCPaA9iPWXVPx6ndUpgiYzRoQQ" />
    <meta name="description" content="Zindhu Co, çatısı altında Zindhu HR, Zindhu Games ve Zindhu Ventures olacak şekilde özelleştirilmiş servisler sunan, kaliteli ve sürekliliğe odaklanmış hizmet anlayışıyla danışmanlık veren bir oyun danışmanlık şirketidir.">
    <meta name="keywords" content="Oyun, mobil oyun, oyun stüdyosu, oyun yayıncısı, gaming, mobile gaming, yatırım, indie gaming, danışmanlık">
    <title>Zindhu - Oyun Sektörünün Kalbi Burada Atıyor!</title>
    <meta name="description" content="Oyun sektörünü oluşturan oyun geliştiricilerden,  yayıncılara, oyun monetizasyonu  yöntemlerinden insan kaynaklarına kadar oyun sektöründe 360 çözümler sunan tüm hizmetler Zindhu çatısı altında!" />
    <meta property="og:title" content="Zindhu: Oyun sektörünün kalbi" />
    <meta property="og:description" content="Oyun sektörünü oluşturan oyun geliştiricilerden,  yayıncılara, oyun monetizasyonu  yöntemlerinden insan kaynaklarına kadar oyun sektöründe 360 çözümler sunan tüm hizmetler Zindhu çatısı altında!" />
    <meta property="og:image" content="<?=base_url()?>assets/home/og.jpg">
    <meta property="og:url" content="<?=base_url()?>">
    <meta property="og:type" content="website">
    <meta name="twitter:card" content="summary" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="theme-color" content="#3d00bf" />
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?=base_url()?>assets/home/img/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=base_url()?>assets/home/img/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=base_url()?>assets/home/img/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=base_url()?>assets/home/img/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?=base_url()?>assets/home/img/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?=base_url()?>assets/home/img/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="<?=base_url()?>assets/home/img/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?=base_url()?>assets/home/img/favicon-16x16.png" sizes="16x16" />
    <meta name="application-name" content="Zindhu Games" />
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="<?=base_url()?>assets/home/img/mstile-144x144.png" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-175472421-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-175472421-1');
    </script>
</head>

<body style="background: #320866;">

    <center>
        <img src="<?=base_url('assets/home/img/zindhu-co-logo.svg')?>" style="margin-top: 100px;">
    </center>
   
</body>

</html>