<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Sayfa Bulunamadı</title>
	<link rel="shortcut icon" href="/assets/admin/img/favicon.png">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="/assets/admin/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/plugins/line-awesome/css/line-awesome.min.css" />
	<link rel="stylesheet" href="/assets/frontend/fonts/fonts.css" />
	<style type="text/css">
		body {
			font-family: "Visuelt Pro", sans-serif;
			background: #fff;
			color: black
		}

		._404-wrapper {
			height: 100vh;
		}

		._404-wrapper .inner {
			position: relative;
			z-index: 1;
		}

		._404-wrapper .logo {
			width: 150px;
			object-fit: contain;
			position: absolute;
			top: 20px;
			left: 20px;
			right: 0;
			display: block;
		}
		._404-wrapper .logo img {
			width: 100%;
			object-fit: contain;
		}
		._404-wrapper .social-media {
			position: absolute;
			right: 0;
			top: 0;
			background:black;
			padding:10px 50px
		}
		._404-wrapper .social-media a{
			color:white;
			background:black;
			padding:10px 15px;
			display: inline-block;
			font-size: 25px;
		}
		._404-wrapper ._404-text {
			padding: 20px;
			text-align: center;
			position: relative;
		}
		._404-wrapper ._404-text  h4{
			font-weight:600;
		}
		._404-wrapper ._404-text  p{
			color:gray;
			font-size: 18px;
		}
		@media screen and (max-width: 993px) {
			._404-wrapper .social-media {
			position: absolute;
			right: 0;
			bottom: 0;
			top: auto;
			background:black;
			padding:10px 50px
		}
		}
	</style>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div class="w-100 d-flex _404-wrapper">
		<a href="/zindhu-hr" class="logo">
			<img src="/assets/frontend/img/zindhu-hr-logo.svg" />
		</a>
		<div class="social-media">
			<a href="https://www.linkedin.com/company/zindhu"><i class="la la-linkedin"></i></a>
			<a href="https://www.instagram.com/zindhugames/"><i class="la la-instagram"></i></a>
		</div>
		<div class="m-auto inner">
			<div class="_404">
				<img src="/assets/frontend/img/404_new.svg" class="w-100" />
			</div>
			<div class="_404-text">
				<h4>Aradığın sayfayı bulamadık.</h4>
				<p>Aradığın sayfa linki silinmiş ya da bir yanlışlık olmuş olabilir.</p>			
			</div>
		</div>
	</div>
</body>

</html>