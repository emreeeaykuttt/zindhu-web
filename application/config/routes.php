<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['admin'] = 'admin/dashboard';
$route['user'] = 'user/dashboard';
$route['company'] = 'company/dashboard';
$route['zindhu-hr'] = 'zindhu';
$route['sitemap.xml'] = 'home/sitemap';

$route['zindhu-hr/kullanici'] = 'user/dashboard';
$route['zindhu-hr/kullanici/dashboard'] = 'user/dashboard';
$route['zindhu-hr/kullanici/giris'] = 'user/auth/login';
$route['zindhu-hr/kullanici/kayit-ol'] = 'user/auth/register';
$route['zindhu-hr/kullanici/cikis'] = 'user/auth/logout';
$route['zindhu-hr/kullanici/sifrenizi-mi-unuttunuz'] = 'user/auth/forgot';
$route['zindhu-hr/kullanici/sifre-yenile'] = 'user/auth/reset';
$route['zindhu-hr/kullanici/onay-bekliyor'] = 'user/auth/pending';
$route['zindhu-hr/kullanici/reddedildi'] = 'user/auth/rejected';
$route['zindhu-hr/kullanici/cv'] = 'user/cv';
$route['zindhu-hr/kullanici/zindhu-hr-ulasin'] = 'user/contact';
$route['zindhu-hr/kullanici/konusmalar'] = 'user/conversations';
$route['zindhu-hr/kullanici/post/detay/(:any)'] = 'user/post/detail/$1';
$route['zindhu-hr/kullanici/profil'] = 'user/profile';
$route['zindhu-hr/kullanici/is-ilanlari'] = 'user/ad/listing';
$route['zindhu-hr/kullanici/is-ilanlari/(:any)'] = 'user/ad/listing/$1';
$route['zindhu-hr/kullanici/is-ilani/(:any)'] = 'user/ad/detail/$1';
$route['zindhu-hr/studyo'] = 'company/dashboard';
$route['zindhu-hr/studyo/dashboard'] = 'company/dashboard';
$route['zindhu-hr/studyo/giris'] = 'company/auth/login';
$route['zindhu-hr/studyo/kayit-ol'] = 'company/auth/register';
$route['zindhu-hr/studyo/cikis'] = 'company/auth/logout';
$route['zindhu-hr/studyo/sifrenizi-mi-unuttunuz'] = 'company/auth/forgot';
$route['zindhu-hr/studyo/sifre-yenile'] = 'company/auth/reset';
$route['zindhu-hr/studyo/cv-havuzu'] = 'company/pool';
$route['zindhu-hr/studyo/konusmalar'] = 'company/conversations';
$route['zindhu-hr/studyo/zindhu-hr-ulasin'] = 'company/contact';
$route['zindhu-hr/studyo/profil'] = 'company/profile';
$route['zindhu-hr/studyo/is-ilanlari'] = 'company/ad';

$route['admin/giris'] = 'admin/auth/login';

$route['kullanici/onay-bekliyor'] = 'user/auth/pending';
$route['kullanici/reddedildi'] = 'user/auth/rejected';
$route['kullanici/cv'] = 'user/cv';











