gsap.registerPlugin(ScrollTrigger);
var elem = document.querySelector(".zindhu__scrollbar");
const bodyScrollBar = Scrollbar.init(elem, {
    damping: 0.1,
    delegateTo: document
});
Scrollbar.detachStyle();

$(".menu-inner ul li:first-child a").click(function() {
    bodyScrollBar.scrollIntoView($("#zindhu-co")[0]);
})
$(".menu-inner ul li:nth-child(2) a").click(function() {
    bodyScrollBar.scrollIntoView($("#zindhu-hr")[0]);
})
$(".menu-inner ul li:nth-child(3) a").click(function() {
    bodyScrollBar.scrollIntoView($(".zindhu-games-logo")[0]);
})
$(".menu-inner ul li:nth-child(4) a").click(function() {
    bodyScrollBar.scrollIntoView($("#zindhu-venture")[0]);
})
$(".menu-inner ul li:nth-child(5) a").click(function() {
    bodyScrollBar.scrollIntoView($("#contact")[0]);
})

particlesJS.load('particles-js', 'assets/home/js/particles.json', function() {
    console.log('callback - particles.js config loaded');
});
ScrollTrigger.scrollerProxy(".zindhu__scrollbar", {
    scrollTop(value) {
        if (arguments.length) {
            bodyScrollBar.scrollTop = value; // setter
        }
        return bodyScrollBar.scrollTop; // getter
    },
    getBoundingClientRect() {
        return {
            top: 0,
            left: 0,
            width: window.innerWidth,
            height: window.innerHeight
        };
    }
});

bodyScrollBar.addListener(ScrollTrigger.update);
$(".crew-carousel").owlCarousel({
    loop: true,
    margin: 20,
    autoplay: true,
    autoplayTimeout: 5000,
    smartSpeed: 200,
    mouseDrag: true,
    pullDrag: true,
    autoplayHoverPause: false,
    animateIn: "fadeIn",
    animateOut: "fadeOut",
    navText: [
        "<i class='la la-angle-left'></i>",
        "<i class='la la-angle-right'></i>",
    ],
    nav: true,
    dots: false,
    responsive: {
        0: {
            items: 1,
        },
        600: {
            items: 1,
        },
        1000: {
            items: 4,
        },
    },
});
// Menu Timeline
menuTL = gsap.timeline({
    paused: true
});
menuTL.from("section.zindhu-co>p", {
    scrollTrigger: {
        trigger: "section.zindhu-co",
        scroller: ".zindhu__scrollbar",
        scrub: true,
    },
    y: "200px",
    ease: "slow"
});
var controlit = $('#navToggle');
var menuToggle = gsap.timeline({
    paused: true,
    reversed: true
});
menuToggle
    .add('rotate')
    .to('#menuToggle', .2, {
        rotation: '-90deg',
        transformOrigin: "50% 50%"
    }, 'rotate')
    .to(' #toggleTop', .2, {
        y: '3'
    }, 'rotate')
    .to(' #toggleBottom', .2, {
        y: '-3'
    }, 'rotate')
    .to(' #toggleTop', .2, {
        rotationZ: 45,
        transformOrigin: '50% 50%'
    }, 'rotate')
    .to(' #toggleBottom', .2, {
        rotationZ: -45,
        transformOrigin: '50% 50%'
    }, 'rotate')
    .to(' .header .menu ul li a', .2, {
        bottom: 0,
        stagger: .1,
        ease: "power3.out",
    }, 'rotate')

controlit.click(function() {
    menuToggle.reversed() ? menuToggle.restart() : menuToggle.reverse();
});

// Timeline Section #02
zindhuCoTL = gsap.timeline({
    paused: true
});
zindhuCoTL.to(".main img", {
    scrollTrigger: {
        trigger: "section.zindhu-co",
        scroller: ".zindhu__scrollbar",
        scrub: true,
    },
    y: "-20vh",
    ease: "slow"
});
zindhuCoTL.to(".main .main-bottom", {
    scrollTrigger: {
        trigger: "section.zindhu-co",
        scroller: ".zindhu__scrollbar",
        scrub: true,
    },
    y: "20vh",
    ease: "slow"
});

zindhuCoTL.from("section.zindhu-co .drawings div", {
    scrollTrigger: {
        trigger: "section.zindhu-co",
        scroller: ".zindhu__scrollbar",
        scrub: true,
        start: "top center",
        end: "bottom bottom",
    },
    y: Math.random() * 700,
    ease: "slow",
    stagger: .2
});
zindhuCoTL.from("section.zindhu-co .drawings .line", {
    scrollTrigger: {
        trigger: "section.zindhu-co",
        scroller: ".zindhu__scrollbar",
        scrub: true,
        start: "center center",
        end: "center  center",
    },
    rotate: "-90deg",
    ease: "slow",
    stagger: .2
});
zindhuCoTL.from("section.zindhu-co  .text", {
    scrollTrigger: {
        trigger: "section.zindhu-co",
        scroller: ".zindhu__scrollbar",
        scrub: true,
    },
    y: "20vh",
    ease: "slow"
});


// Timeline Section #03
zindhuHRTL = gsap.timeline({
    paused: false
});

zindhuHRTL.from("section.zindhu-hr .zindhu-hr-logo", {
    scrollTrigger: {
        trigger: "section.zindhu-hr .zindhu-hr-logo",
        scroller: ".zindhu__scrollbar",
        scrub: true,
    },
    y: "1rem",
    ease: "slow"
});
zindhuHRTL.from("section.zindhu-hr .drawings div", {
    scrollTrigger: {
        trigger: "section.zindhu-hr",
        scroller: ".zindhu__scrollbar",
        scrub: true,
        start: "top center",
        end: "bottom bottom",
    },
    y: Math.random() * 700,
    ease: "slow",
    stagger: .2
});
zindhuCoTL.from("section.zindhu-hr .drawings .line", {
    scrollTrigger: {
        trigger: "section.zindhu-hr",
        scroller: ".zindhu__scrollbar",
        scrub: true,
        start: "center center",
        end: "center  center",
    },
    rotate: "-90deg",
    ease: "slow",
    stagger: .2
});
zindhuCoTL.from("section.zindhu-hr .text", {
    scrollTrigger: {
        trigger: "section.zindhu-hr",
        scroller: ".zindhu__scrollbar",
        scrub: true,
    },
    y: "-1rem",
    ease: "slow"
});

// Timeline Section #04
zindhuGamesTL = gsap.timeline({
    paused: false
});

zindhuGamesTL.to("section.zindhu-games .text h2", {
    scrollTrigger: {
        trigger: "section.zindhu-games .text",
        scroller: ".zindhu__scrollbar",
        scrub: true,
        start: "-150px center",
        end: "300px top",
    },
    x: "-70%",
    ease: "slow"
});
zindhuGamesTL.to("section.zindhu-middle .zindhu-middle-bar ", {
    scrollTrigger: {
        trigger: "section.zindhu-middle",
        scroller: ".zindhu__scrollbar",
        scrub: true,
        start: "-150px center",
        end: "300px top",
    },
    y: "20%",
    ease: "slow"
});



// Timeline Section HR
zindhuHRTL = gsap.timeline({
    paused: false
});

zindhuHRTL.from("section.zindhu-venture .zindhu-venture-logo", {
    scrollTrigger: {
        trigger: "section.zindhu-venture .zindhu-venture-logo",
        scroller: ".zindhu__scrollbar",
        scrub: true,
    },
    y: "200px",
    ease: "slow"
});
zindhuHRTL.from("section.zindhu-venture .drawings div", {
    scrollTrigger: {
        trigger: "section.zindhu-venture",
        scroller: ".zindhu__scrollbar",
        scrub: true,
        start: "top center",
        end: "bottom bottom",
    },
    y: Math.random() * 700,
    ease: "slow",
    stagger: .2
});
zindhuCoTL.from("section.zindhu-venture .drawings .line", {
    scrollTrigger: {
        trigger: "section.zindhu-venture",
        scroller: ".zindhu__scrollbar",
        scrub: true,
        start: "center center",
        end: "center  center",
    },
    rotate: "-90deg",
    ease: "slow",
    stagger: .2
});
zindhuCoTL.from("section.zindhu-venture .text", {
    scrollTrigger: {
        trigger: "section.zindhu-venture",
        scroller: ".zindhu__scrollbar",
        scrub: true,
    },
    y: "2rem",
    ease: "slow"
});


// Timeline Section Crew
zindhuHRTL = gsap.timeline({
    paused: false
});


zindhuHRTL.from("section.crew h2", {
    scrollTrigger: {
        trigger: "section.crew",
        scroller: ".zindhu__scrollbar",
        start: "top center",
        scrub: true,
    },
    y: "-50px",
    ease: "slow"
});
zindhuHRTL.from(".footer-icon", {
    scrollTrigger: {
        trigger: "section.crew .crew-carousel",
        scroller: ".zindhu__scrollbar",
        start: "top center",
        end: "-200px bottom",
        scrub: true,
    },
    y: "200px",
    ease: "slow"
});