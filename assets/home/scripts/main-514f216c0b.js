'use strict'
function validateForm(e, t) {
    e.find('.error + span').remove().prev().removeClass('error'),
        e.find('input, textarea').each(function () {
            var t = this
            t.disabled ||
                t.validity.valid ||
                (t.validity.valueMissing
                    ? t.insertAdjacentHTML('afterend', '<span>' + e.data('msg-missing') + '</span>')
                    : t.insertAdjacentHTML('afterend', '<span>' + t.dataset.msg + '</span>'),
                t.classList.add('error'))
        }),
        e.find('.error').length || (t && t())
}
function postData(e, n, a, o, s) {
    a && a.prop('disabled', !0),
        $.post('/service/' + e, n, function (t) {
            console.log(e, n, t),
                a && a.prop('disabled', !1),
                t.status ? o && o(t.data) : !t.data && s ? s(t) : showMessageOverlay(t.description)
        })
}
function clearForm(t) {
    t.find('.error + span').remove().prev().removeClass('error'), t.find('input, textarea').val('')
}
$.fn.marquee = function () {
    this.each(function () {
        for (
            var e = this,
                n = e.querySelector('p'),
                a = n.querySelector('span'),
                t = Math.ceil(e.offsetWidth / a.getBoundingClientRect().width),
                o = '',
                s = 0;
            s < t;
            s++
        )
            o += a.outerHTML
        n.insertAdjacentHTML('beforeend', o)
        var i = 0
        function r() {
            var t = n.style.transform.match(/translateX\((-?\d+\.?\d*px)\)/),
                t = (t = t ? parseFloat(t[1]) : 0) - (e.offsetWidth / 1e3) * 2,
                t = Math.abs(t) > a.offsetWidth ? 0 : t
            ;(n.style.transform = 'translateX(' + t + 'px)'), (i = requestAnimationFrame(r))
        }
        $(window)
            .on('scroll', function () {
                cancelAnimationFrame(i),
                    e.getBoundingClientRect().top < vh && 0 < e.getBoundingClientRect().bottom && (i = requestAnimationFrame(r))
            })
            .trigger('scroll')
    })
}
var vw,
    vh,
    vhFixed,
    resizeTimer,
    lastScrollTop = 0
function updateHomeHeight(t) {
    480 < vw ? $('#home').removeAttr('style') : $('#home').css('max-height', t)
}
function init(t) {
    document.querySelector('h1 > span:last-child span').addEventListener('transitionstart', function () {
        document.querySelector('h1 + p').classList.add('anim')
    }),
        document.querySelector('h1').classList.add('anim'),
        t()
}
gsap.registerPlugin(MotionPathPlugin),
    $(function () {
        ;(vw = $(window).width()),
            (vh = window.innerHeight),
            $(window)
                .on('resize', function () {
                    var t = vw
                    ;(vw = $(window).width()),
                        (vh = window.innerHeight),
                        document.querySelector('h1').classList.contains('anim') &&
                            (clearTimeout(resizeTimer),
                            (resizeTimer = setTimeout(function () {
                                ;(navigator.userAgent.match(/Mobile/) && vw === t) || spinningPlanets(),
                                    768 < vw
                                        ? $('#detail .marq').addClass('vertical')
                                        : $('#detail .marq').removeClass('vertical')
                            }, 200)))
                })
                .trigger('resize')
                .on('scroll', function () {
                    var t = window.pageYOffset
                    0 < t
                        ? ($('header').addClass('compact'),
                          vh < t ? $('header').addClass('hidden') : $('header').removeClass('hidden'))
                        : $('header').removeClass('compact'),
                        t < lastScrollTop && $('header').removeClass('hidden'),
                        (lastScrollTop = Math.max(0, t)),
                        $('#hand')[0].parentNode.getBoundingClientRect().bottom <= vh && $('#hand').addClass('anim')
                    for (var e = $('#about')[0].getBoundingClientRect(), n = 0; n < planets.length; n++) {
                        var a = planets[n].anim
                        null !== a && (e.top < (2 * vh) / 3 && 0 < e.bottom ? a.resume() : a.pause())
                    }
                    $('#vision, #detail, #detail > div').each(function () {
                        var t,
                            e,
                            n = $(this),
                            a = this.getBoundingClientRect()
                        ;(n.data('anim-width') && vw > n.data('anim-width')) || !n.data('anim-width')
                            ? ((t = n.is('section') ? 0 : vh / 2),
                              (e = n.is('section') ? vh : vh / 2),
                              a.top < t && a.bottom > e ? n.addClass('fixed') : n.removeClass('fixed'),
                              a.bottom < e ? n.addClass('end') : n.removeClass('end'))
                            : n.removeAttr('class')
                    })
                })
                .trigger('scroll'),
            init(function () {
                spinningPlanets(),
                    $('.marq').marquee(),
                    autosize($('textarea')),
                    $('#contact').on('click', '.rbtnOLD', submitContactForm),
                    $('#share-game').on('click', '.rbtn', submitGame),
                    $('form').on('input focus', 'input, textarea', function () {
                        // $(this).removeClass('error').next().remove()
                    }),
                    $('.overlay .close, #success .rbtn').on('click', function () {
                        $(this).closest('.overlay').fadeOut()
                    }),
                    $('button[data-overlay]').on('click', function () {
                        $('#' + this.dataset.overlay).fadeIn()
                    }),
                    $('body').on('click', 'button[data-scroll]', function () {
                        var t = this.dataset.scroll,
                            e = $('#' + t).offset().top
                        vw <= 768 &&
                            ('contact' !== t && (e -= vh / 50 + 30),
                            'detail-hr' === t && (e += $('#' + t + ' .sticky').height())),
                            $('html, body').animate({ scrollTop: Math.round(e) }),
                            $('body').removeClass('menu-open')
                    }),
                    $('#home-btn').on('click', function () {
                        $('html, body').animate({ scrollTop: 0 })
                    }),
                    $('body').on('click', '.dd-btn > button', function (t) {
                        var e = $(this).parent().toggleClass('open')
                        e.hasClass('open') &&
                            setTimeout(function () {
                                $(document).one('click', function () {
                                    e.removeClass('open')
                                })
                            }, 100)
                    }),
                    $('#menu').html($('footer').html()),
                    $('#menu > img').replaceWith($('header .dd-btn').clone()),
                    $('#menu-btn').on('click', function () {
                        $('body').toggleClass('menu-open')
                    })
            })
    })
var planets = [
    { id: 1, anim: null, path: 1, start: 0.5, end: 1.5, duration: 8 },
    { id: 2, anim: null, path: 1, start: 0, end: 1, duration: 8 },
    { id: 3, anim: null, path: 2, start: 0, end: 1, duration: 7 },
    { id: 4, anim: null, path: 2, start: 0.49, end: 1.49, duration: 7 }
]
function createPlanetSpin(t) {
    return gsap.to('#ap-' + t.id, {
        duration: t.duration,
        repeat: -1,
        ease: 'none',
        motionPath: { path: '#path-' + t.path, align: '#path-' + t.path, start: -t.start, end: -t.end }
    })
}
function spinningPlanets() {
    if (!window.document.documentMode) {
        for (var t = 0; t < planets.length; t++) {
            var e = 0,
                n = planets[t].anim
            null !== n && ((e = n.progress()), n.kill()),
                (planets[t].anim = createPlanetSpin(planets[t])),
                planets[t].anim.progress(e),
                planets[t].anim.pause()
        }
        $(window).trigger('scroll')
    }
}
function showSuccessMessage(t) {
    $('#success').fadeIn().find('p').text(t)
}
function submitContactForm() {
    var t = $(this)
    validateForm(t.closest('form'), function () {
        clearForm(t.closest('form')), showSuccessMessage(t.closest('form').data('msg-success'))
    })
}
function submitGame() {
    var t = $(this)
    validateForm(t.closest('form'), function () {
        t.closest('.overlay').fadeOut(), clearForm(t.closest('form')), showSuccessMessage(t.closest('form').data('msg-success'))
    })
}
