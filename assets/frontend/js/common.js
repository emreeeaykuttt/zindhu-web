function modal_open(content = '', img = '', btn_text = '')
{
    $('#modal-content').html(content);
    $('#modal-image').attr('src', img);

    if (btn_text) {
        $('#modal-btn-text').html(btn_text)
    }

    $('#general-modal').modal('show');
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

// Preloader
$(window).on('load', function() {
    gsap.to('#preloader svg', {
        scale: 0
    }, .5);
    gsap.to('#preloader', {
        top: "-100vh"
    }, .5);
});

$(document).ready(function () {

    // Material select initialization
    $('.mdb-select').materialSelect();
    
    // Header scroll
    if ($(document).scrollTop() > 0) {
      $(".header").addClass("scrolled");
    }

});

$(window).scroll(function () {
    var scroll = $(document).scrollTop();

    if (scroll == 0) {
        $(".header").removeClass("scrolled");
    } else {
        $(".header").addClass("scrolled");
    }
  
});

// GSAP batch [data-ani]
gsap.registerPlugin(ScrollTrigger);
  ScrollTrigger.batch("[data-ani]", {
      scrub: false,
      onEnter: batch => gsap.to(batch, {
          opacity: 1,
          y: "0",
          stagger: 0.2,
      }),
  });

// Mobile menu open/close and animation 
var menuTL = gsap.timeline({
  paused: true,
});
var hamburger = $(".open-hamburger");
var closeHamburger = $(".close-hamburger");
menuTL.to(".mobile-menu", {
  opacity: 1,
  height: innerHeight,
  right: 0,
  visibility: "visible",
});
menuTL.from(".mobile-menu>*", {
  y: "100px",
  opacity: 0,
  stagger: .2
});
menuTL.to(".open-hamburger svg", {
  right: "-100%"
});
menuTL.to(".close-hamburger", {
  display: "flex",
  opacity: 1
});

$(".open-hamburger").on("click", function () {
  menuTL.play();
});
$(".close-hamburger").on("click", function () {
  menuTL.reverse();
});

$("#scrollTop").click(function () {
  return $("html, body").animate({ 
    scrollTop: 0 }, 1500), !1
}),
  $(window).scroll(function () {
    300 < $(this).scrollTop() ? $("#scrollTop").addClass("active") : $("#scrollTop").removeClass("active")
  })