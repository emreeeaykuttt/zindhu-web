gsap.registerPlugin(ScrollTrigger);
var elem = document.querySelector(".zindhu__scrollbar");
const bodyScrollBar = Scrollbar.init(elem, {
    damping: 0.1,
    delegateTo: document
});
Scrollbar.detachStyle();
if (window.matchMedia("(max-width: 700px)").matches) {
    bodyScrollBar.addListener((e) => {
        if (e.offset.y > 50) {
            $('.header').addClass('scrolled')
        } else {
            $('.header').removeClass('scrolled')
        }
    })
} else {}


$(".menu-inner ul li:first-child a").click(function() {
    bodyScrollBar.scrollIntoView($("#about-us")[0]);
})
// $(".menu-inner ul li:nth-child(2) a").click(function() {
//     bodyScrollBar.scrollIntoView($("#developers")[0]);
// })
$(".menu-inner ul li:nth-child(2) a").click(function() {
    bodyScrollBar.scrollIntoView($("#contact")[0]);
})

particlesJS.load('particles-js', '/assets/hr/js/particles.json', function() {
    console.log('callback - particles.js config loaded');
});
ScrollTrigger.scrollerProxy(".zindhu__scrollbar", {
    scrollTop(value) {
        if (arguments.length) {
            bodyScrollBar.scrollTop = value; // setter
        }
        return bodyScrollBar.scrollTop; // getter
    },
    getBoundingClientRect() {
        return {
            top: 0,
            left: 0,
            width: window.innerWidth,
            height: window.innerHeight
        };
    }
});

bodyScrollBar.addListener(ScrollTrigger.update);
$(".main-carousel").owlCarousel({
    loop: true,
    margin: 0,
    mouseDrag: false,
    autoplay: true,
    autoplayTimeout: 5000,
    smartSpeed: 200,
    autoplayHoverPause: true,
    animateIn: "fadeIn",
    animateOut: "fadeOut",
    navText: [
        "<i class='la la-angle-left'></i>",
        "<i class='la la-angle-right'></i>",
    ],
    nav: false,
    dots: true,
    items: 1,
});
$(".companies-carousel").owlCarousel({
    loop: true,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 5000,
    smartSpeed: 200,
    autoplayHoverPause: true,
    animateIn: "fadeIn",
    animateOut: "fadeOut",
    navText: [
        "<i class='la la-angle-left'></i>",
        "<i class='la la-angle-right'></i>",
    ],
    nav: true,
    dots: false,
    responsive: {
        0: {
            items: 1,
        },
        600: {
            items: 3,
        },
        1000: {
            items: 5,

        }
    }

});



// Menu Timeline
menuTL = gsap.timeline({
    paused: true
});
menuTL.from("section.zindhu-co>p", {
    scrollTrigger: {
        trigger: "section.zindhu-co",
        scroller: ".zindhu__scrollbar",
        scrub: true,
    },
    y: "200px",
    opacity: 0,
    ease: "Power4.easeOut"
});
var controlit = $('#navToggle');
var menuToggle = gsap.timeline({
    paused: true,
    reversed: true
});
menuToggle
    .add('rotate')
    .to('#menuToggle', .2, {
        rotation: '-90deg',
        transformOrigin: "50% 50%"
    }, 'rotate')
    .to(' #toggleTop', .2, {
        y: '3'
    }, 'rotate')
    .to(' #toggleBottom', .2, {
        y: '-3'
    }, 'rotate')
    .to(' #toggleTop', .2, {
        rotationZ: 45,
        transformOrigin: '50% 50%'
    }, 'rotate')
    .to(' #toggleBottom', .2, {
        rotationZ: -45,
        transformOrigin: '50% 50%'
    }, 'rotate')
    .to(' .header .menu ul', .2, {
        height: "auto"
    }, 'rotate')
    .to(' .header .menu ul li', .2, {
        bottom: 0,
        opacity: 1,
        stagger: .1,
        ease: "power4.out",
    }, 'rotate')
    .to(' .header .menu ul li a', .2, {
        opacity: 1,
        bottom: 0,
        stagger: .1,
    }, 'rotate')

controlit.click(function() {
    menuToggle.reversed() ? menuToggle.restart() : menuToggle.reverse();
});

gsap.from(".about-us .drawings div", {
    scrollTrigger: {
        trigger: ".about-us",
        scroller: ".zindhu__scrollbar",
        scrub: true,
        start: "top center",
        end: "bottom bottom"
    },
    y: Math.random() * 700,
    ease: "slow",
    stagger: .2
});
gsap.to(".main-carousel .item", {
    scrollTrigger: {
        trigger: ".main-carousel",
        scroller: ".zindhu__scrollbar",
        scrub: true,
        start: "bottom bottom",
    },
    y: -100,
    ease: "power4.out",
});
gsap.from(".developers .bg-line svg", {
    scrollTrigger: {
        trigger: ".developers",
        scroller: ".zindhu__scrollbar",
        scrub: true,
        start: "top bottom",
    },
    strokeDashoffset: "500px",
    ease: "slow",
    stagger: .2
});

// gsap.from(".developers .developer-infos .info", {
//     scrollTrigger: {
//         trigger: ".developers",
//         scroller: ".zindhu__scrollbar",
//         scrub: true,
//         start: "-100px bottom",
//     },
//     bottom:200,
//     ease: "power4.out",
//     stagger: .1
// });
gsap.from(".companies h2 ", {
    scrollTrigger: {
        trigger: ".companies",
        scroller: ".zindhu__scrollbar",
        scrub: true,
        start: "top bottom",
    },
    y: -200,
    ease: "power3.out",
});
gsap.from(".contact h2 ", {
    scrollTrigger: {
        trigger: ".contact",
        scroller: ".zindhu__scrollbar",
        scrub: true,
        start: "top bottom",
    },
    y: 200,
    ease: "power3.out",
});