check_data();
unread_contact();

function check_data()
{
	let delay = 60 * 1000;
	setInterval(function(){ 
		unread_contact();
	}, delay);
}

function unread_contact()
{
    $.ajax({
        url : base_url + 'admin/contact/unread_contact',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            $('#unread-company-count, #dashboard-unread-company-count').text(data.company);
            $('#unread-user-count, #dashboard-unread-user-count').text(data.user);
        }
    });
}