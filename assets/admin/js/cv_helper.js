function company_list()
{
    $.ajax({
        url : base_url + 'ajax/company',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            if (data != "") 
            {
                let count = data.length;
                $.each(data, function(key, val){
                    $('#working-status-company-id').append('<option value="'+val.id+'">'+val.company_name+'</option>');
                    if (key+1 === count) {
                        $('#working-status-company-id').append('<option value="0">Diğer</option>');
                        setTimeout(function(){
                            $('#working-status-company-id').materialSelect();
                        }, 100);
                    }
                });
            }
        }
    });
}

function position_list()
{
    $.ajax({
        url : base_url + 'ajax/position',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            if (data != "") 
            {
                let count = data.length;
                $.each(data, function(key, val){
                    $('#personal-information-position-id').append('<option value="'+val.id+'">'+val.title+'</option>');
                    if (key+1 === count) {
                        setTimeout(function(){
                            $('#personal-information-position-id').materialSelect();
                        }, 100);
                    }
                });
            }
        }
    });
}

function position_render(val)
{
    let position = '';

    $.ajax({
        url : base_url + 'ajax/position_by_id/' + val,
        type: 'GET',
        dataType: 'JSON',
        async: false,
        success: function(data)
        {
            position = data.title;
        }
    });

    return position;
}

function city_list()
{
    $.ajax({
        url : base_url + 'ajax/city',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            if (data != "") 
            {
                let count = data.length;
                $.each(data, function(key, val){
                    $('#personal-information-city-id, #personal-information-city-json').append('<option value="'+val.id+'">'+val.city+'</option>');
                    if (key+1 === count) {
                        setTimeout(function(){
                            $('#personal-information-city-id, #personal-information-city-json').materialSelect();
                        }, 100);
                    }
                });
            }
        }
    });
}

function cities_render(val)
{
    let cities = '';

    $.ajax({
        url : base_url + 'ajax/city_by_ids?json=' + val,
        type: 'GET',
        dataType: 'JSON',
        async: false,
        success: function(data)
        {
            cities = data;
        }
    });

    return cities;
}

function city_render(val)
{
    let city = '';

    $.ajax({
        url : base_url + 'ajax/city_by_id/' + val,
        type: 'GET',
        dataType: 'JSON',
        async: false,
        success: function(data)
        {
            city = data.city;
        }
    });

    return city;
}

function district_render(val)
{
    let district = '';

    $.ajax({
        url : base_url + 'ajax/district_by_id/' + val,
        type: 'GET',
        dataType: 'JSON',
        async: false,
        success: function(data)
        {
            district = data.district;
        }
    });

    return district;
}

function game_list()
{
	$.ajax({
        url : base_url + 'ajax/game',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            if (data != "") 
            {
            	let count = data.length;
            	$.each(data, function(key, val){
            		$('#game-future-json, #game-past-json').append('<option value="'+val.id+'">'+val.title+'</option>');
            		if (key+1 === count) {
            			setTimeout(function(){
            				$('#game-future-json, #game-past-json').materialSelect();
            			}, 100);
				    }
            	});
            }
        }
    });
}

function game_single_render(val)
{
    let game = '';

    $.ajax({
        url : base_url + 'ajax/game_by_id/' + val,
        type: 'GET',
        dataType: 'JSON',
        async: false,
        success: function(data)
        {
            game = data.title;
        }
    });

    return game;
}

function game_render(val)
{
    let game = '';

    $.ajax({
        url : base_url + 'ajax/game_by_ids?json=' + val,
        type: 'GET',
        dataType: 'JSON',
        async: false,
        success: function(data)
        {
            game = data;
        }
    });

    return game;
}

function program_list()
{
	$.ajax({
        url : base_url + 'ajax/program',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            if (data != "") 
            {
                let count = data.length;
            	$.each(data, function(key, val){
            		$('#used-program-id').append('<option value="'+val.id+'" data-icon="'+base_url+upload_folder+val.photo+'">'+val.title+'</option>');
            		if (key+1 === count) {
            			setTimeout(function(){
            				$('#used-program-id').materialSelect();
            			}, 100);
				    }
            	});
            }
        }
    });
}

function program_render(val)
{
    let program = '';

    $.ajax({
        url : base_url + 'ajax/program_by_id/' + val,
        type: 'GET',
        dataType: 'JSON',
        async: false,
        success: function(data)
        {
            program = data.title;
        }
    });

    return program;
}

function program_icon_render(val)
{
    let program_icon = '';

    $.ajax({
        url : base_url + 'ajax/program_by_id/' + val,
        type: 'GET',
        dataType: 'JSON',
        async: false,
        success: function(data)
        {
            program_icon = '/upload/' + data.photo;
        }
    });

    return program_icon;
}

function university_list()
{
	$.ajax({
        url : base_url + 'ajax/university',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            if (data != "") 
            {
            	let count = data.length;
            	$.each(data, function(key, val){
            		$('#education-university-id').append('<option value="'+val.id+'">'+val.university+'</option>');
            		if (key+1 === count) {
                        $('#education-university-id').append('<option value="0">Diğer</option>');
            			setTimeout(function(){
            				$('#education-university-id').materialSelect();
            			}, 100);
				    }
            	});
            }
        }
    });
}

function university_render(val)
{
    let university = '';

    $.ajax({
        url : base_url + 'ajax/university_by_id/' + val,
        type: 'GET',
        dataType: 'JSON',
        async: false,
        success: function(data)
        {
            university = data.university;
        }
    });

    return university;
}

function education_level_render(val)
{
    let level = '';

    $.ajax({
        url : base_url + 'ajax/education_level/' + val,
        type: 'GET',
        async: false,
        success: function(data)
        {
            level = data;
        }
    });

    return level;
}

function date_render(val1, val2)
{   
    if (val1 == 0 && val2 == 0) 
    {
        return '';
    }
    else if (val1 == 0)
    {
        return val2;
    }
    else if (val1.length == 1) 
    {
        return '0' + val1 + '.' + val2;
    }
    else
    {
        return val1 + '.' + val2;
    }
}

function end_date_render(val1, val2)
{
    if (val1 == '0' && val2 != '0') 
    {
        return val2;
    }
    else if (val1 == '0' && val2 == '0') 
    {
        return 'Hala Çalışıyorum';
    }
    else 
    {
        if (val1.length == 1) {
            return '0' + val1 + '.' + val2;
        }else{
            return val1 + '.' + val2;
        }
    }
}