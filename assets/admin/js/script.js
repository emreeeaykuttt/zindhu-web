function modal_open(content = '', img = '', btn_text = '')
{
    $('#modal-content').html(content);
    $('#modal-image').attr('src', img);

    if (btn_text) {
        $('#modal-btn-text').html(btn_text)
    }

    $('#general-modal').modal('show');
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

function module_export_name(text)
{
    let new_date = new Date();
    let month = new_date.getMonth()+1;
    let day = new_date.getDate();
    let full_date = new_date.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
    let module_export_name = text + '_' + full_date;

    return module_export_name;
}

function replace_special_chars(str) 
{
    var specialChars = [['ş', 's'], ['ğ', 'g'], ['ü', 'u'], ['ı', 'i'],['_', '-'],
        ['ö', 'o'], ['Ş', 'S'], ['Ğ', 'G'], ['Ç', 'C'], ['ç', 'c'],
        ['Ü', 'U'], ['İ', 'I'], ['Ö', 'O'], ['ş', 's']];

    for (var i = 0; i < specialChars.length; i++) {
        str = str.replace(eval('/' + specialChars[i][0] + '/ig'), specialChars[i][1]);
    }
    return str;
}

function round_number(val, digit = 2)
{
    return Number(Math.round(val + 'e' + digit) + 'e-' + digit);
}

function time_render(val = '')
{
    let diff = Math.abs(new Date() - new Date(val));
    let minutes = Math.floor((diff/1000)/60);
    let hour = Math.floor(minutes / 60);
    let day = Math.floor(hour / 24);
    let diff_time = '';
    
    if(minutes < 60) {
        diff_time = minutes + ' dakika önce';
    }
    else if(hour < 24) {
        diff_time = hour + ' saat önce';
    }
    else if(day) {
        diff_time = day + ' gün önce';
    }

    return diff_time
}

function loading_render()
{
    return '<div class="d-flex justify-content-center">' +
                '<div class="spinner-border" role="status">' +
                    '<span class="sr-only">Loading...</span>' +
                '</div>' +
            '</div>';
}

function small_loading_render()
{
    return '<div>' +
                '<i class="la la-spinner"></i>' +
            '</div>';
}

function user_system_message()
{
    return '<a href="javascript:void(0)" data-toggle="tooltip" title="Kullanıcının işlemi sonucu sistem tarafından atılan otomatik bir mesajdır."><i class="fas fa-info-circle fa-2x"></i></a>';
}

function company_system_message()
{
    return '<a href="javascript:void(0)" data-toggle="tooltip" title="Stüdyonun işlemi sonucu sistem tarafından atılan otomatik bir mesajdır."><i class="fas fa-info-circle fa-2x"></i></a>';
}

function date_convert(datetime = '')
{
    let diff = Math.abs(new Date() - new Date(datetime));
    let minutes = Math.floor((diff/1000)/60);
    let hour = Math.floor(minutes / 60);
    let day = Math.floor(hour / 24);
    
    if(minutes < 60) {
        return minutes + ' dakika'
    }
    else if(hour < 24) {
        return hour + ' saat'
    }
    else {
        return day + ' gün'
    }
}

function time_difference_find(startdate = '', enddate = '')
{
    startdate = new Date(startdate);
    enddate = enddate ? new Date(enddate) : new Date();
    let time_difference = Math.abs(enddate.getTime() - startdate.getTime());
    let day_difference = Math.ceil(time_difference / (1000 * 3600 * 24));
    let data = ' ';

    if (day_difference > 365) {
        data += Math.floor(day_difference / 365) + ' YIL ';
    }

    let month_difference = month_diff(startdate, enddate);
    if (month_difference % 12 > 0) {
        data += Math.floor(month_difference % 12) + ' AY ';
    }

    if (month_difference < 1) {
        data = ' 0-1 AY ';
    }

    if (data != '') {
        data = '(' + data + ')';
    }

    return data;
}

function month_diff(d1, d2) 
{
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
}