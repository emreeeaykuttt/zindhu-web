check_data();
unread_conversations();
unread_contact();

function check_data()
{
	let delay = 60 * 1000;
	setInterval(function(){ 
		unread_conversations();
        unread_contact();
	}, delay);
}

function unread_conversations()
{
    $.ajax({
        url : base_url + 'user/ajax/unread_conversations',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            if (data > 0) {
                $('#unread-count, #dashboard-unread-count, #header-unread-count').text(data);
            } else {
                $('#unread-count, #header-unread-count').text('');
                $('#dashboard-unread-count').text('0');
            }
        }
    });
}

function unread_contact()
{
    $.ajax({
        url : base_url + 'user/contact/unread_contact',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            if (data > 0) {
                $('#unread-admin-count, #dashboard-unread-admin-count, #header-unread-admin-count').text(data);
            } else {
                $('#unread-admin-count, #header-unread-admin-count').text('');
                $('#dashboard-unread-admin-count').text('0');
            }
        }
    });
}
