university_list();
program_list();
game_list();
city_list();
position_list();
view_personal_information_all();
view_educational_background_all();
view_work_experience_all();
view_used_program_all();
view_game_choice_all();
view_portfolio_all();

function working_status_edit()
{
    $('#working-status-from')[0].reset();
    $('.error-text').text('');
    $('#working-status-title').text('Çalışma Durumunu Düzenle');

    $.ajax({
        url : base_url + 'user/cv/working_status_edit/',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            $('#working-status-modal').modal('show');

            $('[name="working_status_company_text"]').val(data.company_text).trigger("change");
            $('[name="working_status_company_id"]').val(data.company_id);
            $('#working-status-company-id').materialSelect('destroy');
            $('#working-status-company-id').materialSelect();

            if (data.is_working == 1) {
                $('#is-working').attr('checked', true);
                $('.working-status-company-id').css('display', 'block');
            }else {
                $('#is-working').attr('checked', false);
                $('.working-status-company-id, .working-status-company-text').css('display', 'none');
            }

            if (data.company_id == 0) {
                $('.working-status-company-text').attr('style', 'display: block !important');
            } else {
                $('.working-status-company-text').attr('style', 'display: none !important');
            }

            $('.step-new-content').stop().animate({
                scrollTop:0
            });
        }
    });
}

function working_status_save()
{
    $('#working-status-btn').text('Kaydediliyor...');
    $('#working-status-btn').attr('disabled',true);
    $('.error-text').text('');

    var form_data = {
        working_status_company_text: $('[name="working_status_company_text"]').val(),
        working_status_company_id: $('[name="working_status_company_id"]').val(),
        working_status_is_working: $('[name="working_status_is_working"]').is(':checked') ? 1 : 0,
    }

    $.ajax({
        url : base_url + 'user/cv/working_status_save',
        type: "POST",
        data: form_data,
        dataType: "JSON",
        success: function(data)
        {
            if(data.status) 
            {
                toastr.success('Kayıt başarılı bir şekilde güncellendi');

                $('#working-status-from')[0].reset();
                $('.error-text').text('');
                view_personal_information_all();

                $('#working-status-modal').modal('hide');
            }
            else
            {
                toastr.error('Formu kontrol ediniz!');

                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('#' + data.inputerror[i] + '_error').text(data.error_string[i]); 
                }
            }

            $('#working-status-btn').text('Kaydet');
            $('#working-status-btn').attr('disabled', false);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Form kayıt edilirken bir sorun oluştu');
            $('#working-status-btn').text('Kaydet');
            $('#working-status-btn').attr('disabled', false);
        }
    });
}

function view_personal_information_all()
{
    $('#personal-information-view').html('');

    $.ajax({
        url : base_url + 'user/cv/view_personal_information_all',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            let user = data.user;
            let company = data.company_names;
            data = data.personal_information;

            $('#working-status-view').html(
                '<div class="working-status-list">' + 
                    '<div class="d-flex">' +
                        '<div class="col-md-2 col-6">Çalışma Durumu:</div>' +
                        '<div> <strong>'+ (user.is_working==1 ? 'Çalışıyorum' : 'Çalışmıyorum')  +' </strong></div>' +
                    '</div>' +
                '</div>'
            );

            if (user.is_working == 1) 
            {
                if (user.company_ids) 
                {
                    $('.working-status-list').append(
                        '<div class="d-flex">' +
                            '<div class="col-md-2 col-6">Stüdyo:</div>' +
                            '<div> <strong>'+ company +' </strong></div>' +
                        '</div>'
                    );
                } 
                else 
                {
                    $('.working-status-list').append(
                        '<div class="d-flex">' +
                            '<div class="col-md-2 col-6">Stüdyo:</div>' +
                            '<div> <strong>'+ user.company_text +' </strong></div>' +
                        '</div>'
                    );
                }
            }

            if (data != "") 
            {
                for (var i = 0; i < data.length; i++)
                {
                    let cities_text = '';
                    let remote_text = '';
                    let city_text = '';
                    
                    $.each(cities_render(data[i].city_json), function(key, val){
                        cities_text += val['city'] + ', ';
                    });
                    cities_text = cities_text.replace(/,\s*$/, "");

                    if (data[i].is_remote == '1') {
                        remote_text = 'İstiyorum';
                    } else if (data[i].is_remote == '0') {
                        remote_text = 'İstemiyorum';
                    } else {
                        remote_text = 'Belirtilmemiş';
                    }

                    if (data[i].city_id == 341) {
                        city_text = ' - ' + data[i].city_text;
                    }

                    $('#personal-information-view').append(
                        '<div class="item d-flex justify-content-between personal-information'+data[i].id+'">' +
                            '<div class="info">' +
                                '<div class="row '+data[i].id+'">' +
                                    '<div class="col-md-2 col-6">Ad:</div>' +
                                    '<div class="col-md-4 col-6"><strong>'+ user.firstname +'</strong></div>' +
                                    '<div class="col-md-2 col-6">Soyad:</div>' +
                                    '<div class="col-md-4 col-6"><strong>'+ user.lastname +'</strong></div>' +
                                    '<div class="col-md-2 col-6">E-mail:</div>' +
                                    '<div class="col-md-4 col-6"><strong>'+ user.email +'</strong></div>' +
                                    '<div class="col-md-2 col-6">Telefon: </div>' +
                                    '<div class="col-md-4 col-6"><strong>'+ data[i].phone +'</strong></div>' +
                                    '<div class="col-md-2 col-12">Yaşadığın şehir: </div>' +
                                    '<div class="col-md-4 col-12"><strong>'+ city_render(data[i].city_id) + city_text +'</strong></div>' +
                                    '<div class="col-md-2 col-12">Çalışmak İstediğiniz Şehirler: </div>' +
                                    '<div class="col-md-4 col-12"><strong>'+ cities_text +'</strong></div>' +
                                    '<div class="col-md-2 col-12">Uzaktan Çalışma Durumu: </div>' +
                                    '<div class="col-md-4 col-12"><strong>'+ remote_text +'</strong></div>' +                                
                                '</div>'+
                            '</div>'+
                            '<div class="edit">'+
                                '<button class="btn" onclick="personal_information_edit('+data[i].id+')"><i class="la la-edit"></i></button>'+
                            '</div>'+
                        '</div>'
                    );
                }

                $('#personal-information-tab').addClass('done')
            }
            else
            {
                $('#personal-information .add-new').css('display', 'flex');
                $('#personal-information-tab').removeClass('done')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Hata oluştu');
        }
    });
}

function personal_information_add()
{
    $('#personal-information-title').text('Kişisel Bilgiler');
    $('#personal-information-from')[0].reset();

    $.ajax({
        url : base_url + 'user/cv/user',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            $('#personal-information-modal').modal('show');

            $('[name="user_firstname"]').val(data.user.firstname).trigger("change");
            $('[name="user_lastname"]').val(data.user.lastname).trigger("change");
            $('[name="user_email"]').val(data.user.email).trigger("change");

            $('.step-new-content').stop().animate({
                scrollTop:0
            });
        }
    });
}

function personal_information_edit(id)
{
    $('#personal-information-from')[0].reset();
    $('.error-text').text('');
    $('#personal-information-title').text('Kişisel Bilgileri Düzenle');

    $.ajax({
        url : base_url + 'user/cv/personal_information_edit/' + id,
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            $('#personal-information-modal').modal('show');

            $('[name="user_firstname"]').val(data.user.firstname).trigger("change");
            $('[name="user_lastname"]').val(data.user.lastname).trigger("change");
            $('[name="user_email"]').val(data.user.email).trigger("change");
            $('[name="personal_information_id"]').val(data.id);
            $('[name="personal_information_phone"]').val(data.phone).trigger("change");
            $('[name="personal_information_position_id"]').val(data.position_id);
            $('[name="personal_information_city_id"]').val(data.city_id);
            $('[name="personal_information_city_text"]').val(data.city_text).trigger("change");
            $('[name="personal_information_city_json"]').val(JSON.parse(data.city_json));
            $('#personal-information-position-id, #personal-information-city-id, #personal-information-city-json').materialSelect('destroy');
            $('#personal-information-position-id, #personal-information-city-id, #personal-information-city-json').materialSelect();

            if (data.city_id == 341) {
                $('.city-text').removeClass('d-none');
                $('.city-text').addClass('d-block');
            } else {
                $('.city-text').removeClass('d-block');
                $('.city-text').addClass('d-none');
            }

            if (data.is_remote == 1) {
                $('#personal-information-is-remote').attr('checked', true);
            }else {
                $('#personal-information-is-remote').attr('checked', false);
            }

            $('.step-new-content').stop().animate({
                scrollTop:0
            });
        }
    });
}


function personal_information_save()
{
    $('#personal-information-btn').text('Kaydediliyor...');
    $('#personal-information-btn').attr('disabled',true);
    $('.error-text').text('');

    var form_data = {
        personal_information_id: $('[name="personal_information_id"]').val(),
        personal_information_phone: $('[name="personal_information_phone"]').val(),
        personal_information_position_id: $('[name="personal_information_position_id"]').val(),
        personal_information_city_id: $('[name="personal_information_city_id"]').val(),
        personal_information_city_text: $('[name="personal_information_city_text"]').val(),
        personal_information_city_json: $('[name="personal_information_city_json"]').val(),
        personal_information_is_remote: $('[name="personal_information_is_remote"]').is(':checked') ? 1 : 0,
        user_firstname: $('[name="user_firstname"]').val(),
        user_lastname: $('[name="user_lastname"]').val(),
        user_email: $('[name="user_email"]').val(),
    }
   
    $.ajax({
        url : base_url + 'user/cv/personal_information_save',
        type: "POST",
        data: form_data,
        dataType: "JSON",
        
        success: function(data)
        {

            if(data.status) 
            {
                if (data.id > 0) {
                    toastr.success('Kayıt başarılı bir şekilde eklendi');
                    
                } else {
                    toastr.success('Kayıt başarılı bir şekilde güncellendi');
                }

                $('#personal-information-from')[0].reset();
                $('[name="personal_information_id"]').val('');
                $('.error-text').text('');
                $('#personal-information-position-id, #personal-information-city-id, #personal-information-city-json').materialSelect('destroy');
                $('#personal-information-position-id, #personal-information-city-id, #personal-information-city-json').materialSelect();
                $('#personal-information-title').text('Kişisel Bilgiler');
                view_personal_information_all();

                $('#personal-information-modal').modal('hide');
            }
            else
            {
                toastr.error('Formu kontrol ediniz!');

                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('#' + data.inputerror[i] + '_error').text(data.error_string[i]); 
                }
            }

            $('#personal-information-btn').text('Kaydet');
            $('#personal-information-btn').attr('disabled', false);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Form kayıt edilirken bir sorun oluştu');
            $('#personal-information-btn').text('Kaydet');
            $('#personal-information-btn').attr('disabled', false);
        }
    });
}

function personal_information_delete(id)
{
    Swal.fire({
        title: 'Emin misiniz?',
        text: "Sil derseniz kayıt silinecektir.",
        imageUrl: '/assets/frontend/img/modal-images/remove.svg',
        showCloseButton: true,
        cancelButtonText: 'Vazgeç',
        confirmButtonText: 'Sil'
    }).then((result) => {
        if (result.value) 
        {
            $.ajax({
                url : base_url + 'user/cv/personal_information_delete/' + id,
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {   
                    toastr.success('Kayıt başarılı bir şekilde silindi');
                    view_personal_information_all();

                    $('#personal-information-from')[0].reset();
                    $('[name="personal_information_id"]').val('');
                    $('.error-text').text('');
                    $('#personal-information-position-id, #personal-information-city-id, #personal-information-city-json').materialSelect('destroy');
                    $('#personal-information-position-id, #personal-information-city-id, #personal-information-city-json').materialSelect();
                }
            });
        }
    });
}

function view_educational_background_all()
{
	$('#educational-background-view').html('');
    $('#educational-background .add-new, #educational-background .title .add').css('display', 'none');

	$.ajax({
        url : base_url + 'user/cv/view_educational_background_all',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            if (data != "") 
            {
            	let end_year_text = '';
                let university_text = '';

            	for (var i = 0; i < data.length; i++)
            	{
                    if (data[i].university_id == 0) {
                        university_text = data[i].university_text;
                    } else {
                        university_text = university_render(data[i].university_id);
                    }

            		end_year_text = data[i].continues == 1 ? 'Devam Ediyor' : data[i].end_year;

            		$('#educational-background-view').append(
                        '<div class="item d-flex justify-content-between educational-background'+data[i].id+'">' +
                            '<div class="info">' +
                                '<p class="blue-text">' + education_level_render(data[i].education_level) + '</p>' +
                                '<h5>' + university_text + '</h5>' +
                                '<p>' + data[i].university_section + '</p>' +
                                '<p class="gray-text">' + data[i].start_year + ' - ' + end_year_text + '</p>' +
                            '</div>'+
                            '<div class="edit">'+
                                '<button class="btn" onclick="educational_background_edit('+data[i].id+')"><i class="la la-edit"></i></button>'+
                                '<button class="btn" onclick="educational_background_delete('+data[i].id+')"><i class="la la-times"></i></button>'+
                            '</div>'+
                        '</div>'
                    );
            	}

                $('#educational-background-tab').addClass('done');
                $('#educational-background .title .add').css('display', 'flex');
            }
            else
            {
                $('#educational-background .add-new').css('display', 'flex');
                $('#educational-background-tab').removeClass('done');
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Hata oluştu');
        }
    });
}

function educational_background_add()
{
    $('#educational-background-from')[0].reset();
    $('#education-title').text('Üniversite Ekle');
    $('#continues').attr('checked', false);
    $('.education-end-year').css('display', 'block');
    $('#education-level, #education-start-year, #education-end-year, #education-university-id').materialSelect('destroy');
    $('#education-level, #education-start-year, #education-end-year, #education-university-id').materialSelect();
    $('.university-other').attr('style', 'display: none !important');
    $('#educational-background-modal').modal('show');
}

function educational_background_edit(id)
{
    $('#educational-background-from')[0].reset();
    $('.error-text').text('');
    $('#education-title').text('Üniversite Düzenle');

    $.ajax({
        url : base_url + 'user/cv/educational_background_edit/' + id,
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            $('#educational-background-modal').modal('show');

            $('[name="education_id"]').val(data.id);
            $('[name="education_level"]').val(data.education_level);
            $('[name="education_university_id"]').val(data.university_id);
            $('[name="education_university_text"]').val(data.university_text).trigger("change");
            $('[name="education_university_section"]').val(data.university_section).trigger("change");
            $('[name="education_start_year"]').val(data.start_year);
            $('[name="education_end_year"]').val(data.end_year);
            $('#education-level, #education-start-year, #education-end-year, #education-university-id').materialSelect('destroy');
            $('#education-level, #education-start-year, #education-end-year, #education-university-id').materialSelect();

            if (data.university_id == 0) {
                $('.university-other').attr('style', 'display: block !important');
            } else {
                $('.university-other').attr('style', 'display: none !important');
            }

            if (data.continues == 1) {
                $('#continues').attr('checked', true);
                $('.education-end-year').css('display', 'none');

            }else {
                $('#continues').attr('checked', false);
                $('.education-end-year').css('display', 'block');
            }

            $('.step-new-content').stop().animate({
                scrollTop:0
            });
        }
    });
}

function educational_background_save()
{
	$('#education-btn').text('Kaydediliyor...');
    $('#education-btn').attr('disabled',true);
    $('.error-text').text('');

    var form_data = {
    	education_id: $('[name="education_id"]').val(),
    	education_level: $('[name="education_level"]').val(),
    	education_university_id: $('[name="education_university_id"]').val(),
        education_university_text: $('[name="education_university_text"]').val(),
    	education_university_section: $('[name="education_university_section"]').val(),
    	education_start_year: $('[name="education_start_year"]').val(),
    	education_end_year: $('[name="education_end_year"]').val(),
    	education_continues: $('[name="education_continues"]').is(':checked') ? 1 : 0,
    }

    $.ajax({
        url : base_url + 'user/cv/educational_background_save',
        type: "POST",
        data: form_data,
        dataType: "JSON",
        success: function(data)
        {
            if(data.status) 
            {
                if (data.id > 0) {
                	toastr.success('Kayıt başarılı bir şekilde eklendi');
                } else {
                	toastr.success('Kayıt başarılı bir şekilde güncellendi');
                }

                $('#educational-background-from')[0].reset();
                $('[name="education_id"]').val('');
                $('.error-text').text('');
                $('#continues').attr('checked', false);
    			$('.education-end-year').css('display', 'block');
   				$('#education-level, #education-start-year, #education-end-year, #education-university-id').materialSelect('destroy');
            	$('#education-level, #education-start-year, #education-end-year, #education-university-id').materialSelect();
            	$('#education-title').text('Üniversite Ekle');
                view_educational_background_all();

                $('#educational-background-modal').modal('hide');
            }
            else
            {
            	toastr.error('Formu kontrol ediniz!');

                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('#' + data.inputerror[i] + '_error').text(data.error_string[i]); 
                }
            }

            $('#education-btn').text('Kaydet');
    		$('#education-btn').attr('disabled', false);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Form kayıt edilirken bir sorun oluştu');
            $('#education-btn').text('Kaydet');
    		$('#education-btn').attr('disabled', false);
        }
    });
}

function educational_background_delete(id)
{
	Swal.fire({
        title: 'Emin misiniz?',
        text: "Sil derseniz kayıt silinecektir.",
        imageUrl: '/assets/frontend/img/modal-images/remove.svg',
        showCloseButton: true,
        showCancelButton: true,
        cancelButtonText: 'Vazgeç',
        confirmButtonText: 'Sil'
    }).then((result) => {
        if (result.value) 
        {
            $.ajax({
	            url : base_url + 'user/cv/educational_background_delete/' + id,
	            type: "POST",
	            dataType: "JSON",
	            success: function(data)
	            {   
	                toastr.success('Kayıt başarılı bir şekilde silindi');
	                view_educational_background_all();

	                $('#educational-background-from')[0].reset();
	                $('[name="education_id"]').val('');
	                $('.error-text').text('');
	                $('#continues').attr('checked', false);
        			$('.education-end-year').css('display', 'block');
	   				$('#education-level, #education-start-year, #education-end-year, #education-university-id').materialSelect('destroy');
	            	$('#education-level, #education-start-year, #education-end-year, #education-university-id').materialSelect();
	            }
	        });
        }
    });
}

function view_work_experience_all()
{
	$('#work-experience-view').html('');
    $('#work-experience .add-new, #work-experience .title .add').css('display', 'none');

	$.ajax({
        url : base_url + 'user/cv/view_work_experience_all',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            if (data != "") 
            {
            	let end_year_text = '';

            	for (var i = 0; i < data.length; i++)
            	{
            		end_year_text = data[i].working == 1 ? 'Hala Çalışıyorum' : data[i].end_year;
            		$('#work-experience-view').append(
                        '<div class="item d-flex justify-content-between work-experience'+data[i].id+'">' +
                            '<div class="info">' +
                                '<p class="blue-text">' +data[i].company_position+ '</p>' +
                                '<h5>' + data[i].company_name+' ('+data[i].company_sector + ')' + '</h5>' +
                                '<p>' +
                                date_render(data[i].start_month, data[i].start_year) +
                                ' - ' +
                                end_date_render(data[i].end_month, data[i].end_year) +'</p>' +
                                '<p class="gray-text">'+data[i].detail +'</p>' +
                            '</div>'+
                            '<div class="edit">'+
                                '<button class="btn" onclick="work_experience_edit('+data[i].id+')"><i class="la la-edit"></i></button>'+
                                '<button class="btn" onclick="work_experience_delete('+data[i].id+')"><i class="la la-times"></i></button>'+
                            '</div>'+
                        '</div>'
                    );
            	}

                $('#work-experience-tab').addClass('done');
                $('#work-experience .title .add').css('display', 'flex');
            }
            else
            {
            	$('#work-experience .add-new').css('display', 'flex');
                $('#work-experience-tab').removeClass('done');
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Hata oluştu');
        }
    });
}

function work_experience_add()
{
    $('#work-experience-from')[0].reset();
    $('#work-title').text('Deneyim Ekle');
    $('#working').attr('checked', false);
    $('.work-end-year').css('display', 'block');
    $('#work-start-month, #work-start-year, #work-end-month, #work-end-year').materialSelect('destroy');
    $('#work-start-month, #work-start-year, #work-end-month, #work-end-year').materialSelect();
    $('#work-experience-modal').modal('show');
}

function work_experience_edit(id)
{
    $('#work-experience-from')[0].reset();
    $('.error-text').text('');
    $('#work-title').text('Deneyim Düzenle');

    $.ajax({
        url : base_url + 'user/cv/work_experience_edit/' + id,
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            $('#work-experience-modal').modal('show');

            $('[name="work_id"]').val(data.id);
            $('[name="work_company_name"]').val(data.company_name).trigger("change");
            $('[name="work_company_sector"]').val(data.company_sector).trigger("change");
            $('[name="work_company_position"]').val(data.company_position).trigger("change");
            $('[name="work_start_month"]').val(data.start_month);
            $('[name="work_start_year"]').val(data.start_year);
            $('[name="work_end_month"]').val(data.end_month);
            $('[name="work_end_year"]').val(data.end_year);
            $('[name="work_detail"]').val(data.detail).trigger("change");
            $('#work-start-month, #work-start-year, #work-end-month, #work-end-year').materialSelect('destroy');
            $('#work-start-month, #work-start-year, #work-end-month, #work-end-year').materialSelect();

            if (data.working == 1) {
                $('#working').attr('checked', true);
                $('.work-end-year').css('display', 'none');

            }else {
                $('#working').attr('checked', false);
                $('.work-end-year').css('display', 'block');
            }

            $('.step-new-content').stop().animate({
                scrollTop:0
            });
        }
    });
}

function work_experience_save()
{
	$('#work-btn').text('Kaydediliyor...');
    $('#work-btn').attr('disabled',true);
    $('.error-text').text('');

    var form_data = {
        work_status: $('[name="work_status"]').val(),
    	work_id: $('[name="work_id"]').val(),
    	work_company_name: $('[name="work_company_name"]').val(),
    	work_company_sector: $('[name="work_company_sector"]').val(),
    	work_company_position: $('[name="work_company_position"]').val(),
    	work_start_month: $('[name="work_start_month"]').val(),
    	work_start_year: $('[name="work_start_year"]').val(),
    	work_end_month: $('[name="work_end_month"]').val(),
    	work_end_year: $('[name="work_end_year"]').val(),
    	work_detail: $('[name="work_detail"]').val(),
    	work_working: $('[name="work_working"]').is(':checked') ? 1 : 0,
    }

    $.ajax({
        url : base_url + 'user/cv/work_experience_save',
        type: "POST",
        data: form_data,
        dataType: "JSON",
        success: function(data)
        {
            if(data.status) 
            {
                if (data.id > 0) {
                	toastr.success('Kayıt başarılı bir şekilde eklendi');
                } else {
                	toastr.success('Kayıt başarılı bir şekilde güncellendi');
                }

                $('#work-experience-from')[0].reset();
                $('[name="work_id"]').val('');
                $('.error-text').text('');
                $('#working').attr('checked', false);
    			$('.work-end-year').css('display', 'block');
   				$('#work-start-month, #work-start-year, #work-end-month, #work-end-year').materialSelect('destroy');
            	$('#work-start-month, #work-start-year, #work-end-month, #work-end-year').materialSelect();
            	$('#work-title').text('Deneyim Ekle');
                view_work_experience_all();

                $('#work-experience-modal').modal('hide');
            }
            else
            {
            	toastr.error('Formu kontrol ediniz!');

                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('#' + data.inputerror[i] + '_error').text(data.error_string[i]); 
                }
            }

            $('#work-btn').text('Kaydet');
    		$('#work-btn').attr('disabled', false);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Form kayıt edilirken bir sorun oluştu');
            $('#work-btn').text('Kaydet');
    		$('#work-btn').attr('disabled', false);
        }
    });
}

function work_experience_delete(id)
{
	Swal.fire({
        title: 'Emin misiniz?',
        text: "Sil derseniz kayıt silinecektir.",
        imageUrl: '/assets/frontend/img/modal-images/remove.svg',
        showCloseButton: true,
        showCancelButton: true,
        cancelButtonText: 'Vazgeç',
        confirmButtonText: 'Sil'
    }).then((result) => {
        if (result.value) 
        {
            $.ajax({
	            url : base_url + 'user/cv/work_experience_delete/' + id,
	            type: "POST",
	            dataType: "JSON",
	            success: function(data)
	            {   
	                toastr.success('Kayıt başarılı bir şekilde silindi');
	                view_work_experience_all();

	                $('#work-experience-from')[0].reset();
	                $('[name="work_id"]').val('');
	                $('.error-text').text('');
	                $('#working').attr('checked', false);
        			$('.work-end-year').css('display', 'block');
	   				$('#work-start-month, #work-start-year, #work-end-month, #work-end-year').materialSelect('destroy');
            		$('#work-start-month, #work-start-year, #work-end-month, #work-end-year').materialSelect();
	            }
	        });
        }
    });
}

function view_used_program_all()
{
    $('#used-program-view').html('');
    $('#used-program .add-new, #used-program .title .add').css('display', 'none');

    $.ajax({
        url : base_url + 'user/cv/view_used_program_all',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            if (data != "") 
            {
                for (var i = 0; i < data.length; i++)
                {
                    $('#used-program-view').append(
                        '<div class="item d-flex justify-content-between used-program'+data[i].id+'">' +
                            '<div class="info w-100">' +
                                '<div class="d-flex align-items-center">'+
                                    '<div class="icon">'+
                                        '<img src="'+ program_icon_render(data[i].program_id) +'" class="w-100" />'+
                                    '</div>'+
                                '<div class="flex-1">'+
                                '<h5>'+ program_render(data[i].program_id) +'</h5>'+
                                '<p class="gray-text">'+ data[i].duration +' Yıl Tecrübe</p>'+
                            '</div>'+
                            '<div class="edit">'+
                                '<button class="btn" onclick="used_program_edit('+data[i].id+')"><i class="la la-edit"></i></button>'+
                                '<button class="btn" onclick="used_program_delete('+data[i].id+')"><i class="la la-times"></i></button>'+
                            '</div>'+
                        '</div>'
                    );
                }

                $('#used-program-tab').addClass('done');
                $('#used-program .title .add').css('display', 'flex');
            }
            else
            {
                $('#used-program .add-new').css('display', 'flex');
                $('#used-program-tab').removeClass('done');
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Hata oluştu');
        }
    });
}

function used_program_add()
{
    $('#used-program-from')[0].reset();
    $('#used-program-id, #used-program-duration').materialSelect('destroy');
    $('#used-program-id, #used-program-duration').materialSelect();
    $('#used-program-modal').modal('show');
}

function used_program_edit(id)
{
    $('#used-program-from')[0].reset();
    $('.error-text').text('');
    $('#used-program-title').text('Program Düzenle');

    $.ajax({
        url : base_url + 'user/cv/used_program_edit/' + id,
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            $('#used-program-modal').modal('show');

            $('[name="program_id"]').val(data.id);
            $('[name="used_program_id"]').val(data.program_id);
            $('[name="used_program_duration"]').val(data.duration);
            $('#used-program-id, #used-program-duration').materialSelect('destroy');
            $('#used-program-id, #used-program-duration').materialSelect();

            $('.step-new-content').stop().animate({
                scrollTop:0
            });
        }
    });
}

function used_program_save()
{
    $('#used-program-btn').text('Kaydediliyor...');
    $('#used-program-btn').attr('disabled',true);
    $('.error-text').text('');

    var form_data = {
        program_id: $('[name="program_id"]').val(),
        used_program_id: $('[name="used_program_id"]').val(),
        used_program_duration: $('[name="used_program_duration"]').val(),
    }

    $.ajax({
        url : base_url + 'user/cv/used_program_save',
        type: "POST",
        data: form_data,
        dataType: "JSON",
        success: function(data)
        {
            if(data.status) 
            {
                if (data.id > 0) {
                    toastr.success('Kayıt başarılı bir şekilde eklendi');
                } else {
                    toastr.success('Kayıt başarılı bir şekilde güncellendi');
                }

                $('#used-program-from')[0].reset();
                $('[name="program_id"]').val('');
                $('.error-text').text('');
                $('#used-program-id, #used-program-duration').materialSelect('destroy');
            	$('#used-program-id, #used-program-duration').materialSelect();
                $('#used-program-title').text('Program Ekle');
                view_used_program_all();

                $('#used-program-modal').modal('hide');
            }
            else
            {
                toastr.error('Formu kontrol ediniz!');

                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('#' + data.inputerror[i] + '_error').text(data.error_string[i]); 
                }
            }

            $('#used-program-btn').text('Kaydet');
            $('#used-program-btn').attr('disabled', false);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Form kayıt edilirken bir sorun oluştu');
            $('#used-program-btn').text('Kaydet');
            $('#used-program-btn').attr('disabled', false);
        }
    });
}

function used_program_delete(id)
{
    Swal.fire({
        title: 'Emin misiniz?',
        text: "Sil derseniz kayıt silinecektir.",
        imageUrl: '/assets/frontend/img/modal-images/remove.svg',
        showCloseButton: true,
        showCancelButton: true,
        cancelButtonText: 'Vazgeç',
        confirmButtonText: 'Sil'
    }).then((result) => {
        if (result.value) 
        {
            $.ajax({
                url : base_url + 'user/cv/used_program_delete/' + id,
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {   
                    toastr.success('Kayıt başarılı bir şekilde silindi');
                    view_used_program_all();

                    $('#used-program-from')[0].reset();
                    $('[name="program_id"]').val('');
                    $('.error-text').text('');
                    $('#used-program-id, #used-program-duration').materialSelect('destroy');
            		$('#used-program-id, #used-program-duration').materialSelect();
                }
            });
        }
    });
}

function view_game_choice_all()
{
    $('#game-choice-view').html('');
    $('#game-choice .add-new, #game-choice .add').css('display', 'none');

    $.ajax({
        url : base_url + 'user/cv/view_game_choice_all',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            if (data != "") 
            {
                for (var i = 0; i < data.length; i++)
                {
                	let future_text = '';
                	let past_text = '';
                	
                	$.each(game_render(data[i].future_json), function(key, val){
                		future_text += '<div class="kind">' + val['title'] + '</div>';
                	});

                	$.each(game_render(data[i].past_json), function(key, val){
                		past_text += '<div class="kind">' + val['title'] + '</div>';
                	});

                    $('#game-choice-view').append(
                        '<div class="item d-flex justify-content-between game-choice'+data[i].id+'">' +
                            '<div>'+
                                '<div>'+
                                    '<h5 class="p-2">Gelecekte üretiminde bulunmak istediğiniz türler:</h5> '+
                                    '<div class="d-flex flex-wrap">'+
                                        future_text +
                                    '</div>'+
                                '</div>'+
                                '<div>'+
                                    '<h5 class="p-2">Geçmişte üretiminde bulunduğunuz türler:</h5>'+
                                    '<div class="d-flex flex-wrap">'+
                                        past_text +
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="edit">'+
                                '<button class="btn" onclick="game_choice_edit('+data[i].id+')" ><i class="la la-edit"></i></button>'+
                                '<button class="btn" onclick="game_choice_delete('+data[i].id+')"><i class="la la-times"></i></button>'+
                            '</div>'+
                        '</div>'
                    );
                }

                $('#game-choice-tab').addClass('done')
            }
            else
            {
                $('#game-choice .add-new, #game-choice .add').css('display', 'flex');
                $('#game-choice-tab').removeClass('done')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Hata oluştu');
        }
    });
}

function game_choice_add()
{
    $('#game-choice-from')[0].reset();
    $('#game-future-json, #game-past-json').materialSelect('destroy');
    $('#game-future-json, #game-past-json').materialSelect();
    $('#game-choice-modal').modal('show');
}

function game_choice_edit(id)
{
    $('#game-choice-from')[0].reset();
    $('.error-text').text('');
    $('#game-choice-title').text('Tür Düzenle');

    $.ajax({
        url : base_url + 'user/cv/game_choice_edit/' + id,
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            $('#game-choice-modal').modal('show');

            $('[name="game_id"]').val(data.id);
            $('[name="game_future_json"]').val(JSON.parse(data.future_json));
            $('[name="game_past_json"]').val(JSON.parse(data.past_json));
            $('#game-future-json, #game-past-json').materialSelect('destroy');
            $('#game-future-json, #game-past-json').materialSelect();

            $('.step-new-content').stop().animate({
                scrollTop:0
            });
        }
    });
}

function game_choice_save()
{
    $('#game-choice-btn').text('Kaydediliyor...');
    $('#game-choice-btn').attr('disabled',true);
    $('.error-text').text('');

    var form_data = {
        game_id: $('[name="game_id"]').val(),
        game_future_json: $('[name="game_future_json"]').val(),
        game_past_json: $('[name="game_past_json"]').val(),
    }

    $.ajax({
        url : base_url + 'user/cv/game_choice_save',
        type: "POST",
        data: form_data,
        dataType: "JSON",
        success: function(data)
        {
            if(data.status) 
            {
                if (data.id > 0) {
                    toastr.success('Kayıt başarılı bir şekilde eklendi');
                } else {
                    toastr.success('Kayıt başarılı bir şekilde güncellendi');
                }

                $('#game-choice-from')[0].reset();
                $('[name="game_id"]').val('');
                $('.error-text').text('');
                $('#game-future-json, #game-past-json').materialSelect('destroy');
            	$('#game-future-json, #game-past-json').materialSelect();
                $('#game-choice-title').text('Tür Ekle');
                view_game_choice_all();

                $('#game-choice-modal').modal('hide');
            }
            else
            {
                toastr.error('Formu kontrol ediniz!');

                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('#' + data.inputerror[i] + '_error').text(data.error_string[i]); 
                }
            }

            $('#game-choice-btn').text('Kaydet');
            $('#game-choice-btn').attr('disabled', false);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Form kayıt edilirken bir sorun oluştu');
            $('#game-choice-btn').text('Kaydet');
            $('#game-choice-btn').attr('disabled', false);
        }
    });
}

function game_choice_delete(id)
{
    Swal.fire({
        title: 'Emin misiniz?',
        text: "Sil derseniz kayıt silinecektir.",
        imageUrl: '/assets/frontend/img/modal-images/remove.svg',
        showCloseButton: true,
        showCancelButton: true,
        cancelButtonText: 'Vazgeç',
        confirmButtonText: 'Sil'
    }).then((result) => {
        if (result.value) 
        {
            $.ajax({
                url : base_url + 'user/cv/game_choice_delete/' + id,
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {   
                    toastr.success('Kayıt başarılı bir şekilde silindi');
                    view_game_choice_all();

                    $('#game-choice-from')[0].reset();
                    $('[name="game_id"]').val('');
                    $('.error-text').text('');
                    $('#game-future-json, #game-past-json').materialSelect('destroy');
            		$('#game-future-json, #game-past-json').materialSelect();
                }
            });
        }
    });
}

function view_portfolio_all()
{
    $('#portfolio-view').html('');
    $('#portfolio .add-new, #portfolio .add').css('display', 'none');
    $('#portfolio-name-text').html('');

    $.ajax({
        url : base_url + 'user/cv/view_portfolio_all',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            if (data != "") 
            {
                let portfolio_link = '';
                let portfolio_file = '';
                for (var i = 0; i < data.length; i++)
                {
                    if (data[i].link) {
                        portfolio_link = '<div class="mb-2">Portfolyo Linki:<br> <b class="break-all">'+data[i].link+'</b></div>';
                    }

                    if (data[i].name) {
                        portfolio_file = '<div class="summary"><span><i class="icon-pdf-icon"></i></span> <a href="'+base_url+upload_folder+'portfolios/'+data[i].name+'" target="_blank"><b>'+ data[i].name +'</b></a></div>';
                    }

                    $('#portfolio-view').append(
                        '<div class="item d-flex justify-content-between portfolio'+data[i].id+'">' +
                            '<div>'+
                                portfolio_link +
                                portfolio_file +
                            '</div>'+
                            '<div class="edit">'+
                                '<button class="btn" onclick="portfolio_edit('+data[i].id+')" ><i class="la la-edit"></i></button>'+
                                '<button class="btn" onclick="portfolio_delete('+data[i].id+')"><i class="la la-times"></i></button>'+
                            '</div>'+
                        '</div>'
                    );
                }

                $('#portfolio-tab').addClass('done')
            }
            else 
            {
                $('#portfolio .add-new, #portfolio .add').css('display', 'flex');
                $('#portfolio-tab').removeClass('done')
            }

            setTimeout(function() {
                $('.list-view').removeClass('d-none');
                $('.page-loading').removeClass('d-flex');
                $('.page-loading').addClass('d-none');
            }, 300);
        }
    });
}

function portfolio_add()
{
    $('#portfolio-form')[0].reset();
    $('#portfolio-modal').modal('show');
}

function portfolio_edit(id)
{
    $('#portfolio-form')[0].reset();
    $('.error-text').text('');
    $('#portfolio-title').text('Portfolyo Düzenle');

    $.ajax({
        url : base_url + 'user/cv/portfolio_edit/' + id,
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            $('#portfolio-modal').modal('show');

            $('[name="portfolio_id"]').val(data.id);
            $('#portfolio-name-text').html('Portfolyo için yüklenen dosya: <a href="'+base_url+upload_folder+'portfolios/'+data.name+'" target="_blank"><b>'+ data.name +'</b></a>');
            $('[name="portfolio_name_text"]').val(data.name).trigger("change");
            $('[name="portfolio_link"]').val(data.link).trigger("change");

            $('.step-new-content').stop().animate({
                scrollTop:0
            });
        }
    });
}

function portfolio_save()
{
    $('#portfolio-btn').text('Kaydediliyor...');
    $('#portfolio-btn').attr('disabled',true);
    $('.error-text').text('');

	var formData = new FormData($('#portfolio-form')[0]);

    $.ajax({
        url : base_url + 'user/cv/portfolio_save',
        type: "POST",
        data: formData,
        contentType: false,
    	processData: false,
    	cache: false,
        dataType: "JSON",
        success: function(data)
        {
            if(data.status) 
            {
                if (data.id > 0) {
                    toastr.success('Kayıt başarılı bir şekilde eklendi');
                } else {
                    toastr.success('Kayıt başarılı bir şekilde güncellendi');
                }

                $('#portfolio-form')[0].reset();
                $('[name="portfolio_id"]').val('');
                $('.error-text').text('');
                $('#portfolio-title').text('Portfolyo Ekle');
                view_portfolio_all();

                $('#portfolio-modal').modal('hide');
            }
            else
            {
                toastr.error('Formu kontrol ediniz!');

                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('#' + data.inputerror[i] + '_error').text(data.error_string[i]); 
                }
            }

            $('#portfolio-btn').text('Kaydet');
            $('#portfolio-btn').attr('disabled', false);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Form kayıt edilirken bir sorun oluştu');
            $('#portfolio-btn').text('Kaydet');
            $('#portfolio-btn').attr('disabled', false);
        }
    });
}

function portfolio_delete(id)
{
    Swal.fire({
        title: 'Emin misiniz?',
        text: "Sil derseniz kayıt silinecektir.",
        imageUrl: '/assets/frontend/img/modal-images/remove.svg',
        showCloseButton: true,
        showCancelButton: true,
        cancelButtonText: 'Vazgeç',
        confirmButtonText: 'Sil'
    }).then((result) => {
        if (result.value) 
        {
            $.ajax({
                url : base_url + 'user/cv/portfolio_delete/' + id,
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {   
                    toastr.success('Kayıt başarılı bir şekilde silindi');
                    view_portfolio_all();

                    $('#portfolio-form')[0].reset();
                    $('[name="portfolio_id"]').val('');
                    $('.error-text').text('');
                }
            });
        }
    });
}

function confirm_save()
{
	$('#confirm-btn').text('Kaydediliyor...');
	$('#confirm-btn').attr('disabled',true);
	$('.error-text').text('');

	var form_data = {
	    user_privacy: $('[name="user_privacy"]').is(':checked') ? 1 : 0,
	    user_kvkk: $('[name="user_kvkk"]').is(':checked') ? 1 : 0,
	    user_confirm: $('[name="user_confirm"]').is(':checked') ? 1 : 0,
	}

	$.ajax({
	    url : base_url + 'user/cv/confirm_save',
	    type: "POST",
	    data: form_data,
	    dataType: "JSON",
	    success: function(data)
	    {
	        if(data.status) 
	        {
                completed = true;
                $('#confirm-tab').addClass('done')

                modal_open(
                    '<h2>' + user_fullname + '</h2><h6>Zindhu HR\'a başarıyla kayıt oldun.</h6>', 
                    '/assets/frontend/img/modal-images/success.svg',
                    'Hazırım'
                );

	            $('#confirm-form')[0].reset();
	            $('.error-text').text('');
	        }
	        else
	        {
	            toastr.error('Formu kontrol ediniz!');

	            for (var i = 0; i < data.inputerror.length; i++) 
	            {
	            	if (data.inputerror[i] == 'user_control') {
                        modal_open(
                            '<h2>Uyarı</h2><h6>'+ data.error_string[i] +'</h6>', 
                            '/assets/frontend/img/modal-images/error.svg'
                        );
	            	} else {
	            		$('#' + data.inputerror[i] + '_error').text(data.error_string[i]); 
	            	}
	            }
	        }

	        $('#confirm-btn').text('Kaydet');
	        $('#confirm-btn').attr('disabled', false);
	    },
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	        alert('Form kayıt edilirken bir sorun oluştu');
	        $('#confirm-btn').text('Kaydet');
	        $('#confirm-btn').attr('disabled', false);
	    }
	});
}